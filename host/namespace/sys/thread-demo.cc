//------------------------
//thread-demo
//(c) H.Buchmann FHSO 2005
//$Id: thread-demo.cc 169 2006-01-03 12:29:21Z buchmann $
//------------------------
#include "sys/sys.h"
IMPLEMENTATION(sys_thread_demo,$Revision$)
#include "sys/msg.h"
#include "sys/thread.h"

namespace sys
{
 class Ticker:public Thread,
              public Runnable
 {
  private:
   unsigned id;
   void run();
   Semaphore sem;
  public: 
   Ticker(unsigned id,Mutex& mutex);
 };
 
 Ticker::Ticker(unsigned id,Mutex& mutex)
 :Thread(this),
  id(id),
  mutex(mutex)
 {
  start();
 }
 
 void Ticker::run()
 {
  mutex.lock();
  sys::msg<<"start "<<id<<"\n";
  mutex.unlock();
  for(unsigned i=0;i<2;i++)
  {
   for(unsigned cnt=0;cnt<1000000;cnt++){}
  }
  mutex.lock();
  sys::msg<<"stop "<<id<<"\n";
  mutex.unlock();
 }
 
 class Demo
 {
  static Demo demo;
  static const unsigned N=20;
  Mutex mutex;
  Ticker* t[N];
  Demo();
 };
 
 Demo Demo::demo;
 
 Demo::Demo()
 {
  for(unsigned i=0;i<N;i++) t[i]=new Ticker(i,mutex);
  Thread::wait();
//  for(unsigned i=0;i<N;i++) delete t[i];
 }
}
