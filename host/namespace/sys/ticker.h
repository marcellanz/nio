#ifndef SYS_TICKER_H
#define SYS_TICKER_H
//--------------------------------
//Ticker
//(c) H.Buchmann FHSO 2003
//$Id: ticker.h 193 2006-01-11 15:21:16Z buchmann $
//--------------------------------
INTERFACE(sys_ticker,$Revision: 137 $)
#include "sys/timeout.h"
#include "sys/thread.h"
namespace sys
{
 class Ticker:public sys::Timeout,
              public sys::Runnable
 {
  private:
   Ticker(const Ticker&);
   Ticker operator=(const Ticker&);
   
   Thread thread;
   long dt;
   unsigned dt_ms;
   void run();
   
  public: 
   Ticker();
   void start(unsigned tick_ms);
   unsigned getTick_ms(){return dt_ms;}
 };
}
#endif
