#ifndef SYS_THREAD_H
#define SYS_THREAD_H
INTERFACE(sys_thread,$Revision: 169 $)
//-----------------------
//thread
//(c) H.Buchmann FHSO 2000
//$Id: thread.h 209 2006-01-24 13:49:16Z buchmann $
//-----------------------
namespace sys
{
 class Runnable
 {
  public:
   virtual void run()=0;
   virtual ~Runnable();
 };

 class Mutex
 {
  private:
   Mutex(const Mutex&);
   Mutex operator=(const Mutex&);
   
   struct Posix;
   Posix* posix;

  public:
   Mutex();
   void lock();
   void unlock();
 };
 
 class Semaphore
 {
  private:
   Semaphore(const Semaphore&);
   Semaphore operator=(const Semaphore&);

   struct Posix;
   Posix* posix;

  public:
            Semaphore(int val);
            Semaphore();   //Semaphore *closed*
   virtual ~Semaphore();
   void wait();
   void open();
 };

 class Thread
 {
  private:
   friend class Mutex;
   friend class Semaphore;
   friend class Signal;
   struct Counter
   {
    Mutex mCount;
    Semaphore sWait; 
    unsigned count; //of running threads
    Counter();
    void inc();
    void dec();
    void wait();    
   };
   
   static Counter counter;
   
   Thread(const Thread&);
   Thread operator=(const Thread&);

   struct Posix;
   Posix* posix;
   Runnable& run;
   static void* startIt(void* start);
   static void error(const char msg[],int id);
   
  public:
            Thread(Runnable& r);
	    Thread(Runnable* r);
   virtual ~Thread();
   void start();
   void cancel();
   void signal(Signal& s);
   static void exit(); //terminates current thread
   static void wait(); 
 };

 class Signal
 {
  private:
   friend class Thread;
   int id;
   
  public:
            Signal(int id);
   virtual ~Signal();
   void wait(); //at signal 
   int getId()const{return id;}  
 };
 
}
#endif
