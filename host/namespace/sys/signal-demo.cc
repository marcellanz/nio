//------------------------
//signal-demo
//(c) H.Buchmann FHNW 2006
//$Id$
//------------------------
#include "sys/sys.h"
IMPLEMENTATION(signal_demo,$Revision$)
#include "sys/msg.h"
#include "sys/deb.h"

#include <signal.h>

namespace sys
{
 class Demo
 {
  static Demo demo;
  static void handler(int signal);
  Demo();
 };
 
 Demo Demo::demo;
 
 void Demo::handler(int signal)
 {
  sys::msg<<"signal "<<signal<<"\n";
 }
 
 Demo::Demo()
 {
  sys::msg<<"Signal Demo SIGRTMIN/SIGRTMAX="
          <<SIGRTMIN<<"/"<<SIGRTMAX<<"\n";
  ::signal(SIGRTMIN,handler);
  sys::deb::get();  
 }
}
