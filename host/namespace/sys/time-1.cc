//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//----------------------------
//time.h
//(c) H.Buchmann FHSO 2004
//$Id: time-1.cc 193 2006-01-11 15:21:16Z buchmann $
//----------------------------
#include "sys/sys.h"
IMPLEMENTATION(sys_time_1,$Revision: 139 $)
#include "sys/time.h"
#include <sys/timex.h>

//#define SYS_TIME_TEST
#ifdef SYS_TIME_TEST
#include "sys/msg.h"
#endif

namespace sys
{
 class Time_1:public Time
 {
  static Time_1 time_1;
  unsigned sec_ (Stamp_us ts);
  unsigned usec_(Stamp_us ts);
  Stamp_us stamp_();
 };
 
 Time_1 Time_1::time_1;
 
 Time::Stamp_us Time_1::stamp_()
 {
  struct ntptimeval time;
  ntp_gettime(&time);
  return 1000000*(Stamp_us)time.time.tv_sec+time.time.tv_usec;
 }

 unsigned Time_1::sec_ (Stamp_us ts)
 {
  return ts/SECOND;
 }
 
 unsigned Time_1::usec_(Stamp_us ts)
 {
  return ts%SECOND;
 }
}

#ifdef SYS_TIME_TEST
class Tester
{
 static Tester tester;
 Tester();
};
Tester Tester::tester;

Tester::Tester()
{
 sys::msg<<__PRETTY_FUNCTION__<<"\n";
 sys::Time::Stamp_us ts=sys::Time::stamp();
 sys::msg<<sys::Time::sec(ts)<<" : "<<sys::Time::usec(ts)<<"\n";
}
#endif
