//
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//-----------------------
//thread
//(c) H.Buchmann FHSO 2000
//$Id: thread.cc 209 2006-01-24 13:49:16Z buchmann $
//-----------------------
#include "sys/sys.h"
IMPLEMENTATION(sys_thread,$Revision: 169 $)
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <signal.h>

#include "sys/thread.h"
#include "sys/msg.h"

#include "sys/deb.h"

namespace sys
{
 Runnable::~Runnable()
 {
 }
 
 Thread::Counter Thread::counter;
 
 struct Mutex::Posix
 {
  pthread_mutex_t mu;
 };
 
 Mutex::Mutex()
 :posix(new Posix)
 {
  Thread::error(__PRETTY_FUNCTION__,::pthread_mutex_init(&posix->mu,0));
 }

 void Mutex::lock()
 {
  Thread::error(__PRETTY_FUNCTION__,::pthread_mutex_lock(&posix->mu));
 }
 
 void Mutex::unlock()
 {
  Thread::error(__PRETTY_FUNCTION__,::pthread_mutex_unlock(&posix->mu));
 }
  
 struct Semaphore::Posix
 {
  sem_t se;
 };
 
 Semaphore::Semaphore(int val):
 posix(new Posix)
 {
  Thread::error(__PRETTY_FUNCTION__,::sem_init(&posix->se,0,val));
 }
 
 Semaphore::Semaphore()
 :posix(new Posix)
 {
  Thread::error(__PRETTY_FUNCTION__,::sem_init(&posix->se,0,0));
 }

 Semaphore::~Semaphore()
 {
  Thread::error(__PRETTY_FUNCTION__,::sem_destroy(&posix->se));
  delete posix;
 }

 void Semaphore::wait()
 {
  Thread::error(__PRETTY_FUNCTION__,::sem_wait(&posix->se));
 }
 
 void Semaphore::open()
 {
  Thread::error(__PRETTY_FUNCTION__,::sem_post(&posix->se));
 }
 
 
 void Signal::wait()
 {
  ::sigset_t set;
  ::sigemptyset(&set);
  ::sigaddset(&set,id);
  int sig;
  ::sigwait(&set,&sig);
 }

 Signal::Signal(int id)
 :id(id)
 {
 }
 
 Signal::~Signal()
 {
 }
 

 struct Thread::Posix
 {
  pthread_t th;
 };

 Thread::Counter::Counter()
 :count(0)
 {
 }

 void Thread::Counter::inc()
 {
  mCount.lock();
  count++;
  mCount.unlock();
 }
 
 void Thread::Counter::dec()
 {
  mCount.lock();
  count--;
  if (count==0) sWait.open();
  mCount.unlock();
 }
 
 void Thread::Counter::wait()
 {
  sWait.wait();
 }
 
 Thread::Thread(Runnable& r)
 :posix(new Posix),run(r)
 {
  sys::msg<<__PRETTY_FUNCTION__<<" "<<this<<"\n";
 }

 Thread::Thread(Runnable* r)
 :posix(new Posix),run(*r)
 {
  sys::msg<<__PRETTY_FUNCTION__<<" "<<this<<"\n";
 }

 Thread::~Thread()
 {
  sys::msg<<__PRETTY_FUNCTION__<<" "<<this<<"\n";
  cancel();
 }

 void Thread::error(const char msg[],int id)
 {
  if (id<0)
     {
      sys::msg.error()<<msg<<": "<<::strerror(errno)<<"\n";
     }
 }
 
 void Thread::signal(Signal& sig)
 {
  sys::msg<<"----------------++\n";
  int res=::pthread_kill(posix->th,sig.id);
  sys::msg<<"-- "<<res<<"\n";
 }
 
 void* Thread::startIt(void* th)
 {
//  ((Thread*)th)->semaphore.open();
  ::pthread_setcancelstate(PTHREAD_CANCEL_ENABLE,0);
  ((Thread*)th)->run.run();
  counter.dec();
  return 0;
 }

 void Thread::start()
 {
  counter.inc(); //so we guarantee that the thread will start for sure
  Thread::error(__PRETTY_FUNCTION__,::pthread_create(&posix->th,0,startIt,this));
//  semaphore.wait();
 }

 void Thread::cancel()
 {
  Thread::error(__PRETTY_FUNCTION__,::pthread_cancel(posix->th));
 }


 void Thread::exit()
 {
  ::pthread_exit(0);
 }
  
 void Thread::wait()
 {
//  ::pause();
  counter.wait();
 }
}
