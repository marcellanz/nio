//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//-------------------------
//sys
//(c) H.Buchmann FHSO 2003
//$Id: host.cc 193 2006-01-11 15:21:16Z buchmann $
//-------------------------
#include "sys/sys.h"
IMPLEMENTATION(sys_host,$Revision: 144 $)
#include <stdlib.h>
#include <signal.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <algorithm>

#include "sys/host.h"
#include "sys/deblow.h"
#include "sys/sys.h"

extern "C" int __cxa_atexit(void (*function)(void));
extern "C"{
  int atexit(void (*function)(void))
  {
   return __cxa_atexit(function);
  }
}

namespace sys
{

 unsigned argc=0;
 char** args=0;
 const char** env=0;
 unsigned envLen=0;
 
 unsigned argCnt(){return argc;}
 const char* argAt(unsigned idx)
 {
  return (idx<argc)?args[idx]:0;
 }
 const char** getEnv(){return env;}
 void exit(unsigned cod){::exit(cod);}
 
 const char* getEnv(const char key[])
 {
  if (key==0) return 0;
//do binary search myself due to special comparing
  int i0=-1;
  int i1=envLen;
  while(true)
  {
   if ((i1-i0)<=1) return 0;
   unsigned im=(i0+i1)/2;
   const char* keym=env[im];
   for(unsigned i=0;;i++)
   {
    char chkey   = key[i];
    char chkeym  =(keym[i]=='=')?'\0':keym[i]; //= will be '\0'
    if (chkey<chkeym){i1=im;break;}
    if (chkey>chkeym){i0=im;break;}
    if (chkey=='\0') return keym+i+1;
   }
  } 
 }

 extern "C"
 {
  static sighandler_t prevSignalHandler;
  static pid_t mainPID;
  void signalHandler(int signum)
  {
//   printf("signal %d pid %d\n",signum,getpid());
   if (mainPID==::getpid())
      {
       switch(signum)
       {
        case SIGINT:
	 Mod::stop();
	 ::exit(0);
	break;
       }
      }
  }

  void _start() 
  {
   mainPID=::getpid(); 
   unsigned* arg=(unsigned*)&arg;
   argc=arg[1];
   args=(char**)(arg+2);
   env=(const char**)(args+argc+1);
   sys::deb::init();
   prevSignalHandler=::signal(SIGINT,signalHandler);
   Mod::start();
   Mod::stop();
   ::exit(0);
  }

//------------------------------- builtins
  void __cxa_pure_virtual()
  {
   printf("%s\n",__PRETTY_FUNCTION__);
   ::exit(1);
  } 
  
 }
}

Sys Sys::sys;

Sys::Sys()
{
 const char** e=sys::env;
 sys::envLen=0;
 while(*e++)sys::envLen++;
 std::sort<const char**,bool (*)(const char*,const char*)>(sys::env,
                                                      sys::env+sys::envLen,
                                                      envCmp);
}

bool Sys::envCmp(const char s1[],const char s2[])
{
 unsigned i=0;
 while(true)
 {
  char ch1=s1[i];
  char ch2=s2[i];
  if (ch1!=ch2) return ch1<ch2;
  if (ch1=='=') return false;
  if (ch1=='\0') return false; //should not happen
  i++;
 }
}

void operator delete(void* v)
{
 ::free(v);
}


