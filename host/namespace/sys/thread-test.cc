//---------------------
//thread-test
//(c) H.Buchmann FHSO 2005
//$Id: thread-test.cc 193 2006-01-11 15:21:16Z buchmann $
//---------------------
#include "sys/sys.h"
IMPLEMENTATION(sys_thread_test,$Revision$)
#include "sys/thread.h"

namespace sys
{
 class Tester
 {
  static Tester tester;
#if 0  
  class Thread:public sys::Thread,public Runnable
  {
   private:
    Semaphore sem;
   public:
    Thread(); 
  };
#endif  
  Tester();
 };
 
 Tester Tester::tester;
 
 Tester::Tester()
 {
  Thread::wait();
 }
}//namespace sys
