//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//-----------------------
//debug low level 
//should be implemented in 'board/sys'
//(c) H.Buchmann FHSO 2001
//$Id: deblow.cc 193 2006-01-11 15:21:16Z buchmann $
//-----------------------
#include "sys/sys.h"
IMPLEMENTATION(sys_deblow,$Revision: 137 $)
#include "sys/deblow.h"
#include <unistd.h>
#include <stdlib.h>
#include <termios.h>
extern "C" int __cxa_atexit(void (*function)(void));
namespace sys
{
 namespace deb
 {
  static struct termios settingsData;
  static struct termios* settings=0;
  
  void restore()
  {
   if (settings) ::tcsetattr(STDERR_FILENO,TCSANOW,settings);
  }
  

  void init()
  {
   ::tcgetattr(STDERR_FILENO,&settingsData);
   settings=&settingsData;
   
   struct termios newSettings;
   
   ::tcgetattr(STDERR_FILENO,&newSettings);
   newSettings.c_lflag&=~(ICANON|ECHO);
   ::tcsetattr(STDERR_FILENO,TCSANOW,&newSettings);
   ::__cxa_atexit(restore);
  }

  void signal0()
  {
   static const char Sig0[]="Signal 0\n"; 
   ::write(STDERR_FILENO,Sig0,sizeof(Sig0));
   ::exit(1);
   while(true);
  }
  
  void signal1()
  {
   static const char Sig1[]="Signal 1\n"; 
   ::write(STDERR_FILENO,Sig1,sizeof(Sig1));
   ::exit(1);
   while(true);
  }
  
  void signal2()
  {
   static const char Sig2[]="Signal 2\n"; 
   ::write(STDERR_FILENO,Sig2,sizeof(Sig2));
   ::exit(1);
   while(true);
  }
  
  void signal3()
  {
   static const char Sig3[]="Signal 3\n"; 
   ::write(STDERR_FILENO,Sig3,sizeof(Sig3));
   ::exit(1);
   while(true);
  }

  void out(char ch)
  {
   if (ch!='\r') ::write(STDERR_FILENO,&ch,sizeof(ch));
  }
  
  void halt()
  {
   ::write(STDERR_FILENO,"\n",1);
   ::exit(1);
  }
  
  char get()
  {
   char ch;
   ::read(STDERR_FILENO,&ch,sizeof(ch));
   return ch;
  }
  
  bool avail()
  {
   return true;
  }
  
  void test()
  {
  }

  void enter()
  {
   static const char Enter[]="**** entering debug ****";
   ::write(STDERR_FILENO,Enter,sizeof(Enter));
  }
 }
}
