#ifndef SYS_OUT
#define SYS_OUT
//-------------------------
//former C++ cout Java System.out
//(c) H.Buchmann FHSO 2004
//$Id: out.h 193 2006-01-11 15:21:16Z buchmann $
//-------------------------
INTERFACE(sys_out,$Revision: 137 $)
#include "io/ascii/write.h"
namespace sys
{
 class Output:public io::ascii::Writer,
              public io::ascii::Output
 {
  private:
   Output();
   void put(char d);
  public:
   static Output out;
 };
 	    
 extern io::ascii::Writer& out;
} 
#endif
