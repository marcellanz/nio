#ifndef SYS_HOST_H
#define SYS_HOST_H
INTERFACE(sys_host,$Revision: 137 $)
//-------------------------
//sys
//(c) H.Buchmann FHSO 2003
//$Id: host.h 193 2006-01-11 15:21:16Z buchmann $
//-------------------------
namespace sys
{
 unsigned argCnt();
 const char* argAt(unsigned idx);
 const char** getEnv(); //the whole environment
 const char* getEnv(const char key[]);
 void exit(unsigned cod);
 inline void exit(){exit(0);}
}

//for compatibility with former Sys
class Sys
{
 private:
  static Sys sys;
  Sys();
  static bool envCmp(const char s1[],const char s2[]);
  
 public:
  virtual ~Sys(){}
  static unsigned argCount(){return sys::argCnt();}
  static const char* argAt(unsigned i){return sys::argAt(i);}
  static const char* progName(){return sys::argAt(0);}
  static void exit(){sys::exit();}
  static void exit(unsigned cod){sys::exit(cod);}
  static const char** getenv(){return sys::getEnv();}
  static const char*  getenv(const char key[]){return sys::getEnv(key);} 
};

//extern void operator delete(void*);
#endif

