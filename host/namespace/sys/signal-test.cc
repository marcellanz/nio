//-------------------------
//signal-test
//(c) H.Buchmann FHNW 2006
//$Id$
//-------------------------
#include "sys/sys.h"
IMPLEMENTATION(sys_signal_test,$Revision$)
#include "sys/thread.h"
#include "sys/msg.h"
#include <signal.h>

namespace sys
{
 class Tester:public Runnable
 {
  static Tester tester;
  static void signalHandler(int signal);
  Thread th;
  Signal signal;
  Tester();
  void run();
 };

 
 Tester Tester::tester;
 
 Tester::Tester()
 :th(*this)
 ,signal(35)
 {
  ::signal(35,signalHandler);
  th.start();
  Thread::wait();
 }

 void Tester::run()
 {
  while(true)
  {
   signal.wait();
   sys::msg<<"--------- signal\n";
  }
 }
 
 void Tester::signalHandler(int signal) 
 {
  sys::msg<<"signal "<<signal<<"\n";
  tester.th.signal(tester.signal);
 }
}//namespace sys
