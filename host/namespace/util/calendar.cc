//----------------------------
//calendar
//(c) H.Buchmann FHSO 2005
//$Id: calendar.cc 193 2006-01-11 15:21:16Z buchmann $
//----------------------------
#include "sys/sys.h"
IMPLEMENTATION(util_calendar,$Revision: 137 $)
#include "util/calendar.h"
#include "sys/msg.h"
#include <time.h>
//#define CALENDAR_TEST

namespace util
{
 io::ascii::Writer& Calendar::Time::show(io::ascii::Writer& out) const
 {
  static const char* Month[]=
  {
   "Jan","Feb","Mar","Apr",
   "May","Jun","Jul","Aug",
   "Sep","Oct","Nov","Dec"
  };
  static const char* DoW[]=
  {
   "Son","Mon","Tue","Wed","Thu","Fri","Sat"
  };
  
  out<<DoW[dow]<<" "<<Month[month]<<" "<<day<<" "<<hour<<":"<<min<<":"<<sec
               <<" "<<year;
  return out;
 }

 void Calendar::toLocal(const Time_Sec sec,Time& time)
 {
  struct tm tim;
  time_t ti=sec;
  ::localtime_r(&ti,&tim);
  time.sec   =tim.tm_sec;   
  time.min   =tim.tm_min;   
  time.hour  =tim.tm_hour;  
  time.day   =tim.tm_mday;   
  time.month =tim.tm_mon; 
  time.year  =1900+tim.tm_year;  
  time.dow   =tim.tm_wday;     
 }
 
 Calendar::Time_Sec Calendar::getTime()
 {
  return ::time(0);
 }

 io::ascii::Writer& Calendar::show(io::ascii::Writer& out)
 {
  Time time;
  toLocal(getTime(),time);
  return time.show(out);
 }
 
#ifdef CALENDAR_TEST
 class Tester
 {
  static Tester tester;
  Tester();
 };
 
 Tester Tester::tester;
 
 Tester::Tester()
 {
  Calendar::Time time;
  Calendar::toLocal(Calendar::getTime(),time);
  time.show(sys::msg)<<"\n";
 }
 
#endif
}//namespace util
