//
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//--------------------------------
//regex C++ API on libc regex
//(c) H.Buchmann FHSO 2004
//$Id: regex.cc 193 2006-01-11 15:21:16Z buchmann $
//--------------------------------
#include "sys/sys.h"
IMPLEMENTATION(util_regex,$Revision: 137 $)
#include "util/regex.h"
#include "util/str.h"
#include <sys/types.h>
#include <regex.h>
#define REGEXP_TEST

#ifdef REGEXP_TEST
#include "sys/msg.h"
#endif

namespace util
{
 struct Regex::Posix
 {
  regex_t regex;
 };
 
 struct Regex::String::Posix
 {
  regmatch_t match;
 };
 
 Regex::String::String(Regex& regex,const char s[])
 :regex(*regex.regex),m(new String::Posix),
  s(s) 
 {
  m->match.rm_so=-1;
  m->match.rm_eo=-1;
  
  ::regexec(&regex.regex->regex,s,1,&m->match,0);
  sys::msg<<m->match.rm_so<<" "<<m->match.rm_eo<<"\n";
 }
 
 Regex::String::~String()
 {
  delete m;
 }
 
 bool Regex::String::match()
 {
  return (m->match.rm_so)==0&&(s[m->match.rm_eo]=='\0');
 }

 bool Regex::String::find()
 {
  return (m->match.rm_so!=-1);
 }
 
 Regex::Regex(const char pattern[],unsigned flags)
 :regex(new Regex::Posix),errCode(0)
 {
  if (!compile(pattern,flags))
     {
      char msg[80];
      sys::msg.error()<<errorMsg(msg,sizeof(msg))<<"\n";
     }
 }
 
 Regex::~Regex()
 {
  if (regex) 
     {
      ::regfree(&regex->regex);
      delete regex;
     }
 }
 
 bool Regex::compile(const char pattern[],unsigned flags)
 {
  if (regex==0) regex=new Regex::Posix;
  errCode=::regcomp(&regex->regex,pattern,flags|REG_EXTENDED);
  return errCode==0;
 }

 const char* Regex::errorMsg(char msg[],unsigned len) const
 {
  if (regex==0) 
     {
      Str::copy("no regex available",msg,len);
      return msg;
     }
  ::regerror(errCode,&regex->regex,msg,len);
  return msg;
 }
 
 io::ascii::Writer& Regex::showError(io::ascii::Writer& out) const
 {
  char msg[80];
  return out<<errorMsg(msg,sizeof(msg));     
 }

}

#ifdef REGEXP_TEST
class Tester
{
 static Tester tester;
 Tester();
};

Tester::Tester()
{
 sys::msg<<__PRETTY_FUNCTION__<<"\n";
 util::Regex regex("X(A*)",0);
 regex.showError(sys::msg)<<"\n";

 util::Regex::String s(regex,"mmmmXAAAAY");
 sys::msg<<"match "<<s.match()<<"\n";  
}
Tester Tester::tester;
#endif

