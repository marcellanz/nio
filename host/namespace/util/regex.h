#ifndef UTIL_REGEX_H
#define UTIL_REGEX_H
//--------------------------------
//regex C++ API on libc regex
//(c) H.Buchmann FHSO 2004
//$Id: regex.h 193 2006-01-11 15:21:16Z buchmann $
//--------------------------------
INTERFACE(util_regex,$Revision: 137 $)
#include "io/ascii/write.h"

namespace util
{
 class Regex
 {
  private:
   struct Posix;
   
  public:
   class String
   {
    private:
     struct Posix;
     Regex::Posix& regex;
     Posix* m;
     const char* s;
     
    public:
              String(Regex& regex,const char s[]);
     virtual ~String();
     bool match();
     bool find();
   };

  private:
   Posix* regex;
   int errCode;
   
  public:
            Regex(const char pattern[],unsigned flags);
   virtual ~Regex();
   
   bool compile(const char pattern[],unsigned flags);
   bool isOk(){return errCode==0;}
   io::ascii::Writer& showError(io::ascii::Writer&) const;
   const char* errorMsg(char msg[],unsigned len) const;
       //returns msg
 };
}//namesapce util
#endif
