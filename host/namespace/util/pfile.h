#ifndef PFILE_H
#define PFILE_H
INTERFACE(util_pfile,$Revision: 137 $)
//----------------------------------------
// Parameterfile
// (c) H.Buchmann ISOe 1998 
// $Id: pfile.h 193 2006-01-11 15:21:16Z buchmann $
//----------------------------------------
// format BNF
// (parameter)*
// parameter 	= name '=' value
// name		= string
// value	= string
// comment: # to eol 
// delimiters: blank tab 
// predefined names
// _VERBOSE_ = ON|OFF default on
#include "util/plist.h"
#include "io/ascii/out.h"
#include "io/byte/file/file.h"
namespace util
{
 class PFile:public PList
 {
  private:
   static const char* VerboseVal[];
   void read(io::byte::file::Input& src);
//   const char* fileName(const char key[]);
   void onError(io::ascii::Writer& msg) const;
   PFile(const PFile&); //no cloning
   
  public:
            PFile(unsigned arg,const char env[],bool verbose=true);
	    //if arg>0  takes arg-th argument as fileName
	    //if env!=0 takes value of env as fileName
	    //arg takes precedence 
	    PFile(io::byte::file::Input& src,bool verbose=true);
	    //otherwise exit
	    PFile(const char fileName[],bool verbose=true);
   virtual ~PFile();
//files
#if 0
change it
   void open(const char key[],std::ifstream& src);
   void openAsTextFile(const char key[],std::ifstream& src);
   void open(const char key[],std::ofstream& dest);
   void openAsTextFile(const char key[],std::ofstream& src);
#endif   
 };
}
#endif

