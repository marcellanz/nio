//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//----------------------------------------
// Parameterfile
// (c) H.Buchmann ISOe 1998 
// $Id: pfile.cc 193 2006-01-11 15:21:16Z buchmann $
// todo: use Tokenizer
//       garantee uniqness of key
//----------------------------------------
//#define DEBUG
#include "sys/sys.h"
IMPLEMENTATION(util_pfile,$Revision: 137 $)
#include "util/pfile.h"
#include "util/str.h"
#include "sys/host.h"
#include "sys/msg.h"
namespace util
{
 const char* PFile::VerboseVal[]={"OFF","ON",0};

 PFile::PFile(unsigned arg,const char env[],bool verbose)
 :PList(sys::msg,verbose)
 {
  const char* fName=((0<arg)&&(arg<sys::argCnt()))
                        ?sys::argAt(arg)
			:sys::getEnv(env);
  if (fName==0) 
     {
      sys::msg<<"**** PFile: ";
      if (arg>0) {sys::msg<<"no arg at "<<arg<<"\n";::sys::exit(1);}
      if (env==0){sys::msg<<"no env defined\n";::sys::exit(1);}
      sys::msg<<"no value for variable '"<<env<<"'\n";
      sys::exit(1);
     }
  io::byte::file::Input src(fName);
  read(src);
}

 PFile::PFile(io::byte::file::Input& src,bool verbose)
 :PList(sys::msg,verbose)
 {
  read(src);
 }
 
 PFile::PFile(const char fileName[],bool verbose)
 :PList(sys::msg,verbose)
 {
  io::byte::file::Input src(fileName);
  read(src);
 }
 
 PFile::~PFile()
 {
 }

 void PFile::read(io::byte::file::Input& src)
 {
  while(true)
  {
   int ch=src.get();
   if (ch==-1) break;
   put(ch);
  }
  put('\n'); //for sure 
//predefined names
  verbose=valueOf("_VERBOSE_",VerboseVal,true);  
 }

#if 0
 const char* PFile::fileName(const char key[])
 {
  const char* s;
  if (!valueOf(key,s))
     {
      sys::msg<<"Error: no such name :'"<<key<<"'\n";
      ::exit(0);
     }
  return s;
 }


 void PFile::open(const char key[],std::ifstream& src)
 {
  const char* s=fileName(key);
  src.open(s);
  if (!src)
  {
   sys::msg<<"Error: file :'"<<s<<"' dont exists\n";
   ::exit(0);
  }   
 }

 void PFile::openAsTextFile(const char key[],std::ifstream& src)
 {
  const char* s=fileName(key);
  src.open(s,std::ios::in);
  if (!src)
  {
   sys::msg<<"Error: file :'"<<s<<"' dont exists\n";
   ::exit(0);
  }   
 }

 void PFile::open(const char key[],std::ofstream& dest)
 {
  dest.open(fileName(key),std::ios::binary|std::ios::out);   
 }

 void PFile::openAsTextFile(const char key[],std::ofstream& dest)
 {
  dest.open(fileName(key),std::ios::out);
 }
#endif

 void PFile::onError(io::ascii::Writer& msg) const
 {
  sys::exit(1);
 }

}
#ifdef DEBUG
PFile pFile(cin);
int main(){return 0;}
#endif

