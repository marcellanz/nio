#ifndef UTIL_CALENDAR_H
#define UTIL_CALENDAR_H
//----------------------------
//calendar
//(c) H.Buchmann FHSO 2005
//$Id: calendar.h 193 2006-01-11 15:21:16Z buchmann $
//----------------------------
INTERFACE(util_calendar,$Revision: 137 $)
#include "io/ascii/write.h"
namespace util
{
 class Calendar
 {
  public:
   typedef unsigned Time_Sec;
   
   struct Time
   {
    unsigned sec;    //0..59
    unsigned min;    //0..59
    unsigned hour;   //0..23
    unsigned day;    //1..31
    unsigned month;  //0..11
    unsigned year;   //the current one
    unsigned dow;    //0..6 day of week
    io::ascii::Writer& show(io::ascii::Writer&) const;
   };
   
   static void toLocal(const Time_Sec t,Time& time);
   static Time_Sec getTime();
   static io::ascii::Writer& show(io::ascii::Writer& out);
 };
}
#endif
