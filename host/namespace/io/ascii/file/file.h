//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#ifndef IO_ASCII_FILE_FILE_H
#define IO_ASCII_FILE_FILE_H
//-------------------------------
//File
//(c) H.Buchmann FHSO 2003
//$Id: file.h 193 2006-01-11 15:21:16Z buchmann $
//-------------------------------
INTERFACE(io_ascii_file_file,$Revision: 137 $)
#include "io/ascii/out.h"
#include "io/byte/file/file.h"
namespace io
{
 namespace ascii
 {
  namespace file
  {
   class Writer:public ascii::Writer,
                public io::ascii::Output
   {
    private:
     byte::file::Output out;
     void put(char ch);
     
    public:
              Writer(const char name[],
	             byte::file::Output::Mode mode=byte::file::Output::CREATE);
     virtual ~Writer();
     ascii::Writer& info(ascii::Writer& out)const
     {return this->out.info(out);} 
   };
  }
 }//namespace ascii
}//namespace io
#endif
