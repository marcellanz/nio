//------------------------
//bin2intel
//(c) H.Buchmann FHSO 2005
//$Id: bin2intel.cc 193 2006-01-11 15:21:16Z buchmann $
//------------------------
#include "sys/sys.h"
IMPLEMENTATION(io_ascii_file,$Revision$)
#include "sys/host.h"
#include "util/str.h"
#include "sys/msg.h"
#include "io/byte/file/file.h"
#include "sys/out.h"

namespace io
{
 namespace ascii
 {
  namespace file
  {
   class Converter
   {
    static Converter converter;
    static const char Digits[];
    unsigned short start;
    unsigned char* data;
    unsigned size;
    template<typename T>int hex(T val);
    template<typename T>int checkSum(T val);
    void convert();
    
    Converter();
    ~Converter();
   };
   
   Converter Converter::converter;
   const char Converter::Digits[]="0123456789ABCDEF";

   template<typename T>
   int Converter::checkSum(T val)
   {
    unsigned shift=8*sizeof(T);
    int cs=0;
    do
    {
     shift-=8;
     cs+=0xff&(val>>shift);
    }while(shift>0);
    return cs;
   }

   template<typename T>
   int Converter::hex(T val)
   {
    unsigned shift=8*sizeof(T);
    do
    {
     shift-=4;
     sys::out<<Digits[0xf&(val>>shift)];
    }while(shift>0);
    return checkSum(val);
   }

   
   Converter::Converter()
   :start(0),
    data(0),
    size(0)
   {
    if (sys::argCnt()!=3)
       {
        sys::msg<<"usage: "<<sys::argAt(0)<<" file start\n"
	          "   start: number\n";
	sys::exit(1);	  
       }
    if (!util::Str::to(sys::argAt(2),start))
       {
        sys::msg<<"start= '"<<sys::argAt(2)<<"' not a number\n";
	sys::exit(1);
       }
   {
     io::byte::file::Input src;
     if (!src.open(sys::argAt(1)))
	{
         sys::msg<<"file= '"<<sys::argAt(1)<<"' dont exists\n";
	 sys::exit(1);
	} 
     size=src.getSize();
     data=new unsigned char[size];
     src.get(data,size);
    }
    convert();
   }
   
   Converter::~Converter()
   {
    if (data) delete [] data;
   }

   void Converter::convert()
   {
    static const unsigned DefaultLength=0x10;
    unsigned short addr=start;
    unsigned i=0; //index in data
    unsigned rest=size;
    while(rest>0)
    {
     sys::out<<':';
     unsigned char len=(rest<DefaultLength)?rest:DefaultLength;
     int cs =hex(len);
     cs+=hex(addr);
     cs+=hex((unsigned char)0);
     while(len>0)
     {
      cs+=hex(data[i++]);
      len--;
      addr++;
      rest--;
     }
     hex((unsigned char)-cs);
     sys::out<<"\n";
    }
    sys::msg<<":00000001FF\n"; //end of file
   }
   
  }//namespace file
 }//namespace ascii
}//namespace io
