#ifndef IO_FS_DEVICE
#define IO_FS_DEVICE
//--------------------------
//device 
//(c) H.Buchmann FHNW 2006
//$Id: device.h 193 2006-01-11 15:21:16Z buchmann $
//--------------------------
INTERFACE(io_fs_device,$Revision$)
#include "io/fs/fs.h"
namespace io
{
 namespace fs
 {
  class Device:public Media
  {
   public:
    typedef unsigned Pos;

   private:
    int id;
    int error(int cod,const char s[]);
   public:
             Device(const char name[]);
    virtual ~Device();
    void get(Data& d,Listener& li);
    void setPos(Pos p);
  };
 }//namespace fs
}//namespace io
#endif
