//---------------------------
//test
//(c) H.Buchmann FHNW 2006
//$Id: test.cc 193 2006-01-11 15:21:16Z buchmann $
//---------------------------
#include "sys/sys.h"
IMPLEMENTATION(io_fs_test,$Revision$)
#include "io/fs/vfat/fs.h"
#include "io/fs/fs.h"
#include "io/fs/device.h"
#include "sys/msg.h"

namespace io
{
 namespace fs
 {
  class Tester:public FS::Listener
  {
   static Tester tester;
   Device dev;
   vfat::FS fs;

   void onMount(fs::FS& fs);

   Tester();
  };
  Tester Tester::tester;

  Tester::Tester()
  :dev("/dev/sda")
  {
   fs.mount(dev,*this);
  }

  void Tester::onMount(fs::FS& fs)
  {
   sys::msg<<"mounted\n";
  }
   
 }//namespace fs
}//namespace io
