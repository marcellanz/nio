//--------------------------
//device 
//(c) H.Buchmann FHNW 2006
//$Id: device.cc 193 2006-01-11 15:21:16Z buchmann $
//--------------------------
#include "sys/sys.h"
IMPLEMENTATION(io_fs_device,$Revision$)
#include "io/fs/device.h"
#include "sys/msg.h"
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

namespace io
{
 namespace fs
 {
  Device::Device(const char name[])
  :id(-1)
  {
   id=error(::open(name,O_RDONLY),"open");
  } 
  
  Device::~Device()
  {
   if (id>0) ::close(id);
  }
  
  int Device::error(int cod,const char s[])
  {
   if (cod>=0) return cod;
   sys::msg.error()<<s<<" '"<<::strerror(errno)<<"'\n";
   return cod;
  }
  
  void Device::get(Data& d,Listener& li)
  {
   d.len=error(::read(id,d.data,d.capacity),"get");
   li.onData(d);
  }
  
  void Device::setPos(Pos p)
  {
   ::lseek(id,p,SEEK_SET);
  }
 }//namespace fs
}//namespace io
