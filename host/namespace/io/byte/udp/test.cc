//------------------------
//test
//(c) H.Buchmann FHSO 2005
//$Id: test.cc 193 2006-01-11 15:21:16Z buchmann $
//------------------------
#include "sys/sys.h"
IMPLEMENTATION(io_byte_udp_test,$Revision: 137 $)
#include "io/byte/udp/udp.h"
#include "sys/msg.h"
#include "sys/thread.h"

namespace io
{
 namespace byte
 {
  namespace udp
  {
   class Tester:public Client::Listener
   {
    static const unsigned short PORT=7;
    static Tester tester;
    Client client;
    void onRX(unsigned char pac[],unsigned len);
    unsigned char rx0[100];
    unsigned char rx1[100];
    Tester();
    ~Tester();
   };
   
   Tester Tester::tester;
   
   void Tester::onRX(unsigned char pac[],unsigned len)
   {
    sys::msg<<"onRX "<<(char*)pac<<"\n";
   }
   
   Tester::Tester()
   :client("192.168.1.132",PORT)
   {
    sys::msg<<"UDP Test\n";
//    Socket sock(7);
#if 1
    static const char Msg0[]="0123456789";
    static const char Msg1[]="ABCD";
    client.listen(rx0,sizeof(rx0),*this);
    client.listen(rx1,sizeof(rx1),*this);
    client.send((unsigned char*)Msg0,sizeof(Msg0));
    client.send((unsigned char*)Msg1,sizeof(Msg1));
    sys::Thread::wait();
#endif
   }
   
   Tester::~Tester()
   {
    sys::msg<<"~Tester\n";
   }
  }//namespace udp
 }//namespace byte
}//namespace io
