//------------------------
//udp
//(c) H.Buchmann FHSO 2005
//$Id: udp.cc 193 2006-01-11 15:21:16Z buchmann $
//------------------------
#include "sys/sys.h"
IMPLEMENTATION(io_byte_udp,$Revision: 137 $)
#include "io/byte/udp/udp.h"
#include "sys/msg.h"
#include "util/endian.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

namespace io
{
 namespace byte
 {
  namespace udp
  {
//----------------------------------------- Client   
   Client::Listener::~Listener()
   {
   }

   
   Client::Client(const char ip[],unsigned short port)
   :ip4(ip4),
    port(port),
    id(-1),
    thread(*this)
   {
    ::hostent* host=::gethostbyname(ip);
    if (host==0)
       {
        sys::msg.error()<<"host '"<<host<<"' not known\n";
       }
    id=::socket(PF_INET,SOCK_DGRAM,0);
    if (id<0)
       {
        sys::msg.error()<<"Client: "<<::strerror(errno)<<"\n";
       }
    addr.sin_family=AF_INET;
    addr.sin_port  = util::Endian::big(port);
    addr.sin_addr.s_addr=(*(unsigned*)(host->h_addr));
    thread.start();   
   }
   
   Client::~Client()
   {
    if (id>0) ::close(id);
    sys::msg<<"~Client\n";
   }
   
   void Client::listen(unsigned char pac[],unsigned capacity,Listener& li)
   {
    queue.push_back(new Entry(pac,capacity,li));
    rxS.open();
   }

   void Client::run()
   {
    while(true)
    {
     rxS.wait();
     Entry* e=queue.front();
     ::sockaddr_in addr;
     ::socklen_t len=0;
     unsigned siz=::recvfrom(id,e->pac,e->capacity,0,
                            (::sockaddr*)&addr,&len);
     e->li.onRX(e->pac,siz);
     queue.pop_front();
     delete e;
    }
   }
   
   void Client::send(unsigned char pac[],unsigned len)
   {
    int res=::sendto(id,pac,len,0,(::sockaddr*)&addr,sizeof(addr));
    if (res<0)
       {
        sys::msg.error()<<"send "<<::strerror(errno)<<"\n";
       }
   }
   
  }//namespace udp
 }//namespace byte
}//namespace io
