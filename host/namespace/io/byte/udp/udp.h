#ifndef IO_BYTE_UDP_H
#define IO_BYTE_UDP_H
//------------------------
//udp
//(c) H.Buchmann FHSO 2005
//$Id: udp.h 193 2006-01-11 15:21:16Z buchmann $
//------------------------
INTERFACE(io_byte_udp,$Revision: 137 $)
#include "io/byte/byte.h"
#include "sys/thread.h"
#include <list>
#include <netinet/in.h>

namespace io
{
 namespace byte
 {
  namespace udp
  {
//----------------------------------------- Socket   
   class Socket
   {
    private:
     int id; //the socket
     
    public:
              Socket(unsigned short port);
     virtual ~Socket();
   };
   
//----------------------------------------- RX   
   class Client:public sys::Runnable //receiver
   {
    public:
     struct Listener
     {
      virtual void onRX(unsigned char pac[],unsigned len)=0;
      virtual ~Listener();
     };
     
    private:
     struct Entry
     {
      unsigned char* pac;
      unsigned capacity;
      Listener& li;
      Entry(unsigned char pac[],unsigned capacity,Listener& li)
      :pac(pac),capacity(capacity),li(li){}
     };
     unsigned ip4;
     unsigned short port;
     int id;
     ::sockaddr_in addr;
     std::list<Entry*> queue;
     sys::Semaphore rxS;
     sys::Thread thread;
     
     void run();

    public:
              Client(const char ip[],unsigned short port);
     virtual ~Client();
     void listen(unsigned char pac[],unsigned capacity,Listener& li);
     void send(unsigned char pac[],unsigned len);
   };
   
  }//namespace udp
 }//namespace byte
}//namespace io
#endif
