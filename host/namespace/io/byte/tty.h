#ifndef IO_BYTE_TTY_H
#define IO_BYTE_TTY_H
//------------------------------
//uart on host
//(c) H.Buchmann FHSO 2004
//$Id: tty.h 193 2006-01-11 15:21:16Z buchmann $
//------------------------------
INTERFACE(io_byte_tty,$Revison$)
#include "io/byte/uart.h"
#include "sys/thread.h"
namespace io
{
 namespace byte
 {
   class TTY:public UART,
             public sys::Runnable
   {
    private:
     int id; 
     byte::RXListener* li;
     sys::Thread thread;

              TTY(int id,const char name[]);
     virtual ~TTY();
     void lock();
     void unlock();
 //implementation uart::Device
     void init();
 //implementation sys::Runnable
     void run();
     static int open(const char name[]);
     
    public:
     static UART* create(const char name[]);
     void put(unsigned char);    
     byte::RXListener* setRX(byte::RXListener* li);
   };
 } //namespace byte
} //namespace io
#endif
