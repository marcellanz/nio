//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//------------------------
//tcp channel
//(c) H.Buchmann FHSO 2003
//$Id: tcp.cc 193 2006-01-11 15:21:16Z buchmann $
//------------------------
#include "sys/sys.h"
IMPLEMENTATION(io_byte_tcp,$Revision: 169 $)
#include "io/byte/tcp/tcp.h"
#include "sys/host.h"
#include "sys/msg.h"
#include "util/endian.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <stdio.h>
namespace io
{
 namespace byte
 {
  namespace tcp
  {
//-------------------------------------------- Connection  
   Connection::Connection(int sock,Listener& li)
   :thread(this),
    rxli(0),li(&li),
    sock(sock),
    connected(false)
   {
   }

   Connection::~Connection()
   {
    ::close(sock);
   }
   
   void Connection::run()
   {
    while(true)
    {
     unsigned char ch;
     int len=::read(sock,&ch,sizeof(ch));
     if (len==0)
        {
	 li->onClose(*this);
	 connected=false;
	 break;
	}
     if (rxli) rxli->onRX(ch);
    }
   }
   
   void Connection::put(unsigned char d)
   {
    ::write(sock,&d,sizeof(d));
   }
   
   RXListener* Connection::setRX(RXListener* li)
   {
    RXListener* old=rxli;
    rxli=li;
    return old;
   }
   
//-------------------------------------------- Client
   Client::Client(const char ip[],unsigned short port,Listener& li)
   :Connection(-1,li),
    ip(ip),port(port)
   {
   }
   
   
   Client:: ~Client()
   {
   }

   bool Client::connect()
   {
    if (connected) return true; //already connected
    sock=::socket(PF_INET,SOCK_STREAM,0);
    if (sock<0)
       {
        ::perror("Client");
	sys::exit(1);
       } 
    hostent* host=::gethostbyname(ip);
    if (host==0)
       {
        sys::msg<<"host '"<<ip<<"' not known\n";
	return false;
       }
    struct ::sockaddr_in addr; 
    addr.sin_family=AF_INET;
    addr.sin_port=util::Endian::big(port);
    addr.sin_addr.s_addr=(*(unsigned*)(host->h_addr));
    if (::connect(sock,(const sockaddr*)&addr,sizeof(addr))<0) 
       {
        ::perror("connect");
	return false;
       }
    sys::msg<<"connected\n";
    connected=true; 
    thread.start(); 
    return true; 
   }
   
//-------------------------------------------- Server::Connection
   Server::Connection::Connection(tcp::Connection::Listener& li,
				  int sock):
   tcp::Connection(sock,li)
   {
    thread.start();
   }
   
//-------------------------------------------- Server
   Server::Server(unsigned short port,Listener& li):
   li(li),
   thread(this)
   {
    serverSock=::socket(PF_INET,SOCK_STREAM,0);
    if (serverSock<0)
       {
        ::perror("Server");
	sys::exit(1);
       } 
    struct ::sockaddr_in addr; 
    addr.sin_family=AF_INET;
    addr.sin_port=util::Endian::big(port);
    addr.sin_addr.s_addr=util::Endian::big(INADDR_ANY);
    if (::bind(serverSock,(struct sockaddr*)&addr,sizeof(addr)))
       {
        ::perror("bind");
	sys::exit(1);
       }
    sys::msg<<"serverSocket "<<serverSock<<"\n";
    thread.start();
   }
   
   Server::~Server()
   {
   }
   
   void Server::run()
   {
    if (::listen(serverSock,1)<0)
       {
	::perror("listen");
	sys::exit(1);
       }
    while(true)
    {
     struct ::sockaddr_in client;
     unsigned clientSize;
     int sock=::accept(serverSock,(struct sockaddr*)&client,&clientSize);
     if (sock<0)
        {
	 ::perror("accept");
	 sys::exit(1);
	}
     Connection* conn=new Connection(li,sock);
     li.onConnect(*conn);
    }
   }
  } //namespace tcp
 } //namespace byte
} //namespace io
