#ifndef IO_BYTE_TCP_H
#define IO_BYTE_TCP_H
//------------------------
//tcp channel
//(c) H.Buchmann FHSO 2003
//$Id: tcp.h 193 2006-01-11 15:21:16Z buchmann $
//------------------------
INTERFACE(io_byte_tcp,$Revision: 137 $)
#include "io/byte/byte.h"
#include "sys/thread.h"

namespace io
{
 namespace byte
 {
  namespace tcp
  {
//-------------------------------------------- Connection  
   class Connection:public Channel,
                    public sys::Runnable
   {
    public:
//-------------------------------------------- Connection::Listener 
     struct Listener
     {
      virtual void onClose(Connection&)=0;
      virtual ~Listener(){}
     };
     
    private:
     friend class Server;
     RXListener* rxli;
     Listener* li;
     void run();
     
    protected:
     Connection(int sock,Listener& li);
     int sock;
     sys::Thread thread;
     bool connected;
     
    public:
     virtual ~Connection(); //close the connection
     RXListener* setRX(RXListener* li);
     void put(unsigned char d);
     bool isConnected(){return connected;}
   };
   
//-------------------------------------------- Client  
   class Client:public Connection
   {
    private:
     const char* ip;
     const unsigned short port;
     
     
    public:
              Client(const char ip[],unsigned short port,Listener& li);
     virtual ~Client();
     bool connect();
   };

//-------------------------------------------- Server
   class Server:public sys::Runnable
   {
    public:
//-------------------------------------------- Server::Listener 
     struct Listener:public tcp::Connection::Listener
     {
      virtual void onConnect(Connection& ch)=0;
      virtual ~Listener(){}
     };
     
    private:
     struct Connection:public tcp::Connection
     {
      Connection(tcp::Connection::Listener& li,int sock);
     };
     
     Listener& li;
     int serverSock;
     sys::Thread thread;
     void run();
          
    public: 
              Server(unsigned short port,Listener& li);
     virtual ~Server();     
   };
  }
 }
}
#endif
