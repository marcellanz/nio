//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#ifndef IO_BYTE_FILE_FILE
#define IO_BYTE_FILE_FILE
//------------------------------------
//file
//(c) H.Buchmann FHSO 2003
//$Id: file.h 193 2006-01-11 15:21:16Z buchmann $
//------------------------------------
INTERFACE(io_byte_file_file,$Revision: 169 $)
#include "io/byte/byte.h"
#include "io/ascii/write.h"
#include "io/byte/stream/stream.h"
#include "util/path.h"
#include <string>

namespace io
{
 namespace byte
 {
  namespace file
  {
//----------------------------------------------- File  
   class File
   {
    private:
     File(const File&);
     File operator=(const File&);
     std::string fileName;
     
    protected:
//     const char* name;
     int id;
     bool isOpen;
     File(const char name[]);
     File();
     void setName(const char name[]);
     int error(int cod); 
     
    public:
     virtual ~File();
     const char* getName()const{return fileName.c_str();}
     unsigned long getPos()const;
//     virtual void open(const char name[])=0;
     virtual ascii::Writer& info(ascii::Writer&) const;
     void close();
//path tests
     static bool isAbsolute(const char name[]);
     static bool isAbsolute(const std::string& name)
           {return isAbsolute(name.c_str());}
   };

//----------------------------------------------- Input  
   class Input:public File
   {
    public:
//----------------------------------------------- Input::Stream  
     class Stream:public io::byte::stream::Input
     {
      private:
      public:
                Stream(const char name []);
       virtual ~Stream(); 
     };
     
    private:
     unsigned long size;
     
    public:
              Input(const char name[]);
	       //terminates if error on opening
	      Input();
     virtual ~Input();

     bool open(const char name[]);
     bool open(const std::string& name){return open(name.c_str());}
     bool open(const util::Path& path){return open(path.getName());}
     
              //returns true iff opened
     int get(); //-1 eof 
                //otherwise 0..255 byte val
     unsigned get(const unsigned char data[],unsigned len);
     unsigned get(         char data[],unsigned len)
                 {return get((unsigned char*)data,len);}
             //returns number of chars filled in data
	     //return<=len
     template<typename T>
     bool get(T& t){return get((unsigned char)(&t))==sizeof(T);}	     
     unsigned long getSize() const{return size;}
   };

//----------------------------------------------- Output  
   class Output:public File,
                public io::byte::Output
   {
    public:
     enum Mode {CREATE,APPEND};
     static const char* ModeName[];
     
              Output(const char name[],Mode mode=CREATE);
	       //terminates if error on opening
	      Output(){}
     virtual ~Output();
     void open(const char name[],Mode mode=CREATE);
     void put(unsigned char ch);
     void put(const unsigned char data[],unsigned len);
     void put(const          char data[],unsigned len)
             {put((const unsigned char*)data,len);}
#if 0
     template<typename T>
     void put(const T& t)
          {
	   put((const unsigned char*)&t,sizeof(T));
	  }
#endif     
     template<typename T>
     void put(const T* t)
         {
	  put((const unsigned char*)t,sizeof(T));
	  
	 }
     ascii::Writer& info(ascii::Writer&) const;

    private:
     Mode mode;
   };
  } //namespace file 
 } //namespace byte
}//namespace io
#endif
