//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//------------------------------------
//file
//(c) H.Buchmann FHSO 2003
//$Id: file.cc 193 2006-01-11 15:21:16Z buchmann $
//------------------------------------
#include "sys/sys.h"
IMPLEMENTATION(io_byte_file_file,$Revision: 169 $)
#include "io/byte/file/file.h"
#include "sys/msg.h"
#include "sys/host.h"
#include "util/str.h"
//#include <sys/types.h>
//#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

//#define IO_BYTE_FILE_DEBUG

namespace io
{
 namespace byte
 {
  namespace file
  {
//----------------------------------------------- File  
   File::File(const char name[])
   :id(-1),isOpen(false)
   {
    setName(name);
   }
   
   File::File()
   :id(-1),isOpen(false)
   {
    fileName[0]='\0';
   }
   
   File::~File()
   {
    close();
   }
   
   void File::setName(const char name[])
   {
    if (name==0) sys::msg.error()<<"'null'  file name\n";
    fileName=name;
   }

   int File::error(int cod)
   {
    if (cod>=0) return cod;
    sys::msg.error()<<::strerror(errno)<<" file '"<<fileName.c_str()<<"'\n";
    return cod; //for the compiler
   } 

   unsigned long File::getPos()const
   {
    return ::lseek(id,0,SEEK_CUR);
   }
  
   ascii::Writer& File::info(ascii::Writer& out) const
   {
    out<<"'"<<fileName.c_str()<<"' "<<getPos()<<" bytes";
    return out;
   }
   
   void File::close()
   {
    if (isOpen) error(::close(id));
    isOpen=false;
   }

   bool File::isAbsolute(const char name[])
   {
    if (name==0) return false;
    unsigned i=0;
    while(true)
    {
     char ch=name[i++];
     if (ch=='\0') return false;
     if (ch!=' ') return ch=='/';
    }
   }
   
//----------------------------------------------- Input::Stream  
   Input::Stream::Stream(const char name [])
   {
    io::byte::file::Input src(name);
    size=src.getSize();
    data=new unsigned char[size];
    src.get(data,size);
   }
   
   Input::Stream::~Stream()
   {
    if (data) delete data;
   } 
 //----------------------------------------------- Input  
   Input::Input(const char name[])
   :File(name),size(0)
   {
    open(name);
    if (!isOpen) error(-1);
   }
   
   Input::Input()
   :size(0)
   {
   }
   
   Input::~Input()
   {
   }

   bool Input::open(const char name[])
   {
    setName(name);
    id=::open(name,O_RDONLY,0666);
    if (id<0) return false;
    size=::lseek(id,0,SEEK_END);
    isOpen=::lseek(id,0,SEEK_SET)>=0;
    return isOpen;
   }

   int Input::get()
   {
    if (!isOpen) return -1;
    unsigned char ch;
    long int siz=::read(id,&ch,sizeof(ch));
    error(siz);
    return (siz==sizeof(ch))?ch:-1;
   }
   
   unsigned Input::get(const unsigned char data[],unsigned len)
   {
    if (!isOpen) return 0;
    long int siz=::read(id,(void*)data,len);
    error(siz);
    return siz;
   }
  
//----------------------------------------------- Output  
   const char* Output::ModeName[]={"CREATE","APPEND",0};
                                                  // ^terminates list
   Output::Output(const char name[],Mode mode)
   :File(name),
    mode(mode)
   {
    open(name,mode);
   }

   Output::~Output()
   {
   }
   
   void Output::open(const char name[],Mode mode)
   {
    static const unsigned ModeFlag[]={O_TRUNC,  //CREATE
                                      O_APPEND}; //APPEND
    if (isOpen) return;
    id=error(::open(name,O_WRONLY|O_CREAT|ModeFlag[mode],0666));
    isOpen=true;
   }
   
   void Output::put(unsigned char ch)
   {
    if (!isOpen) return;
    error(::write(id,&ch,sizeof(ch)));
   }

   void Output::put(const unsigned char data[],unsigned len)
   {
    if (!isOpen) return;
    error(::write(id,data,len));
   }
   
   ascii::Writer& Output::info(ascii::Writer& out) const
   {
    return File::info(out)<<" "<<ModeName[mode];
   }
   
#ifdef IO_BYTE_FILE_DEBUG
//-------------------------------------------- Tester
   class Tester
   {
    static Tester tester;
    Tester();
   };
   
   Tester Tester::tester;
   
   Tester::Tester()
   {
    sys::msg<<"File Test\n";
    Input src("/dev/sda");
    unsigned char data[1024];
    src.get(data,sizeof(data));
    sys::msg.dump(data,31);
   }
#endif
  }//namespace file
 }//namespace byte
}//namespace io
