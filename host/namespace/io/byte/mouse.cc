//---------------------------
//mouse
//(c) H.Buchmann FHSO 2005
//$Id: mouse.cc 146 2005-10-10 08:28:32Z buchmann $
//see http://users.tkk.fi/~then/mytexts/mouse.html
//---------------------------
#include "sys/sys.h"
IMPLEMENTATION(io_byte_mouse,$Revision$)
#include "io/byte/mouse.h"
#include "sys/msg.h"

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>

//#define IO_BYTE_MOUSE_DEBUG


namespace io
{
 namespace byte
 {
  sys::Device::List Mouse::dList;

  Mouse::Listener::~Listener()
  {
  }
  
  Mouse Mouse::mice[]={"/dev/mouse"};
  
  Mouse::Mouse(const char device[])
  :sys::Device(device,dList,false),
   id(-1),
   x(0),y(0), //inital position
   th(this),
   li(0)
  {
   id=::open(device,O_RDONLY);
   if (id<0)
      {
       sys::msg.error()<<"device '"<<device<<"' "<<::strerror(errno)<<"\n";
      }
   th.start();
  }
  
  void Mouse::run()
  {
   while(true)
   {
    bool key[3];
    char d[3];
//ps2
// 7  6  5  4  3  2  1  0   
// xv xv ys xs 1  m  r  l     d[0]
// x7 x6 x5 x4 x3 x2 x1 x0    d[1]
// y7 y6 y5 y4 y3 y2 y1 y0    d[2]
    ::read(id,d,sizeof(d));
    mutex.lock();
    x+=d[1];
    y+=d[2];
    key[Listener::LEFT]  =d[0]&(1<<0);
    key[Listener::MIDDLE]=d[0]&(1<<2);
    key[Listener::RIGHT] =d[0]&(1<<1);
    Listener* li=this->li;
    mutex.unlock();
    if (li) li->onMouse(x,y,key);
   }
  }

  io::ascii::Writer& Mouse::info(io::ascii::Writer& out)const
  {
   out<<getName()<<":ps2";
   return out;
  }

  void Mouse::setPos(int x,int y)
  {
   mutex.lock();
   this->x=x;
   this->y=y;
   mutex.unlock();
  }

  void Mouse::setListener(Listener* li)
  {
   mutex.lock();
   this->li=li;
   mutex.unlock();
  }
  
#ifdef IO_BYTE_MOUSE_DEBUG
  class Tester:public Mouse::Listener
  {
   static Tester tester;
   Tester();
   void onMouse(int x,int y,const bool key[]);
  };
  
  Tester Tester::tester;

  void Tester::onMouse(int x,int y,const bool key[])
  {
   static const char* L[]={"","left"};
   static const char* M[]={"","middle"};
   static const char* R[]={"","right"};
   sys::msg<<"mouse: "<<x<<" "<<y<<" "
           <<L[key[LEFT  ]]<<" "
	   <<M[key[MIDDLE]]<<" "
	   <<R[key[RIGHT ]]<<"\n";
  }

  Tester::Tester()
  {
   sys::msg<<"Mouse Tester\n";
   Mouse::get(0u).setListener(this);
   sys::Thread::wait();
  }
#endif
 }//namespace byte
}//namespace io

