//---------------------------
//mouse
//(c) H.Buchmann FHSO 2005
//$Id: mouse.h 144 2005-10-05 17:06:28Z buchmann $
//---------------------------
INTERFACE(io_byte_mouse,$Revision$)
#include "sys/device.h"
#include "sys/thread.h"

namespace io
{
 namespace byte
 {
  class Mouse:public sys::Device,
              public sys::Runnable
  {
   public:
    struct Listener
    {
     enum Key {LEFT,MIDDLE,RIGHT};
     virtual ~Listener();
     virtual void onMouse(int x,int y,const bool key[])=0;
    };
    
   private:
    static Mouse mice[]; // the available ones
    static Device::List dList;
    sys::Mutex mutex;
    int id;  // of posix/device 
    int x,y; //the position  
    sys::Thread th;
    Listener* li;

    Mouse(const Mouse&);
    Mouse(const char device[]);
    void run(); 

    
   public:
    static Mouse& get(unsigned  id)
          {return (Mouse&)dList.get(id);}
    static Mouse& get(const char name[])
          {return (Mouse&)dList.get(name);}
     void setListener(Listener* li);
    void setPos(int x,int y); 
//------------------------------- implementation sys::Device
    io::ascii::Writer& info(io::ascii::Writer&)const;     
  };
  
 }//namespace byte
}//namespace io
