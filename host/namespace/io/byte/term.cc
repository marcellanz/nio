//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//------------------------
//terminal
//(c) H.Buchmann FHSO 2003
//$Id: term.cc 193 2006-01-11 15:21:16Z buchmann $
//------------------------
#include "sys/sys.h"
IMPLEMENTATION(io_byte_term,$Revision: 137 $)
#include "io/byte/term.h"
#include "sys/device.h"
#include "sys/msg.h"
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>

namespace io
{
 namespace byte
 {
  sys::Device::List Terminal::dList;
  Terminal Terminal::term[]={Terminal("term",STDOUT_FILENO)};
  
  Terminal::Terminal(const char name[],int id)
  :sys::Device(name,dList,true),
   id(id),
   log(-1),
   rec(this),
   li(0),
   echo(true)
  {
   if (::isatty(id)==0)
      {
       log=id;
       id=::open("/dev/tty",O_CREAT|O_RDWR);
       sys::msg<<"logging enabled\n";
      }
   ::tcgetattr(id,&settings);
   termios newSettings;
   ::tcgetattr(id,&newSettings);
   newSettings.c_lflag&=~(ICANON|ECHO);
   
   this->id=id;
   rec.start();
  }
  
  Terminal::~Terminal()
  {
   if (id>=0) 
      {
       ::tcsetattr(id,TCSANOW,&settings);
      }
   if (log>=0)
      {
       ::close(log);
      }   
  }

  void Terminal::run()
  {
   while(true)
   {
    unsigned char ch;
    ::read(id,&ch,sizeof(ch));
    if (echo)    ::write(STDOUT_FILENO,&ch,sizeof(ch));
    if (log>=0)  ::write(log          ,&ch,sizeof(ch));
    if (li) li->onRX(ch);
   }
  }
  
  void Terminal::put(unsigned char d)
  {
   ::write(id,&d,sizeof(d));
   if (log>=0) ::write(log          ,&d,sizeof(d));
  }

  ascii::Writer& Terminal::info(ascii::Writer& out) const
  {
   out<<"terminal\n";
   return out;
  }
  
  RXListener* Terminal::setRX(RXListener* li)
  {
   RXListener* old=this->li;
   this->li=li;
   return old;
  }
 } //byte
} //io
