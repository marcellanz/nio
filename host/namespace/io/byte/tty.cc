//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//------------------------------
//uart on host
//(c) H.Buchmann FHSO 2004
//$Id: tty.cc 193 2006-01-11 15:21:16Z buchmann $
//------------------------------
#include "sys/sys.h"
IMPLEMENTATION(io_byte_tty,$Revison$)
#include "io/byte/tty.h"
#include "sys/msg.h"
#include "sys/host.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>
#include <sys/file.h>
#include <stdio.h>

//#define IO_BYTE_TTY_TEST

#ifdef IO_BYTE_TTY_TEST
#include "io/byte/term.h"
#endif

namespace io
{
 namespace byte
 {
   int TTY::open(const char name[])
   {
    int id=::open(name,O_RDWR|O_NOCTTY|O_NDELAY);
    if (id<0) return id;
    if (!::isatty(id)) 
       {
        ::close(id);
	return -1;
       }
    unsigned flags=::fcntl(id, F_GETFL,0);
    ::fcntl(id,F_SETFL,flags&~O_NDELAY);
    return id;     
   }

   TTY::TTY(int id,const char name[])
   :UART(name,false),
    id(id),li(0),
    thread(this)
   {
    thread.start();   
   }
   
   TTY::~TTY()
   {
    thread.cancel();
    ::close(id);
   }

   void TTY::put(unsigned char ch)
   {
    ::write(id,&ch,sizeof(ch));
   }

   UART* TTY::create(const char name[])
   {
    if (exists(name)) return (TTY*)&get(name);
    int id=open(name);
    return (id<0)?0:new TTY(id,name);
   }
   
   void TTY::init()
   {
    termios term;
    ::tcgetattr(id,&term);
    term.c_iflag=IGNBRK;
    term.c_lflag=0;
    term.c_oflag=0;
    
    static const tcflag_t StopBit[]={0,CSTOPB};  
    term.c_cflag&=~CSTOPB;
    term.c_cflag|=StopBit[cfg.sb];
    term.c_cflag|=(CLOCAL|CREAD);
    static const tcflag_t WL[]={CS5,CS6,CS7,CS8};
    term.c_cflag&=~CSIZE;
    term.c_cflag|=WL[cfg.wl];

    static const speed_t SPEED[]=
     { //see io::uart::Baudrate for valid enum
//    B50,    B75,   B110,  B134,  B150,  B200,
//     B300  B600  
     B1200,  
//   B1800  
     B2400,  B4800,
     B9600,  B19200,B38400,B57600,B115200,
     B230400,B460800
     };
    ::cfsetspeed(&term,SPEED[cfg.br]);
    ::tcsetattr(id,TCSANOW,&term);
   }
   
   byte::RXListener* TTY::setRX(byte::RXListener* li)
   {
    byte::RXListener* old=this->li;
    this->li=li;
    return old;
   }
   
   void TTY::run()
   {
    while(true)
    {
     unsigned char ch;
     if (::read(id,&ch,sizeof(ch))<0)
        {
	 ::perror("rx error");
	 continue;
	}
     if (li) li->onRX(ch);
    }
   }

   void TTY::lock()
   {
    int cod=flock(id,LOCK_EX);
    if (cod<0)::perror("lock");
       
   }
   
   void TTY::unlock()
   {
   }   
 } //namespace byte
} //namespace io

#ifdef IO_BYTE_TTY_TEST
#undef B57600
class Tester:public io::byte::RXListener
{
 static Tester tester;
 io::byte::UART& uart;
 io::byte::Terminal& term;
 io::byte::Switch switch_;
 io::ascii::Writer out;
 Tester();
 void onRX(unsigned char ch);
};

Tester Tester::tester;

Tester::Tester()
:uart(io::byte::TTY::create("/dev/ttyS0")->init(io::byte::UART::B57600)),
 term(io::byte::Terminal::get(0u)),
 switch_(uart,term),
 out(uart)

{
 uart.setRX(this);
 sys::msg<<__PRETTY_FUNCTION__<<"\n";
 sys::Thread::wait();
}

void Tester::onRX(unsigned char ch)
{
 sys::msg<<(char)ch;
}
#endif
