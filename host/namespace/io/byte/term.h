#ifndef IO_BYTE_TERM_H
#define IO_BYTE_TERM_H
INTERFACE(io_byte_term,$Revision: 144 $)
//------------------------
//terminal
//(c) H.Buchmann FHSO 2003
//$Id: term.h 193 2006-01-11 15:21:16Z buchmann $
//------------------------
#include "sys/device.h"
#include "io/byte/byte.h"
#include "io/ascii/out.h"
#include "sys/thread.h"
#include <termios.h>
namespace io
{
 namespace byte
 {
  class Terminal:public sys::Device,
                 public byte::Channel,
		 public sys::Runnable
  {
   private:
    static sys::Device::List dList;
    static Terminal term[];
    int  id;
    int log; //log file>0 iff STDOUT_FILENO not a terminal 
    termios settings;
    sys::Thread rec;
    RXListener* li;
    bool echo;
    Terminal(const char name[],int id);
    Terminal(const Terminal&); //no copy
    void run();
    
   public:
    virtual ~Terminal();
    static Terminal& get(unsigned id){return (Terminal&)dList.get(id);} 
    static Terminal& get(const char name[]){return (Terminal&)dList.get(name);} 
    void put(unsigned char d);
    RXListener* setRX(RXListener* li);
    ascii::Writer& info(ascii::Writer& out) const;
    Terminal& enableEcho(){echo=true;return *this;}
    Terminal& disableEcho(){echo=false;return *this;}
  };
 }
} //namespace io
#endif
