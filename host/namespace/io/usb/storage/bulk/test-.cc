//-----------------------------
//test
//(c) H.Buchmann FHSO 2005
//$Id: test-.cc 206 2006-01-14 13:37:32Z buchmann $
//-----------------------------
#include "sys/sys.h"
IMPLEMENTATION(io_usb_test,$Revision$)
#include "sys/msg.h"
#include "io/usb/usb-0.h"
#include "io/usb/storage/bulk/bulk.h"
#include "io/fs/atapi/atapi.h"
#include "sys/deb.h"

namespace io
{
 namespace usb
 {
  namespace storage
  {
   namespace bulk
   {
    class Tester:public USB::Listener
    {
     static Tester tester;
     Device* dev;
     static void wait(const char msg[]);
     bool onDevice(Device& dev);
     Tester();
     ~Tester();
    };

    Tester Tester::tester;

    void Tester::wait(const char msg[])
    {
     sys::msg<<msg;
     sys::deb::get();
     sys::msg<<"\n";
    }


    Tester::Tester()
    :dev(0)
    {
     static const char Sig[]={0x43,0x42,0x53,0x55,0}; 

     sys::msg<<"USB-Storage-Test\n";
     sys::msg<<Sig<<" size "<<sizeof(CBW)<<"\n";
     USB& usb=USB::get();
     usb.enumerate(*this);
     if (dev==0) 
	{
	 sys::msg<<"no device available\n";
	 return;
	}
#if 0
     
     dev->claimInterface(0);
     dev->control(0x21,0xff,0,0,0,0); //reset
     unsigned char maxLun;
     dev->control(0xa1,0xfe,0,0,&maxLun,1);
     sys::msg<<"maxLun= "<<maxLun<<"\n";
     unsigned char inquiry[]=
	      {      // atapi 
	       0x12, // inquiry
	       0,    // lun
	       0,    // byte# 2       
	       0,    // byte# 3       
	       0x25, //allocation length
	       0,
	       0,
	       0,
	       0,
	       0,
	       0,
	       0
	      };
     unsigned char readCapacity[]=
              {
	       0x25,
	       0
	      };
     unsigned char read[]=
	      {
	       0x28,
	       0,
	       0,0,0,0, //logical address
	       0,
	       0,1, //tranfer lenght 512
	      };
     	      
     CBW cbw;
     cbw.set(3,CBW::IN,read,sizeof(read));
     cbw.show(sys::msg)<<"\n";    
     dev->bulkWrite(1,(unsigned char*)&cbw,sizeof(cbw));
     char reply[1024];
     wait("bulkRead 1");
     unsigned len=dev->bulkRead(2,(unsigned char*)reply,512);
     sys::msg.dump(reply,len);

  //second time
     cbw.tag++;
     wait("bulkRead 2");
     CSW csw;
     len=dev->bulkRead(2,(unsigned char*)&csw,sizeof(csw));
     csw.show(sys::msg)<<"\n";
#endif
    }

    Tester::~Tester()
    {

     if (dev)
	{
	 sys::msg<<"releasing device\n";
#if 0
	 dev->releaseInterface(0);
	 dev->reset();
	 dev->close();
#endif
	}
    }
    bool Tester::onDevice(Device& dev)
    {
     const Descriptor::POD& d=dev.getDescriptor().pod;
     if ((d.idVendor==0x54c)&&(d.idProduct==0x8b))
	{
         dev.show(sys::msg)<<"\n";
	 this->dev=&dev;
	}
     return this->dev!=0;
    }
   }//namespace bulk
  }//namespace storage
 }//namespace usb
}//namespace io
