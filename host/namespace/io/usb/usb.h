#ifndef IO_USB_USB_H
#define IO_USB_USB_H
//-----------------------------
//USB will be general USB 
//(c) H.Buchmann FHSO 2004
//$Id: usb.h 190 2006-01-10 20:50:41Z buchmann $
//-----------------------------
INTERFACE(io_usb_usb,$Revision: 1.3 $)
#include "io/ascii/write.h"
#include "io/usb/usb-0.h"
#include <usb.h>
namespace io
{
 namespace usb
 {
 //------------------------------------- Link
 //superclass of Bus and Device
  struct Link
  {
   Link* next;
   Link* prev;
   Link();
   void preInsert(Link* lnk);
  };

 //------------------------------------- Device
  class Device:Link
  {
   public:
    struct Listener
    {
     virtual bool onDevice(Device&)=0;
            //returns true iff stop to scan
     virtual ~Listener();
    };

   private:
    friend class Bus;
    friend class USB;
    struct usb_device* dev;
    struct usb_dev_handle* handle;
    unsigned timeout_ms;
    int ifc;
    
    Device(struct usb_device* dev);

   public:
    ~Device();
    io::ascii::Writer& show(io::ascii::Writer&) const;
    unsigned short getVendor() const;
    unsigned short getProduct() const;
    int open(unsigned timeout_ms);
    int open(){return open(0);}
    int close();
    int clearHalt(int ep);
    int claimInterface(int ifc);
    int detachKernelDriver(int ifc);
    int altInterface(int ifc);
    int releaseInterface(int ifc);
    int detach(int ifc);
    int reset();
    int control(int reqType,int req,int val,int idx,
                   unsigned char d[],unsigned len);
    void bulkWrite(int ep,
                   const unsigned char data[],unsigned len);
    void interruptWrite(int ep,
                   const unsigned char data[],unsigned len);
    unsigned bulkRead(int ep,
                   unsigned char data[],unsigned len);
    unsigned interruptRead(int ep,
                   unsigned char data[],unsigned len);
    unsigned getDescriptor(unsigned char type,unsigned char index,
                   unsigned char buffer[],unsigned len) const;



    template<typename T>
     unsigned get(unsigned idx,T& t) const
     {return getDescriptor(T::Type,idx,(unsigned char*)&t,sizeof(T));}
    
    template<typename T>
    T get(unsigned idx) const
    {
     T t;
     get(idx,t);
     return t;
    }
  };

//------------------------------------- Bus
  class Bus:Link
  {
   private:
    friend class Device;
    friend class USB;
    usb_bus* bus;
    Device* anchor;
    Bus(usb_bus* bus);
   public:
    ~Bus();
    io::ascii::Writer& show(io::ascii::Writer&) const;
  };
    
//------------------------------------- USB
  class USB
  {
   private:
    friend class Device;
    static USB usb;
    Bus* anchor;

    void enumerate();
    static int error(int cod,const char msg[]);
    USB();

   public:
    ~USB();
    static USB& get(){return usb;}
    io::ascii::Writer& show(io::ascii::Writer& out) const;
    void scan(Device::Listener& filter);
  };
 }//namespace usb
}//namespace io
#endif
