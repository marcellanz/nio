#ifndef IO_USB_USB_1_H
#define IO_USB_USB_1_H
//-----------------------------
//USB will be general USB 
//(c) H.Buchmann FHSO 2004
//$Id: usb-1.cc 210 2006-01-27 15:58:14Z buchmann $
//-----------------------------
INTERFACE(io_usb_usb_1,$Revision$)
#include "io/usb/usb-0.h"
#include "sys/thread.h"
#include "io/byte/pac.h"
#include <signal.h>

#include <list>
#include <string>
#include <dirent.h>
extern "C"
{
#include <linux/usbdevice_fs.h>
}

namespace io
{
 namespace usb
 {
//---------------------------------- USB_1
  class USB_1:public USB
             ,public sys::Runnable
  {
   static USB_1 usb;
   static const char* Dirs[];
   
   static void signalHandler(int sigN,siginfo_t* info,void*); 
   static int SIGNAL;//=SIGRTMIN;//35;
   
   class Bus;
   class Device;
   
//---------------------------------- USB_1::Transfer
//will be *deleted* in signalHandler
   struct Transfer
   {
    int id;
    usbdevfs_urb urb;
             Transfer(int id);
    virtual ~Transfer();
    virtual void onTransfer()=0;
    void transferIt();
   };

//---------------------------------- USB_1::Control
   struct Control:public usb::Control
   {
    struct Transfer:public USB_1::Transfer
    {
      Control::POD&           ctl;
      usb::Control::Listener& li;
      Transfer(int id,
               Control::POD& ctl,
               usb::Control::Listener& li);
     ~Transfer();
      void onTransfer();
    };
    
     Device& dev;
     Control(Device& dev);
    ~Control();
    void transfer(Control::POD& ctl,usb::Control::Listener& li);
   };
   
//---------------------------------- USB_1::BulkTransfer
   struct BulkTransfer:public USB_1::Transfer
   {
    byte::Packet&           pac;
    byte::Packet::Listener& li;
    BulkTransfer(int id,
                 byte::Packet& pac,
		 byte::Packet::Listener& li);
    ~BulkTransfer();
   };
   
//---------------------------------- USB_1::BulkInput
   struct BulkInput:public Bulk::Input
   {
     Device& dev;
        
     struct Transfer:public BulkTransfer
     {
      Transfer(int id,
               byte::Packet& pac,
	       byte::Packet::Listener& li);
      ~Transfer();	      
       void onTransfer();
     };
    
     BulkInput(Device& dev,EndP& endP);
    ~BulkInput();
     void transfer(byte::Packet& pac,byte::Packet::Listener& li);
   };

//---------------------------------- USB_1::BulkOutput
   struct BulkOutput:public Bulk::Output
   {
     Device& dev;
     struct Transfer:public BulkTransfer
     {
      Transfer(int id,
               byte::Packet& pac,
	       byte::Packet::Listener& li);
      ~Transfer();	      
       void onTransfer();
     };

     BulkOutput(Device& dev,EndP& endP);
    ~BulkOutput();
     void onTransfer();
     void transfer(byte::Packet& pac,byte::Packet::Listener& li);
   };
     
//---------------------------------- USB_1::Device
   struct Device:public usb::Device
   {
    struct Bulk:public usb::Device::Bulk
    {
     Device& device;
     Bulk(Device* device);
     usb::Bulk::Input*  input(EndP&);
     usb::Bulk::Output* output(EndP&);
    };
    
    USB_1& usb;
    int id;   //fileId
    bool ok;  //must be true for an operable device
    const Bus& bus;
    Bulk bulk_;
    std::string name;
    std::string fullName;
    
    void onControl(const usb::Control::POD&,unsigned char d[]);
        
             Device(USB_1& usb,const Bus& bus,const char name[]);
    virtual ~Device();
    Configuration* getConfiguration(int id);
    Ifc* getIfc(unsigned char*& buffer);
    EndP* getEndP(unsigned char*& buffer);
    void open();
    void close();
    int bulkTransfer(usbdevfs_urb* urb,unsigned char b[],int len);
    usb::Control* control();
    void claimIfc(int ifc);
    usb::Device::Bulk& bulk(){return bulk_;}
   };

//---------------------------------- USB_1::Bus
   struct Bus
   {
    USB_1& usb;
    const std::string& root;
    const std::string  name;
    typedef std::list<Device*> Device_;
    Device_ device;
     Bus(USB_1& usb,const std::string& root,const char name[]);
    ~Bus();
   };
 
   sys::Thread    thread;
   sys::Semaphore signal;   //initially closed
   std::string 	  root;
   
   typedef std::list<Bus*> Bus_;
   
   Bus_ bus;   
   void scanBus(DIR* dir);
   void enumerate(USB::Listener& li);
   void run();
    USB_1();
   ~USB_1();
  };
 }//namespace usb
}//namespace io
#endif
