//-----------------------------
//USB will be general USB 
//(c) H.Buchmann FHSO 2004
//$Id: usb.cc 190 2006-01-10 20:50:41Z buchmann $
//-----------------------------
#include "sys/sys.h"
IMPLEMENTATION(io_usb_usb,$Revision: 1.2 $)
#include "io/usb/usb.h"
#include "sys/msg.h"

namespace io
{
 namespace usb
 {

  USB USB::usb;
  Link::Link()
  :next(0)
  ,prev(0)
  {
  }

  void Link::preInsert(Link* lnk)
  {
   next=lnk;
   if (lnk) 
      {
       prev=lnk->prev;
       lnk->prev=this;
      } 
  }


  Device::Listener::~Listener()
  {
  }
  
  Device::Device(struct usb_device* dev)
  :dev(dev)
  ,handle(0)
  ,timeout_ms(0)
  {
  }

  Device::~Device()
  {
//   show(sys::msg<<"~Device ")<<"\n";
   close();
  }
  
  unsigned short Device::getVendor() const
  {
   return dev->descriptor.idVendor;
  }
  
  unsigned short Device::getProduct() const
  {
   return dev->descriptor.idProduct;
  }

  int Device::open(unsigned timeout_ms)
  {
   this->timeout_ms=timeout_ms;
   if (handle) return 0; //already open
   handle=usb_open(dev);
   return handle==0;
  }

  int Device::close()
  {
   if (handle) usb_close(handle);
   handle=0;
   return 0;
  }

  int Device::clearHalt(int ep)
  {
   if (handle==0) return -1;
   return USB::error(usb_clear_halt(handle,ep),"clearHalt");
  }

  int Device::detachKernelDriver(int ifc)
  {
   if (handle==0) return -1;
   int cod=usb_detach_kernel_driver_np(handle,ifc);
   return cod;
#if 0
   if (cod>0) return cod;
   if (cod==-61) return -cod; //already detached
   return USB::error(cod,"detachKernelDriverNp");
#endif   
  }

  int Device::claimInterface(int ifc)
  {
   if (handle==0) return -1;
   return USB::error(usb_claim_interface(handle,ifc),"claimInterface");
  }

  int Device::releaseInterface(int ifc)
  {
   if (handle==0) return -1;
   return USB::error(usb_release_interface(handle,ifc),"releaseInterface");
  }

  int Device::detach(int ifc)
  {
   if (handle==0) return -1;
   return USB::error(usb_detach_kernel_driver_np(handle,ifc),"detach");
  }

  int Device::reset()
  {
   if (handle==0) return -1;
   return USB::error(usb_reset(handle),"reset");
  }

  int Device::control(int reqType,int req,int val,int idx,
              unsigned char d[],unsigned len)
  {
   if (handle==0) return -1;
   return USB::error(
       usb_control_msg(
       handle,
       reqType,
       req,
       val,
       idx,
       (char*)d,
       len,
       0
                      ),
     "control"
               );
  }

  void Device::bulkWrite(int ep,const unsigned char data[],unsigned len)
  {
   USB::error(usb_bulk_write(handle,ep,(char*)data,len,timeout_ms),"write");
  }
  
  void Device::interruptWrite(int ep,const unsigned char data[],unsigned len)
  {
   USB::error(usb_interrupt_write(handle,ep,(char*)data,len,timeout_ms),
              "interruptWrite");
  }
  
  
  unsigned Device::bulkRead(int ep,unsigned char data[],unsigned len)
  {
   return USB::error(
          usb_bulk_read(handle,ep,(char*)data,len,timeout_ms),"bulkRead"
	            );
  }


  unsigned Device::interruptRead(int ep,unsigned char data[],unsigned len)
  {
    return
    USB::error(usb_interrupt_read(handle,ep,(char*)data,len,timeout_ms),
                      "interruptRead");
  }
  

  unsigned Device::getDescriptor(unsigned char type,unsigned char index,
                                 unsigned char buffer[],unsigned len)
				 const
  {
   return USB::error(usb_get_descriptor(handle,type,index,buffer,len),
                     "getDescriptor");
  }				 

  Bus::Bus(usb_bus* bus)
  :bus(bus),
   anchor(0)
  {
   struct usb_device* d=bus->devices;
   while(d)
   {
    Device* dev=new Device(d);
    dev->preInsert(anchor);
    anchor=dev;
    d=d->next;
   }
  }
  
  Bus::~Bus()
  {
   Device* d=anchor;
   while(d)
   {
    Device* dn=(Device*)(d->next);
    delete d;
    d=dn;
   }
  }
  
  io::ascii::Writer& Bus::show(io::ascii::Writer& out) const
  {
   out<<bus->dirname;
   return out;
  }

  io::ascii::Writer& Device::show(io::ascii::Writer& out) const
  {
   out<<dev->filename<<" ";
   out.hex(dev->descriptor.idVendor).put(":").hex(dev->descriptor.idProduct);
   return out;
  }

  io::ascii::Writer& USB::show(io::ascii::Writer& out) const
  {
   Bus* b=anchor;
   while(b)
   {
    b->show(out)<<"\n";
    Device* d=b->anchor;
    while(d)
    {
     d->show(out<<"  ")<<"\n";
     d=(Device*)(d->next);
    }
    b=(Bus*)(b->next);
   }
   return out;
  }

  USB::USB()
  :anchor(0)
  {
   usb_init();
   int find_busses=usb_find_busses();
//   sys::msg<<"----------- "<<find_busses<<" busses\n";
   int find_devices=usb_find_devices();
//   sys::msg<<"----------- "<<find_devices<<" devices\n";
   enumerate();
  }

  USB::~USB()
  {
   Bus* b=anchor;
   while(b)
   {
    Bus* bn=(Bus*)(b->next);
    delete b;
    b=bn;
   }
  }

  void USB::enumerate()
  {
   usb_bus* b=usb_get_busses();
   while(b)
   {
    Bus* bus=new Bus(b);
    bus->preInsert(anchor);
    anchor=bus;
    b=b->next;
   }
  }

  void USB::scan(Device::Listener& li)
  {
   Bus* b=anchor;
   while(b)
   {
    Device* d=b->anchor;
    while(d)
    {
     if (li.onDevice(*d)) return; //leave scanner
     d=(Device*)(d->next);
    }
    b=(Bus*)(b->next);
   }
  }
  
  int USB::error(int cod,const char msg[])
  {
   if (cod>=0) return cod;
   sys::msg.error()<<msg<<" : "<<cod<<" "<<usb_strerror()<<"\n";
   return cod;
  }
 }//namespace usb
}//namespace io
