//-----------------------------
//USB will be general USB 
//(c) H.Buchmann FHSO 2004
//$Id: usb-1.cc 219 2006-02-06 16:26:14Z buchmann $
//-----------------------------
#include "sys/sys.h"
IMPLEMENTATION(io_usb_usb_1,$Revision: 1.2 $)
#include "io/usb/usb-1.h"
//#define  IO_USB_1_TEST

#define IO_USB_ASYNC
#include "sys/msg.h"
#include "util/str.h"
#include "sys/host.h"


#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/select.h>
#include <fcntl.h>

namespace io
{
 namespace usb
 {
  
  int USB_1::SIGNAL=SIGRTMIN;
  
  USB_1 USB_1::usb; 

  const char* USB_1::Dirs[]={"/proc/bus/usb",
                             "/sys/bus/usb/devices",
			     0 //terminates list
			     };

  USB_1::Bus::Bus(USB_1& usb,const std::string& root,const char name[])
  :usb(usb)
  ,root(root)
  ,name(name)
  {
   DIR* dir=::opendir((root+"/"+name).c_str());
   if (dir==0) return;
   while(true)
   {
    dirent* e=::readdir(dir);
    if (e==0) break;
    unsigned di=0;
    if (util::Str::to(e->d_name,di))
       {
        Device* dev=new Device(usb,*this,e->d_name);
	if (dev->ok) 
	   {
//	    dev->show(sys::msg);
	    device.push_back(dev);
	   }
	   else      delete dev; 
       }
   }
   ::closedir(dir);
  }

  USB_1::Bus::~Bus()
  {
   for(Device_::iterator d=device.begin();d!=device.end();++d)
   {
    delete *d;
   }
  }

  USB_1::Transfer::Transfer(int id)
  :id(id)
  {
   urb.flags             = 0;
   urb.usercontext       = this;
   urb.signr             = SIGNAL;
   urb.start_frame       = 0;
  }

  USB_1::Transfer::~Transfer()
  {
  }

  void USB_1::Transfer::transferIt()
  {
   int ret=::ioctl(id,USBDEVFS_SUBMITURB,&urb);
   if (ret<0)::perror(" transferIt USBDEVFS_SUBMITURB");
  }
  
  USB_1::Control::Transfer::Transfer(int id,
                                     Control::POD& ctl,
                                     usb::Control::Listener& li)
  :USB_1::Transfer(id)
  ,ctl(ctl)
  ,li(li)
  {
   urb.type              = USBDEVFS_URB_TYPE_CONTROL;
   urb.endpoint          = 0;
   urb.actual_length     = 0;
   urb.number_of_packets = 0;
   urb.buffer            = &ctl;
   urb.buffer_length     = ctl.wLength+sizeof(Control::POD);
  }

  USB_1::Control::Transfer::~Transfer()
  {
  }


  void USB_1::Control::Transfer::onTransfer()
  {
   li.onControl(ctl);
  }
    				     
  USB_1::Control::Control(Device& dev)
  :dev(dev)
  {
  }
  
  USB_1::Control::~Control()
  {
  }
 
  void USB_1::Control::transfer(Control::POD& ctl,
                                usb::Control::Listener& li)
  {
   Transfer* t=new Transfer(dev.id,ctl,li);
   t->transferIt();
  }

  
  USB_1::Device::Bulk::Bulk(Device* device)
  :device(*device)
  {
  }
  
  usb::Bulk::Input*  USB_1::Device::Bulk::input(EndP& ep)
  {
   return (ep.is(IN)&&ep.is(EndP::BULK))?new BulkInput(device,ep):0;
  }
  
  usb::Bulk::Output* USB_1::Device::Bulk::output(EndP& ep)
  {
   return (ep.is(OUT)&&ep.is(EndP::BULK))?new BulkOutput(device,ep):0;
  }
  

  USB_1::BulkTransfer::BulkTransfer(int id,
                                    byte::Packet& pac,
				    byte::Packet::Listener& li)
  :USB_1::Transfer(id)
  ,pac(pac)
  ,li(li)
  {
   urb.type=USBDEVFS_URB_TYPE_BULK;
   urb.buffer            = pac.data;
   urb.buffer_length     = pac.capacity;
   urb.actual_length     = 0;
   urb.number_of_packets = 0;
  }
  
  USB_1::BulkTransfer::~BulkTransfer()
  {
  }
  
  
//-------------------------------- BulkInput
  USB_1::BulkInput::Transfer::Transfer(int id,
                                       byte::Packet& pac,
				       byte::Packet::Listener& li)
  :BulkTransfer(id,pac,li)
  {
  }				    

  USB_1::BulkInput::Transfer::~Transfer()
  {
  }
  
  void USB_1::BulkInput::Transfer::onTransfer()
  {
   pac.len=urb.actual_length;
   li.onPacket(pac);
  }	 
                   
  USB_1::BulkInput::BulkInput(Device& dev,EndP& endP)
  :Bulk::Input(endP)
  ,dev(dev)
  {
  }
  
  USB_1::BulkInput::~BulkInput()
  {
  }
  
  void USB_1::BulkInput::transfer(byte::Packet& pac,
                                  byte::Packet::Listener& li)
  {
   Transfer* t=new Transfer(dev.id,pac,li);
   t->urb.endpoint          = 0x80|eP; //output
   t->transferIt();
  }
  

//-------------------------------- BulkOutput
  USB_1::BulkOutput::Transfer::Transfer(int id,
                                        byte::Packet& pac,
                                        byte::Packet::Listener& li)
  :BulkTransfer(id,pac,li)
  {
  }
  
  USB_1::BulkOutput::Transfer::~Transfer()
  {
  }

  void USB_1::BulkOutput::Transfer::onTransfer()
  {
   li.onPacket(pac);
  }	 

  USB_1::BulkOutput::BulkOutput(Device& dev,EndP& endP)
  :Bulk::Output(endP)
  ,dev(dev)
  {
  }
  
  USB_1::BulkOutput::~BulkOutput()
  {
  }
  
  
  void USB_1::BulkOutput::transfer(byte::Packet& pac,
                                   byte::Packet::Listener& li)
  {
   Transfer* t=new Transfer(dev.id,pac,li);
   t->urb.endpoint = eP; //output
   t->transferIt();
  }
  
  USB_1::Device::Device(USB_1& usb,const Bus& bus,const char name[])
  :usb(usb)
  ,id(-1)
  ,ok(false)
  ,bus(bus)
  ,bulk_(this)
  ,name(name)
  ,fullName(bus.root+"/"+bus.name+"/"+name)
  {
   int id=::open(fullName.c_str(),O_RDONLY); //local id
   if (id<0)
      {
       ::perror((fullName+" open'").c_str());
       return;
      }
   if (::read(id,&descriptor.pod,sizeof(descriptor.pod))<0)
      {
       ::perror((fullName+" descriptor'").c_str());
       ::close(id);
       return;
      }
   unsigned cN=descriptor.pod.bNumConfigurations;
   descriptor.configuration=new Configuration*[cN];
   for(unsigned i=0;i<cN;i++)
   {
    Configuration* cfg=getConfiguration(id);
    if (cfg==0) return; 
    descriptor.configuration[i]=cfg;
   }
   ::close(id);
   ok=true;
  }

  USB_1::Device::~Device()
  {
   close();
  }
  
  Configuration* USB_1::Device::getConfiguration(int id)
  {
   Configuration::POD pod;
   if (::read(id,&pod,sizeof(pod))<0)
      {
       ::perror((fullName+" getConfiguration").c_str());
       return 0;
      }

   unsigned len=pod.wTotalLength;
   unsigned char* buffer=new unsigned char[len];
   if (::read(id,buffer+sizeof(pod),
                 len-sizeof(pod))<0)
      {
       ::perror((fullName+" getConfiguration:rest").c_str());
       return 0;
      }
  *(Configuration::POD*)buffer=pod; //copy

   Desc::Reader rd(buffer,len);
   return new Configuration(rd);  
  }


  void USB_1::Device::open()
  {
   if (id>0) return; //already open
   id=::open(fullName.c_str(),O_RDWR);
   if (id<0)
      {
       ::perror((fullName+" open").c_str());
      }
  }

  void USB_1::Device::close()
  {
   if (id>0) ::close(id);
  }

  
  usb::Control* USB_1::Device::control()
  {
   return new USB_1::Control(*this);
  } 

  void USB_1::Device::claimIfc(int ifc)
  {
   if (li==0) return; 
   open();
   this->ifc=0;
   if (::ioctl(id,USBDEVFS_CLAIMINTERFACE,&ifc)>=0)
      {
       this->ifc=descriptor.configuration[0]->ifc[ifc];
                                       // ^------------ not yet in final form   
      }
      else ::perror((fullName+" claimIfc").c_str()); 
 //  li->onClaim(this->ifc);
  }
  
  USB_1::USB_1()
  :thread(*this)
  {
//   thread.start();
   struct sigaction act;
   act.sa_handler=0;
   act.sa_sigaction=signalHandler;
   ::sigemptyset(&act.sa_mask);
   act.sa_flags=SA_SIGINFO;
   struct sigaction old;
   int res=::sigaction(SIGNAL,&act,&old);
   sys::msg<<"res= "<<res<<"\n";
   
      
   const char** d=Dirs;
   while(true)
   {
    if (*d==0) sys::msg.error()<<"no USB available\n";
    DIR* dir=::opendir(*d);
    if (dir)
       {
        root=*d;
        scanBus(dir);
        ::closedir(dir);
	break;
       }
    ++d;   
   }
   sys::msg<<"----------------------- start\n";
   install();
  }

  USB_1::~USB_1() 
  {
   for(Bus_::iterator b=bus.begin();b!=bus.end();++b)
   {
    delete *b;
   }
  }

  void USB_1::signalHandler(int sigN,siginfo_t* info,void*)
  {
//TODO delegate to thread   
   unsigned context;
#if 0
   sys::msg<<"---------+ "<<sigN<<"\n"
             "si_signo= "<<info->si_signo<<"\n"
	     "si_errno= "<<info->si_errno<<"\n"
	     "si_code = "<<info->si_code<<"\n"
	     "si_addr = "<<info->si_addr<<"\n";
//   usb.signal.open();
#endif
   usbdevfs_urb* urb=(usbdevfs_urb*)info->si_addr;
   USB_1::Transfer* t=(USB_1::Transfer*)(urb->usercontext);
   int ret=::ioctl(t->id, USBDEVFS_REAPURB,&context);
   if (ret<0)
     {
      ::perror("transfer USBDEVFS_REAPURB");
     }
//   sys::msg<<urb->status<<"\n";
   t->onTransfer();
   delete t;
  }
  
  void USB_1::scanBus(DIR* dir)
  {
   while(true)
   {
    dirent* e=::readdir(dir);
    if (e==0) break;
    unsigned bi=0;
    if (util::Str::to(e->d_name,bi))
       {
        bus.push_back(new Bus(*this,root,e->d_name));       
       }
   }
  } 

  void USB_1::enumerate(Listener& li)
  {
   for(Bus_::iterator b=bus.begin();b!=bus.end();++b)
   {
    Bus::Device_& dev=(*b)->device;
    for (Bus::Device_::iterator d=dev.begin();
         d!=dev.end();
	 ++d)
    {
     if (li.onDevice(**d)) return;
    }	 
   }
  }
  
  void USB_1::run()
  {
   sys::msg<<"start\n";
   while(true)
   {
    sys::msg<<"wait\n";
    signal.wait();
   }
  }
 }//namespace usb
}//namespace io
