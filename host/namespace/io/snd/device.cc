//---------------------------------
//sound
//TODO stereo 
//(c) H.Buchmann FHSO 2005
//$Id: device.cc 141 2005-09-18 10:54:54Z buchmann $
//---------------------------------
#include "sys/sys.h"
IMPLEMENTATION(io_snd_device,$Revision$)
#include "io/snd/device.h"
#include "sys/msg.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <linux/soundcard.h>
#include <sys/ioctl.h>

namespace io
{
 namespace snd
 {
  Device::Device(const char devName[],Mode mode,unsigned fs)
  :devName(devName),
   id(-1)
  {
   static const unsigned Mode2Flag[]={O_RDONLY,O_WRONLY};
   id=::open(devName,Mode2Flag[mode]);
   error(id);
   int format=AFMT_S16_LE;
   error(::ioctl(id,SNDCTL_DSP_SETFMT,&format));

   int sampleSize=16;
   error(::ioctl(id,SNDCTL_DSP_SAMPLESIZE,&sampleSize));

   int channels=1; //Mono
   error(::ioctl(id,SNDCTL_DSP_CHANNELS,&channels));

   error(::ioctl(id,SNDCTL_DSP_SPEED,&fs));

   error(::ioctl(id,SNDCTL_DSP_SYNC,0));
  }
  
  Device::~Device()
  {
   if (id>0) ::close(id);
  }
  
  void Device::error(int cod)
  {
   if (cod<0)
      {
       sys::msg<<::strerror(errno)<<"\n";
      }
  }


  Writer::Writer(const char devName[],unsigned fs)
  :Device(devName,WRITE,fs)
  {
  }

  Writer::~Writer()
  {
  }
  
  void Writer::put(short int sample)
  {
   ::write(id,&sample,sizeof(sample));
  }

  Reader::Reader(const char devName[],unsigned fs)
  :Device(devName,READ,fs)
  {
  }
  
  Reader::~Reader()
  {
  }
  
  short int Reader::get()
  {
   short int v;
   ::read(id,&v,sizeof(v));
   return v;
  }
 }//namespace snd
}//namespace io
