#ifndef IO_SND_DEVICE_H
#define IO_SND_DEVICE_H
//---------------------------------
//sound
//TODO stereo 
//(c) H.Buchmann FHSO 2005
//$Id: device.h 141 2005-09-18 10:54:54Z buchmann $
//---------------------------------
INTERFACE(io_snd_device,$Revision$)
namespace io
{
 namespace snd
 {
  class Device
  {
   protected:
    enum Mode {READ,WRITE};
    const char* devName;
    int id;
             Device(const char devName[],Mode mode,unsigned fs);
    virtual ~Device();
    void error(int cod); //exits if cod<0 see POSIX
  };
  
//-------------------------------------------- Writer
  class Writer:public Device
  {
   public:
             Writer(const char devName[],unsigned fs); 
    virtual ~Writer();
    void put(short int sample);
  };
  
//-------------------------------------------- Reader
  class Reader:public Device
  {
   private:
   public:
             Reader(const char devName[],unsigned fs);
    virtual ~Reader();
    short int get();
  };
 }//namespace snd
}//namespace io
#endif

