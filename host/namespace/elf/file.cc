//-------------------------
//file
//(c) H.Buchmann FHSO 2005
//$Id: file.cc 193 2006-01-11 15:21:16Z buchmann $
//-------------------------
#include "sys/sys.h"
IMPLEMENTATION(elf_file,$Revision: 1.2 $)
#include "elf/file.h"
#include "sys/msg.h"
#include "sys/host.h"

namespace elf
{
//---------------------------------------------- File::Section::Listener
 File::Section::Listener::~Listener()
 {
 }
 
//---------------------------------------------- File::Section
 File::Section::Section(File& file)
 :file(file)
 {
 }

 void File::Section::enumerate(Listener& li)
 {
  for(int i=0;i<sectionHeader.size();i++)
  {
   SectionHeader& sh=*sectionHeader[i];
//   sys::msg<<"--- "<<io::ascii::setw(4)<<i<<": "<<sh.sh_name.ref<<"\n";
   switch(sh.sh_type)
   {
    case elf::SHT_NULL:break;
    case elf::SHT_PROGBITS:li.onProgBits(i,sh);break;
    case elf::SHT_SYMTAB:  li.onSymTab(i,sh);break;
    case elf::SHT_STRTAB:  li.onStrTab(i,sh);break;
    case elf::SHT_RELA:    li.onRelA(i,sh);break;
    case elf::SHT_HASH:    li.onHash(i,sh);break;
    case elf::SHT_DYNAMIC: li.onDynamic(i,sh);break;
    case elf::SHT_NOTE:    li.onNote(i,sh);break;
    case elf::SHT_NOBITS:  li.onNoBits(i,sh);break;
    case elf::SHT_REL:     li.onRel(i,sh);break;
    case elf::SHT_SHLIB:   li.onShLib(i,sh);break;
    case elf::SHT_DYNSYM:  li.onDynSym(i,sh);break;
    default: li.onOther(i,sh);break;
#if 0
    case elf::SHT_LOPROC:
    case elf::SHT_HIPROC:
    case elf::SHT_LOUSER:
    case elf::SHT_HIUSER:
#endif
   }
  }
 }

 SectionHeader& File::Section::operator[](unsigned idx)
 {
  return *sectionHeader[idx];
 }

 void File::Section::add(SectionHeader* sh)
 {
  sectionHeader.push_back(sh);
 }

 unsigned File::Section::getSize()const
 {
  unsigned siz=0;
  for(unsigned i=0;i<sectionHeader.size();i++)
  {
   siz+=sectionHeader[i]->sh_size;
  }
  return siz;
 }
 
 void File::Section::write(io::byte::file::Output& dst)
 {
//------------------------------ layout
//sections the bytes
//sections the headers

//the bytes
  unsigned start=dst.getPos();
  
  for(unsigned i=0;i<sectionHeader.size();i++)
  {
   SectionHeader* sh=sectionHeader[i];
   dst.put(sh->sh_offset.ref,sh->sh_size);
  } 
//the header
  for(unsigned i=0;i<sectionHeader.size();i++)
  {
   SectionHeader* sh=sectionHeader[i];
   sh->sh_name.toIdx(file.stringTab);
   sh->sh_offset.idx=start;
   start+=sh->sh_size;
   dst.put(sh);
  }
//write contents 
 }
 
//---------------------------------------------- File::Prog::Listener
 File::Prog::Listener::~Listener()
 {
 }

//---------------------------------------------- File::Prog
 File::Prog::Prog(File& file)
 :file(file)
 {
 }

 void File::Prog::enumerate(Listener& li)
 {
  for(unsigned i=0;i<progHeader.size();i++)
  {
   ProgHeader& ph=*progHeader[i];
   switch(ph.p_type)
   {
    case PT_NULL:break;
    case PT_LOAD:   li.onLoad(i,ph);   break;
    case PT_DYNAMIC:li.onDynamic(i,ph);break;
    case PT_INTERP: li.onInterp(i,ph); break;
    case PT_NOTE:   li.onNote(i,ph);   break;
    case PT_SHLIB:  li.onShLib(i,ph);  break;
    case PT_PHDR:   li.onPHdr(i,ph);   break;
    case PT_TLS:    li.onTLS(i,ph);    break;
    case PT_LOOS ...   
         PT_HIOS:   li.onOS(i,ph);     break;
    case PT_LOPROC ... 
         PT_HIPROC: li.onProc(i,ph);   break;
    break;
   } //switch(p_type)
  }
 }

 ProgHeader& File::Prog::operator[](unsigned idx)
 {
  return *progHeader[idx];
 }

 unsigned File::Prog::getByteSize()const
 {
  return progHeader.size()*sizeof(ProgHeader);
 }

 void File::Prog::write(io::byte::file::Output& dst)
 {
//not yet in final form
  for(unsigned i=0;i<progHeader.size();i++)
  {
   dst.put(progHeader[i]);
  }
 }
//---------------------------------------------- File
 File::File()
 :fName(0),
  header(0),
  changeEndian(false),
  stringTabIndex(-1),
  stringTab(0),
  section(*this),
  prog(*this)
 {
 }

 File::~File()
 {
 }

void File::write(const char fName[])
{
 this->fName=fName;
//------------------------------ layout
// header
// section 	   the bytes  section.getSize()
// sectionHeader              section.getN()*sizeof(SectionHeader)
// progheader      if available

 io::byte::file::Output dst(fName);

 unsigned idx=sizeof(Header); //in the file
 idx+=section.getSize();
 header->e_shoff.idx =idx;

 idx+=section.getN()*sizeof(SectionHeader);
  
 header->e_phoff.idx =(prog.getN())?idx:0;

 header->e_ehsize    =sizeof(Header);
 header->e_pentsize  =sizeof(ProgHeader);
 header->e_phnum     =prog.getN();
 header->e_shentsize =sizeof(SectionHeader);
 header->e_shnum     =section.getN();
 if (stringTabIndex<0)
    {
     sys::msg.error()<<"Output 'stringTabIndex' not defined\n";
    }
 header->e_shstrndx  =stringTabIndex;

 dst.put(header);
 section.write(dst);
 prog.write(dst);
}

//---------------------------------------------- Input
 void Input::setupSection()
 {
  if (changeEndian)
  {
   SectionHeader* sh=header->e_shoff.ref;
   for(unsigned i=0;i<header->e_shnum;i++) sh++->changeEndian();
  }
  SectionHeader* sh0=header->e_shoff.ref;
  stringTabIndex=header->e_shstrndx;
  stringTab=(char*)(data+sh0[stringTabIndex].sh_offset.idx);

  {
   SectionHeader* sh=sh0;
   for(unsigned i=0;i<header->e_shnum;i++)
   {
    sh->sh_name.toRef(stringTab);
    sh->sh_offset.toRef(data);
    section.add(sh);
    sh++;
   }
  }
 }

 void Input::setupProg()
 {
  ProgHeader* ph=header->e_phoff.ref;
  for(unsigned i=0;i<header->e_phnum;i++)
  {
   if (changeEndian) ph->changeEndian();
   ph->p_offset.toRef(data);
   prog.add(ph);
   ph++;
  }
 }

 void Input::setup()
 {
  header=(Header*)data;
  if (!header->isElf())
     {
      sys::msg.error()<<"'"<<fName<<"' is not an ELF file\n";
     }
  changeEndian=header->getTargetEndian()!=header->getHostEndian();
  if (changeEndian) header->changeEndian();
  header->e_phoff.toRef(data);
  header->e_shoff.toRef(data);
  
  setupSection();
  setupProg();  
 }

 Input::Input()
 :size(0),
  data(0)
 {
 }
 
 Input::~Input()
 { 
  if (data) delete [] data;
 }

 void Input::open(const char fName[])
 {
  this->fName=fName;
  io::byte::file::Input src(fName);
  size=src.getSize();
  data=new unsigned char[size];
  src.get(data,size);
  setup();
 }
}
