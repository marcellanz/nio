#ifndef ELF_FILE_H
#define ELF_FILE_H
//-------------------------
//file
//(c) H.Buchmann FHSO 2005
//$Id: file.h 193 2006-01-11 15:21:16Z buchmann $
//-------------------------
INTERFACE(elf_file,$Revision: 1.1 $)
#include "elf/show.h"
#include "io/byte/file/file.h"
#include "io/ascii/out.h"

#include <vector>

namespace elf
{
//------------------------------------------------- File
 class File
 {
  public:
//------------------------------------------------- File::Section
   class Section
   {
    private:
     friend class File;
     File& file;
     std::vector<SectionHeader*> sectionHeader;
     Section(File& file);
     void indexIt(unsigned start);
     
    public:
//------------------------------------------------- File::Section::Listener
     struct Listener
     {
      virtual ~Listener();
      virtual void onProgBits(unsigned idx,SectionHeader& sh)=0;
      virtual void onSymTab  (unsigned idx,SectionHeader& sh)=0;
      virtual void onStrTab  (unsigned idx,SectionHeader& sh)=0;
      virtual void onRelA    (unsigned idx,SectionHeader& sh)=0;
      virtual void onHash    (unsigned idx,SectionHeader& sh)=0;
      virtual void onDynamic (unsigned idx,SectionHeader& sh)=0;
      virtual void onNote    (unsigned idx,SectionHeader& sh)=0;
      virtual void onNoBits  (unsigned idx,SectionHeader& sh)=0;
      virtual void onRel     (unsigned idx,SectionHeader& sh)=0;
      virtual void onShLib   (unsigned idx,SectionHeader& sh)=0;
      virtual void onDynSym  (unsigned idx,SectionHeader& sh)=0;
      virtual void onOther   (unsigned idx,SectionHeader& sh)=0;
     };
     unsigned getN() const{return sectionHeader.size();}
     unsigned getSize()const; //in bytes
     void enumerate(Listener& li);
     SectionHeader& operator[](unsigned idx);
     void add(SectionHeader* sh);
     void write(io::byte::file::Output&);
   };

//------------------------------------------------- File::Prog
   class Prog
   {
    private:
     friend class File;
     File& file;
     std::vector<ProgHeader*> progHeader;

     Prog(File& file);

    public:
//------------------------------------------------- File::Prog::Listener
     struct Listener
     {
      virtual ~Listener();
      virtual void onLoad   (unsigned idx,ProgHeader&)=0;
      virtual void onDynamic(unsigned idx,ProgHeader&)=0;
      virtual void onInterp (unsigned idx,ProgHeader&)=0;
      virtual void onNote   (unsigned idx,ProgHeader&)=0;
      virtual void onShLib  (unsigned idx,ProgHeader&)=0;
      virtual void onPHdr   (unsigned idx,ProgHeader&)=0;
      virtual void onTLS    (unsigned idx,ProgHeader&)=0;
      virtual void onOS     (unsigned idx,ProgHeader&)=0;
      virtual void onProc   (unsigned idx,ProgHeader&)=0;
     };
     unsigned getN()const{return progHeader.size();}
     unsigned getByteSize()const;
     void enumerate(Listener& li);
     ProgHeader& operator[](unsigned idx);
     void add(ProgHeader* ph){progHeader.push_back(ph);}
     void write(io::byte::file::Output&);
   };
   
  protected:
   const char* fName;
   Header*     header;
   bool        changeEndian;
   int         stringTabIndex;
   const char* stringTab;
    
  public:
   Section section;
   Prog    prog;
            File();
   virtual ~File();
   bool sameEndian()const {return !changeEndian;}
   const char* getName()const{return fName;}
   void showIt(io::ascii::Writer& out);
   const Header& getHeader()const{return *header;}
   void write(const char fName[]);
 };

//------------------------------------------------- Input
 class Input:public File
 {
  private:
   unsigned       size;
   unsigned char* data;
   void setupSection();
   void setupProg();
   void setup();
   
  public:
            Input();
   virtual ~Input();
   void open(const char fName[]);
 };
}
#endif
