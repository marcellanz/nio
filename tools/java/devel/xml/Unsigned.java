//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//-----------------------------
//Unsigned
//attribute handling
//(c) H.Buchmann FHSO 2003
//-----------------------------
package devel.xml;

public class Unsigned extends Long
{
 private long max=(1l <<32)-1;
 public Unsigned()
 {
 }

 public Unsigned(Long.Scale scale)
 {
  super(scale);
 }
   
 public Unsigned(long max)
 {
  this.max=max;
 }

 public Unsigned(Long.Scale scale,long max)
 {
  super(scale);
  this.max=max;
 }
 
 public void fromString(String s) throws AttributeException
 {
  parse(s);
//  System.err.println("val "+val);
  if (max<val) 
     throw new AttributeException("not in range [0" +max+"]");

 }
 
}
