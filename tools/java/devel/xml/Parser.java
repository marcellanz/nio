//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//-------------------
//Depender
//(c) H.Buchmann FHSO 2002
//$Id: Parser.java 193 2006-01-11 15:21:16Z buchmann $
//-------------------
package devel.xml;
import java.io.*;
import org.xml.sax.*;
import java.util.*;

public class Parser implements ContentHandler
{
 String src;
 private Locator loc=null;
 private Map tagMap=new HashMap(); 
 private Element current=null;
 private boolean debug=false;
 
 public Parser(InputStream src) throws IOException,Exception
 {
  try
  {
   XML.parse(src,this);
  }catch(SAXException ex)
   {
    System.err.println("-------------------------------");
    if (loc!=null) error(ex.getMessage());
       else 
       {
        System.err.println(ex.getMessage());
	System.exit(1);
       }
   }
 }

 public Parser()
 {
 }
 
 public void setDebug()
 {
  debug=true;
 }
 
 public void clearDebug()
 {
  debug=false;
 }

 private void printDebug(String s)
 {
  System.err.println(s+" line: "+loc.getLineNumber());
 }
  
 public void parseIt(String fName,EntityResolver resolver) throws Exception
 {
  src=fName;
  try
  {
   XML.parse(new BufferedInputStream(new FileInputStream(src)),
             this,resolver,true);
  }
  catch(SAXException ex)
  {
   ex.printStackTrace();
   error(loc.getLineNumber(),ex.getMessage());
  }
  catch(FileNotFoundException ex)
  {
   System.err.println("file '"+src+"' not found");
   System.exit(1);
  }
  catch(java.lang.Exception ex)
  {
   System.err.println("unexcpected exception");
   ex.printStackTrace();
   System.exit(1);
  }
 }

 public void parseIt(String fName) throws Exception
 {
  parseIt(fName,null);
 }
 
 public void error(java.lang.Exception ex)
 {
  error(ex.getMessage());
 }
 
 public void error(String msg)
 {
  error(loc.getLineNumber(),msg);
 }
 
 public void error(int lineNbr,String msg)
 {
  System.err.println("*** error line "+lineNbr+" : "+msg);
  System.exit(1);
 }

 public void warning(String msg)
 {
  System.err.println("**** warning line "+loc.getLineNumber()+" : "+msg);
 }
 
 public Parser register(String tag,ElementFactory elf)
 {
  tagMap.put(tag,elf);
  return this;
 }
 
 private Element crElement(String tag)
 {
  ElementFactory elf=(ElementFactory)tagMap.get(tag);
  if (elf==null)
     {
      elf=current.createElementFactory(tag);
      tagMap.put(tag,elf);
      
//      System.err.println("ElementFactory '"+tag+ "' dont exists");
//      System.exit(0);
     }
  return elf.create();
 }
 
//implementation of ContentHandler
 public void characters(char[] ch,int start,int length) throws SAXException
 {
  if (debug) 
     {
      System.err.println("characters: "+new String(ch,start,length));
      
     }
  if (current!=null) current.addPCData(ch,start,length);
 } 
 
 public void startDocument() 
 {
 } 

 public void endDocument() 
 {
 } 

 public void startElement(String name,
                          String localName,
			  String qName,
			  Attributes atts) throws SAXException
 {
  if (debug) printDebug("startElement: '"+qName+"'");
  Element e=crElement(qName);
  e.atts=atts;
  e.parser=this;
  e.owner=current;
  e.name=qName;
  e.lineNumber=loc.getLineNumber();
  Element parent=current;
  current=e;
  current.setAttributes();
  current.onStart(parent);
 } 

 public void endElement(String name,
                        String localName,
			String qName) throws SAXException 
 {
  Element e=current.owner;
  current.finalizePCData();  
  current.onEnd();
  if (e!=null) e.notify(current);
  if (debug) printDebug("endElement: '"+qName+"'");
  current=e;
 }

 public void startPrefixMapping(String prefix,String uri)
 {
  System.out.println("startPrefixMapping prefix= "+prefix+
                   "\n                      uri= "+uri);
 }

 public void endPrefixMapping(String prefix)
 {
  System.out.println("endPrefixMapping prefix= "+prefix);
 }

 public void ignorableWhitespace(char[] ch,int start,int length) 
 {
 } 

 public void skippedEntity(String name)
 {
  System.out.println("skippedEntity name= "+name);
 }

 public void processingInstruction(String target,String data) 
 {
  System.out.println("processingInstruction "+target+","+data);
 }

 public void setDocumentLocator(Locator locator) 
 {
  loc=locator;
 }
}
