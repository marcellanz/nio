//
//    tools
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//----------------------
//Element
//(c) H.Buchmann 2001
//$Id: Element.java 193 2006-01-11 15:21:16Z buchmann $
//----------------------
package devel.xml;
import java.util.*;
import java.lang.reflect.*;
import org.xml.sax.*;

public class Element
{
 protected Attributes atts;
 protected Parser parser;
 Element owner;
 String name;
 private StringBuffer pcDataBuffer;
 private String pcData;
 
 int lineNumber=-1;
  
 protected Element(){}
 
 final public Element getOwner()
 {
  return owner;
 }

 public int getLineNumber(){return lineNumber;}
 
//attribute handling
 void setAttributes()
 {
//  System.err.println("tag '"+name+"'");
  try
  {
   Class claz=getClass();
   while(claz!=null)
   {
    Field[] field=claz.getDeclaredFields();
    for(int i=0;i<field.length;i++)
    {
     Field f=field[i];
//     System.err.println("      "+f);
     f.setAccessible(true);

     if (Attribute.class.isAssignableFrom(f.getType()))
	{
         String aName=f.getName();
	 String val=atts.getValue(aName);
//	 System.err.println("  name= '"+aName+"'\t val= '"+val+"'");
	 if ((val!=null)&&(val.length()>0))
            {
	     Attribute a=(Attribute)f.get(this);
	     try
	     {
	      a.fromString(val);
	     }
	     catch(AttributeException ex)
	     {
	      parser.error("attribute '"+aName+"' = '"+val+"' "+
	                    ex.getMessage());
	     }
	     a.defined=true;
	    }
	}
    }
    claz=claz.getSuperclass();
   }
  }
  catch(java.lang.Exception ex)
  {
   ex.printStackTrace();
   System.exit(1);
  }
 }

//pcdata handling
 final void addPCData(char ch[],int offset,int length)
 {
  if (pcDataBuffer==null) pcDataBuffer=new StringBuffer();
  pcDataBuffer.append(ch,offset,length);
 }
 
 final void finalizePCData()
 {
  if (pcDataBuffer!=null)
  {
   pcData=new String(pcDataBuffer);
  }
 }

 final public String getPCData(){return pcData;}
   
 final public double asDouble(String aName)
 {
  String val=atts.getValue(aName);
  try
  {
   if (val==null) parser.error("attribute '"+aName+"' dont exists");
   return Double.parseDouble(val);
  }
  catch(NumberFormatException ex){parser.error("value '"+val+"' not a number");}
  return 0;
 }

 final public boolean isDefined(String aName)
 {
  String val=atts.getValue(aName);
  return (val!=null)&&(val.length()>0);
 }
  
 final public double asDouble(String aName,double def)
 {
  String val=atts.getValue(aName);
  try
  {
   if ((val==null)||(val.length()==0)) return def;
   return Double.parseDouble(val);
  } 
  catch(NumberFormatException ex){parser.error("value '"+val+"' not a number");}
  return 0;  
 }

 final public int asInt(String aName)
 {
  String val=atts.getValue(aName);
  try
  {
   if (val==null) parser.error("attribute '"+aName+"' dont exists");
   return Integer.parseInt(val);
  } catch(NumberFormatException ex){parser.error("value '"+val+"' not a number");}
  return 0;
 }

 final public int asInt(String aName,int def)
 {
  String val=atts.getValue(aName);
  try
  {
   if ((val==null)||(val.length()==0)) return def;
   return Integer.parseInt(val);
  } catch(NumberFormatException ex)
   {parser.error("value '"+val+"' not a number");
   }
  return 0;
 }
 
 final public int asUnsigned(String aName)
 {
  String val=atts.getValue(aName);
  try
  {
   if (val==null) parser.error("attribute '"+aName+"' dont exists");
   int v=(val.startsWith("0x")||val.startsWith("0X"))
         ?Integer.parseInt(val.substring(2),16)
	 :Integer.parseInt(val);
   if (v<0) parser.error("value '"+v+" is negative");
   return v;
  }catch(NumberFormatException ex){parser.error("value '"+val+"' not a number");}
  return 0;
 }
 
 final public String asString(String aName)
 {
  return atts.getValue(aName);
 }
 
 public String toString(){return "<"+name+">";}
//overwriteables
//possible exceptions *must* be handled in these methods
 protected void notify(Element child){}
 protected void onStart(Element parent)
 {
//  System.err.println("------ "+getClass().getName());
 }
 protected void onEnd(){}

 protected ElementFactory createElementFactory(String tag)
 {
  System.err.println("ElementFactory for tag '"+tag+"' dont exists");
  System.exit(1);
  return null;
 }
}
