//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//-----------------------------
//Long and Scales
//attribute handling
//(c) H.Buchmann FHSO 2003
//$Id: Long.java 193 2006-01-11 15:21:16Z buchmann $
//-----------------------------
package devel.xml;

public class Long extends Attribute
{
 static class Scale
 {
  static class Entry
  {
   public char name;
   public long factor;
   public Entry(char name,long factor)
   {
    this.name=name;
    this.factor=factor;
   }
  }
  
  private Entry scale[];
  private long cFactor;
  
  public Scale(Entry scale[])
  {
   this.scale=scale;
  }

  boolean isScale(char ch)
  {
   int i=0;
   while(true)
   {
    if (i==scale.length) return false;
    if (ch==scale[i].name) 
       {
        cFactor=scale[i].factor;
	return true;
       }
    i++;
   }
  }
    
  long factor()
  {
   return cFactor;
  }
 }
 
//------------------------------
//some scales
//------------------------------
 static private Scale.Entry NoScaleE[]    ={new Scale.Entry('\0',1)};
 static private Scale.Entry BinaryScaleE[]={new Scale.Entry('\0',1),
                                            new Scale.Entry( 'B',1),
					    new Scale.Entry( 'K',1<<10),
					    new Scale.Entry( 'M',1<<20),
					    new Scale.Entry( 'G',1<<30)};
					    
 static public Scale NoScale=new Scale(NoScaleE);
 static public Scale BinaryScale=new Scale(BinaryScaleE);
 
 protected long val=0;
 
 private Scale scale=NoScale; 

 public Long()
 {
 }
 
 public Long(Scale scale)
 {
  this.scale=scale;
 }
 
 private boolean isDecDigit(char ch)
 {
  return ('0'<=ch)&&(ch<='9');
 }

 private int asDecDigit(char ch)
 {
  return (int)ch-(int)'0';
 }

 private boolean isHexDigit(char ch)
 {
  return (('0'<=ch)&&(ch<='9'))
         ||
	 (('a'<=ch)&&(ch<='f'))
         ||
	 (('A'<=ch)&&(ch<='F'));
 }

 private int asHexDigit(char ch)
 {
  if (('0'<=ch)&&(ch<='9')) return    (int)ch-(int)'0';
  if (('a'<=ch)&&(ch<='f')) return 10+(int)ch-(int)'a';
  return 10+(int)ch-(int)'A';
 }
 
 
 protected void parse(String s) throws AttributeException
 {
  int state=0;
  int len=s.length();
  int i=0;
  boolean negative=false;
//  System.err.println(s);
  while(true)
  {
   char ch=(i<len)?s.charAt(i++):'\0';
//   System.err.println("    "+state+"\t"+ch);
   switch(state)
   {
    case 0: //+|-|0|1|
     if (ch=='-'){negative=true;state=1;break;}
     if (ch=='+'){state=1;break;} //
     if (ch=='0'){state=2;break;} //x
     if (isDecDigit(ch)){state=3;val=asDecDigit(ch);break;}
    throw new  AttributeException("not a number");
    
    case 1:
     if (ch=='0'){state=2;break;}
     if (isDecDigit(ch)){state=3;val=asDecDigit(ch);}
    throw new  AttributeException("not a number");
    
    case 2:
     if ((ch=='x')||(ch=='X')){state=5;break;} //hexnumber
     if (ch=='\0'){val=0;return;}
     if (isDecDigit(ch)){state=3;val=asDecDigit(ch);}
    throw new  AttributeException("not a number");
    
    case 3:
     if (isDecDigit(ch)){val=10*val+asDecDigit(ch);break;}
     if (scale.isScale(ch)){state=4;val=val*scale.factor();break;}
    throw new  AttributeException("not a number");
   
    case 4:
     if (ch=='\0') return;
    throw new  AttributeException("not a number");
     
    
    case 5: //0x
     if (isHexDigit(ch)){state=6;val=asHexDigit(ch);break;}
    throw new  AttributeException("not a number");
    
    case 6:
     if (scale.isScale(ch)){state=4;val=val*scale.factor();break;}
     if (isHexDigit(ch)){val=0x10*val+asHexDigit(ch);break;}
    throw new  AttributeException("not a number");
   }
  }
 }
 
 public void fromString(String s) throws AttributeException
 {
  parse(s);
 }
 
 public long getVal()
 {
  return val;
 }
 
}
