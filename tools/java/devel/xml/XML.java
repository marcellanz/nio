//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//-----------------------------
//XML
//(c) H.Buchmann FHSO 2000
//didactic version
//$Id: XML.java 193 2006-01-11 15:21:16Z buchmann $
//-----------------------------
package devel.xml;
/**
easy access to the javax.xml package:<br>
@author H.Buchmann FHSO
*/
import java.io.*;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.*;

class SimpleErrorHandler implements ErrorHandler
{
/**
 print messages
*/
 private void message(String title,SAXParseException ex)
 {
  System.err.println(title+" : "+ex.getLineNumber()+" "+ex);
 }

 public void error(SAXParseException ex) throws SAXParseException
 {
//  message("------------------------- error",ex);
  throw ex;
 }

 public void fatalError(SAXParseException ex) throws SAXParseException
 {
//  message("fatalError",ex);
  throw ex;
 }

 public void warning(SAXParseException ex) throws SAXParseException
 {
//  message("warning",ex);
  throw ex;
 }
}

class SimpleDTDHandler implements DTDHandler
{
 public void notationDecl(String name,String publicId, String systemId)
 {
  System.err.println("notationDecl"+
                   "\nname      "+name+
                   "\npublicId  "+publicId+
		   "\nsystemId  "+systemId);
 } 
 
 public void unparsedEntityDecl(String name,
                                String publicId,
				String systemId,
				String notationName)
 {
  System.err.println("unparsedEntityDecl"+
                   "\nname         "+name+
                   "\npublicId     "+publicId+
		   "\nsystemId     "+systemId+
		   "\nnotationName "+notationName);
 }				
}

public class XML
{
 private XML(){} //no instances allowed
 {
 }

/**
 parses an InputStream
 @param src: the input
 @param validating: if set to true validating parsing
 @param ignoreWhiteSpace:if set to true white space will be ignored
 @param ignoreComments: if set to true comments will be ignored
 @return the DOM 
*/
 public static Document parse(InputStream src,boolean validating,
                                              boolean ignoreWhiteSpace,
					      boolean ignoreComments) 
       throws SAXException,IOException
 {
  try
  {
   DocumentBuilderFactory factory=DocumentBuilderFactory.newInstance();
   factory.setValidating(validating);
   factory.setIgnoringElementContentWhitespace(ignoreWhiteSpace);
   factory.setIgnoringComments(ignoreComments);

   DocumentBuilder p=factory.newDocumentBuilder();
   p.setErrorHandler(new SimpleErrorHandler());
   return p.parse(src);
  }
  catch(ParserConfigurationException ex)
  {
   System.err.println(ex);
   System.exit(0);
  }
  return null; //for the jikes compiler 
 }

/**
 parses an InputStream with default setting
 <ul>
 <li>validating true</li>
 <li>ignoreWhiteSpace true</li>
 <li>ignoreComments</li>
 </ul>
 @param src: the input
 @return the DOM 
*/
 public static Document parse(InputStream src) 
       throws SAXException,IOException
 {
  return parse(src,true,true,true);
 } 

/**
 parses an InputStream 
 @param src: the input
 @param handler: handler SAX parsing 
 @param resolver: if not null called for resolving
 @param validating: if set to true validating parsing
*/
 public static void parse(InputStream src,
                          ContentHandler handler,
			  EntityResolver resolver,
                          boolean validating) 
       throws SAXException,IOException
 {
  try
  {
   SAXParserFactory factory=SAXParserFactory.newInstance();
   factory.setValidating(validating);
   XMLReader p=factory.newSAXParser().getXMLReader();
   p.setErrorHandler(new SimpleErrorHandler());
   p.setDTDHandler(new SimpleDTDHandler());
   if (resolver!=null) p.setEntityResolver(resolver);
   p.setContentHandler(handler);
   p.parse(new InputSource(src));
  }
  catch(ParserConfigurationException ex)
  {
   System.err.println(ex);
   System.exit(0);
  }
 }

/**
 parses an InputStream with default seting 
 @param src: the input
 @param handler: handler SAX parsing 
 validating is set true
*/
 public static void parse(InputStream src,ContentHandler handler) 
       throws SAXException,IOException
 {
  parse(src,handler,null,true);
 } 
}

