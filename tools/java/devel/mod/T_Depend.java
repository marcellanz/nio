//------------------------
//Depend
//(c) H.Buchmann FHSO 2004
//$Id: T_Depend.java 193 2006-01-11 15:21:16Z buchmann $
//------------------------
package devel.mod;
import java.io.*;
import java.util.*;

class T_Depend extends devel.xml.Element
{
 static private final String Version =   "$Id: T_Depend.java 193 2006-01-11 15:21:16Z buchmann $";
 static private final String Copyright = "(c) H.Buchmann FHSO 2004";
 private String xmlFile;
 T_Makefile makefile;
 
 T_Depend(String file)
 {
  xmlFile=file;
  System.err.println(getClass().getName()+","+Copyright+
                    "\nPublicId: '"+DTD.getPublicID()+"'");
  try
  {
  Factory.depend=this;
  devel.xml.Parser p=new devel.xml.Parser();
//  p.setDebug();
  p.register("depend"  ,new T_Depend.Factory())
   .register("makefile",new T_Makefile.Factory())
   .register("pre"     ,new T_Pre.Factory())
   .register("post"    ,new T_Post.Factory())
   .register("search"  ,new T_Search.Factory())
   .register("specify" ,new T_Specify.Factory())
   .register("spec"    ,new T_Spec.Factory())
   .register("dir"     ,new T_Dir.Factory())
   .register("project" ,new T_Project.Factory())
   .register("mod"     ,new T_Mod.Factory())
   .register("list"    ,new T_List.Factory())
   .register("ref"     ,new T_Ref.Factory())
   .register("pac"     ,new T_Pac.Factory())
   .parseIt(file,new DTD());
  }
  catch(Exception ex)
  {
   System.err.println(ex);
   System.exit(1);
  } 
 }
 
 static class Factory implements devel.xml.ElementFactory
 {
  static private T_Depend depend;
  public devel.xml.Element create()
  {
   return depend;
  }
 }

 Map lst=new HashMap();
 Map prj=new TreeMap();

// devel.xml.CData makefile=new devel.xml.CData();
 
 void add(T_List lst)
 {
  if (this.lst.put(lst.name.getVal(),lst)!=null)
     {
      System.err.println("'"+lst.name.getVal()+"' already defined");
      System.exit(1);
     }
 }

 void add(T_Project prj)
 {
  if (this.prj.put(prj.name.getVal(),prj)!=null)
     {
      System.err.println("'"+prj.name.getVal()+"' already defined");
      System.exit(1);
     }
 }

 private void show(Map map)
 {
  Iterator i=map.values().iterator();
  while(i.hasNext())
  { 
   ((List)i.next()).show();
  }
 }
 
 void show()
 {
  System.err.println("++++++++++++++++++++++++ List");
  show(lst);
  System.err.println("++++++++++++++++++++++++ Projects");
  show(prj);
 }
 
 private T_Project lookupProject(String prjName)
 {
  T_Project p=(T_Project)prj.get(prjName);
  if (p==null)
     {
      System.err.println("project '"+prjName+"' dont exists in '"+xmlFile+"'");
      System.exit(1);
     }
  return p;   
 }
 
 public void projectList()
 {
  System.out.println("----------- ProjectList of '"+xmlFile+"'");
  Iterator i=prj.values().iterator();
  while(i.hasNext()) System.out.println(i.next());
 }

 public void fileList()
 {
  System.out.println("----------- FileList of '"+xmlFile+"'");
  Module.fileList();
 }

 public void dirList()
 {
  System.out.println("----------- DirList of '"+xmlFile+"'");
  java.util.Iterator dl=Module.dirList();
  while(dl.hasNext())
  {
   System.out.println(dl.next());
  }
 }

 public void includeList(String prj)
 {
  PrintWriter out=new PrintWriter(System.out);   
  lookupProject(prj).includeList(out); 
  out.flush();   
 }
 
 public void graph(String prj)
 {
  PrintWriter out=new PrintWriter(System.out);   
  lookupProject(prj).graph(out); 
  out.flush();   
 }
  
 public void make()
 {
  try
  {
   File fil=new File(makefile.name.getVal());
   Makefile mf=new Makefile(new FileWriter(fil));
   System.err.print("generating '"+fil+"'...");
   mf.println("#-----------------------------------------------\n"+
              "#calling: '"+getClass().getName()+" "+xmlFile+"'\n"+
	      "#"+new Date()+
	      "\n#-----------------------------------------------\n\n");
   makefile.pre(mf);	      
   Module.make(mf);
   mf.printMsg("Projects");
   Iterator i=prj.values().iterator();
   while(i.hasNext()) mf.print((T_Project)i.next());
   mf.printList("PRJ_LIST",prj.values().iterator(),"","");
   makefile.post(mf);
   mf.close();
   System.err.println("done");
  }
  catch(IOException ex)
  {
   ex.printStackTrace();
   System.exit(1);
  }
 }
 
 protected void onEnd()
 {
  Iterator i=prj.values().iterator();
  while(i.hasNext())((T_Project)i.next()).makeModList();
//  show();
 }

 
 static T_List getList(String name)
 {
  T_List lst=(T_List)(Factory.depend.lst.get(name));
  if (lst==null)
     {
      System.err.println("list '"+name+"' not defined");
      System.exit(1);
     }
  return lst;   
 } 
 
}
