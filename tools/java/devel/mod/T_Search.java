//------------------------
//Search
//(c) H.Buchmann FHSO 2004
//$Id: T_Search.java 193 2006-01-11 15:21:16Z buchmann $
//------------------------
package devel.mod;
import java.util.*;
import java.io.*;

class T_Search extends devel.xml.Element
{
 static class Factory implements devel.xml.ElementFactory
 {
  public devel.xml.Element create(){return new T_Search();}
 }

 private static T_Search search;
 
 public static File lookup(String fileName){return search.lookup_(fileName);}
 
 T_Search(){search=this;}
 
 private devel.xml.CData home   =new devel.xml.CData();

 private File homeDir;
 
 private Vector pathList=new Vector();
 
 public void onStart(devel.xml.Element parent)
 {
  homeDir=new File(home.getVal());
//  System.err.println("home "+home.getVal());
  if (!homeDir.isDirectory()) parser.error(homeDir+" is not a directory");
 }

 public void onEnd()
 {
/*
  for(int i=0;i<pathList.size();i++)
  {
   System.err.println(pathList.get(i));
  }
*/  
  Module.createList(this);
 }
  
 public void notify(devel.xml.Element child)
 {
  try
  {
   File dir=new File(homeDir,((T_Dir)child).name.getVal());
   if (!dir.isDirectory()) parser.error("'"+dir.getCanonicalPath()+"' is not a directory");
   pathList.add(dir);
  }catch(IOException ex)
  {
   ex.printStackTrace();
   System.exit(1);
  }
 }
 
 private java.io.File lookup_(String fileName)
 {
  {
   File f=new File(fileName);
   if (f.isAbsolute())
      {
       return (f.exists())?f:null;
      }
  }
  for(int i=0;i<pathList.size();i++)
  {
   File f=new File((File)pathList.get(i),fileName);
   if (f.exists()) return f;
  } //end for
  return null;
 }
   
 void make(Makefile mf)
 {
  mf.printList("SRC_DIRS",pathList.iterator(),"","");
  mf.println("\n");
  mf.println("vpath %.cc $(SRC_DIRS)\n"+
	     "vpath %.s  $(SRC_DIRS)\n"+
	     "vpath %.h  $(SRC_DIRS)\n");
  mf.printList("INCLUDE",pathList.iterator(),""+" ","");	     

 }
}
