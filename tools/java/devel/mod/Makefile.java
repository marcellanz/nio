//------------------------
//Makefile
//(c) H.Buchmann FHSO 2004
//$Id: Makefile.java 193 2006-01-11 15:21:16Z buchmann $
//------------------------
package devel.mod;
import java.io.*;
import java.util.*;

class Makefile extends PrintWriter
{
 private final int LINE_LEN = 5; //number of prerequisites per line

 static interface Rule
 {
  String targetName();
  String sourceName();  //return null  if not available
  String includeName(); //return null if not available
  String format(Module depend);
  ModuleList getDepend();
  String command();
 }
 
  
 Makefile(Writer wr) throws IOException
 {
  super(wr);
 }
 
 void printList(String name,Iterator i,String prefix,String postfix)
 {
  super.print(name+"=\\\n\t");
  int cnt=0;
  while(i.hasNext())
  {
   if (cnt>0) print("\\\n\t");
   print(prefix+i.next()+postfix);
   cnt++;
  }
  println();
 }
 
 void printMsg(String msg)
 {
  println("#------------------------- "+msg);
 }
 
 void print(Rule rule)
 {
  print(rule.targetName()+":\t");
  String src=rule.sourceName();
  int len=0;
  if (src!=null) {print(src);len++;}
  String inc=rule.includeName();
  if (inc!=null) {print(" "+inc);len++;}
  ModuleList depend=rule.getDepend();
  for(int i=0;i<depend.size();i++)
  {
   if (len>=LINE_LEN)
      {
       print("\\\n\t");
       len=0;
      }
      else print(" ");
   print(rule.format(depend.get(i)));   
   len++;   
  }
        
  String cmd=rule.command();
  println();
  if ((cmd!=null)&&(cmd.length()>0)) println("\t"+cmd);
 }
}
