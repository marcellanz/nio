//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//------------------------
//Project
//(c) H.Buchmann FHSO 2004
//$Id: T_Project.java 193 2006-01-11 15:21:16Z buchmann $
//------------------------
package devel.mod;
import java.util.*;

class T_Project extends List
                implements Makefile.Rule
{
 static class Factory implements devel.xml.ElementFactory
 {
  public devel.xml.Element create(){return new T_Project();}
 }

 devel.xml.CData make=new devel.xml.CData();
 devel.xml.Token closure= new devel.xml.Token();
 
 ModuleList modList=new ModuleList(); 
 
//implementation Rule
 public String targetName(){return name.getVal();}
 public String sourceName(){return null;}
 public String includeName(){return null;}
 public String format(Module m){return m+".o";}
 public ModuleList getDepend(){return modList;}
 
 public String command(){return make.getVal();}
  

 protected void onEnd()
 {
  ((T_Depend)getOwner()).add(this);
 }
 
 void makeModList()
 {
  for(int i=0;i<size();i++) get(i).addTo(this);
  if (closure.getVal().equals("auto"))
     {
      ModuleList deep=new ModuleList();
      for(int i=0;i<modList.size();i++)
      {
       Module m=modList.get(i);
       if (deep.add(m)) m.addDepend(m,deep);
      }
      modList=deep;
     }
 }

 void add(Module m){modList.add(m);}
 
 public String toString(){return name.getVal();}
 
 void show()
 {
  System.err.println("-------------- "+name.getVal());
  for(int i=0;i<modList.size();i++)
  {
   System.err.println(modList.get(i));
  }
 }
 
 void includeList(java.io.PrintWriter out)
 {
  out.println("----------- IncludeList of '"+name.getVal()+"'");
  for(int i=0;i<modList.size();i++) modList.get(i).dependList(out);
 }
 
 void graph(java.io.PrintWriter out)
 {
  modList.graph(name.getVal(),out);
 }
}
