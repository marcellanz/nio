//
//    tools
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//------------------------
//T_Makefile
//(c) H.Buchmann FHSO 2005
//$Id: T_Makefile.java 193 2006-01-11 15:21:16Z buchmann $
//------------------------
package devel.mod;
class T_Makefile extends devel.xml.Element
{
 devel.xml.CData name=new devel.xml.CData();
 devel.xml.Element pre;
 devel.xml.Element post;

 static class Factory implements devel.xml.ElementFactory
 {
  public devel.xml.Element create(){return new T_Makefile();}
 }
 
 protected void onStart(devel.xml.Element parent)
 {
  ((T_Depend)parent).makefile=this;
 }

 void pre(Makefile mf)
 {
  if (pre!=null) 
   {
    mf.printMsg("pre");
    mf.println(pre.getPCData());
   }
 }

 void post(Makefile mf)
 {
  if (post!=null) 
    {
     mf.printMsg("post");
     mf.println(post.getPCData());
    }
 }
}
