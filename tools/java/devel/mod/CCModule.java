//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//------------------------
//CCModule
//(c) H.Buchmann FHSO 2004
//$Id: CCModule.java 193 2006-01-11 15:21:16Z buchmann $
//------------------------
package devel.mod;
import java.io.*;
import java.util.regex.*;

class CCModule extends Module
{
 static class Exception extends java.lang.Exception
 {
  Exception(String msg)
  {
   super(msg);
  }
 }
 
 static Pattern include=Pattern.compile(
//   "#include\\s*\"(.+)\\.h\""
   "\\s*#include\\s*\""+
   "(([a-zA-Z_0-9/\\-])+)\\.h"+
   "\""
                                       );
 
 CCModule(String name){super(name);}
 
 private void parse(File file)
 {
//  System.err.println("-------------- parsing '"+file+"'");
  try
  {
   LineNumberReader src=new LineNumberReader(new FileReader(file));
   Matcher matcher=null;
   while(true)
   {
    String li=src.readLine();
    if (li==null) break;
    if (matcher==null) matcher=include.matcher(li);
       else            matcher.reset(li);
    if (matcher.lookingAt()) //start of line
       { 
	String inc=matcher.group(1);
	try
	{
	 if (name.equals(inc))
	    {
	     ifc=T_Search.lookup(inc+".h");
	     parse(ifc);
	    }
	    else 
	    {
	     addDepend(Module.lookup(inc,"cc"));
	    }
	}
	catch(Module.Exception ex)
	{
	 System.err.println("*** module '"+inc+"' not found\n"+ 
	                    "    file '"+file.getCanonicalFile()+"' line "+src.getLineNumber());
	 System.exit(1);		    
	}
       }
   }
   src.close();
 //  System.err.println("-------------- parsing '"+file+"' done");
  }
  catch(IOException ex)
  {
   ex.printStackTrace();
   System.exit(1);
  }
 }
 
 void parse()
 {
  parse(impl);
 }
 
 String asSourceFile(){return name+".cc";}
 String asIncludeFile(){return name+".h";}
}
