//------------------------
//Module
//(c) H.Buchmann FHSO 2004
//$Id: Module.java 193 2006-01-11 15:21:16Z buchmann $
//------------------------
package devel.mod;
import java.io.*;
import java.util.*;
import java.util.regex.*;

abstract class Module 
               implements Makefile.Rule,
                          List.Entry,
			  Comparable
{
 static class Exception extends java.lang.Exception
 {
  Exception(String msg){super(msg);}
  Exception(Throwable cause){super(cause);}
 }

 static private List lst;

 static Module lookup(String name,String ext) 
        throws Exception 
 {
  return lst.lookup_(name,ext);
 }
 
 static void fileList()
 {
  lst.fileList();
 }

 static Iterator dirList()
 {
  return lst.dirList();
 }
 
 
/*
 static void graph(PrintWriter out)
 {
  lst.graph_(out);
 }  
*/

 static void make(Makefile mf){lst.make_(mf);}
 static void createList(T_Search search){lst=new List(search);}
 
 private static class List
 {
  private T_Search search;
  private Map mList=new TreeMap();

  List(T_Search search){this.search=search;lst=this;}

  private Module lookup_(String name,String type) 
                 throws Exception
  {
   Module m=(Module)mList.get(name);
   if (m==null)
      {
       String file=name+"."+type;
       if        (type.equals("cc")) m=new CCModule(name);
         else if (type.equals("s"))  m=new SModule(name);
       mList.put(name,m); //very early see include files
       m.impl=search.lookup(file);
       if (m.impl==null) throw new Exception("lookup: module '"+name+"' not found");;
       m.parse();
      }
   return m;   
  }

  private void graph_(PrintWriter out)
  {
   out.println("digraph G\n{ node [shape=plaintext]");
   java.util.Iterator i=mList.values().iterator();
   int index=0;
   while(i.hasNext())
   {
    ((Module)i.next()).graph(out,index++);
   }
   out.println("}");
   out.close();
  }
  
  private void make_(Makefile mf)
  {
   search.make(mf);   mf.printMsg("Modules");
   Iterator i=mList.values().iterator();
   while(i.hasNext()) mf.print((Module)i.next());
   mf.printList("MOD_LIST",mList.values().iterator(),"",".o");
   mf.printList("DIR_LIST",lst.dirList(),"","");
  }
  
  private void fileList()
  {
   try
   {
    Iterator i=mList.values().iterator();
    while(i.hasNext()) 
    {
     Module m=(Module)i.next();
     System.out.println(m.impl.getCanonicalFile());
     if (m.ifc!=null) System.out.println(m.ifc.getCanonicalFile());

    }
   } catch(IOException ex)
      {
       ex.printStackTrace();
       System.exit(1);
      }
  }
  
  private static final String FileName="[a-zA-Z_0-9\\-]+";
  
  private Iterator dirList()
  {
   Pattern pat=Pattern.compile("(("+FileName+"/)*)"+"("+FileName+")"); 
   Set dir=new TreeSet();
   Iterator i=mList.values().iterator();
   while(i.hasNext()) 
   {
    Module m=(Module)i.next();
    Matcher matcher=pat.matcher(m.name);
    if (matcher.matches())
       {
        String d=matcher.group(1);
	dir.add(d);
       }
   }
   return dir.iterator();
  }
 } //end List
 
 public int compareTo(Object obj)
 {
  return name.compareTo(obj.toString());
 }

 public void addDepend(Module m,ModuleList deep)
 {
  for(int i=0;i<m.depend.size();i++)
  {
   Module md=m.depend.get(i);
   if ((md!=this)&& deep.add(md)) addDepend(md,deep);
  }
 }

 protected void addDepend(Module m)
 {
  depend.add(m);
 }

//---------------------------------- implementation Makefile.Rule  
 public String targetName(){return name+".o";}
 public String sourceName(){return asSourceFile();}
 public String includeName(){return (ifc==null)?null:asIncludeFile();}
 public String format(Module m){return m.asIncludeFile();}

 public ModuleList getDepend()
 {
  ModuleList deep=new ModuleList();
  addDepend(this,deep);
  return deep;
 }
 public String command(){return make;}

 File impl;
 File ifc =null;       //the public part
 int inDegree=0;    
 ModuleList depend=new ModuleList();
 String name;
 String make;
 boolean includeIntoGraph=true;
 
 public void addTo(T_Project prj){prj.add(this);}
 
 protected Module(String name){this.name=name;}
 abstract void parse(); //variable 'impl' set
 abstract String asSourceFile();
 abstract String asIncludeFile();
 
 public String toString(){return name;}
 
 void dependList(PrintWriter out)
 {
  out.println(name);
  for (int i=0;i<depend.size();i++)
  {
   out.println("\t"+depend.get(i));
  }
 }
 
 int graph(PrintWriter out,int index) //graphviz dot format
     //returns index of *next* top module
 {
  if (!includeIntoGraph) return index;
  if (inDegree==0) 
     {
      index++;
      out.println("\""+name+"\"["+
                  "shape=ellipse "+
		  "label=\""+name+"("+index+")\""+
		  "]");
     }
  out.print("\""+name+"\"->{");   
  int cnt=0; //for proper formatting
  for(int i=0;i<depend.size();i++)
  {
   Module md=depend.get(i);
   if (md.includeIntoGraph) 
      {
       if (cnt==0) out.println();
       out.println("\t\""+md.name+"\"");
       cnt++;
      }
  }
  out.println("};");
  return index;
 }
}