//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//------------------------
//List 
//(c) H.Buchmann FHSO 2004
//$Id: List.java 193 2006-01-11 15:21:16Z buchmann $
//------------------------
package devel.mod;
import java.util.*;
import java.io.*;

//superclass of T_Project and T_List
class List extends devel.xml.Element
{
 devel.xml.Token name=new devel.xml.Token();
 
  
 private Vector lst=new Vector(); //the entries
 
 static interface Entry
 {
  abstract void addTo(T_Project prj);
 }
 
//superclass of T_Mod T_Ref 
 abstract static class Child extends devel.xml.Element
 {
  abstract void onNotify(List lst);
 }
 
 public void notify(devel.xml.Element child)
 {
  ((Child)child).onNotify(this);
 }
 
 void add(T_Mod mod)
 {
  try
  {
   Module m=Module.lookup(mod.name.getVal(),"cc"); 
   lst.add(m);
  }
  catch(Module.Exception ex){parser.error(ex);}
 }
 
 void add(T_Ref ref)
 {
  lst.add(ref);
 }
 
 int size(){return lst.size();}
 Entry get(int i){return (Entry)lst.get(i);}
 
 void show()
 {
  System.err.println("------------------- "+name.getVal());
  for(int i=0;i<lst.size();i++)
  {
   System.err.println(lst.get(i));
  }
 }
}