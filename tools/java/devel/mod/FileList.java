//-------------------------
//FileList
//(c) H.Buchmann FHSO 2004
//$Id: FileList.java 193 2006-01-11 15:21:16Z buchmann $
//-------------------------
package devel.mod;
class FileList extends T_Depend
{
 private FileList(String file)
 {
  super(file);
  fileList();
 }
 
 public static void main(String args[]) throws Exception
 {
  if (args.length!=1)
     {
      System.err.println("usage: FileList configFile");
      System.exit(1);
     }
   new FileList(args[0]);
 }
 
}