//----------------------
//DTD of AutoMaker
//(c) H.Buchmann FHSO 2002
//$Id: DTD.java 193 2006-01-11 15:21:16Z buchmann $
//----------------------
package devel.mod;
import java.io.*;
import org.xml.sax.*;

class DTD implements EntityResolver
{
 static final private String Revision="$Revision: 137 $";
 static final private String PublicID;
 static final private String DTD=
  "<!ELEMENT depend (makefile,search,specify?,(list|project)+)>\n"+
  "<!ELEMENT makefile (pre?,post?)>\n"+
  "<!ATTLIST makefile name CDATA \"Makefile\">\n"+
  "<!ELEMENT pre (#PCDATA)>\n"+
  "<!ELEMENT post (#PCDATA)>\n"+
  "<!ELEMENT search (dir)*>\n"+
  "<!ATTLIST search home CDATA #IMPLIED>\n"+
  "<!ELEMENT dir EMPTY>\n"+
  "<!ATTLIST dir name CDATA #REQUIRED>\n"+
  "<!ELEMENT specify (spec)+>\n"+
  "<!ELEMENT spec EMPTY>\n"+
  "<!ATTLIST spec mod CDATA #REQUIRED\n"+
  "		type (cc | s) \"cc\"\n"+
  "		make CDATA \"\"\n"+
  "		graph (exclude | include) \"include\">\n"+	       
  "<!ELEMENT list (mod | pac |ref)+>\n"+
  "<!ATTLIST list name ID #REQUIRED>\n"+
  "<!ELEMENT project (mod | pac | ref)+>\n"+
  "<!ATTLIST project name ID #REQUIRED\n"+ 
  "		make CDATA \"\"\n"+
  "		closure (auto | manual) \"auto\">\n"+
  "<!ELEMENT mod EMPTY>\n"+
  "<!ATTLIST mod name CDATA #REQUIRED>\n"+
  "<!ELEMENT ref EMPTY>\n"+
  "<!ATTLIST ref id IDREF #REQUIRED>\n"+
  "<!ELEMENT pac (mod | pac |ref)+ >\n"+
  "<!ATTLIST pac p NMTOKEN #REQUIRED>\n";

 public InputSource resolveEntity(String publicId,String systemId)
 {
  if (publicId==null) return new InputSource(systemId);
  if (publicId.equals(PublicID)) return new InputSource(new StringReader(DTD));
     else  return new InputSource(systemId);
 }
 
 static
 {
  //create PublicID based on Revision
  int i0=Revision.indexOf(':');
  PublicID="FHSO-mod"+Revision.substring(i0,Revision.indexOf('$',i0)).trim();
 }
 
 static String getPublicID(){return PublicID;}
//------------------ print DTD 
 public static void main(String args[])
 {
  System.out.println(
  "<!--\n"+
  "\tdepend.dtd Version: "+PublicID+"\n"+
  "\t(c) H.Buchmann FHSO 2003\n"+ 
  "-->\n"
                    );
  System.out.println(DTD);
 }
}
