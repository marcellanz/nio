//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//-----------------------
//ModuleList
//(c) H.Buchmann FHSO 2004
//$Id: ModuleList.java 193 2006-01-11 15:21:16Z buchmann $
//-----------------------
package devel.mod;
import java.util.*;

class ModuleList
{
 
 private Set    mSet  = new HashSet(); 
 private Vector mList = new Vector();
 
 ModuleList(){}
 
 private void resetInDegree()
 {
  Iterator i=mList.iterator();
  while(i.hasNext()) ((Module)i.next()).inDegree=0;
 }

 private void calcInDegree()
 {
  Iterator i=mList.iterator();
  while(i.hasNext())
  {
   Module m=(Module)i.next();
   for(int k=0;k<m.depend.size();k++)
   {
    m.depend.get(k).inDegree++;
   }
  }
 }
 
 boolean add(Module m)
 {
  boolean res=mSet.add(m);
  if (res) mList.add(m); 
  return res;
 }
 
 int size(){return mList.size();}
 
 Module get(int i){return (Module)mList.get(i);}
 
 void graph(String name,java.io.PrintWriter out)
 {
  resetInDegree();
  calcInDegree();
  int index=0;
  out.println("digraph \""+name+"\"\n{ node [shape=plaintext]");
  Iterator i=mList.iterator();
  while(i.hasNext()) index=((Module)i.next()).graph(out,index);
  out.println("}");
 }
}