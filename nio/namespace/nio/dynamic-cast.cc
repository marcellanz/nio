//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//-----------------------
//dynamic-cast could be useful
//(c) H.Buchmann FHSO 2003
//$Id: dynamic-cast.cc 193 2006-01-11 15:21:16Z buchmann $
//-----------------------
#include "sys/sys.h"
IMPLEMENTATION(dynamic_cast_demo,$Revision: 137 $)
#include "sys/msg.h"
#include <typeinfo>
class ClassA
{
 public:
  ClassA(){sys::msg<<__PRETTY_FUNCTION__<<"\n";}
  virtual ~ClassA(){sys::msg<<__PRETTY_FUNCTION__<<"\n";}
};

class ClassB:public ClassA
{
 public:
  ClassB(){sys::msg<<__PRETTY_FUNCTION__<<"\n";}
  virtual ~ClassB(){sys::msg<<__PRETTY_FUNCTION__<<"\n";}
};

class ClassC
{
 public:
  ClassC(){sys::msg<<__PRETTY_FUNCTION__<<"\n";}
  virtual ~ClassC(){sys::msg<<__PRETTY_FUNCTION__<<"\n";}
};

class Demo
{
 private:
  static Demo demo;
  Demo(); 
};

Demo Demo::demo;
extern "C" void xxx(){}

Demo::Demo()
{
 
 sys::msg<<"--------------- dynamic-cast demo\n";
 ClassA a;
 ClassB b;
 ClassC c;
 ClassA* ap=&b;
 ClassC* cp=&c;
 ClassB* bp=&b;
 xxx();
 sys::msg<<":  "<<typeid(bp).name()<<"\n";
// ClassB* bp=dynamic_cast<ClassB*>(cp);
 xxx();
 sys::msg<<bp<<"\n";
}
