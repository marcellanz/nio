//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//---------------------------
//icmp-demo
//(c) H.Buchmann FHSO 2004
//$Id: icmp-demo.cc 193 2006-01-11 15:21:16Z buchmann $
//---------------------------
#include "sys/sys.h"
IMPLEMENTATION(nio_icmp_demo,$Revision: 137 $)
#include "nio/nio.h"
#include "util/pfile.h"
namespace nio
{
 class ICMPDemo:public Test
 {
  static ICMPDemo icmpDemo;
  ICMPDemo();
 };
 
 ICMPDemo ICMPDemo::icmpDemo;
 
 ICMPDemo::ICMPDemo()
 {
  util::PFile cfg(1,0);
  eth.logon();
  icmp::Echo tx;
  tx.icmp=Default::ICMP;
  tx.icmp.ip4.eth.dst=cfg.valueOf("ICMP_DEMO_MAC_DST");
  tx.icmp.ip4.dst=cfg.valueOf("ICMP_DEMO_IP4_DST");
  tx.icmp.type=icmp::Echo::REQUEST;
  tx.id=1;
  tx.seq=0;
  tx.pld.set("1234567890");
  eth<<tx;
  icmp::Packet rx;
  rx.setTimeout(500); 
  eth>>rx;
  if (rx.isTimeout()) fail("no reply from probe");
     else  if (rx.ip4.src==cfg.valueOf("ICMP_DEMO_IP4_DST"))
                {     
                 success();
		}
 }
}
