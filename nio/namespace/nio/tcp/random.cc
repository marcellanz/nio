//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//----------------------------
//random
//using the linux random generator
//(c) H.Buchmann FHSO 2005
//$Id: random.cc 193 2006-01-11 15:21:16Z buchmann $
//----------------------------
#include "sys/sys.h"
IMPLEMENTATION(nio_tcp_random,$Random$)
#include "nio/tcp/random.h"
#include "sys/msg.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

//#define NIO_TCP_RANDOM_TEST
namespace nio
{
 namespace tcp
 {
  const char Random::Device[]="/dev/urandom";
  Random Random::random;
  
  Random::Random()
  {
   id=::open(Device,O_RDONLY);
   if (id<0) sys::msg.error()<<"error opening '"<<Device<<"\'n";
  }
  
  unsigned Random::number()
  {
   unsigned val;
   ::read(random.id,&val,sizeof(val));
   return val;
  }
  
#ifdef NIO_TCP_RANDOM_TEST
 class Tester
 {
  static Tester tester;
  Tester();
 };
 
 Tester Tester::tester;
 
 Tester::Tester()
 {
  for(unsigned i=0;i<5;i++) sys::msg<<Random::number()<<"\n";
 }
 
#endif

 }//namespace tcp
}//namespace nio
