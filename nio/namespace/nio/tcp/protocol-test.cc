//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//--------------------------
//protocol-test
//(c) H.Buchmann FHSO 2005
//$Id: protocol-test.cc 193 2006-01-11 15:21:16Z buchmann $
//--------------------------
#include "sys/sys.h"
IMPLEMENTATION(nio_tcp_protocol_test,$Revision: 137 $)
#include "tcp/protocol.h" 
#include "tcp/random-port-pool.h"
#include "util/pfile.h"
#include "arp/arp.h"

namespace nio
{
 namespace tcp
 {
  class Tester:public Connection::Listener
  {
   static Tester tester;
   Tester();
   ~Tester();
   RandomPortPool portPool;
   Host* host;
   void onConnect(Connection&);
   void onOctet(unsigned char oct);
  };

  Tester Tester::tester;
  Tester::Tester()
  :host(0)
  {
   sys::msg<<"TCP-RX-Test\n";
   util::PFile config(1,0);
   host=new Host(config.valueOf("IP4_SRC_ADDRESS"),portPool);
   arp::Pair dst(config.valueOf("MAC_DST_ADDRESS"),
                 config.valueOf("IP4_DST_ADDRESS"));
   Connection* c=host->create(*this,dst,7);
   sys::msg<<*c<<"\n";
   sys::Thread::wait();
  }

  Tester::~Tester()
  {
   if (host) delete host;
  }
  
  void Tester::onConnect(Connection&)
  {
  }

  void Tester::onOctet(unsigned char oct)
  {
  }

 }//namespace tcp
}//namespace nio
