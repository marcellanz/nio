//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//------------------------------
//RandomPortRandomPortPool
//(c) H.Buchmann FHSO 2005
//$Id: random-port-pool.cc 193 2006-01-11 15:21:16Z buchmann $
//------------------------------
#include "sys/sys.h"
IMPLEMENTATION(nio_tcp_random_port_pool,$Revision: 137 $)
#include "tcp/protocol.h"
#include "tcp/random-port-pool.h"
#include "tcp/random.h"
namespace nio
{
 namespace tcp
 {
//-------------------------------------- RandomPortPool
  RandomPortPool::RandomPortPool()
  {
   for(unsigned i=0;i<POOL_SIZE;i++) pool[i]=0;
  }
  
  RandomPortPool::~RandomPortPool()
  {
  }
   
  Port* RandomPortPool::create(Port::Listener&li)
  {
   unsigned p=Random::number()%POOL_SIZE;
   for(unsigned i=0;i<POOL_SIZE;i++)
   {
    if (pool[p]==0)
       {
	pool[p]=newPort(p,li);
	return pool[p];
       }
    p++;
    if (p==POOL_SIZE) p=0;
   }
   sys::msg.error()<<"RandomPortPool: no free port available\n";
   return 0;
  }

  Port* RandomPortPool::create(Port::ID at,Port::Listener& li)
  {
   if (pool[at])
      {
       sys::msg.error()<<"RandomPortPool: port '"<<at<<"' already used\n";
       return 0;
      }
   pool[at]=create(at,li);
   return pool[at];
  }

  void RandomPortPool::remove(Port* p)
  {
   Port::ID id=p->getID();
   if (pool[id]==0)
      {
       sys::msg.error()<<"RandomPortPool: not port at '"<<id<<"'\n";
      }
   delete pool[id];
   pool[id]=0;      
  }

 }//namespace tcp
}//namespace nio
