//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#ifndef NIO_TCP_TCB
#define NIO_TCP_TCB
//----------------------------
//tcb Transmission Control Block
// see rfc793
//(c) H.Buchmann FHSO 2003
//$Id: tcb.h 193 2006-01-11 15:21:16Z buchmann $
//-----------------------------
INTERFACE(nio_tcp_tcb,$Revision: 137 $)
namespace nio
{
 namespace tcp
 {
  struct TCB
  {
   struct SND
   {
    unsigned una; //unacknowledged 
    unsigned nxt; //next
    unsigned up;  //urgent pointer
    unsigned wl1;
    unsigned wl2;
    unsigned isn; //initial send sequence number
   };
   struct RCV
   {
    unsigned nxt;
    unsigned wnd;
    unsigned up; 
    unsigned isn;
   };
   SND snd;
   RCV rcv;
  };
 }
}
#endif
