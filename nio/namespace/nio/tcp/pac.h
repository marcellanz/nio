//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#ifndef NIO_TCP_PAC_H
#define NIO_TCP_PAC_H
//-------------------------------------
//tcp
//(c) H.Buchmann FHSO 2003
//$Id: pac.h 193 2006-01-11 15:21:16Z buchmann $
//-------------------------------------
INTERFACE(nio_tcp_pac,$Revision: 137 $)
#include "nio/pac/pac.h"
#include "nio/ip4/pac.h"
#include "nio/pac/stream/checksum.h"
#include "nio/pac/stream/raw.h"
#include <vector>

namespace nio
{
 namespace tcp
 {
//------------------------------------------------------ Seq
  class Seq:public pac::Unsigned
  {
   private:
    Seq(pac::Packet* p,const Seq*); //not allowed because const Seq& seq
   public:
            Seq(pac::Packet* p);
	    Seq(pac::Packet* p,const Seq& seq);
    virtual ~Seq();
    Seq& operator=(unsigned v);
  };
  
//------------------------------------------------------ Packet
  class Packet:public pac::Packet
  {
   public:
    //flag names
    static const char* URG[];
    static const char* ACK[];
    static const char* PSH[];
    static const char* RST[];
    static const char* SYN[];
    static const char* FIN[];
     
   private:
    bool is();
    void init();
    void onRaw();
      
   public:
//------------------------------------------------------ DataOffset   
    class DataOffset:public pac::Bits<4>
    {
     private:
      std::vector<Type*> list;
      unsigned pos; //in raw
      void fromRaw(pac::stream::RawInput& in);
      void toRaw(pac::stream::RawOutput& out);
      
     public:
      DataOffset(pac::Packet* p);
      DataOffset& add(Type& t){list.push_back(&t);return *this;}
      unsigned getPos(){return pos;}
    };
    
//------------------------------------------------------ Option   
    class Option
    {
     public:
      class List:public pac::Type
      {
       private:
        unsigned size; 
        std::vector<Option*> list;

       public:
                 List(pac::Packet* p);
		 List(pac::Packet* p,const List& lst);
		 
	virtual ~List();
	void get(pac::stream::Input& in);
	void put(pac::stream::Output& out);
	void add(Option* opt);
	void add(Option& opt){add(&opt);}
	io::ascii::Writer& show(io::ascii::Writer& out) const;
      };
     
     protected:
      unsigned char kind;
      virtual void put(pac::stream::Output& out)=0;
     public:
               Option(unsigned char kind):kind(kind){}
      virtual ~Option(){}
      virtual io::ascii::Writer& show(io::ascii::Writer& out) const=0;
      virtual Option* clone()=0;
    };
    
//------------------------------------------------------ Nop   
    class Nop:public Option
    {
     private:
      void put(pac::stream::Output& out);
      
     public:
               Nop():Option(1){};
      virtual ~Nop(){}
      io::ascii::Writer& show(io::ascii::Writer& out) const;
      Option* clone(){return new Nop;}
    };
    
//------------------------------------------------------ EoO
    class EoO:public Option //end of list
    {
     private:
      void put(pac::stream::Output& out);
      
     public:
               EoO():Option(0){};
      virtual ~EoO(){}
      io::ascii::Writer& show(io::ascii::Writer& out) const;
      Option* clone(){return new  EoO;}
    };
    
//------------------------------------------------------ LenOption
    class LenOption:public Option //kind len almost all options
    {
     protected:
      unsigned short len;
      LenOption(const LenOption* lo):Option(lo->kind),len(lo->len){}

     public:
               LenOption(unsigned char kind,pac::stream::Input& in);
      virtual ~LenOption(){}
//      Option* clone(){return new LenOption(this);}
    };
    
//------------------------------------------------------ MSS
    class MSS:public LenOption //Maximum Segment Size
    {
     private:
      unsigned short mss;
      void put(pac::stream::Output& out);
      MSS(const MSS* mss_):LenOption(this),mss(mss_->mss){}
     public:
      MSS(pac::stream::Input& in);
      io::ascii::Writer& show(io::ascii::Writer& out) const;  
      Option* clone(){return new MSS(this);}
    };

//------------------------------------------------------ TSO Time Stamp Option
    class TSO:public LenOption
    {
     private:
      unsigned tsVal;
      unsigned tsEcr;
      void put(pac::stream::Output& out);
      TSO(const TSO* tso):LenOption(this),tsVal(tso->tsVal),tsEcr(tso->tsEcr){}
     public:
      TSO(pac::stream::Input& in); 
      io::ascii::Writer& show(io::ascii::Writer& out) const;
      Option* clone(){return new TSO(this);}
    };

//------------------------------------------------------ WSO Windowscale
    class WSO:public LenOption
    {
     private:
      unsigned char scale;
      void put(pac::stream::Output& out);
      WSO(const WSO* wso):LenOption(this),scale(wso->scale){}
     public:
      WSO(pac::stream::Input& in); 
      io::ascii::Writer& show(io::ascii::Writer& out) const;  
      Option* clone(){return new WSO(this);}
    };

//------------------------------------------------------ SACK permitted
    class SACK:public LenOption
    {
     private:
      void put(pac::stream::Output& out);
      SACK(const SACK* wso):LenOption(this){}
     public:
      SACK(pac::stream::Input& in); 
      io::ascii::Writer& show(io::ascii::Writer& out) const;  
      Option* clone(){return new SACK(this);}
    };
        
//------------------------------------------------------ NotSupOption
    class NotSupOption:public LenOption
    {
     private:
      unsigned char* data;
      void put(pac::stream::Output& out);
      NotSupOption(const NotSupOption* nsp);
      
     public:
               NotSupOption(unsigned char kind,pac::stream::Input& in);
      virtual ~NotSupOption();
      io::ascii::Writer& show(io::ascii::Writer& out) const;
      Option* clone(){return new NotSupOption(this);}
    };
    
    class Checksum:public pac::Short
    {
     private:
      void calc(pac::stream::Checksum& cs);
      void toRaw(pac::stream::RawOutput& out);
     public:
      Checksum(pac::Packet* p);
      Checksum(pac::Packet* p,const Checksum& cs);
      bool isOK();
    };
    
    static const unsigned char PROT=6;
    Packet();
    Packet(const Packet& p);
    ip4::Packet ip4;
    
    struct Port
    {
     pac::Short src;
     pac::Short dst;
     Port(Packet* p):src(p,(unsigned short)0),dst(p,(unsigned short)0){}
     Port(Packet* p,const Port& prt):src(p,prt.src),dst(p,prt.dst){}
     bool to(const Port& p);
    };
    
    Port prt;
    Seq           seqN;
    Seq           ackN;
    DataOffset    ofs;
    pac::Bits<6>  res;
    pac::Flag     urgF;
    pac::Flag     ackF;
    pac::Flag     pshF;
    pac::Flag     rstF;
    pac::Flag     synF;
    pac::Flag     finF;
    pac::Short    wnd;
    Checksum      chk;
    pac::Short    urg;
    Option::List  opt;
    pac::Padding  pad;
    pac::Payload  pld;
    pac::End      end;
//some shows
    io::ascii::Writer& showConnection(io::ascii::Writer&)const;
                        //src:port->dst:port
    io::ascii::Writer& showFlags(io::ascii::Writer&)const;
                        //[flags]			
    io::ascii::Writer& showSeq(io::ascii::Writer&)const;
                        //src:port->dst:port <ack seq> flags
    io::ascii::Writer& show(io::ascii::Writer&)const;
                        //all
    bool co(const Packet& p) const
         {
	  return ip4.co(p.ip4)&&(prt.src==p.prt.src) && (prt.dst==p.prt.dst);
	 }
    bool aco(const Packet& p) const
         {
	  return ip4.aco(p.ip4)&&(prt.dst==p.prt.src) && (prt.src==p.prt.dst);
	 }
			
  };
 }
}

#endif

