//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#ifndef NIO_TCP_RANDOM_PORT_POOL_H
#define NIO_TCP_RANDOM_PORT_POOL_H
//------------------------------
//RandomPortPool
//(c) H.Buchmann FHSO 2005
//$Id: random-port-pool.h 193 2006-01-11 15:21:16Z buchmann $
//------------------------------
INTERFACE(nio_tcp_random_port_pool,$Revision: 137 $)
#include "tcp/protocol.h"

namespace nio
{
 namespace tcp
 {
  class RandomPortPool:public Port::Pool
  {
   private:
    static const unsigned POOL_SIZE=1<<16;
    Port* pool[POOL_SIZE];

   public:
     RandomPortPool();
    ~RandomPortPool();
    Port* create(Port::Listener&); 
    Port* create(Port::ID at,Port::Listener&); 
    void remove(Port* c);
    Port* get(Packet& p){return pool[p.prt.dst];}
  };
 }//namespace tcp
}//namespace nio
#endif
