//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//---------------------------------
//RX handles incoming tcp packets global object
//(c) H.Buchmann FHSO 2005
//$Id: protocol.cc 193 2006-01-11 15:21:16Z buchmann $
//---------------------------------
#include "sys/sys.h"
IMPLEMENTATION(nio_tcp_protocol,$Revision: 137 $)
#include "host.h"
#include "tcp/protocol.h" 
#include "tcp/random.h"
#include "sys/msg.h"

namespace nio
{
 namespace tcp
 {
//-------------------------------------- Port
  Port::Port(Pool& owner,ID id,Listener& li)
  :id(id),
   li(li),
   owner(owner)
  {
  }
  
  io::ascii::Writer&  Port::show(io::ascii::Writer& out) const
  {
   if (id>=0) out<<io::ascii::dec()<<id;
      else    out<<" - ";
   return out;   
  }

//-------------------------------------- Connection::Listener
  Connection::Listener::~Listener()
  {
  }

//-------------------------------------- Connection
  Connection::Connection(Host& host,Listener& li,
   	                 const arp::Pair& dst,Port::ID id)
  :host(host),
   li(li),
   dst(dst),
   dstPort(id),
   srcPort(host.portPool.create(*this)),
   tx(Random::number())
   
  {
   sync();
  }

  Connection::~Connection()
  {
  }

  io::ascii::Writer& Connection::show(io::ascii::Writer& out) const
  {
   ip4::Address::show(out,host.src)<<":"<<srcPort->getID()
                                   <<"->";
   ip4::Address::show(out,dst.pr)<<":"<<dstPort<<"\n";	   
   return out;
  }

  void Connection::send(Packet& p)
  {
   p.ip4.eth.dst=dst.hw;
   p.ip4.dst=dst.pr;
   p.prt.src=srcPort->getID();
   p.prt.dst=dstPort;
   p.wnd=1500;
   host<<p;
  }

  void Connection::send(const unsigned char data[],unsigned len)
  {
   Packet* pac=new Packet;
   pac->ackN=ackN;
   pac->ackF=true;
   pac->pld.set(data,len);
   send(tx.front(*pac));
  }

  void Connection::sync()
  {
   sys::msg<<"----------------------------- sync\n";
   Packet syncP;
   syncP.synF=true;
   syncP.ackN=0;
   syncP.seqN=tx.getISN();
   send(syncP);
   action=&Connection::onSync;
  }

  void Connection::close()
  {
   Packet* fin=new Packet;
   fin->ackN=ackN;
   fin->ackF=true;
   fin->finF=true;
   send(tx.front(*fin));
   action=&Connection::onFin;  
  }

  void Connection::onFin(Packet& p)
  {
   sys::msg<<"----------------------------- onFin\n";
   Packet fin;
   fin.ackN=p.seqN+1;
   fin.ackF=true;
   fin.seqN=p.ackN;
   send(fin);
   action=0;   
  }
  
  void Connection::onSync(Packet& p)
  {
   sys::msg<<"----------------------------- onSync\n";

   Packet* ack=new Packet;
   ackN=p.seqN+1;
   ack->ackF=true;
   ack->ackN=ackN;
   send(tx.front(*ack));
   action=&Connection::onAck; 
   close();
  }

  void Connection::onAck(Packet& p)
  {
   sys::msg<<"----------------------------- onAck\n";
   tx.ack(p.ackN);
   tx.show(sys::msg<<"--------------TXSequence\n");
  }
  
  void Connection::onPacket(tcp::Packet& pac)
  {
//   sys::msg<<"----------------------- connection\n"<<pac<<"\n";
   if (action) (this->*action)(pac);
  }
   
//-------------------------------------- Host
  Host::Host(const char src[],Port::Pool& portPool)
  :thread(this),
   net(nio::Host::getNet()),
   mac(nio::Host::getMAC()),
   portPool(portPool)
  {
   net.logon();
   ip4::Address::copyExit(src,this->src);
   thread.start();
   start.wait(); //until thread started
  }

  void Host::run()
  {
   sys::msg<<"starting Host\n";
   start.open();
   while(true)
   {
    Packet pac;
    net>>pac;
    pac.showSeq(sys::msg<<"------------------------- rx\n")<<"\n";
    if (pac.ip4.dst!=src) continue;
    Port* p=portPool.get(pac);
    if (p) p->li.onPacket(pac);    
   }   
  }

  Connection* Host::create(Connection::Listener& li,
                           arp::Pair& dst,
 			   Port::ID id)
  {
   return new Connection(*this,li,dst,id);
  }
  
  Host& Host::operator<<(Packet& pac)
  {
   pac.ip4.eth.src=mac;
   pac.ip4.src=src;
   pac.showSeq(sys::msg<<"------------------------- tx\n")<<"\n";
   net<<pac;
   return *this;
  }

 }//namespace tcp
}//namespace nio
