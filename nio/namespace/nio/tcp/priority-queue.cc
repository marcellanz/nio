//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//-------------------------------
//priority-queue
//(c) H.Buchmann FHSO 2003
//$Id: priority-queue.cc 193 2006-01-11 15:21:16Z buchmann $
//-------------------------------
#include "sys/sys.h"
IMPLEMENTATION(priority_queue,$Revision: 137 $)
#include "sys/msg.h"
#include "tcp/tcp.h"
#include <queue>
#include <vector>
namespace nio
{
 struct Cmp
 {
  bool operator()(tcp::Packet* p0,tcp::Packet* p1) const
  {
   return p0->seqNbr.get()<p1->seqNbr.get();
  }
 };
 
 
 class PriorityQueue
 {
  static PriorityQueue pq;
  typedef std::priority_queue<tcp::Packet*,
                              std::vector<tcp::Packet*>, 
			      Cmp> PQueue;
  PQueue queue;
  PriorityQueue();
  void push(unsigned sNbr);
 };

 void PriorityQueue::push(unsigned sNbr)
 {
  tcp::Packet* p=new tcp::Packet;
  p->seqNbr=sNbr;
  sys::msg<<"push\n"<<*p<<"\n";
  queue.push(p);
 }

 PriorityQueue PriorityQueue::pq;

 PriorityQueue::PriorityQueue()
 {
  sys::msg<<"----------------------\n";
  push(0);
  push(1);
  push(2);
  while(!queue.empty())
  {
   sys::msg<<"pop\n"<<*queue.top()<<"\n";
   queue.pop();
  }
 }
}
