//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//-------------------------------------
//tcp_layer
//(c) D.Tschanz FHSO 2003
// $Id: layer.cc 193 2006-01-11 15:21:16Z buchmann $
//-------------------------------------
#include "sys/sys.h"
IMPLEMENTATION(nio_tcp_layer,$Revision: 137 $)
#include "layer.h"

namespace nio
{
 namespace layer
 {
  bool TCB::isActiveOpen()
  {
   char test[]={0,0,0,0};
   if(packet.prt.dst!=0 && packet.ip4.dst!= test)return true;
   return false;
  }  

  u_int32_t TCB::generateISS(){
   timeval time;
   gettimeofday(&time,NULL);
   iss = (time.tv_usec>>2) + (time.tv_sec*250000);
   sndUna = iss;
//FIXME: Modulo add
   sndNxt = iss+1;
   return iss;
  }

  void TCB::copyAddresses(tcp::Packet& aPacket)
  {
   ip4::Address::RAW raw;
   eth::Address::RAW raw2;
   aPacket.prt.src = packet.prt.src;
   aPacket.prt.dst = packet.prt.dst;
   packet.ip4.src.get(raw);
   aPacket.ip4.src.set(raw);
   packet.ip4.dst.get(raw);
   aPacket.ip4.dst.set(raw);
   packet.ip4.eth.src.get(raw2);
   aPacket.ip4.eth.src.set(raw2);
   packet.ip4.eth.dst.get(raw2);
   aPacket.ip4.eth.dst.set(raw2);
   aPacket.wnd = rcvWnd;
  }

  bool TCB::compareAddresses(tcp::Packet& aPacket)
  {
   if( (packet.prt.src==aPacket.prt.dst)&&
       (packet.prt.dst==aPacket.prt.src)&&
       (packet.ip4.src==aPacket.ip4.dst)&&
       (packet.ip4.dst==aPacket.ip4.src)&&
       (packet.ip4.eth.src==aPacket.ip4.eth.dst)&&
       (packet.ip4.eth.dst==aPacket.ip4.eth.src) )
   {
   return true;
   }
   return false;
  }

  void TCB::updateSnd(tcp::Packet& aPacket)
  {
   if(aPacket.seqN>sndWl1 ||
      ((aPacket.seqN==sndWl1)&&(aPacket.ackN>=sndWl2))
   )
   {
    sndWnd=packet.wnd.get();
    sndWl1=packet.seqN.get();
    sndWl2=packet.ackN.get();
   }
  }

  void TCB::updateRcv(tcp::Packet& aPacket)
  {
//FIXME: Modulo add
//   rcvWnd+=aPacket.payload.getLen();
   rcvWnd=aPacket.wnd.get();
   rcvNxt+=aPacket.pld.getLen();
   if(aPacket.finF)rcvNxt+=1;
  }

  Connection::Connection() :
    state(CLOSED), error(NOERROR),net(Host::getNet()),tcb(NULL)
  {
   sem_init(&connectSem,0,0);
   sem_init(&closeSem,0,0);
   sem_init(&receivedSem,0,0);
   pthread_mutex_init(&sendMutex,NULL);
   pthread_mutex_init(&receiveMutex,NULL);
   pthread_create(&dispatcher,NULL,DispatcherLoop,this);
   pthread_create(&reader,NULL,ReaderLoop,this);
  }

  Connection::~Connection(){
   pthread_cancel(dispatcher);
   pthread_cancel(reader);
   pthread_mutex_destroy(&receiveMutex);
   pthread_mutex_destroy(&sendMutex);
   sem_destroy(&receivedSem);
   sem_destroy(&closeSem);
   sem_destroy(&connectSem);
  }

  void Connection::flushSend()
  {
   std::vector<tcp::Packet*>::iterator iter;
   pthread_mutex_lock(&sendMutex);
   for(iter=sendQueue.begin();iter!=sendQueue.end();++iter)
   {
    if((*iter)->pld.getLen()<= tcb->sndWnd)
    {
     tcb->sndWnd-=(*iter)->pld.getLen();
     (*iter)->ackF=1;
     (*iter)->ackN=tcb->rcvNxt;
     (*iter)->seqN=tcb->sndNxt;
     (*iter)->prt.dst=tcb->packet.prt.dst;
     (*iter)->prt.src=tcb->packet.prt.src;
     (*iter)->wnd=tcb->rcvWnd;
//FIXME Modulo add
     tcb->sndNxt=(*iter)->pld.getLen()+tcb->sndNxt;
     net.send(**iter);
     delete *iter;
    }
    else
    {
   pthread_mutex_unlock(&sendMutex);
     return;
    }
   }
   pthread_mutex_unlock(&sendMutex);
  }

  void Connection::printLastError()
  {
   switch(error)
   {
    case NOERROR:
     sys::msg <<"No error\n";
    break;
    case ERROR:
     sys::msg <<"General error\n";
    break;
    case ALREADYEXISTS:
     sys::msg <<"Connection already exists\n";
    break;
    case CONNECTIONNOTEXISTS:
     sys::msg <<"Connection does not exist\n";
    break;
    case CONNECTIONCLOSING:
     sys::msg <<"Connection closing\n";
    break;
    case INSUFFICIENTRESOURCES:
     sys::msg <<"Insufficient resources\n";
    break;
    case CONNECTIONRESET:
     sys::msg <<"Connection reset\n";
    break;
    case REFUSED:
     sys::msg <<"Connection refused\n";
    break;
    default:
     sys::msg <<"Unknown error\n";
    break;
   }
  }

  void Connection::addListener(Listener& aListener)
  {
   listeners.push_back(&aListener);
  }

  void* Connection::ReaderLoop(void* aConnection)
  {
   pthread_setcancelstate(PTHREAD_CANCEL_ASYNCHRONOUS,NULL);
   tcp::Packet pac;
   Connection* that = (Connection*)aConnection;
   while(1)
   {
    that->net >> pac;
    if(that->tcb && that->tcb->compareAddresses(pac))
    {
//that->dumpState();
     that->segment(pac);
    }
   }
   return NULL;
  }

  void* Connection::DispatcherLoop(void* aConnection)
  {
   pthread_setcancelstate(PTHREAD_CANCEL_ASYNCHRONOUS,NULL);
   Connection* that = (Connection*)aConnection;
   tcp::Packet* pac;
   std::vector<Listener*>::iterator iter;
   while(1)
   {
    sem_wait(&that->receivedSem);
    pthread_mutex_lock(&that->receiveMutex);
    pac = that->receiveQueue[0];
    that->receiveQueue.pop_back();
    pthread_mutex_unlock(&that->receiveMutex);
    for(iter=that->listeners.begin();iter!=that->listeners.end();++iter)
    {
     (*iter)->call(*pac);
    }
    delete pac;
   }
  }

  Connection::Error Connection::event(Event aEvent,void* para)
  {
   switch(this->state){
     case CLOSED:{
      switch(aEvent){
       case OPEN:{
        tcb = new TCB(*(tcp::Packet*)para);
        if(tcb->isActiveOpen())
        {
         tcp::Packet pac;
         tcb->copyAddresses(pac);
         pac.synF = 1;
         pac.seqN = tcb->generateISS();
         pac.wnd = tcb->rcvWnd;
         state = SYNSENT;
         net.send(pac);
         sem_wait(&connectSem);
         return error;
        }
        else
        {
         state = LISTEN; 
         sem_wait(&connectSem);
         return error;
        }
       }
       break;
       case SEND:
       case RECEIVE:
       case CLOSE:
       case ABORT:
       case STATUS:
        error = CONNECTIONNOTEXISTS;
        return error;
       break;
       case SEGMENT:{
        tcp::Packet* packet =(tcp::Packet*)para;
        if(packet->rstF)
        {
//Discard Packet
        }
        else
        {
//Send Reset
         tcp::Packet pac;
         tcb->copyAddresses(pac);
         pac.rstF=true;
         if(!packet->ackF)
         {
//Send Reset with AckNbr 0
          pac.seqN=0;
          pac.ackF=1;
         }
         else
         {
//Send Reset with received AckNbr
          pac.seqN=packet->ackN;
         }
         net.send(pac);
        }
       }
       break;
      }
     }
     break;
     case LISTEN:{
      switch(aEvent){
       case OPEN:
       case SEND:
       case RECEIVE:
       case CLOSE:
       case ABORT:
       case STATUS:
break;
       case SEGMENT:{
        tcp::Packet* packet =(tcp::Packet*)para;
//ignore incoming resets
        if(packet->rstF)break;
//send a reset packet
        if(packet->ackF)
        {
         tcp::Packet pac;
         tcb->copyAddresses(pac);
         pac.rstF=true;
         pac.seqN=packet->ackN;
         net.send(pac);
         break;
        }
        if(packet->synF)
        {
//FIXME: Security/Compartment is missing
         tcb->rcvNxt=packet->seqN+1;
         tcb->irs=packet->seqN;
         tcb->generateISS();
         tcp::Packet pac;
         tcb->copyAddresses(pac);
         pac.seqN = tcb->iss;
         pac.ackN = tcb->rcvNxt;
         pac.synF = 1;
         pac.ackF = 1;
         tcb->sndNxt=tcb->iss+1;
         tcb->sndUna=tcb->iss;
         appendToReceive(packet);
         net.send(pac);
         state=SYNRECEIVED;
         break;
        }
       }
       break;
       default:
       break;
      }
     }
     break;
     case SYNSENT:{
      switch(aEvent){
       case OPEN:
        return ALREADYEXISTS;
       break;
       case SEND:{
        if(!appendToSend((tcp::Packet*)para))
         return INSUFFICIENTRESOURCES;
       }
       break;
       case RECEIVE:
       case CLOSE:
       case ABORT:
       case STATUS:
       break;
       case SEGMENT:{
        tcp::Packet* packet =(tcp::Packet*)para;
        if(packet->ackF)
        {
//Check ack
         if((packet->ackN<=tcb->iss)||(packet->ackN>tcb->sndNxt) )
         {
//Send reset
          if(packet->rstF)
          {
//Drop it
           break;
          }
          else
          {
           tcp::Packet pac;
           tcb->copyAddresses(pac);
           pac.seqN=packet->ackN;
           pac.rstF=true;
           net.send(pac);
          }
          if((packet->ackN>=tcb->sndUna)&&(packet->ackN<=tcb->sndNxt))
          {
//Accepted
          }
          else
          {
           break;
          }
         }
        }
//Check reset
        if(packet->rstF)
        {
//Drop it
//         delete tcb;
//         tcb = NULL;
         state=CLOSED;
         error=CONNECTIONRESET;
         sem_post(&connectSem);
         return error;
        }
//FIXME: Security and precedence check missing
//Check syn
        if(packet->synF)
        {
         tcb->rcvNxt=packet->seqN+1;
         tcb->irs=packet->seqN;
         if(packet->ackF)
         {
          tcb->sndUna=packet->ackN;
          removeFromSend(tcb->sndUna);
         }
//Check if our syn has been acked
         if(tcb->sndUna>tcb->iss)
         {
          state=ESTABLISHED;
          tcp::Packet pac;
          tcb->copyAddresses(pac);
          pac.seqN=tcb->sndNxt;
          pac.ackN=tcb->rcvNxt;
          pac.ackF=true;
          net.send(pac);
          error=NOERROR;
          sem_post(&connectSem);
          return error;
         }
        }
       }
       break;
       default:
       break;
      }
     }
     break;
     case SYNRECEIVED:{
      switch(aEvent){
       case OPEN:
        return ALREADYEXISTS;
       break;
       case SEND:{
        if(!appendToSend((tcp::Packet*)para))
         return INSUFFICIENTRESOURCES;
       }
       break;
       case RECEIVE:
       case CLOSE:
       case ABORT:
       case STATUS:
       break;
       case SEGMENT:{
        tcp::Packet* packet =(tcp::Packet*)para;
//Check seqN
        if(!isInRcvWnd(*packet))
        {
         break;
        }
        if(packet->rstF)
        {
         cleanSendQueue();
//         delete tcb;
//         tcb=NULL;
         state=CLOSED;
         error=REFUSED;
         sem_post(&connectSem);
         return error;
        }
        if(isSynRst(packet))return CONNECTIONRESET;
        if(!packet->ackF)
        {
//Drop it
         break;
        }
        else
        {
         if((tcb->sndUna<=packet->ackN) && (packet->ackN<=tcb->sndNxt))
         {
          state=ESTABLISHED;
          error=NOERROR;
          sem_post(&connectSem);
          break;
         }
         else
         {
//Not accepted send reset
          tcp::Packet pac;
          tcb->copyAddresses(pac);
          pac.seqN=packet->ackN.get();
          pac.rstF=true;
          net.send(pac);
         }
        }
       }
       break;
       default:
        break;
      }
     }
     break;
     case ESTABLISHED:{
      switch(aEvent){
       case OPEN:
        return ALREADYEXISTS;
       break;
       case SEND:{
        if(!appendToSend((tcp::Packet*)para))
        {
         return INSUFFICIENTRESOURCES;
        }
       }
       break;
       case RECEIVE:
       case CLOSE:{
        flushSend();
        sendFin();
        state=FINWAIT1;
        sem_wait(&closeSem);
       }
       break; 
       case ABORT:
       case STATUS:
       break;
       case SEGMENT:{
        tcp::Packet* packet =(tcp::Packet*)para;
        if(!isInRcvWnd(*packet))
        {
         break;
        }
        if(packet->rstF)
        {
         flushSend();
         cleanReceiveQueue();
         sem_post(&closeSem);
         state=CLOSED;
         return CONNECTIONRESET;
        }
        if(isSynRst(packet))return CONNECTIONRESET;
        if(packet->ackF)
        {
         if((tcb->sndUna<packet->ackN)&&(packet->ackN<=tcb->sndNxt))
         {
          u_int32_t temp = packet->ackN;
          removeFromSend(temp);
          packet->ackN=temp;
          tcb->updateSnd(*packet);
         }
         appendToReceive(packet);
         tcb->updateRcv(*packet);
         sendAck();
        }
//Drop it
       }
       break;
       default:
        break;
      }
     }
     break;
     case FINWAIT1:{
      switch(aEvent){
       case OPEN:
        return ALREADYEXISTS;
       break;
       case SEND:
        return CONNECTIONCLOSING;
       break;
       case RECEIVE:
       case CLOSE:
       case ABORT:
       case STATUS:
       break;
       case SEGMENT:{
        tcp::Packet* packet =(tcp::Packet*)para;
        if(!isInRcvWnd(*packet))
        {
//Drop it
         break;
        }
        if(packet->rstF)
        {
         flushSend();
         cleanReceiveQueue();
         sem_post(&closeSem);
         state=CLOSED;
         return CONNECTIONRESET;
        }
        if(isSynRst(packet))return CONNECTIONRESET;
        if(packet->ackF)
        {
         if((tcb->sndUna<packet->ackN)&&(packet->ackN<=tcb->sndNxt))
         {
          u_int32_t temp = packet->ackN;
          removeFromSend(temp);
          packet->ackN=temp;
          tcb->updateSnd(*packet);
//Check if our fin has been acked
          if((packet->ackN==(tcb->sndNxt)))
          {
//sys::msg<<"fin has acked change to wait2 "<<tcb->sndNxt<<"\n";
           appendToReceive(packet);
           tcb->updateRcv(*packet);
           state=FINWAIT2;
           break;
          }
         }
         appendToReceive(packet);
         tcb->updateRcv(*packet);
         sendAck();
        }
        if(packet->finF) 
        {
         state=CLOSEING;
        }
       }
       break;
       default:
        break;
      }
     }
     break;
     case FINWAIT2:{
      switch(aEvent){
       case OPEN:
        return ALREADYEXISTS;
       break;
       case SEND:
        return CONNECTIONCLOSING;
       break;
       case RECEIVE:
       case CLOSE:
       case ABORT:
       case STATUS:
       break;
       case SEGMENT:{
        tcp::Packet* packet =(tcp::Packet*)para;
        if(!isInRcvWnd(*packet))
        {
         break;
        }
        if(packet->rstF)
        {
         flushSend();
         cleanReceiveQueue();
         sem_post(&closeSem);
         state=CLOSED;
         return CONNECTIONRESET;
        }
        if(isSynRst(packet))return CONNECTIONRESET;
        if(packet->ackF)
        {
         if((tcb->sndUna<packet->ackN)&&(packet->ackN<=tcb->sndNxt))
         {
          u_int32_t temp = packet->ackN;
          removeFromSend(temp);
          packet->ackN=temp;
          tcb->updateSnd(*packet);
         }
         appendToReceive(packet);
         tcb->updateRcv(*packet);
         sendAck();
        }
        if((packet->finF))
        {
         appendToReceive(packet);
          sendAck();
//           state=TIMEWAIT;
           state=CLOSED;
          sem_post(&closeSem);
         }
       }
       break;
       default:
        break;
      }
     }
     break;
     case CLOSEWAIT:{
      switch(aEvent){
       case OPEN:
        return ALREADYEXISTS;
       break;
       case SEND:{
        if(!appendToSend((tcp::Packet*)para)){ 
         return INSUFFICIENTRESOURCES;
        }
        flushSend();
       }
       break;
       case RECEIVE:
       case CLOSE:
       case ABORT:
       case STATUS:
       break;
       case SEGMENT:{
        tcp::Packet* packet =(tcp::Packet*)para;
        if(!isInRcvWnd(*packet))
        {
         break;
        }
        if(packet->rstF)
        {
         flushSend();
         cleanReceiveQueue();
         sem_post(&closeSem);
         state=CLOSED;
         return CONNECTIONRESET;
        }
        if(isSynRst(packet))return CONNECTIONRESET;
        if(packet->ackF)
        {
         if((tcb->sndUna<packet->ackN)&&(packet->ackN<=tcb->sndNxt))
         {
          u_int32_t temp = packet->ackN;
          removeFromSend(temp);
          packet->ackN=temp;
          tcb->updateSnd(*packet);
         }
        }
       }
       break;
       default:
        break;
      }
     }
     break;
     case CLOSEING:{
      switch(aEvent){
       case OPEN:
        return ALREADYEXISTS;
       break;
       case SEND:
        return CONNECTIONCLOSING;
       break;
       case RECEIVE:
       case CLOSE:
       case ABORT:
       case STATUS:
       break;
       case SEGMENT:{
        tcp::Packet* packet =(tcp::Packet*)para;
        if(!isInRcvWnd(*packet))
        {
         break;
        }
        if(packet->rstF)
        {
         sem_post(&closeSem);
         state=CLOSED;
//         delete tcb;
//         tcb=NULL;
         break;
        }
        else
        {
         sendAck();
        }
        if(isSynRst(packet))return CONNECTIONRESET;
        if(packet->ackF)
        {
         if((tcb->sndUna<packet->ackN)&&(packet->ackN<=tcb->sndNxt))
         {
          u_int32_t temp = packet->ackN;
          removeFromSend(temp);
          packet->ackN=temp;
          tcb->updateSnd(*packet);
         }
//Check for fin acked
         if((packet->ackN==(tcb->sndNxt-1)))
         {
          state=TIMEWAIT;
          sem_post(&closeSem);
          break;
         }else break;
        }
       }
       break;
       default:
        break;
      }
     }
     break;
     case LASTACK:{
      switch(aEvent){
       case OPEN:
        return ALREADYEXISTS;
       break;
       case SEND:
        return CONNECTIONCLOSING;
       break;
       case RECEIVE:
       case CLOSE:
       case ABORT:
       case STATUS:
       break;
       case SEGMENT:{
        tcp::Packet* packet =(tcp::Packet*)para;
        if(!isInRcvWnd(*packet))
        {
         break;
        }
        if(packet->rstF)
        {
         sem_post(&closeSem);
         state=CLOSED;
//         delete tcb;
//         tcb=NULL;
         break;
        }
        if(isSynRst(packet))return CONNECTIONRESET;
        if(packet->ackF)
        {
         if((packet->ackN==(tcb->sndNxt-1)))
         {
           state=CLOSED;
           sem_post(&closeSem);
           break;
         }else break;
        }
       }
        break;
       default:
        break;
      }
     }
     break;
     case TIMEWAIT:{
      switch(aEvent){
       case OPEN:
        return ALREADYEXISTS;
       break;
       case SEND:
        return CONNECTIONCLOSING;
       break;
       case RECEIVE:
       case CLOSE:
       case ABORT:
       case STATUS:
       break;
       case SEGMENT:{
        tcp::Packet* packet =(tcp::Packet*)para;
        if(!isInRcvWnd(*packet))
        {
         break;
        }
        if(packet->rstF)
        {
         sem_post(&closeSem);
         state=CLOSED;
//         delete tcb;
//         tcb=NULL;
         break;
        }
//        else
//        {
//         sendAck();
//        }
        if(isSynRst(packet))return CONNECTIONRESET;
        if(packet->ackF)
        {
         if((packet->ackN==(tcb->sndNxt-1)))
         {
           state=CLOSED;
           sem_post(&closeSem);
           break;
         }else break;
        }
       }
       break;
       default:
        break;
      }
     }
     break;
     default:
     break;
   }
   return NOERROR;
  }

  bool Connection::appendToSend(tcp::Packet* aPacket)
  {
   if(aPacket->pld.getLen()<= tcb->sndWnd)
   {
    tcb->sndWnd-=aPacket->pld.getLen();
    tcp::Packet* packet = new tcp::Packet(*aPacket);
    pthread_mutex_lock(&sendMutex);
    sendQueue.push_back(packet);
    pthread_mutex_unlock(&sendMutex);
    return true;
   }
   return false;
  }

  bool Connection::appendToReceive(tcp::Packet* aPacket)
  {
   if(aPacket->pld.getLen()<= tcb->rcvWnd)
   {
    tcb->rcvWnd-=aPacket->pld.getLen();
    if(!aPacket->pld.getLen())return false;
    tcp::Packet* packet = new tcp::Packet(*aPacket);
    pthread_mutex_lock(&receiveMutex);
    receiveQueue.push_back(packet);
    pthread_mutex_unlock(&receiveMutex);
    sem_post(&receivedSem);
    return true;
   }
   return false;
  }

  void Connection::cleanSendQueue()
  {
   std::vector<tcp::Packet*>::iterator iter;
   pthread_mutex_lock(&sendMutex);
   for(iter=sendQueue.begin();iter!=sendQueue.end();++iter)
   {
    delete *iter;
   }
   pthread_mutex_unlock(&sendMutex);
  }

  void Connection::cleanReceiveQueue()
  {
   std::vector<tcp::Packet*>::iterator iter;
   for(iter=receiveQueue.begin();iter!=receiveQueue.end();++iter)
   {
    delete *iter;
   }
  }

  void Connection::removeFromSend(u_int32_t& ack)
  {
   bool changed = false;
   std::vector<tcp::Packet*>::iterator iter;
   pthread_mutex_lock(&sendMutex);
   for(iter=sendQueue.begin();iter!=sendQueue.end();++iter)
   {
    if((*iter)->seqN<=ack)
    {
     if(((*iter)->seqN.get()+(*iter)->pld.getLen())<=ack)
     {
//      delete *iter;
     }
     else
     {
      ack=(*iter)->seqN.get();
     }
    }
   }
   pthread_mutex_unlock(&sendMutex);
  }

  bool Connection::isInRcvWnd(tcp::Packet& aPacket)
  {
   if( (!aPacket.pld.getLen() && !tcb->rcvWnd)&&
       (aPacket.seqN==tcb->rcvNxt))
   {
    return true;
   }
   if( (!aPacket.pld.getLen() && tcb->rcvWnd>0)&& (
        (aPacket.seqN>=tcb->rcvNxt)&&
        (aPacket.seqN<=tcb->rcvNxt+tcb->rcvWnd) )
     )
   {
    return true;
   }
   int load = aPacket.seqN+aPacket.pld.getLen()-1;
   int wnd = tcb->rcvNxt+tcb->rcvWnd;
   if( (aPacket.pld.getLen() && tcb->rcvWnd)&& (
        ((tcb->rcvNxt<=aPacket.seqN)&&(aPacket.seqN<wnd))||
        ((tcb->rcvNxt<=load)&&(load<wnd))
        )
     )
   {
    return true;
   }
   //send ack and drop it
   if(!aPacket.rstF)
    sendAck();
   return false; 
  }

  void Connection::sendAck()
  {
   tcp::Packet pac;
   tcb->copyAddresses(pac);
   pac.seqN=tcb->sndNxt;
   pac.ackN=tcb->rcvNxt;
   pac.ackF=true;
   net.send(pac);
  }

  bool Connection::isSynRst(tcp::Packet* aPacket)
  {
   if(aPacket->synF)
   {
    tcp::Packet pac;
    tcb->copyAddresses(*aPacket);
    pac.rstF=true;
    if(!aPacket->ackF)
    {
     pac.seqN=aPacket->seqN;
    }
    net.send(pac);
    flushSend();
    cleanReceiveQueue();
    return true;
   }
  return false;
  }

  void Connection::sendFin()
  {
    tcp::Packet pac;
    tcb->copyAddresses(pac);
    pac.finF = 1;
//    state=FINWAIT1;
    pac.seqN=tcb->sndNxt;
    pac.ackN=tcb->rcvNxt;
    net.send(pac);
tcb->sndNxt+=1;//For fin
  }

  ConnectionHandler::ConnectionHandler(){
  }
  ConnectionHandler::~ConnectionHandler(){
  }

  Connection* ConnectionHandler::getConnection(tcp::Packet& aID){
/*
   std::map<std::string,Connection*>::iterator pos;
   pos = connections.find(aID);
   if(pos.end())
   {
    createConnection(aID);
   }
   return connections[aID];
*/
  }
  Connection* ConnectionHandler::createConnection(tcp::Packet& aID){
/*
   std::string* str = new std::string();
   convertSocket(aID, *str);
   connections[*str] = new connection(aID);
   return connections[*str];
*/   
  }
  Connection* ConnectionHandler::deleteConnection(tcp::Packet& aID){
  }
  void ConnectionHandler::dispatch(pac::Packet& aPacket){
  }

  void ConnectionHandler::convertSocket(tcp::Packet& aPacket,std::string& aSocket){
/*
    unsigned char data[4];
    aSocket = aPacket.ip4.src.get(data) + aPacket.port.src +
              aPacket.ip4.dst.get(data) + aPacket.port.dst;
*/
  }

 }
}
