//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#ifndef NIO_TCP_LAYER_H
#define NIO_TCP_LAYER_H
//-------------------------------------
//tcp_layer
//(c) D.Tschanz FHSO 2003
// $Id: layer.h 193 2006-01-11 15:21:16Z buchmann $
//-------------------------------------
INTERFACE(nio_tcp_layer,$Revision: 137 $)
#include <string>
#include <map>
#include "pac/pac.h"
#include "tcp.h"
#include <sys/types.h>
#include <semaphore.h>
#include <pthread.h>
#include <sys/types.h>
#include "host.h"

namespace nio
{
 namespace layer
 {
  class TCB
  {
  public:
   TCB(tcp::Packet& aPacket): packet(aPacket),sndWnd(1000),rcvWnd(1000){}
   tcp::Packet packet;
   u_int32_t iss;
   u_int32_t sndUna;  //send unacknowledged
   u_int32_t sndNxt;  //send next
   u_int16_t sndWnd;  //send window
   u_int32_t sndUp;   //send urgent pointer
   u_int16_t sndWl1;  //segment sequence number used for last window update
   u_int16_t sndWl2;  //segment acknowledgment number used for last window update

   u_int32_t rcvNxt;  //receive next
   u_int16_t rcvWnd;  //receive window
   u_int32_t rcvUp;   //receive urgent pointer
   u_int32_t irs;     //initial receive sequence number
   bool isActiveOpen();
   bool isPassive(){return !isActiveOpen();}
   u_int32_t generateISS();
   void copyAddresses(tcp::Packet& aPacket);
   bool compareAddresses(tcp::Packet& aPacket);
   void updateSnd(tcp::Packet& aPacket);
   void updateRcv(tcp::Packet& aPacket);
//UserTimeout
  };

  class Listener
  {
  public:
   Listener(void (*aCallBack)(tcp::Packet& aPacket)) : callBack(aCallBack){}
   void call(tcp::Packet& aPacket){callBack(aPacket);}
  protected:
  private:
   void (*callBack)(tcp::Packet& aPacket);
  };

  class Connection
  {
  public:
   enum Event
   {
    OPEN,
    SEND,
    RECEIVE,
    CLOSE,
    ABORT,
    STATUS,

    SEGMENT,

    USERTIMEOUT,
    RETRANSMITTIMEOUT,
    TIMEWAITTIMEOUT
   };
   enum Error
   {
    NOERROR,ERROR,ALREADYEXISTS,CONNECTIONNOTEXISTS,CONNECTIONCLOSING,
    INSUFFICIENTRESOURCES,CONNECTIONRESET,REFUSED
   };
   Connection();
   virtual ~Connection();
   void printLastError();
   Error open(tcp::Packet* aPacket){return event(OPEN,aPacket);}
   Error send(tcp::Packet* aPacket){return event(SEND,aPacket);}
   Error receive(tcp::Packet* aPacket){return event(RECEIVE,aPacket);}
   Error close(){return event(CLOSE,NULL);}
   Error abort(){return event(ABORT,NULL);}
   Error status(){return event(STATUS,NULL);}
   Error segment(tcp::Packet& aPacket){return event(SEGMENT,&aPacket);}
   Error userTimeout(){return event(USERTIMEOUT,NULL);}
   Error retransmissionTimeout(){return event(RETRANSMITTIMEOUT,NULL);}
   Error timeWaitTimeout(){return event(TIMEWAITTIMEOUT,NULL);}
//debug section
   void dumpState(){sys::msg<<"Current state is: "<<state<<"\n";};
   void addListener(Listener& aListener);

  protected:
  private:
   static void* ReaderLoop(void* aConnection);
   static void* DispatcherLoop(void* aConnection);
   Error event(Event aEvent,void* para);
   void flushSend();
   bool appendToSend(tcp::Packet* aPacket);
   void cleanSendQueue();
   bool appendToReceive(tcp::Packet* aPacket);
   void cleanReceiveQueue();
   void removeFromSend(u_int32_t& ack);
   bool isInRcvWnd(tcp::Packet& aPacket);
   void sendAck();
   bool isSynRst(tcp::Packet* aPacket);
   void sendFin();
   void sndRst();
   enum State
   {
    LISTEN,
    SYNSENT,
    SYNRECEIVED,
    ESTABLISHED,
    FINWAIT1,
    FINWAIT2,
    CLOSEWAIT,
    CLOSEING,
    LASTACK,
    TIMEWAIT,
    CLOSED
   };
   State state;
   Error error;
   pac::Net& net;
   TCB* tcb;
   std::vector<tcp::Packet*> sendQueue;
   std::vector<tcp::Packet*> receiveQueue;
   sem_t connectSem;
   sem_t closeSem;
   sem_t receivedSem;
   pthread_mutex_t sendMutex;
   pthread_mutex_t receiveMutex;
   pthread_t reader;
   pthread_t dispatcher;
   std::vector<Listener*> listeners;
  };

  class ConnectionHandler
  {
  public:
   ConnectionHandler();
   virtual ~ConnectionHandler();
   Connection* getConnection(tcp::Packet& aID);
   Connection* createConnection(tcp::Packet& aID);
   Connection* deleteConnection(tcp::Packet& aID);
   void dispatch(pac::Packet& aPacket);
  protected:
  private:
   void convertSocket(tcp::Packet& aPacket,std::string& aSocket);
   std::map<std::string,Connection*> connections;
  };

 }
}


#endif
