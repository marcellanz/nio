//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//----------------------------
//tcb Transmission Control Block
// see rfc793
//(c) H.Buchmann FHSO 2003
//$Id: tcb.cc 193 2006-01-11 15:21:16Z buchmann $
//-----------------------------
#include "sys/sys.h"
IMPLEMENTATION(nio_tcp_tcb,$Revision: 137 $)
namespace nio
{
 namespace tcp
 {
 }
}
