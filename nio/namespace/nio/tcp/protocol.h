//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#ifndef NIO_TCP_PROTOCOL_H
#define NIO_TCP_PROTOCOL_H
//---------------------------------
//protocol
//(c) H.Buchmann FHSO 2005
//$Id: protocol.h 193 2006-01-11 15:21:16Z buchmann $
//---------------------------------
INTERFACE(nio_tcp_protocol,$Revision: 137 $)
#include "tcp/tcp.h" 
#include "sys/thread.h"
#include "pac/pac.h"
#include "io/ascii/out.h"
#include "eth/eth.h"
#include "ip4/ip4.h"
#include "arp/arp.h"
#include "tcp/sequence.h"

namespace nio
{
 namespace tcp
 {
//-------------------------------------- Port
  class Port
  {
   public:
    class Pool;
    typedef unsigned short ID;
//-------------------------------------- Port::Listener
    struct Listener
    {
     virtual ~Listener(){}
     virtual void onPacket(Packet& pac)=0;	   
    };

   private:
    friend class Host; //calls the listener
    int id; //id<0 not assigned 
    Listener& li;
    Pool& owner;    
    Port(Pool& owner,ID id,Listener& li);

   public:
//-------------------------------------- Port::Pool
//abstract 
    class Pool
    {
     protected:
      Port* newPort(ID id,Listener& li){return new Port(*this,id,li);}
      //id remains private
      
     public:
      virtual Port* create(Listener&)=0;       //for listener
      virtual Port* create(ID at,Listener&)=0; //for server ports
      virtual void remove(Port* p)=0;
      virtual Port* get(Packet& p)=0;
    };
    ~Pool(){owner.remove(this);}
    io::ascii::Writer& show(io::ascii::Writer&) const;
    friend io::ascii::Writer& operator<<(io::ascii::Writer& out,const Port& p)
           {return p.show(out);}
    int getID()const {return id;}
  };
  
   
//-------------------------------------- Connection
  class Host;
  class Connection:public Port::Listener
  {
   public:
    struct Listener
    {
     virtual ~Listener();
     virtual void onConnect(Connection&)=0;
     virtual void onOctet(unsigned char oct)=0;
    };
    
   private:
    friend class PortPool;
    friend class Host;
    typedef void (Connection::*Action)(Packet&);
    Host&        host;
    Listener&    li;
    arp::Pair	 dst;
    Port::ID     dstPort;
    Port*        srcPort;
    Action	 action;
    unsigned	 ackN;
    TXSequence   tx;
    Connection(Host& host,Listener& li,
	       const arp::Pair& dst,Port::ID id);
    void sync();
    void close();
//----------------------------------- actions
    void onFin(Packet&p);
    void onSync(Packet&);
    void onAck(Packet&);

    void send(Packet& p);    
    void send(const unsigned char data[],unsigned len);

   public:
    ~Connection();
    io::ascii::Writer& show(io::ascii::Writer&) const;
    friend io::ascii::Writer& operator<<(io::ascii::Writer& out,
                                         const Connection& con)
	    {return con.show(out);}

    void onPacket(tcp::Packet& pac);
  };
  
//-------------------------------------- TCP 
  class Host:public sys::Runnable
  {
   private:
    friend class Connection;
    sys::Thread thread;
    sys::Semaphore start; //will be opened by thread
    pac::Net&   net;
    ip4::Address::RAW src;
    const eth::Address::RAW& mac;
    Port::Pool&  portPool;
//------------------------------- implementation Runnable
    void run();
   public:
    Host(const char src[],Port::Pool& poolPool);
    Connection* create(Connection::Listener& li,
                       arp::Pair& dst,
		       Port::ID port);
    Host& operator<<(Packet& pac);
  };
 }//namespace tcp
}//namespace nio
#endif
