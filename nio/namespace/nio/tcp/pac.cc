//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//-------------------------------------
//tcp
//(c) H.Buchmann FHSO 2003
//$Id: pac.cc 193 2006-01-11 15:21:16Z buchmann $
//-------------------------------------
#include "sys/sys.h"
IMPLEMENTATION(nio_tcp_pac,$Revision: 137 $)
#include "nio/tcp/pac.h"
#include "nio/pac/stream/length.h"
#include "util/endian.h"
#include "sys/host.h"

namespace nio
{
 namespace tcp
 {
//------------------------------------------------------ Seq
  Seq::Seq(pac::Packet* p):
  pac::Unsigned(p,0u)
  {
  }

  Seq::Seq(pac::Packet* p,const Seq& seq):
  pac::Unsigned(p,seq.val)
  {
  }
  
  Seq::~Seq()
  {
  }
  
  Seq& Seq::operator=(unsigned v)
  {
   val=v;set();return *this;
  }
    
//------------------------------------------------------ Packet
  const char* Packet::URG[]={"","urg"};
  const char* Packet::ACK[]={"","ack"};
  const char* Packet::PSH[]={"","psh"};
  const char* Packet::RST[]={"","rst"};
  const char* Packet::SYN[]={"","syn"};
  const char* Packet::FIN[]={"","fin"};

  Packet::DataOffset::DataOffset(pac::Packet* p):
  pac::Bits<4>(p,0u)
  {
  }

  void Packet::DataOffset::fromRaw(pac::stream::RawInput& in)
  {
   pos=in.getBytePos();
   get(in);
  }
  
  void Packet::DataOffset::toRaw(pac::stream::RawOutput& out)
  {
   pos=out.getBytePos();
   if (!assigned)
      {
       pac::stream::Length len;
       len.reset();
       for(unsigned i=0;i<list.size();i++)
       {
        list[i]->put(len);
       }
       val=len.get()/4;
       put(out);
      }
  }
    
  Packet::Option::List::List(pac::Packet* p):
  pac::Type(p),size(0)
  {
  }

  Packet::Option::List::List(pac::Packet* p,const List& lst):
  pac::Type(p),size(0)
  {
   for(unsigned i=0;i<lst.size;i++)
   {
    add(lst.list[i]->clone());
   }
  }

  Packet::Option::List::~List()
  {
   for(unsigned i=0;i<list.size();i++)
   {
    delete list[i];
   }
  }


  void Packet::Option::List::get(pac::stream::Input& in)
  {
   Packet& p=*(Packet*)owner;
   size=0; //reset size
   unsigned end=4*p.ofs.get()+p.ofs.getPos()-12; 
                                                       //ackNbr,seqNbr,port
   while(true)
   {
    unsigned char kind=0;
    if (in.getBytePos()==end) return;
    in.get(kind);
//    sys::msg<<"kind "<<kind<<"\n";
    switch(kind) //for the moment
    {
     case 0:add(new EoO);return;
     case 1:add(new Nop);break; 
     case 2:add(new MSS(in));break;
     case 3:add(new WSO(in));break;
     case 4:add(new SACK(in));break;
     case 8:add(new TSO(in));break;
     default:
      add(new NotSupOption(kind,in));
     break;
    }
   }
  }
  
  void Packet::Option::List::add(Option* opt)
  {
   if (opt==0) return;
   if (size<list.size()) 
      {
       if (list[size]) delete list[size]; //the old one
       list[size]=opt;
      }
      else 
      {
       list.push_back(opt);
      }
   size++;   
  }
  
  void Packet::Option::List::put(pac::stream::Output& out)
  {
   for(unsigned i=0;i<size;i++)
   {
    list[i]->put(out);
   }
  }

  io::ascii::Writer& Packet::Option::List::show(io::ascii::Writer& out) const
  {
   for(unsigned i=0;i<size;i++)
   {
    if (i==0) out<<"\n"; //better formatting
    list[i]->show(out);
   }
   return out;
  }

  io::ascii::Writer& Packet::Nop::show(io::ascii::Writer& out) const
  {
   return out<<"<nop>";
  }
  
  void Packet::Nop::put(pac::stream::Output& out)
  {
   out.put(kind);
  }

  io::ascii::Writer& Packet::EoO::show(io::ascii::Writer& out) const
  {
   return out;
  }
  
  void Packet::EoO::put(pac::stream::Output& out)
  {
   out.put(kind);
  }
  
  Packet::LenOption::LenOption(unsigned char kind,pac::stream::Input& in):
  Option(kind)
  {
   unsigned char l=0;
   in.get(l);
   if (l<2) 
      {
       sys::msg<<"**** option length error\n";
       sys::exit(1);
      }
   len=l-2;
  }
  
  
  Packet::MSS::MSS(pac::stream::Input& in):
  LenOption(2,in)
  {
   in.get(mss);
  }
  
  void Packet::MSS::put(pac::stream::Output& out)
  {
   out.put(kind);
   out.put((unsigned char)4);
   out.put(mss);
  }
  
  io::ascii::Writer& Packet::MSS::show(io::ascii::Writer& out) const
  {
   return out<<"<mss "<<mss<<">";
  }
  
  Packet::TSO::TSO(pac::stream::Input& in):
  LenOption(8,in)
  {
   in.get(tsVal);
   in.get(tsEcr);
  }


  void Packet::TSO::put(pac::stream::Output& out)
  {
   out.put(kind);
   out.put((unsigned char)10);
   out.put(tsVal);
   out.put(tsEcr);
  }
  
  io::ascii::Writer& Packet::TSO::show(io::ascii::Writer& out) const
  {
   return out<<"<tso "<<tsVal<<","<<tsEcr<<">";
  }
  
  Packet::WSO::WSO(pac::stream::Input& in):
  LenOption(3,in)
  {
   in.get(scale);
  }


  void Packet::WSO::put(pac::stream::Output& out)
  {
   out.put(kind);
   out.put((unsigned char)3);
   out.put(scale);
  }
  
  io::ascii::Writer& Packet::WSO::show(io::ascii::Writer& out) const
  {
   return out<<"<wso "<<(unsigned)scale<<">";
  }
  
  
  Packet::SACK::SACK(pac::stream::Input& in):
  LenOption(4,in)
  {
  }

  void Packet::SACK::put(pac::stream::Output& out)
  {
   out.put(kind);
   out.put((unsigned char)2);
  }
  
  io::ascii::Writer& Packet::SACK::show(io::ascii::Writer& out) const
  {
   return out<<"<sack>";
  }
  
  
  Packet::NotSupOption::NotSupOption(unsigned char kind,pac::stream::Input& in):
  LenOption(kind,in)
  {
   sys::msg<<"len = "<<len<<"\n";
   data=new unsigned char[len];
   for(unsigned i=0;i<len;i++) in.get(data[i]);
  }  

  Packet::NotSupOption::NotSupOption(const NotSupOption* nsp):
  LenOption(this)
  {
   data=new unsigned char[len];
   __builtin_memcpy(data,nsp->data,len);
  }
  
  Packet::NotSupOption::~NotSupOption()
  {
   delete [] data;
  }
  
  io::ascii::Writer& Packet::NotSupOption::show(io::ascii::Writer& out) const
  {
   out<<(unsigned)kind<<" : ";
   out.dump(data,len);
   return out;
  }

  void Packet::NotSupOption::put(pac::stream::Output& out)
  {
   out.put(kind);
   unsigned char l=(unsigned char)len+2;
   out.put(l);
   out.put(data,len);
  }

  Packet::Checksum::Checksum(pac::Packet* p):
  pac::Short(p,(unsigned short)0)
  {
  }
  
  Packet::Checksum::Checksum(pac::Packet* p,const Checksum& cs):
  pac::Short(p,cs.val)
  {
  
  }

  void Packet::Checksum::toRaw(pac::stream::RawOutput& out)
  {
   if (!assigned)
      {
       val=0;
       pac::stream::Checksum cs;
       calc(cs);
       cs.set(val);
      }
   put(out);   
  }
  
  bool Packet::Checksum::isOK()
  {
   pac::stream::Checksum cs;
   calc(cs);
   return cs.get()==0xffff;
  }
  
  void Packet::Checksum::calc(pac::stream::Checksum& cs)
  {
   Packet& p=*(Packet*)owner;
   p.ip4.src.put(cs);
   p.ip4.dst.put(cs);
   cs.put((unsigned char)0);
   p.ip4.pro.put(cs); 
   
   cs.put((unsigned short)(4*p.ofs.get()));
   unsigned short pl=p.pld.getLen();
 //  sys::msg<<"------------------------ pl "<<pl<<"\n";
   cs.put((unsigned short)pl);   
   p.prt.src.put(cs);
   p.prt.dst.put(cs);
   p.seqN.put(cs);
   p.ackN.put(cs);
   p.ofs.put(cs);
   p.res.put(cs);
   p.urgF.put(cs);
   p.ackF.put(cs);
   p.pshF.put(cs);
   p.rstF.put(cs);
   p.synF.put(cs);
   p.finF.put(cs);
   p.wnd.put(cs);
   p.chk.put(cs);
   p.urg.put(cs);
   p.opt.put(cs);
   p.pld.put(cs);
  }
   
  Packet::Packet():
  pac::Packet("tcp",ip4),
  prt(this),
  seqN(this),
  ackN(this),
  ofs(this),
  res(this,0u),
  urgF(this,URG,0u),
  ackF(this,ACK,0u),
  pshF(this,PSH,0u),
  rstF(this,RST,0u),
  synF(this,SYN,0u),
  finF(this,FIN,0u),
  wnd(this,(unsigned short)0),
  chk(this),
  urg(this,(unsigned short)0),
  opt(this),
  pad(this),
  pld(this),
  end(this)
  {
   ip4.pro=PROT;
   init();
  }

  Packet::Packet(const Packet& p):
  pac::Packet("tcp",ip4),
  ip4(p.ip4),
  prt(this,p.prt),
  seqN(this,p.seqN),
  ackN(this,p.ackN),
  ofs(this), //set in init
  res(this,p.res),
  urgF(this,p.urgF),
  ackF(this,p.ackF),
  pshF(this,p.pshF),
  rstF(this,p.rstF),
  synF(this,p.synF),
  finF(this,p.finF),
  wnd(this,p.wnd),
  chk(this,p.chk),
  urg(this,p.urg),
  opt(this,p.opt),
  pad(this,p.pad),
  pld(this,p.pld),
  end(this)
  {
   init();
  }
  
  void Packet::init()
  {
   ofs.add(prt.src).
       add(prt.dst).
       add(seqN).
       add(ackN).
       add(ofs).
       add(res).
       add(urgF).
       add(ackF).
       add(pshF).
       add(rstF).
       add(synF).
       add(finF).
       add(wnd).
       add(chk).
       add(urg).
       add(opt).
       add(pad).
       add(end);

   ip4.len.add(prt.src).
	   add(prt.dst).
	   add(seqN).
	   add(ackN).
	   add(ofs).
	   add(res).
	   add(urgF).
	   add(ackF).
	   add(pshF).
	   add(rstF).
	   add(synF).
	   add(finF).
	   add(wnd).
	   add(chk).
	   add(urg).
	   add(opt).
	   add(pad).
	   add(pld).
	   add(end);
  }
    
  bool Packet::is()
  {
   return ip4.pro==PROT;
  }

  void Packet::onRaw()
  {
   pld.adjustLen(ip4.len-4*(ofs+ip4.ihl));
  }

  io::ascii::Writer& Packet::showConnection(io::ascii::Writer& out)const
  {
   return out.dec()<<ip4.src<<":"<<prt.src
                   <<" -> "
		   <<ip4.dst<<":"<<prt.dst;
  }
  
  io::ascii::Writer& Packet::showFlags(io::ascii::Writer& out)const
  {
   return out<<"["<<urgF<<" "
		  <<ackF<<" "
		  <<pshF<<" " 
		  <<rstF<<" "
		  <<synF<<" "
		  <<finF<<"]";
  }
  
  io::ascii::Writer& Packet::showSeq(io::ascii::Writer& out)const
  {
   return showFlags(showConnection(out)<<"<"<<seqN<<","<<ackN<<">");
  }
    
  io::ascii::Writer& Packet::show(io::ascii::Writer& out)const
  {
 //   showConnection(out);
    showSeq(out)<<"\n"
	    <<"ofs: "<<ofs<<" "
	    <<"win: "<<wnd<<" "
	    <<"chk: "<<chk<<" "
	    <<"urg: "<<urg<<" "
	    <<opt<<" "<<pad<<" "
	    <<pld;
   return out;	      
  }
    
 }
}
