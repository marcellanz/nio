//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//------------------------
//echo-client
//(c) H.Buchmann FHSO 2003
//$Id: echo-client.cc 193 2006-01-11 15:21:16Z buchmann $
//------------------------
#include "sys/sys.h"
IMPLEMENTATION(tcp_echo_client,$Revision: 137 $)
#include "util/pfile.h"
#include "host.h"
#include "tcp/tcp.h"
#include "default.h"
namespace nio
{
 namespace tcp
 {
  class EchoClient
  {
   private:
    static EchoClient client;
    util::PFile para;
    pac::Net& eth; 
    pac::Dumper logger;   

    unsigned iss;
    unsigned srcPort;
    unsigned window;
    void connect();
    EchoClient();
  };
  
  EchoClient EchoClient::client;
  
  EchoClient::EchoClient():
  para(1,0),
  eth(Host::getNet()),
  logger(sys::msg)
  {
//   eth.setLogger(logger);
   eth.logon();
   connect();
  }
  
     
  void EchoClient::connect()
  {
   Packet txPac(Default::TCP);
   Packet rxPac;
   unsigned seqN=para.valueOf("ECHO_CLIENT_ISS",0u);
   txPac.ip4.eth.dst=para.valueOf("ECHO_CLIENT_MAC");
   txPac.ip4.dst=para.valueOf("ECHO_CLIENT_IP4");
   txPac.port.dst=7;
   txPac.port.src=para.valueOf("ECHO_CLIENT_SRC_PORT",0u);
   txPac.syn=1;
   txPac.window=para.valueOf("ECHO_CLIENT_WINDOW",0u);
   txPac.seqNbr=seqN;

   sys::msg<<txPac<<"\n";
   eth<<txPac;
   eth>>rxPac;
   sys::msg<<rxPac<<"\n";
   txPac.seqNbr=seqN+1;
   txPac.ackNbr=rxPac.seqNbr+1u;
   
   txPac.syn=0;
   txPac.ack=1;
   sys::msg<<txPac<<"\n";
   eth<<txPac;
   txPac.payload.set("1234");
   eth<<txPac;
   eth>>rxPac;
   sys::msg<<rxPac<<"\n";
  }

 }
}
