//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//----------------------------
//tcp-dump
//(c) H.Buchmann FHSO 2005
//$Id: dump.cc 193 2006-01-11 15:21:16Z buchmann $
//----------------------------
#include "sys/sys.h"
IMPLEMENTATION(nio_tcp_dump,$Revision: 137 $)
#include "nio/pac/pcap.h"
#include "nio/pac/pac.h"
#include "nio/tcp/pac.h"

#include "sys/host.h"
#include "sys/msg.h"

namespace nio
{
 namespace tcp
 {
  class Dumper
  {
   static Dumper dumper;
   Packet pattern;
   Dumper();
  };
  
  Dumper Dumper::dumper;
  
  Dumper::Dumper()
  {
   if (sys::argCnt()!=2)
      {
       sys::msg<<"usage: "<<sys::argAt(0)<<" pcapFile\n";
       sys::exit(1);
      }
   pac::PcapDevice dev(sys::argAt(1));
   pac::Net net(dev);
   unsigned cnt=0;
   while(true)
   {
    Packet p;
    net>>p;
    if (cnt==0) 
       {
        pattern=p; 
	sys::msg<<"A = "<<p.ip4.src<<":"<<p.prt.src<<"\n"
	          "B = "<<p.ip4.dst<<":"<<p.prt.dst<<"\n";
       }
    if (p.co(pattern)) sys::msg<<"A->B";
       else  if (p.aco(pattern)) sys::msg<<"B->A";
                else             sys::msg<<"?->?";
    sys::msg<<io::ascii::setw(12)<<p.seqN
            <<io::ascii::setw(12)<<p.ackN<<" "
	    <<io::ascii::setw(5)<<p.pld.getLen()<<" ";
    p.showFlags(sys::msg)<<"\n";	    
    cnt++;
   }   
  }
 }//namespace tcp
}//namespace nio
