//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#ifndef NIO_DEFAULT_H
#define NIO_DEFAULT_H
//----------------------------
//default
//(c) H.Buchmann FHSO 2003
//$Id: default.h 193 2006-01-11 15:21:16Z buchmann $
//----------------------------
INTERFACE(nio_default,$Revision: 137 $)
#include "nio/eth/pac.h"
#include "nio/arp/pac.h"
#include "nio/ip4/pac.h"
#include "nio/icmp/pac.h"
#include "nio/udp/pac.h"
#include "nio/tcp/pac.h"

//#include "agent.h"
namespace nio
{
 class Default
 {
  private:
   static Default default_;
   
   icmp::Packet icmp;
   eth::Packet  eth;
   ip4::Packet  ip4;
   arp::Packet  arp;
   udp::Packet  udp;
   tcp::Packet  tcp;
   
//   agent::Agent* agent;
   Default();
   
  public:
   virtual ~Default();
//the default packets  
   static const eth::Packet&  ETH;
   static const ip4::Packet&  IP4;
   static const arp::Packet&  ARP;
   static const icmp::Packet& ICMP;
   static const udp::Packet&  UDP;
   static const tcp::Packet&  TCP;
//   static agent::Agent& getAgent(){return *default_.agent;}
 };
}
#endif
