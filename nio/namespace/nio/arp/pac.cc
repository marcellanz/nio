//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//--------------------------------
//arp
//(c) H.Buchmann FHSO 2003
//$Id: pac.cc 193 2006-01-11 15:21:16Z buchmann $
//--------------------------------
#include "sys/sys.h"
IMPLEMENTATION(nio_arp_pac,$Revision: 137 $)
#include "nio/arp/pac.h"
#include "util/endian.h"
#include "sys/msg.h"

namespace nio
{
 namespace arp
 {
  Pair::Pair(const char hw[],const char pr[])
  {
   eth::Address::copyExit(hw,this->hw);
   ip4::Address::copyExit(pr,this->pr);
  }
  
  Pair::Pair(const Pair& p)
  {
   eth::Address::copy(p.hw,hw);
   ip4::Address::copy(p.pr,pr);
  }

  io::ascii::Writer& Op::show(io::ascii::Writer& out)const
  {
   static const char* Type[]={"op:0","REQU","RPLY"};
   if (val<=REPLY) return out<<Type[val];
      else         return out<<"op: "<<val;
  }

  //------------------------------------------------ Packet
  Packet::Packet():
  pac::Packet("arp",eth),
  hrd(this,HRD),
  pro(this,PRO),
  hln(this,HLN),
  pln(this,PLN),
  op(this,Op::REQUEST),
  src(this),
  tar(this),
  payload(this)
  {
   eth.type=ETHER_TYPE;
  } 

  Packet::Packet(const Packet& p):
  pac::Packet("arp",eth),
  eth(p.eth),
  hrd(this,p.hrd),
  pro(this,p.pro),
  hln(this,p.hln),
  pln(this,p.pln),
  op(this,p.op),
  src(this,p.src),
  tar(this,p.tar),
  payload(this,p.payload)
  {
   eth.type=ETHER_TYPE;
  }
  
  io::ascii::Writer& Packet::Pair::show(io::ascii::Writer& out) const
  {
   out<<hw<<" <-> "<<pr;
   return out;
  }

  io::ascii::Writer& Packet::show(io::ascii::Writer& out)const
  {
   out<< "hrd: "<<hrd
      <<" pro: "<<pro
      <<" hln: "<<hln
      <<" pln: "<<pln<<" "
                <<op<<"\n"     
	"src: "<<src<<"\n"
	"tar: "<<tar;
   return out;
  }

 }
}
