//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#ifndef NIO_ARP_PAC_H
#define NIO_ARP_PAC_H
//--------------------------------
//arp
//(c) H.Buchmann FHSO 2003
//$Id: pac.h 193 2006-01-11 15:21:16Z buchmann $
//--------------------------------
INTERFACE(nio_arp_pac,$Revision: 137 $)
#include "nio/pac/pac.h"
#include "nio/eth/pac.h"
#include "nio/ip4/pac.h"
#include "io/ascii/out.h"

namespace nio
{
 namespace arp
 {
//----------------------------------- Pair 
//convenience class dont confuse with Packet::Pair
  struct Pair 
  {
   eth::Address::RAW hw;
   ip4::Address::RAW pr;
   Pair(const char hw[],const char pr[]);
   Pair(const Pair& p);
  };
  
  struct Op:public pac::Short
  {
   enum Type {REQUEST=1,REPLY=2};
   Op(pac::Packet* p,unsigned short v):pac::Short(p,v){};
   Op(pac::Packet* p,Type t):pac::Short(p,t){}
   Op(pac::Packet* p,const Op& op):pac::Short(p,op.val){}
   Op& operator = (const Type typ){val=typ;return *this;}
   io::ascii::Writer& show(io::ascii::Writer&) const;
  };

  class Packet:public pac::Packet
  {
   private:
    bool is(){return eth.type==ETHER_TYPE;}
    void onTX(){}
    
   public:
    static const unsigned short ETHER_TYPE = 0x0806;
    
    
    static const unsigned short HRD = 1;
    static const unsigned short PRO = 0x800;
    static const unsigned char  HLN = 6;
    static const unsigned char  PLN = 4;

    struct Pair
    {
     eth::Address hw;
     ip4::Address pr;
     io::ascii::Writer& show(io::ascii::Writer& out)const;

     friend io::ascii::Writer& operator<<(io::ascii::Writer& out,const Pair& p)
     {return p.show(out);}
     Pair(Packet* p):hw(p),pr(p){}
     Pair(Packet* p,const Pair& pair):hw(p,pair.hw),pr(p,pair.pr){}
    };
    
    eth::Packet eth;
    pac::Short hrd;
    pac::Short pro;
    pac::Byte  hln;
    pac::Byte  pln;
    Op         op;
    Pair src;
    Pair tar;
    pac::Payload payload;
    Packet();
    Packet(const Packet&);
    io::ascii::Writer& show(io::ascii::Writer& out)const;
  };
 }
}
#endif
