//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#ifndef NIO_PAC_STREAM_STREAM_H
#define NIO_PAC_STREAM_STREAM_H
//---------------------------
//Stream 
//(c) H.Buchmann FHSO 2003
// $Id: stream.h 193 2006-01-11 15:21:16Z buchmann $
//---------------------------
INTERFACE(nio_pac_stream_stream,$Revision: 137 $)
namespace nio
{
 namespace pac
 {
  namespace stream
  {
   class Stream
   {
    protected:
     static const unsigned char Bits[8];
     unsigned pos; //in bits
     unsigned char cur; //used in putBits/getBits
     Stream();
     virtual void onReset(){}
     virtual void onClose(){}

    public:
     virtual ~Stream();
     void reset();
     virtual void close()=0;
     unsigned getBitPos(){return pos;}
     unsigned getBytePos();
     void align();
   };

   class Output:public Stream
   {
    private:

    protected:
     Output();
     virtual void putByte(unsigned char b)=0;

    public:

     virtual ~Output();
     void close();
     virtual void put(unsigned char d[],unsigned len);
     virtual void put(unsigned char);
     virtual void put(unsigned short);
     virtual void put(unsigned);
     virtual void putBits(unsigned val,unsigned bits);

   };

   class Input:public Stream
   {
    protected:
      Input();
      virtual unsigned char getByte()=0;    
    public:
     virtual ~Input();
     void close();
     virtual void get(unsigned char d[],unsigned len);
     virtual void get(unsigned char&);
     virtual void get(unsigned short&);
     virtual void get(unsigned&);
     virtual void getBits(unsigned& val,unsigned bits);
   };
 } //stream
 } //pac
}
#endif
