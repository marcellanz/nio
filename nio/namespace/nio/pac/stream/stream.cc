//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//---------------------------
//Stream 
//(c) H.Buchmann FHSO 2003
// $Id: stream.cc 193 2006-01-11 15:21:16Z buchmann $
//---------------------------
#include "sys/sys.h"
IMPLEMENTATION(nio_pac_stream_stream,$Revision: 137 $)
#include "nio/pac/stream/stream.h"
#include "sys/msg.h"
#include "sys/host.h"

namespace nio
{
 namespace pac
 {
  namespace stream
  {
   const unsigned char Stream::Bits[8]=
   {
    1<<7,1<<6,1<<5,1<<4,1<<3,1<<2,1<<1,1<<0
   };

   Stream::Stream():
   pos(0),cur(0)
   {
   }

   Stream::~Stream()
   {
   }

   void Stream::reset()
   {
    pos=0;cur=0;
    onReset();
   }

   void Stream::align()
   {
    if (pos&0x7)
       {
	sys::msg<<"**** align error\n";
	sys::exit(1);
       }
   }
   
   unsigned Stream::getBytePos()
   {
    align();
    return pos>>3;
   }

   Output::Output()
   {
   }

   Output::~Output()
   {
   }

   void Output::close()
   {
    if (pos&0x7) putByte(cur);
    onClose();
   }

   void Output::put(unsigned char d[],unsigned len)
   {
    align();
    for(unsigned i=0;i<len;i++)
    {
     putByte(d[i]);
     pos+=8;    
    }
   }

   void Output::put(unsigned char v)
   {
    align();
    putByte(v);
    pos+=8;
   }

   void Output::put(unsigned short v)
   {
    align();
    putByte((unsigned char)(v>>8));
    pos+=8;
    putByte((unsigned char)(v   ));
    pos+=8;
   }

   void Output::put(unsigned v)
   {
    align();
    putByte((unsigned char)(v>>24));
    pos+=8;
    putByte((unsigned char)(v>>16));
    pos+=8;
    putByte((unsigned char)(v>> 8));
    pos+=8;
    putByte((unsigned char)(v    ));
    pos+=8;
   }

   void Output::putBits(unsigned v,unsigned bits)
   {
    unsigned bPos=pos&0x7;
    unsigned mask=1<<bits;
    do
    {
     mask>>=1;
     if (v&mask) cur|=Bits[bPos];
     ++bPos&=0x7;
     pos++;
     if (bPos==0) 
	{
         putByte(cur);
	 cur=0;
	}
    }while(mask!=1);
   }

   Input::Input()
   {
   }

   Input::~Input()
   {
   }

   void Input::close()
   {
   } 

   void Input::get(unsigned char d[],unsigned len)
   {
    align();
    for(unsigned i=0;i<len;i++)
    {
     d[i]=getByte();
     pos+=8;
    }
   }

   void Input::get(unsigned char& v)
   {
    align();
    v=getByte();
    pos+=8;
   }

   void Input::get(unsigned short& v)
   {
    align();
    v =getByte()<<8;
    v|=getByte();
    pos+=16;
   }

   void Input::get(unsigned& v)
   {
    align();
    v =getByte()<<24;
    v|=getByte()<<16;
    v|=getByte()<< 8;
    v|=getByte();
    pos+=32;
   }

   void Input::getBits(unsigned& val,unsigned bits)
   {
    unsigned bPos=pos&0x7;
    unsigned mask=1<<bits;
    val=0;
    do
    {
     if (bPos==0)cur=getByte();
     mask>>=1;
     if (cur&Bits[bPos])val|=mask;
     pos++;
     ++bPos&=0x7;
    }while(mask!=1);   
   }
  } //stream
 } //pac
}
