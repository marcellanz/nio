//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//--------------------------
//checksum
//(c) H.Buchmann FHSO 2003
//$Id: raw.cc 193 2006-01-11 15:21:16Z buchmann $
//--------------------------
#include "sys/sys.h"
IMPLEMENTATION(nio_pac_stream_raw,$Revision: 137 $)
#include "nio/pac/stream/raw.h"
#include "sys/msg.h"
#include "sys/host.h"

namespace nio
{
 namespace pac
 {
  namespace stream
  {
   RawOutput::RawOutput():
   data(0),capacity(0),len(0)
   {
   }

   RawOutput::~RawOutput()
   {
   }

   void RawOutput::set(unsigned char data[],unsigned capacity)
   {
    this->data=data;
    this->capacity=capacity;
    len=0;
   }

   void RawOutput::onOpen()
   {
   }

   void RawOutput::onReset()
   {
   }

   void RawOutput::onClose()
   {
   }

   void RawOutput::putByte(unsigned char v)
   {
    if (data==0)
       {
	sys::msg<<"**** no data defined\n";
	sys::exit(1);       
       }
    data[len++]=v;   
   }

   io::ascii::Writer& RawOutput::dump(io::ascii::Writer& out)
   {
    out<<"len "<<len<<"\n";
    out.dump(data,len);
    return out;
   }

   RawInput::RawInput():
   data(0),size(0),len(0)
   {
   }

   RawInput::~RawInput()
   {
   }

   unsigned char RawInput::getByte()
   {
    if (data==0)
       {
	sys::msg<<"**** no data defined\n";
	sys::exit(1);       
       }
    if (len>=size)
       {
	sys::msg<<"**** no more data available\n";
       }   
    return data[len++];   
   }

   void RawInput::onOpen()
   {
   }

   void RawInput::onReset()
   {
    len=0;
   }

   void RawInput::onClose()
   {
   }

   void RawInput::set(unsigned char data[],unsigned size)
   {
    this->data=data;
    this->size=size;
    len=0;
    reset();   
   }

   io::ascii::Writer& RawInput::dump(io::ascii::Writer& out)
   {
    out<<"len "<<size<<"\n";
    out.dump(data,size);
    return out;
   }

  } //stream
 } //pac
}
