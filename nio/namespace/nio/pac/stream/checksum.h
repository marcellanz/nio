//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#ifndef NIO_PAC_STREAM_CHECKSUM_H
#define NIO_PAC_STREAM_CHECKSUM_H
//--------------------------
//checksum
//(c) H.Buchmann FHSO 2003
//$Id: checksum.h 193 2006-01-11 15:21:16Z buchmann $
//--------------------------
INTERFACE(nio_pac_stream_checksum,$Revision: 137 $)
#include "nio/pac/stream/stream.h"
namespace nio
{
 namespace pac
 {
  namespace stream
  {
   class Checksum:public Output
   {
    private:
     unsigned sum;
     unsigned char b[2];
     unsigned bi;
     void putByte(unsigned char v);
     void onOpen();
     void onReset();
     void onClose();

    public:
              Checksum();
     virtual ~Checksum();
     unsigned short get() const;
     void set(unsigned short& v) const;
   };
  }
 }
}
#endif
