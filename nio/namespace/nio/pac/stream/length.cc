//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//----------------------------
//length
//(c) H.Buchmann FHSO 2003
//$Id: length.cc 193 2006-01-11 15:21:16Z buchmann $
//----------------------------
#include "sys/sys.h"
IMPLEMENTATION(nio_pac_stream_lenghth,$Revision: 137 $)
#include "nio/pac/stream/length.h"
#include "sys/msg.h"
#include "sys/host.h"

namespace nio
{
 namespace pac
 {
  namespace stream
  {
   void Length::close()
   {
   }
   
   template<class type>
   void Length::put(type v)
   {
    align();
    len+=sizeof(v);
   }

   void Length::putByte(unsigned char v)
   {
   }

   void Length::onReset()
   {
    len=0;
    bPos=0;
   }

   void Length::onClose()
   {
   }

   Length::Length()
   {
    len=0;
    bPos=0;
   }

   Length::~Length()
   {
   }

   void Length::put(unsigned char d[],unsigned len)
   {
    align();
    this->len+=len;
   }

   void Length::put(unsigned char v)
   {
    put<unsigned char>(v);
   }

   void Length::put(unsigned short v)
   {
    put<unsigned short>(v);
   }

   void Length::put(unsigned v)
   {
    put<unsigned>(v);
   }

   void Length::putBits(unsigned val,unsigned bits)
   {
    bPos+=bits;
    len+=(bPos>>3);
    bPos&=0x7;
   }

   unsigned Length::get() const
   {
    if (bPos)
       {
	sys::msg<<"**** not aligned\n";
	sys::exit(1);
       }
    return len;   
   }
   
   void  Length::set(unsigned short& v) const
   {
    if (bPos)
       {
	sys::msg<<"**** not aligned\n";
	sys::exit(1);
       }
    v=len;   
   }
  } //stream
 } //pac
}
