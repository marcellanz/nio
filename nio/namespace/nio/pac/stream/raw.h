//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#ifndef PAC_STREAM_RAW_H
#define PAC_STREAM_RAW_H
//--------------------------
//checksum
//(c) H.Buchmann FHSO 2003
//$Id: raw.h 193 2006-01-11 15:21:16Z buchmann $
//--------------------------
INTERFACE(nio_pac_stream_raw,$Revision: 137 $)
#include "nio/pac/stream/stream.h"
#include "io/ascii/write.h"
namespace nio
{
 namespace pac
 {
  namespace stream
  {
   class RawOutput:public Output
   {
    private:
     unsigned char* data;
     unsigned capacity;
     unsigned len;
     void putByte(unsigned char v);
     void onOpen();
     void onReset();
     void onClose();

    public:
              RawOutput();
     virtual ~RawOutput();
     void set(unsigned char data[],unsigned capacity);
     unsigned getLen()const {return len;}
     const unsigned char* getData()const{return data;}
     io::ascii::Writer& dump(io::ascii::Writer& out);
   };

   class RawInput:public Input
   {
    private:
     unsigned char* data;
     unsigned size;
     unsigned len;
     unsigned char getByte();
     void onOpen();
     void onReset();
     void onClose();

    public: 
              RawInput();
     virtual ~RawInput();
     void set(unsigned char data[],unsigned size);
     unsigned getLen()const {return size;}
     const unsigned char* getData()const{return data;}
     io::ascii::Writer& dump(io::ascii::Writer& out);
     unsigned avail(){return size-len;}
   };
 }//stream
 }//pac
}
#endif
