//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#ifndef PAC_RAWSOCKET_H
#define PAC_RAWSOCKET_H
//-------------------------------------
//rawsocket
//(c) H.Buchmann FHSO 2004
//$Id: rawsocket.h 193 2006-01-11 15:21:16Z buchmann $
//-------------------------------------
INTERFACE(nio_pac_rawsocket,$Revision: 137 $)
#include "nio/pac/device.h"
namespace nio
{
 namespace pac
 {
  class RawSocket:public Device
  {
   private:
    const char* name;
    int id;
    int type;
    int idx;
    char mac[6];
    
    void error(int code,const char msg[]);
          //if code<0 writes msg and exits
    void socketId();
    void ifcType();
    void ifcIndex();
    void ifcBind();
    void ifcMAC();

   public:
             RawSocket(const char name[]);
    virtual ~RawSocket();
    void send(const unsigned char d[],unsigned len);
    void receive(unsigned char d[],
                 unsigned capacity,
		 unsigned timeout_ms,
		 Result& res);
    const char* getMAC()const{return mac;}
    unsigned getPacketSize()const{return 1600;}
  };
 }
}
#endif
