//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#ifndef NIO_PAC_H
#define NIO_PAC_H
//-----------------------
//pac 
//(c) H.Buchmann FHSO 2003
//$Id: pac.h 193 2006-01-11 15:21:16Z buchmann $
//-----------------------
INTERFACE(nio_pac,$Revision: 137 $)
#include "sys/time.h"
#include "io/ascii/out.h"
#include "util/endian.h"
#include "nio/pac/stream/stream.h"
#include "nio/pac/stream/raw.h"
#include "nio/pac/device.h"
#include <vector>

namespace nio
{
 namespace pac
 {
 //---------------------------------------------- Type
 //raw data is part of pac
  class Packet;
  class Type
  {
   public:
    typedef std::vector<Type*> TList;
    
   private:
    friend class Packet;
            
   protected:
    bool assigned;
    Packet* owner;
    Type();
    void set(); //for primitive type
    void set(const Type& src);
         
   public:
    virtual ~Type(){}
    Type(Packet* p);
    Type(Packet* p,const Type& t);
    virtual io::ascii::Writer& show(io::ascii::Writer&) const=0;
    bool isAssigned(){return assigned;}
    friend io::ascii::Writer& operator <<(io::ascii::Writer& out,const Type& t)
             {return t.show(out);}
    virtual void put(stream::Output&)=0;
    virtual void get(stream::Input&)=0;
    virtual void toRaw(stream::RawOutput&);
    virtual void fromRaw(stream::RawInput&);
  };

 //---------------------------------------------- some basic Types
 //----------------------------------------------------------- Typ
  template<class typ>
  class Typ:public Type
  {
   private:
    Typ(Packet* p,const Typ* t); //not allowed
   protected:
    typ val;

   public:
             Typ(Packet* p,typ val):Type(p),val(val){}
	     Typ(Packet* p,const Typ& t):Type(p),val(t.val){}
	     Typ(typ val):Type(),val(val){}
    virtual ~Typ(){}
    io::ascii::Writer& show(io::ascii::Writer& out) const{return out<<val;}
    typ get()const{return val;}
    Typ& operator = (const typ  t){val=t;set();return *this;}
    Typ& operator = (const Typ t){val=t.val;set(t);return *this;} 
    operator unsigned()const{return val;}
    void put(stream::Output& out){out.put(val);}
    void get(stream::Input& in){in.get(val);}
  };

 //---------------------------------------------------------- Bits
 // fedcba9876543210
 // 00000000000xxxxx   bits right aligned 
  template<unsigned bits>
  class Bits:public Type   
  {
   private:
    static const unsigned Mask=(1<<bits)-1;
    Bits(Packet* p,const Bits*); //not allowed
    const char** valName;  //for small number of bits
                           //used in show
    
   protected:
    unsigned val;

   public:
             Bits(Packet* p,unsigned val):
	     Type(p),
	     valName(0),
	     val(val&Mask){}
             Bits(Packet* p,const char* valName[],unsigned val):
	     Type(p),
	     valName(valName),
	     val(val&Mask){}
	     
	     Bits(Packet* p,const Bits& b):
	     Type(p),
	     valName(b.valName),
	     val(b.val){}
	     Bits(unsigned val):val(val&Mask){}
    virtual ~Bits(){}
    io::ascii::Writer& show(io::ascii::Writer& out) const
    {return (valName)?out<<valName[val]:out<<val;}

    unsigned short get()const{return val;}

    void operator = (const unsigned short t){val=t;set();}
    void put(stream::Output& out){out.putBits(val,bits);}
    void get(stream::Input& in){in.getBits(val,bits);}
    operator unsigned()const{return val;}
  };
 
  
 //------------------------------------------------------- Payload
  class Payload:public Type
  {
   private:
    Payload(const Payload&);
    Payload(Packet* p,const Payload*);
    unsigned char* data;
    unsigned capacity;
    unsigned len;
    unsigned aLen; //adjusted length 
                   //sometimes the useful payload length is shorter than
		   //the length derived from the raw packet len
		   // aLen<=len<=capacity
    
   public:
    Payload(Packet* p);
    Payload(Packet* p,const Payload& pl);
    virtual ~Payload();
//the set methods
// a) the data will be copied
// b) returns the pointer to data 
    unsigned char const* set(unsigned len);
    unsigned char const* set(const unsigned char data[],unsigned len);
    unsigned char const* set(const char s[]);

    void adjustLen(unsigned aLen); 
    io::ascii::Writer& show(io::ascii::Writer&) const;
    void put(stream::Output&);
    void get(stream::Input&);
    void fromRaw(stream::RawInput&);
    unsigned char const* get()const{return data;}
    unsigned getLen()const{return aLen;}
  };

  typedef Typ<unsigned> Unsigned;
  typedef Typ<unsigned short> Short;
  typedef Bits<8> Byte;
  typedef Bits<1> Flag;
 //------------------------------------------------------- CkeckSum
  class Checksum:public Short 
  {
   private:
    TList tList;
    Checksum(const Checksum&);
    Checksum(Packet* p,const Checksum*);
   public:
    Checksum(Packet* p);
    Checksum(Packet* p,const Checksum& cs);
    void toRaw(stream::RawOutput& out);
    Checksum& add(Type& t){tList.push_back(&t);return *this;}
    void operator = (const unsigned short  t){val=t;set();}
    void operator = (const Checksum& cs){val=cs.val;set(cs);}
    bool isOK();
  };

 //------------------------------------------------------- Len
  class Len:public Short
  {
   private:
    TList tList;
    Len(Packet* p,const Len*);
   public:
    Len(Packet* p);
    Len(Packet* p,const Len& len);
    void toRaw(stream::RawOutput& out);
    Len& add(Type& t){tList.push_back(&t);return *this;}
    void operator = (const unsigned short  t){val=t;set();}
    void operator = (const Len&  len){val=len.val;set(len);}
  };
  
 //------------------------------------------------------- Padding
  class Padding:public Type
  {
   private:
    static const unsigned SIZE=4; //bytes
    unsigned char padding[SIZE];
    unsigned len;
    Padding(Packet* p,const Padding*);
   public:
    Padding(Packet* p);
    Padding(Packet* p,const Padding& pad);
    io::ascii::Writer& show(io::ascii::Writer&) const;
    void put(stream::Output& out);
    void get(stream::Input& in);
  };
  
 //------------------------------------------------------- End
  class End:public Type
  {
   private:
    io::ascii::Writer& show(io::ascii::Writer& out) const{return out;}
    void put(stream::Output& out) ;
    void get(stream::Input& in);
    
   public:
             End(Packet* p):Type(p){}
    virtual ~End(){}
  };

 //---------------------------------------------- Packet
  class Packet
  {
   private:
    friend class Type;
    friend class Payload;
    friend class Net;
    
    const char* name;
    Packet* super;
    Payload* pl;             // not null if valid
    Type::TList tList;
    bool timeout;
    unsigned timeout_ms;
    sys::Time::Stamp_us timestamp_us;   //in milliseconds -1: no timestamp
    void add(Type* t);
    
   protected:
	     Packet(const char name[],Packet& super);
	     Packet(const char name[]);
    virtual bool is()=0;
    virtual void onRaw(); //will be called after fromRaw
                          //useful for packet specific post processing
			  //default nothing will be done
   public:
    virtual ~Packet();
    void operator = (const Packet& p);
    virtual bool operator == (const Packet& p);
    virtual bool operator != (const Packet& p);
    void toRaw(stream::RawOutput& out);
    bool fromRaw(stream::RawInput& in);
    virtual io::ascii::Writer& show(io::ascii::Writer&)const=0;
    friend io::ascii::Writer& operator <<(io::ascii::Writer& out,const Packet& p)
                              {return p.show(out);}
    void setTimeout(unsigned time_ms);
    bool isTimeout();
    sys::Time::Stamp_us getTimestamp_us(){return timestamp_us;} 
  };

 //---------------------------------------------- Net
  class Net
  {
   private:
    Net(const Net&); //no cloning

   protected:
    Device& device;
    Logger* logger;
    bool log;        //state
    
   public:
             Net(Device& device,Logger& logger);
             Net(Device& device);
    virtual ~Net();
    Net& setLogger(Logger& l);
    Logger* getLogger(){return logger;}
    Net& logon();
    Net& logoff();
    Net& send(Packet& pac);
    Net& receive(Packet& pac);
    Net& operator << (Packet& pac){return send(pac);}
    Net& operator >> (Packet& pac){return receive(pac);}
    unsigned getPacketSize()const {return device.getPacketSize();}
  };

 }
}
#endif
