//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#ifndef PAC_PCAP_H
#define PAC_PCAP_H
//--------------------------------
//pcap logger
//(c) D.Tschanz FHSO 2004
//$Id: pcap.h 193 2006-01-11 15:21:16Z buchmann $
//--------------------------------
INTERFACE(nio_pac_pcap,$Revision: 137 $)
#include "nio/pac/device.h"
#include <fstream>
namespace nio
{
 namespace pac
 {
//---------------------------------------------- Pcap
  class Pcap
  {
   protected:
    struct FileHeader{
     static const u_int32_t Magic = 0xa1b2c3d4;
     static const u_int16_t Major = 2;
     static const u_int16_t Minor = 4;
     
     u_int32_t magicNumber;
     u_int16_t majorVersion;
     u_int16_t minorVersion;
     u_int32_t timeZone;
     u_int32_t significantFigures;
     u_int32_t snapshotLength;
     u_int32_t linkType ;
    };
    struct PacketHeader{
     u_int32_t secondsFromDayLight;
     u_int32_t microsFromDayLight;
     u_int32_t capturedPacketLength;
     u_int32_t actualPacketLength;
    };
  };
   
//---------------------------------------------- PcapLogger
  class PcapLogger: public Pcap,
                    public Logger
  {
   std::ofstream output;
   void writeFileHeader();
   void writePacket(const Device::Result& res);

  public:
            PcapLogger(const char logFile[],Logger::Mode aMode);
   virtual ~PcapLogger();
  };
  
//---------------------------------------------- PcapDevice
//reading PcapFiles
  class PcapDevice:public Pcap,
                   public Device
  {
   private:
    std::ifstream src;
    FileHeader hdr;
   public:
             PcapDevice(const char devFile[]);
    virtual ~PcapDevice();
//implementation Device
    void send(const unsigned char data[],unsigned len);
    void receive(unsigned char d[],
                 unsigned capacity,
		 unsigned timeout, //0: no timeout
		 Result& res);
    const char* getMAC()const;
    unsigned getPacketSize()const;
  };
 }
}
#endif
