//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//-------------------------------------
//rawsocket
//(c) H.Buchmann FHSO 2004
//$Id: rawsocket.cc 193 2006-01-11 15:21:16Z buchmann $
//-------------------------------------
#include "sys/sys.h"
IMPLEMENTATION(nio_pac_rawsocket,$Revision: 137 $)
#include "nio/pac/rawsocket.h"
#include "util/endian.h"
#include "util/str.h"
#include "sys/msg.h"
#include "sys/time.h"

#include <unistd.h>
#include <errno.h>
#include <sys/socket.h>
#include <net/if.h>
#include <linux/if_ether.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <linux/if_packet.h>
#include <net/if_arp.h>
#include <sys/select.h>
#include <sys/time.h>
#include <string.h>
namespace nio
{
 namespace pac
 {
  RawSocket::RawSocket(const char name[]):
  name(name)
  {
   socketId();
   ifcType();
   ifcIndex();
   ifcBind();
   ifcMAC();
  }
  
  RawSocket::~RawSocket()
  {
   ::close(id);
  }
  
  void RawSocket::send(const unsigned char d[],unsigned len)
  {
   error(::sendto(id,d,len,0,0,0),"send");
  }
  
  void RawSocket::receive(unsigned char d[],
                              unsigned capacity,
			      unsigned timeout_ms,
			      Result& res)
  {
   ::sockaddr_ll from;
   unsigned fromLen=sizeof(from);
   ::fd_set set;
   ::timeval timev;
   timev.tv_sec=0;
   timev.tv_usec=1000*timeout_ms;
   FD_ZERO(&set);
   FD_SET(id,&set);
   while(true)
   {
    int r=::select(id+1,&set,0,0,(timeout_ms)?&timev:0);
    error(r,"select");
    if (r==0) //timeout
       {
        res.data=0;
	res.len=0;
	res.timeout=true;
	res.timeout_ms=0;
	break;
       }
    if (!FD_ISSET(id,&set)) continue;
    int len=::recvfrom(id,
                       d,capacity,MSG_TRUNC,
		       (struct sockaddr *)&from, &fromLen);
    error(len,"receive");
    res.data=d;
    res.len=len;
    res.timeout=false;
    res.timeout_ms= (unsigned)((1000000l*timev.tv_sec+timev.tv_usec)/1000l);
    res.timestamp_us=sys::Time::stamp();
    break;
   }
  }
  
  void RawSocket::error(int code,const char msg[])
  {
   if (code>=0) return;
//   ::perror(msg);
   sys::msg.error()<<msg<<" : "<<::strerror(errno)<<"\n";
  }
  void RawSocket::socketId()
  {
   id=::socket(PF_PACKET,SOCK_RAW,util::Endian::big((unsigned short)ETH_P_ALL));
   error(id,"socketId");
  }
  
  void RawSocket::ifcType()
  {
   struct ::ifreq ifr;
   util::Str::copy(name,ifr.ifr_name,sizeof(ifr.ifr_name));
   error(::ioctl(id,SIOCGIFHWADDR,&ifr),"ifcType");
   type=ifr.ifr_hwaddr.sa_family;   
  }
  
  void RawSocket::ifcIndex()
  {
   struct ::ifreq ifr;
   util::Str::copy(name,ifr.ifr_name,sizeof(ifr.ifr_name));
   error(::ioctl(id,SIOCGIFINDEX,&ifr),"ifcIndex");
   idx=ifr.ifr_ifindex;   
  }
  
  void RawSocket::ifcBind()
  {
   struct ::sockaddr_ll sll;
   __builtin_memset(&sll,0,sizeof(sll));
   sll.sll_family=AF_PACKET;
   sll.sll_ifindex=idx;
  // sll.sll_halen=6;
   sll.sll_protocol=util::Endian::big((unsigned short)ETH_P_ALL);
   error(::bind(id,(struct sockaddr*)&sll,sizeof(sll)),"ifcBind");
  }

  void RawSocket::ifcMAC()
  {
   struct ::ifreq ifr;
   util::Str::copy(name,ifr.ifr_name,sizeof(ifr.ifr_name));
   error(::ioctl(id,SIOCGIFHWADDR,&ifr),"ifcMAC");
   __builtin_memcpy(mac,ifr.ifr_hwaddr.sa_data,6);
  }

 }
}
