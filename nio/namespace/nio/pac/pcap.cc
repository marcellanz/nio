//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//--------------------------------
//pcap logger
//(c) D.Tschanz FHSO 2004
//$Id: pcap.cc 193 2006-01-11 15:21:16Z buchmann $
//--------------------------------
#include "sys/sys.h"
IMPLEMENTATION(nio_pac_pcap,$Revision: 137 $)
#include "nio/pac/pcap.h"
#include "nio/eth/pac.h"
#include "sys/msg.h"
#include "sys/host.h"
namespace nio
{
 namespace pac
 {
//---------------------------------------------- PcapLogger
  PcapLogger::PcapLogger(const char* logFile ,Mode aMode = Logger::INANDOUT)
  {
   output.open(logFile,std::ios::binary|std::ios::out);
   mode=aMode;
   writeFileHeader();
  }

  void PcapLogger::writeFileHeader()
  {
   FileHeader header;
   header.magicNumber = FileHeader::Magic;
   header.majorVersion = FileHeader::Major;
   header.minorVersion = FileHeader::Minor;
   header.timeZone = 0x0;
   header.significantFigures = 0x0;
   header.snapshotLength = 0xffff;//Maximum Packet length
   header.linkType = 0x1;         // Ethernet
   output.write((char*)&header,sizeof(FileHeader));
   output.flush();
  }

  void PcapLogger::writePacket(const Device::Result& res)
  {
   PacketHeader header;
   header.secondsFromDayLight =res.timestamp_us/1000000l;
   header.microsFromDayLight = res.timestamp_us%1000000l;
#if 0
   sys::msg<<"   "<<res.timestamp_us
           <<" sec "<<header.secondsFromDayLight
           <<" usec "<<header.microsFromDayLight<<"\n";
#endif	   
   header.capturedPacketLength = res.len;
   header.actualPacketLength = res.len;//0xffff;//Maximum Packet length
   output.write((char*)&header,sizeof(PacketHeader));
   output.write((const char*)res.data,res.len);
   output.flush();
  }

  PcapLogger::~PcapLogger()
  {
   output.close();
  }
  
  PcapDevice::PcapDevice(const char devFile[])
  {
   src.open(devFile);
   if (!src) sys::msg.error()<<"pcap file '"<<devFile<<"' dont exists\n";
   src.read((char*)&hdr,sizeof(hdr));
   if (hdr.magicNumber!=FileHeader::Magic)
      sys::msg.error()<<"file '"<<devFile<<"' not a pcap file\n";
      
  }

  PcapDevice::~PcapDevice()
  {
  }

  //implementation Device
  void PcapDevice::send(const unsigned char data[],unsigned len)
  {
   sys::msg.error()<<"PcapDevice is read only\n";
  }

  void PcapDevice::receive(unsigned char d[],
			   unsigned capacity,
			   unsigned timeout, //0: no timeout
			   Result& res)
  {
   PacketHeader ph;
   while(true)
   {
    src.read((char*)&ph,sizeof(ph));
    if (!src)
       {
	sys::msg<<"PcapDevice:: end of file\n";
	sys::exit(0);
       }
    if (ph.capturedPacketLength>=ph.actualPacketLength)
       {
	src.read((char*)d,ph.capturedPacketLength);
	res.data=d;
	res.len=ph.actualPacketLength;
	res.timeout=false;
	res.timeout_ms=0;
	res.timestamp_us=0;
	break;
       }
    sys::msg<<"PcapDevice: snaplen too short: skipping\n";
   }
  }

  const char* PcapDevice::getMAC()const
  {
   return (const char*)eth::Address::Zero;
  }

  unsigned PcapDevice::getPacketSize()const
  {
   return hdr.snapshotLength;
  }  
 }
}
