//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#ifndef PAC_DEVICE_H
#define PAC_DEVICE_H
//-------------------------------------
//device raw devices 
//(c) H.Buchmann FHSO 2004
//$Id: device.h 193 2006-01-11 15:21:16Z buchmann $
//-------------------------------------
INTERFACE(nio_pac_device,$Revision: 137 $)
#include "sys/time.h"
namespace nio
{
 namespace pac
 {
  //---------------------------------------------- Device
  //for reading/writing 
  class Device 
  {
   public:
    struct Result
    {
     unsigned char* data;
     unsigned len;
     bool timeout; 
     unsigned timeout_ms; //remaining time
     sys::Time::Stamp_us timestamp_us;
    };

    
   private:
    Device(const Device&); //no cloning
    
   public:
             Device(){}
    virtual ~Device();
    virtual void send(const unsigned char data[],unsigned len)=0;
    virtual void receive(unsigned char d[],
                         unsigned capacity,
			 unsigned timeout, //0: no timeout
			 Result& res)=0;
            //waits until packet ready
    virtual const char* getMAC()const=0;
    virtual unsigned getPacketSize()const=0;
  };

//---------------------------------------------- Logger
  class Logger
  {
   private:
    Logger(const Logger&);  //no cloning

   public:
             Logger(){}
    virtual ~Logger();
    virtual void open(){}
    virtual void writePacket(const Device::Result& res)=0;
    virtual void flush(){} //perhaps useful
    virtual void close(){}
//    Logger& operator << (const stream::RawOutput&);
//    Logger& operator >> (const stream::RawOutput&);
    enum Mode {IN=1,OUT,INANDOUT};
    Mode mode;
  };
 }
}
#endif
