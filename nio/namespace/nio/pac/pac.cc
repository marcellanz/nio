//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//-----------------------
//pac 
//(c) H.Buchmann FHSO 2003
//$Id: pac.cc 193 2006-01-11 15:21:16Z buchmann $
//-----------------------
#include "sys/sys.h"
IMPLEMENTATION(nio_pac,$Revision: 137 $)
#include "sys/msg.h"
#include "sys/host.h"
#include "util/str.h"
#include "nio/pac/stream/checksum.h"
#include "nio/pac/stream/length.h"
#include "nio/pac/pac.h"
#include <string.h>

namespace nio
{
 namespace pac
 {
 //---------------------------------------------- Type
  Type::Type(Packet* p):
  assigned(false),
  owner(p)
  {
//   sys::msg<<"type "<<p->name<<"\n";
   p->add(this);
  }
  
  Type::Type():
  assigned(false),
  owner(0)
  {
//   sys::msg<<"type\n";
  }

  void Type::set()
  {
   assigned=true;
  }

  void Type::set(const Type& src)
  {
   assigned=src.assigned;
  }

  void Type::toRaw(stream::RawOutput& out){put(out);}
  void Type::fromRaw(stream::RawInput& in){get(in);}
  
 //---------------------------------------------- Payload
  Payload::Payload(Packet* p):
  Type(p),data(0),capacity(0),len(0),aLen(0)
  {
   if (p->super) p->super->pl=0; //invalidate
   p->pl=this;
  }

  Payload::Payload(Packet* p,const Payload& pl):
  Type(p),data(0),capacity(0),len(0),aLen(0)
  {
   if (p->super) p->super->pl=0; //invalidate
   p->pl=this;
   set(pl.data,pl.len);
  }

  io::ascii::Writer& Payload::show(io::ascii::Writer& out) const
  {
   if (owner->pl) out<<"payload len "<<aLen<<"("<<len<<")";
   return out;
  }

  unsigned char const* Payload::set(unsigned len)
  {
   this->len=len;
   aLen=len;
   if (len>capacity)
      {
       if (data) delete [] data;
       data=new unsigned char[len];
       capacity=len;
      }
   return data;   
  }
  
  void Payload::adjustLen(unsigned aLen)
  {
   if (owner->pl==0) return;
   if (aLen>len)
      {
       sys::msg.error()<<"adjusted length ("<<aLen<<") > len("<<len<<")\n";
      }
   this->aLen=aLen;   
  } 
  
  unsigned char const* Payload::set(const char s[])
  {
   return set((const unsigned char*)s,util::Str::len(s));
  }

  Payload::~Payload()
  {
   if (data) delete [] data;
  }
  
  unsigned char const* Payload::set(const unsigned char d[],unsigned len)
  {
   set(len);
   __builtin_memcpy(data,d,len);
   return data;
  }

  void Payload::put(stream::Output& out) 
  {
   if ((owner->pl)&& data) out.put(data,aLen);
  }
  
  void Payload::fromRaw(stream::RawInput& in)
  {
   if (owner->pl)
      {
       set(in.avail());
       in.get(data,len); 
      }
  }

  void Payload::get(stream::Input& in)
  {
  }

 //------------------------------------------------------- CkeckSum
  Checksum::Checksum(Packet* p):
  Short(p,(unsigned short)0)
  {
  }
  
  Checksum::Checksum(Packet* p,const Checksum& cs):
  Short(p,cs.val)
  {
  }

  void Checksum::toRaw(stream::RawOutput& out)
  {
   if (!assigned)
      {
       stream::Checksum cs;
       val=0;
       cs.reset();
       for(unsigned i=0;i<tList.size();i++) tList[i]->put(cs);
       cs.set(val);
      }
   out.put(val);   
  }

  bool Checksum::isOK()
  {
   stream::Checksum cs;
   cs.reset();
   for(unsigned i=0;i<tList.size();i++) tList[i]->put(cs);
   return cs.get()==0xffff;
  }

 //------------------------------------------------------- Len
  Len::Len(Packet* p):
  Short(p,(unsigned short)0)
  {
  }

  Len::Len(Packet* p,const Len& len):
  Short(p,len)
  {
  }

  void Len::toRaw(stream::RawOutput& out) 
  {
   if (!assigned)
      {
       stream::Length len;
       len.reset();
       for(unsigned i=0;i<tList.size();i++) tList[i]->put(len);
       len.set(val);
      }
   out.put(val);   
  }
  
 //---------------------------------------------- Padding
  Padding::Padding(Packet* p):
  Type(p),
  len(0)
  {
   for(unsigned i=0;i<SIZE;i++) padding[i]=0;
  }

  Padding::Padding(Packet* p,const Padding& pad):
  Type(p),
  len(pad.len)
  {
   for(unsigned i=0;i<len;i++) padding[i]=pad.padding[i];
  }
  
  io::ascii::Writer& Padding::show(io::ascii::Writer& out) const
  {
   if (len) out<<"p"<<len;
   return out;
  }

  void Padding::put(stream::Output& out)
  {
#warning Padding::put ethernet compensation  
   for(unsigned i=0;i<len;i++) out.put((unsigned char)0);   
  }
  
  void Padding::get(stream::Input& in)
  {
#warning Padding::get ethernet compensation  
   unsigned pos=in.getBytePos()-14;
   len=(4-pos%4)%4;
   in.get(padding,len);   
  }
  
 //---------------------------------------------- End
  void End::put(stream::Output& out) 
  {
  }
  
  void End::get(stream::Input& in)
  {
  }

 //---------------------------------------------- Packet
  Packet::Packet(const char name[],Packet& super):
  name(name),
  super(&super),
  pl(0),
  timeout(false),
  timeout_ms(0),
  timestamp_us(-1)
  {
//   sys::msg<<"--- name "<<name<<"\n";
  }
  
  Packet::Packet(const char name[]):
  name(name),
  super(0),
  pl(0),
  timeout(false),
  timeout_ms(0),
  timestamp_us(-1)
  {
//   sys::msg<<"name "<<name<<"\n";
  }
  
  Packet::~Packet()
  {
  }
  
  void Packet::add(Type* t)
  {
   tList.push_back(t);
  }

  void Packet::operator = (const Packet& p)
  {
   if(super) *super=*p.super;
   for(unsigned t=0;t<tList.size();t++)
   {
    *(tList[t])=*(p.tList[t]);
   }
  }

  bool Packet::operator == (const Packet& p)
  {
   return !strcmp(name,p.name);
  }

  bool Packet::operator != (const Packet& p)
  {
   return strcmp(name,p.name);
  }

  void Packet::toRaw(stream::RawOutput& out)
  {
   if (super) super->toRaw(out);
   for(unsigned t=0;t<tList.size();t++)
   {
    Type* typ=tList[t];
    typ->toRaw(out);
   }
  }
  
  void Packet::onRaw()
  { 
   //do nothing 
  }
  
  bool Packet::fromRaw(stream::RawInput& in)
  {
   if (super) 
      {
       if (!super->fromRaw(in)) return false;
      }
//   sys::msg<<"----------------------- "<<name<<"\n";   
   if (!is()) return false;
   for(unsigned t=0;t<tList.size();t++)
   {
    Type* typ=tList[t];
    typ->fromRaw(in);
//    sys::msg<<"typ "<<*typ<<"\n";
   }
   onRaw();  //do some postprocessing if needed
   return true;
  }

  void Packet::setTimeout(unsigned time_ms)
  {
   this->timeout_ms = time_ms;
   timeout=false;
  }

  bool Packet::isTimeout()
  {
   return timeout;
  }

 //---------------------------------------------- Net
  Net::Net(Device& device,Logger& logger):
  device(device),
  logger(&logger),
  log(false)
  {
   this->logger->open();
  }

  Net::Net(Device& device):
  device(device),
  logger(0),
  log(false)
  {
  }
  
  Net::~Net()
  {
   if (logger) logger->close();
  }
  
  Net& Net::setLogger(Logger& l)
  {
   if (logger) logger->close();
   logger=&l;
   logger->open();
   return *this;
  }
  
  Net& Net::logon()
  {
   log=true;
   return *this;
  }
  
  Net& Net::logoff()
  {
   log=false;
   return *this;
  }

  Net& Net::send(pac::Packet& pac)
  {
   pac::stream::RawOutput out;
   unsigned capacity=getPacketSize();
   unsigned char data[capacity];
   out.set(data,capacity);
   pac.toRaw(out);

//   sys::msg.dump(data,out.getLen());
   device.send(data,out.getLen());
   Device::Result res;
   res.data=data;
   res.len=out.getLen();
   res.timeout=false;
   res.timestamp_us=sys::Time::stamp();
   pac.timestamp_us=res.timestamp_us;
   if (log&&logger) logger->writePacket(res);
   return *this;
  }

  Net& Net::receive(pac::Packet& pac)
  {
   pac::stream::RawInput in;
   unsigned capacity=getPacketSize();
   unsigned char data[capacity];
   Device::Result res;
   pac.timestamp_us=-1;
   while(true)
   {
    device.receive(data,capacity,pac.timeout_ms,res);
    if (res.timeout)
       {
        pac.timeout=true;
	pac.timeout_ms=0;
	break;
       }
    in.set(res.data,res.len);
    if (pac.fromRaw(in)) 
       {
        pac.timestamp_us=res.timestamp_us;
        if (log&&logger) logger->writePacket(res); 
        break;
       }
    if (pac.timeout_ms)
       {
        pac.timeout_ms=res.timeout_ms;
       }       
   }
   return *this;
  }
  
 }
}
