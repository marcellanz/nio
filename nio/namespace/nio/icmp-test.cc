//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//-----------------------------------
//icmp-test
//(c) H.Buchmann FHSO 2003
//$Id: icmp-test.cc 193 2006-01-11 15:21:16Z buchmann $
//-----------------------------------
#include "sys/sys.h"
IMPLEMENTATION(nio_icmp_test,$Revision: 137 $)
#include "nio/pac/pac.h"
#include "nio/eth/pac.h"
#include "nio/ip4/pac.h"
#include "nio/icmp/pac.h"
#include "nio/host.h"
#include "nio/pac/pcap.h"
#include "nio/pac/device.h"
#include "nio/pac/dump.h"

#include "sys/msg.h"
#include "io/ascii/out.h"

namespace nio
{
 class Tester
 {
  private:
   static Tester tester;
   pac::Net& eth;
   pac::PcapLogger pcap;
   pac::Dumper dump;
   io::ascii::Writer& msg;
   Tester();
 };
 
 Tester Tester::tester;
 
 Tester::Tester():
 eth(Host::getNet()),
 pcap("pcap.log",pac::Logger::INANDOUT),
 dump(sys::msg),
 msg(Host::getMsgWriter())
 {
  eth.setLogger(pcap).logon();
  sys::msg<<"--------------------- icmp-test\n";
  icmp::Echo pac;
  while(true)
  {
   eth>>pac;
   sys::msg<<pac<<"\n";
//   msg<<pac<<"\n";
   icmp::Echo rpl=pac;
   rpl.icmp.ip4.eth.src=pac.icmp.ip4.eth.dst;
   rpl.icmp.ip4.eth.dst=pac.icmp.ip4.eth.src;
   rpl.icmp.ip4.src=pac.icmp.ip4.dst;
   rpl.icmp.ip4.dst=pac.icmp.ip4.src;
   rpl.icmp.type=0;
//   sys::msg<<".";
//   rpl.icmp.ip4.len=pac.icmp.ip4.len;
//   rpl.dump(sys::msg);
   eth<<rpl;
//   sys::msg<<pac.icmp.chk.isOK()<<"\n";
  }
 }
}
