//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//--------------------------
//capture
//(c) H.Buchmann FHSO 2004
//$Id: capture.cc 193 2006-01-11 15:21:16Z buchmann $
//--------------------------
#include "sys/sys.h"
IMPLEMENTATION(nio_capture,$Revision: 137 $)
#include "util/endian.h"
#include "sys/msg.h"
#include "util/str.h"

#include <errno.h>
#include <sys/socket.h>
#include <net/if.h>
#include <linux/if_ether.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <linux/if_packet.h>
#include <net/if_arp.h>

class Capturer
{
 static Capturer capturer;
 static const char IFC[];
 int id;
 int type;
 int idx;
 void socketId();
 void ifcType();
 void ifcIndex();
 void ifcBind();
 void read();
 Capturer();
};

const char Capturer::IFC[]="eth1";
Capturer Capturer::capturer;

Capturer::Capturer()
{
 socketId();
 ifcType();   
 ifcIndex();
 ifcBind();  
 struct ::packet_mreq	mr;
 __builtin_memset(&mr,0,sizeof(mr));
 mr.mr_ifindex=idx;
 mr.mr_type   =PACKET_MR_PROMISC;
 if (::setsockopt(id,SOL_PACKET,PACKET_ADD_MEMBERSHIP,&mr,sizeof(mr))<0)
    {
     ::perror("setsockopt");
     sys::msg.error()<<"\n";
    }
 sys::msg<<"type "<<type<<" ARPHRD_ETHER "<<ARPHRD_ETHER<<"\n";   
 read();   
}

void Capturer::socketId()
{
 id=::socket(PF_PACKET,SOCK_RAW,util::Endian::big((unsigned short)ETH_P_ALL));
 if (id<0)
    {
     ::perror("socketId");
     sys::msg.error()<<"\n";//halt();      
    }
 ::perror("socketId");
}

void Capturer::ifcType()
{
 struct ::ifreq ifr;
 util::Str::copy(IFC,ifr.ifr_name,sizeof(ifr.ifr_name));
 if (::ioctl(id,SIOCGIFHWADDR,&ifr)<0)
    {
     ::perror("ifcType");
     sys::msg.error()<<"\n";//halt();
    }
 ::perror("ifcType");
 type=ifr.ifr_hwaddr.sa_family;   
}

void Capturer::ifcIndex()
{
 struct ::ifreq ifr;
 util::Str::copy(IFC,ifr.ifr_name,sizeof(ifr.ifr_name));
 if (::ioctl(id,SIOCGIFINDEX,&ifr)<0)
    {
     ::perror("ifcIndex");
     sys::msg.error()<<"\n";//halt();
    }
 ::perror("ifcIndex");
 idx=ifr.ifr_ifindex;   
}

void Capturer::ifcBind()
{
 struct ::sockaddr_ll sll;
 __builtin_memset(&sll,0,sizeof(sll));
 sll.sll_family=AF_PACKET;
 sll.sll_ifindex=idx;
// sll.sll_halen=6;
 sll.sll_protocol=util::Endian::big((unsigned short)ETH_P_ALL);
 if (::bind(id,(struct sockaddr*)&sll,sizeof(sll))<0)
    {
     ::perror("ifcBind");
     sys::msg.error()<<"\n";//halt();
    }
 ::perror("ifcBind");
}

void Capturer::read()
{
 unsigned char buffer[1<<12];
 struct ::sockaddr_ll from;
 unsigned fromLen=sizeof(from);
 while(true)
 {
  int pLen=::recvfrom(id,
                      buffer,1<<12,MSG_TRUNC,
		      (struct sockaddr *)&from, &fromLen);
  sys::msg<<"len= "<<pLen<<"\n";
  if (pLen<0)
     {
      ::perror("read");
      sys::msg.error()<<"\n";//halt();
     }
 }
}
