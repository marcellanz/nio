//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#ifndef NIO_ETH_PAC_H
#define NIO_ETH_PAC_H
//-------------------------------
//ethernet
//(c) H.Buchmann FHSO 2003
//$Id: pac.h 193 2006-01-11 15:21:16Z buchmann $
//-------------------------------
INTERFACE(nio_eth_pac,$Revision: 137 $)
#include "io/ascii/out.h"
#include "nio/pac/pac.h"
#include "nio/pac/stream/stream.h"
#include "nio/pac/stream/raw.h"

namespace nio
{
 namespace eth
 {
 //-------------------------------------------- Address
  class Address:public pac::Type
  {
   public:
    typedef unsigned char RAW[6];

   private:
    RAW raw;
    Address(const Address&);
    
   public:
    Address(pac::Packet* p);
    Address(pac::Packet* p,const Address& addr);
    void set(const unsigned char r[]);
    void get(unsigned char r[]);
    
    static io::ascii::Writer& show(io::ascii::Writer&,const RAW& r);
    io::ascii::Writer& show(io::ascii::Writer& out)const
                       {return show(out,raw);}
    void put(pac::stream::Output&);
    void get(pac::stream::Input&);
    bool equal(const Address&) const;
    bool equal(const RAW& r) const;
    bool equal(const char s[]) const;
    void operator=(const unsigned char m[]){set(m);pac::Type::set();}
    void operator=(const char s[]);
    bool operator==(const Address& a)const{return equal(a);}
    bool operator!=(const Address& a)const{return !equal(a);}  
    bool operator==(const char s[]) const{return equal(s);}
    bool operator!=(const char s[]) const{return !equal(s);}
    
//    operator RAW& () {return raw;}
//    const RAW& get()const{return raw;}
    static bool copy(const char s[],RAW& r);
    static void copyExit(const char s[],RAW& r);
             //parse s as MAC address and exits if not wellformed
    static void copy(const RAW& src,RAW& dst);
    static const RAW Broadcast;
    static const RAW Zero;
  };

//-------------------------------------------- Packet
  class Packet:public pac::Packet
  {
   private:
    void onTX(){}
    bool is(){return true;}
        
   public:
    Address dst;
    Address src;
    pac::Short type;
    pac::Payload payload;

	     Packet();
	     Packet(const Packet& p);
    virtual ~Packet();
    io::ascii::Writer& show(io::ascii::Writer& out) const;
    bool operator == (const pac::Packet& p){return true;}
    bool operator != (const pac::Packet& p){return false;}
    bool co(const Packet& p) const
         {
	  return (src==p.src) && (dst==p.dst);
	 }
    bool aco(const Packet& p) const
         {
	  return (dst==p.src) && (src==p.dst);
	 }
  };
 }
}
#endif
