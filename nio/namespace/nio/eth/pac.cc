//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//-------------------------------
//net
//(c) H.Buchmann FHSO 2003
//see man pages packet
//              socket
//$Id: pac.cc 193 2006-01-11 15:21:16Z buchmann $
//-------------------------------
#include "sys/sys.h"
IMPLEMENTATION(nio_eth_pac,$Revision: 137 $)
#include "nio/eth/pac.h"
#include "util/endian.h"
#include "sys/host.h"
#include "sys/msg.h"
#include "util/str.h"

namespace nio
{
 namespace eth
 {
 //--------------------------------------------------- Address
  const Address::RAW Address::Broadcast={0xff,0xff,0xff,0xff,0xff,0xff};
  const Address::RAW Address::Zero     ={0x00,0x00,0x00,0x00,0x00,0x00};

  Address::Address(pac::Packet* p):
  Type(p)
  {
   for(unsigned i=0;i<6;i++) raw[i]=0;
  }

  Address::Address(pac::Packet* p,const Address& addr):
  Type(p)
  {
   set(addr.raw);
  }

  io::ascii::Writer& Address::show(io::ascii::Writer& out,const RAW& raw)
  {
   static const char Digits[]="0123456789abcdef";
   for(unsigned i=0;i<6;i++)
   {
    if (i>0)out<<":";
    unsigned v=raw[i];
    out<<Digits[(v>>4)&0xf]<<Digits[v&0xf];
   }
   return out;
  }   

  bool Address::copy(const char s[],RAW& r)
  {
   unsigned status=0;
   unsigned i=0;  //index in s
   unsigned ri=0; //index in r
   while(true)
   {
    char ch=s[i++];
//    sys::msg<<ch<<"   "<<status<<"\n";
    switch(status)
    {
     case 0: //hex digit
     {
      int hd=util::Str::isHexDigit(ch);
      if (hd<0) return false;
      r[ri]=(unsigned char)hd;
      status=1;
     }
     break;
     
     case 1: //second digit | : | end
      {
       if (ch=='\0')return ri==5;
       if ((ch==':') && (ri<5))
          {
	   ri++;
	   status=0;
	   break;
	  }
       int hd=util::Str::isHexDigit(ch);
       if (hd<0) return false;
       r[ri]=(r[ri]<<4)|(unsigned char)hd;
       status=2;	  
      }	 
     break;
     
     case 2:
      if (ch=='\0') return ri==5;
      if (ch==':')
         {
	  if (ri<5){ri++;status=0;break;}
	 }
     return false; 	 
    }
   }
  }
  
  void Address::copyExit(const char s[],RAW& r)
  {
   if (!copy(s,r))
      {
       sys::msg.error()<<"'"<<s<<"' not a MAC address\n";
       sys::exit(1);
      }
  }
  
  void Address::copy(const RAW& src,RAW& dst)
  {
   dst[0]=src[0];
   dst[1]=src[1];
   dst[2]=src[2];
   dst[3]=src[3];
   dst[4]=src[4];
   dst[5]=src[5];
  }
  
  void Address::operator=(const char s[])
  {
   RAW r;
   copyExit(s,r);
   set(r);
   pac::Type::set();   
  }

  void Address::set(const unsigned char r[])
  {
   assigned=true;
   for(unsigned i=0;i<6;i++) raw[i]=r[i];
  }

  void Address::get(unsigned char r[])
  {
   for(unsigned i=0;i<6;i++) r[i]=raw[i];
  }

  void Address::put(pac::stream::Output& out)
  {
   out.put(raw,6);
  }
  
  void Address::get(pac::stream::Input& in)
  {
   in.get(raw,6);
  }

  bool Address::equal(const Address& m)const
  {
   return equal(m.raw);
  } 

  bool Address::equal(const RAW& r) const
  {
   for(unsigned i=0;i<6;i++)
   {
    if (raw[i]!=r[i]) return false;
   }
   return true;
  }

  bool Address::equal(const char s[])const
  {
   RAW r;
   if (copy(s,r)) return equal(r);
   return false;
  }
  
 //--------------------------------------------------- Packet
  Packet::Packet():
  pac::Packet("eth"),
  dst(this),
  src(this),
  type(this,(unsigned short)0),
  payload(this)
  {
  }

  Packet::Packet(const Packet& p):
  pac::Packet("eth(p)"),
  dst(this,p.dst),
  src(this,p.src),
  type(this,p.type),
  payload(this,p.payload)
  {
  }

  Packet::~Packet()
  {
  } 

  io::ascii::Writer& Packet::show(io::ascii::Writer& out) const
  {
   return out<<src<<"->"<<dst<<":"<<type<<" "
             <<payload; 
  }

 }
}
