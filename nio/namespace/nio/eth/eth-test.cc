//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//-----------------------
//eth-test
//(c) H.Buchmann FHSO 2004
//$Id: eth-test.cc 193 2006-01-11 15:21:16Z buchmann $
//-----------------------
#include "sys/sys.h"
IMPLEMENTATION(nio_eth_test,$Revision: 137 $)
#include "nio/eth/pac.h"
#include "nio/default.h"
#include "nio/host.h"
#include "util/pfile.h"

namespace nio
{
 namespace eth
 {
  class Tester
  {
   static Tester tester;
   pac::Net& net;
   Address::RAW dst;
   unsigned plLen;
   unsigned char* pl;
   unsigned short typ0;
   int inc;
    Tester();
   ~Tester();
  };
  
  Tester Tester::tester;
  
  Tester::Tester()
  :net(Host::getNet()),
   pl(0)
  {
   util::PFile para(1,0,false);
   Address::copyExit(para.valueOf("ETH_TEST_DST"),dst);
   plLen=para.valueOf("ETH_TEST_TX_PAYLOAD_LEN",60u);
   pl=new unsigned char[plLen];
   typ0=para.valueOf("ETH_TEST_TX_TYPE",0xffffu);
   inc =para.valueOf("ETH_TEST_TX_TYPE_INC",0);
   for(unsigned i=0;i<plLen;i++)pl[i]=i;
   Packet pac=Default::ETH;
   pac.payload.set(pl,plLen);
   pac.dst=dst;
   unsigned txCnt=para.valueOf("ETH_TEST_TX_COUNT",1u);
   unsigned short typ=typ0;
   for(unsigned i=0;i<txCnt;i++)
   {
    pac.type=typ;
    typ+=inc;
    net<<pac;
    
   }
  }
  
  Tester::~Tester()
  {
   if (pl) delete [] pl;
  }
 }//namespace eth
}//namespace nio
