//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//-----------------------------------
//ip4-test
//(c) H.Buchmann FHSO 2003
//$Id: ip4-test.cc 193 2006-01-11 15:21:16Z buchmann $
//-----------------------------------
#include "sys/sys.h"
IMPLEMENTATION(nio_ip4_test,$Revision: 137 $)
#include "nio/pac/pac.h"
#include "nio/eth/pac.h"
#include "nio/ip4/pac.h"
#include "nio/host.h"
#include "nio/pac/dump.h"
#include "sys/msg.h"
#include "sys/deb.h"

namespace nio
{
 class Tester
 {
  private:
   static Tester tester;
   static const ip4::Address::RAW myIP;
   static const eth::Address::RAW tarMAC;
   static const ip4::Address::RAW tarIP;
   static const char* Ok[];
   pac::Net& eth;
   pac::Dumper logger;
   void tx();
   void rx();
   Tester();
 };
 
 const char* Tester::Ok[]={"err","ok"};
 
 Tester Tester::tester;
 const ip4::Address::RAW Tester::myIP   ={10,88,65,60};
 const eth::Address::RAW Tester::tarMAC ={1,2,3,4,5,6};
 const ip4::Address::RAW Tester::tarIP  ={10,88,65,123};

 void Tester::tx()
 {
  static unsigned char Data[]={0,1,2,3,4,5,6,7,8,9};
  ip4::Packet pac;
  pac.eth.dst=tarMAC;
  pac.eth.src=Host::getMAC();
  pac.src="10.88.65.60";//myIP;
  pac.dst=tarIP;
  pac.pro=51;
//  pac.len=50;
  pac.pld.set(Data,sizeof(Data));
//  pac.chk=6;
  while(true)
  {
   sys::msg<<pac<<"\n";
   eth<<pac;
   sys::deb::get();
  }
 }
 
 void Tester::rx()
 {
  ip4::Packet pac;
  for(;;)
  {
   pac.setTimeout(4000);
   eth>>pac;
   if (pac.isTimeout()) sys::msg<<"timeout\n";
      else              sys::msg<<pac<<"\n";
//             "Checksum "<<Ok[pac.chk.isOK()]<<"\n";
   
  }
 }
 
 Tester::Tester():
 logger(sys::msg),
 eth(Host::getNet())
 {
  eth.setLogger(logger).logon();
  rx();
//  tx();
 }
}
