//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//-------------------------------------
//time-client
//demonstrates the programming with nio
// by acting as a time protocol client
// see RFC 868 
//(c) H.Buchmann FHSO 2005 
//$Id: time-client.cc 193 2006-01-11 15:21:16Z buchmann $
//-------------------------------------
//the next two lines have to be present in every cc file
//they guarantee correct startup of the program
//IMPLEMENTATION is a macro defined in sys.h
#include "sys/sys.h"                      
IMPLEMENTATION(nio_udp_demo,$Revision: 137 $)
#include "nio/default.h"  //for the default values of the packets
#include "nio/host.h"     //factory for the pac::Net 
#include "nio/udp/pac.h"  //basic nio stuff
#include "sys/msg.h"      //for console output
#include "util/pfile.h"   //for parameter reading key=value
#include "util/endian.h" 
#include "util/calendar.h"//time/date formatting

namespace nio
{
 namespace udp
 {
  class TimeClient
  {
   static const unsigned EPOCH=2208988800; //seconds 1.Jan.1900 1.Jan.1970
                                           //RFC868
   static TimeClient client;  
                      //this private instance will be called  when 
                      //the program starts
   pac::Net& net;     //for sending receiving packets		      
   TimeClient();      //do the relevant work		      
  };
  
  TimeClient TimeClient::client;    //calls the constructor
  
  TimeClient::TimeClient()
  :net(Host::getNet())
  {
   sys::msg<<"UDP TimeClient RFC 868\n";
   util::PFile config(1,0);    //reading config as command argument
   
   Packet tx(Default::UDP);    //creates udp packet from default
//configure packet tx 
   tx.ip4.eth.dst=config.valueOf("TIME_CLIENT_SERVER_MAC"); 
   tx.ip4.dst    =config.valueOf("TIME_CLIENT_SERVER_IP");  
   tx.port.src   =config.valueOf("TIME_CLIENT_PORT",1234);
   tx.port.dst   =config.valueOf("TIME_CLIENT_SERVER_PORT",37);
   sys::msg<<"tx: "<<tx<<"\n"; //show it
   net<<tx;                    //transmit it
   Packet rx;
   while(true)
   {
    net>>rx;                   //receive it
    if (rx.aco(tx)) break;
   }
   sys::msg<<"rx: "<<rx<<"\n"; //show rx packet
   sys::msg.dump(rx.pld.get(),10);
   util::Calendar::Time time;
   util::Calendar::toLocal(util::Endian::big(*(const unsigned*)rx.pld.get())-EPOCH,
			   time);
   time.show(sys::msg)<<"\n";
  }
 }//namespace udp
}//namespace nio
