//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//------------------------------------
//time-server
//for testing this server 
// use an official timeclient like for instance netdate
//(c) H.Buchmann FHSO 2005
//$Id: time-server.cc 193 2006-01-11 15:21:16Z buchmann $
//------------------------------------
//module definition
#include "sys/sys.h" 
IMPLEMENTATION(nio_udp_time_server,$Revision: 137 $)
#include "nio/default.h"
#include "nio/udp/pac.h"
#include "nio/host.h"
#include "util/endian.h"
#include "util/calendar.h"

namespace nio
{
 namespace udp
 {
  class TimeServer
  {
   static TimeServer server;
   static const unsigned short PORT=37;//the default one
   static const unsigned EPOCH=2208988800u;
   unsigned short port;
   pac::Net&   net;
   TimeServer();
   void run();
  };
  
  TimeServer TimeServer::server;
  
  TimeServer::TimeServer()
  :net(Host::getNet())
  {
   sys::msg<<"UDP TimeServer RFC 868\n";
   util::PFile config(1,0); //reading from command line
   port=config.valueOf("TIME_SERVER_PORT",PORT);
   run();
  }
  
  void TimeServer::run()
  {
   sys::msg<<"Listening on port "<<port<<"\n";
   while(true)
   {
    Packet rx;
    net>>rx;
    if (rx.port.dst==port)
       { //service it
         sys::msg<<"rx: "<<rx<<"\n";
         Packet tx(Default::UDP);
	 tx.ip4.eth.dst=rx.ip4.eth.src; //without arp
	 tx.ip4.dst    =rx.ip4.src;
	 tx.port.dst   =rx.port.src;
	 tx.port.src   =port;

         util::Calendar::Time_Sec ti=util::Endian::big(
	                       EPOCH+util::Calendar::getTime()
			                              );
	 tx.pld.set((unsigned char*)&ti,sizeof(ti));
	 net<<tx;
       }
   }
  }
 }//namespace udp
}//namespace nio
