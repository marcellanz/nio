//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#ifndef NIO_UDP_PAC_H
#define NIO_UDP_PAC_H
//--------------------------------
//udp
//(c) D.Tschanz FHSO 2003
//$Id: pac.h 193 2006-01-11 15:21:16Z buchmann $
//--------------------------------
INTERFACE(nio_udp_pac,$Revision: 137 $)
#include "nio/pac/pac.h"
#include "nio/ip4/pac.h"
#include "nio/pac/stream/stream.h"
#include "nio/pac/stream/raw.h"
#include "io/ascii/out.h"

namespace nio
{
 namespace udp
 {
  class Packet:public pac::Packet
  {
   private:
     bool is();
     void init();

   public:
   static const unsigned char PROT=17;
   Packet();
   Packet(const Packet& p);
   ip4::Packet ip4;

   struct Port
   {
    pac::Short src;
    pac::Short dst;
    Port(Packet* p):src(p,(unsigned short)0),dst(p,(unsigned short)0){}
    Port(Packet* p,const Port& prt):src(p,prt.src),dst(p,prt.dst){}
   };

   Port          port;
   pac::Len      len;
   pac::Checksum chk;
   pac::Payload  pld;
   io::ascii::Writer& show(io::ascii::Writer&)const;
   bool co(const Packet& p)
   {
    return ip4.co(p.ip4)&&(port.src==p.port.src)&&(port.dst==p.port.dst);
   }   
   bool aco(const Packet& p)
   {
    return ip4.aco(p.ip4)&&(port.src==p.port.dst)&&(port.dst==p.port.src);
   }   
  };
 }
}

#endif

