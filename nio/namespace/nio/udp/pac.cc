//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//--------------------------------
//udp
//(c) D.Tschanz FHSO 2003
//$Id: pac.cc 193 2006-01-11 15:21:16Z buchmann $
//--------------------------------
#include "sys/sys.h"
IMPLEMENTATION(nio_udp_pac,$Revision: 137 $)
#include "nio/udp/pac.h"
#include "nio/ip4/pac.h"
#include "sys/msg.h"
namespace nio
{ 
 namespace udp
 {

  io::ascii::Writer& Packet::show(io::ascii::Writer& out)const
  {
   ip4.show(out)<<"\n";
   out<<"src: "<<port.src  <<" "
        "dst: "<<port.dst  <<" "
        "len: "<<len  <<" "
        "chk: "<<chk  <<" "<<pld;
   return out;
  }

  Packet::Packet():
  pac::Packet("udp",ip4),
  port(this),
  len(this),
  chk(this),
  pld(this)
  {
   init();
  }

  Packet::Packet(const Packet& p):
  pac::Packet("udp",ip4),
  ip4(p.ip4),
  port(this,p.port),
  len(this,p.len),
  chk(this,p.chk),
  pld(this,p.pld)
  {
   init();
  }

  bool Packet::is()
  {
   return ip4.pro==PROT;
  }
  
  void Packet::init()
  {
   ip4.pro=PROT;
   chk.add(ip4.src).
       add(ip4.dst).
       add(*(new pac::Byte(0))).
       add(ip4.pro).
       add(len).
       add(port.src).
       add(port.dst).
       add(len).
       add(pld);
   ip4.len.add(port.src).
           add(port.dst).
           add(len).
           add(chk).
           add(pld);
   len.add(port.src).
       add(port.dst).
       add(len).
       add(chk).
       add(pld);
  }
 }
}

