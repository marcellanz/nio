//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//----------------------------
//net-test
//(c) H.Buchmann FHSO 2003
//$Id: net-test.cc 193 2006-01-11 15:21:16Z buchmann $
//----------------------------
#include "sys/sys.h"
IMPLEMENTATION(nio_net_test,$Revision: 137 $)
#include "nio/pac/pac.h"
#include "nio/eth/pac.h"
#include "nio/pac/dump.h"
#include "nio/host.h"
#include "nio/default.h"
#include "sys/msg.h"
#include "sys/deb.h"
#include "sys/host.h"
#include "util/str.h"

namespace nio
{
 class Tester
 {
  private:
   static Tester tester;
   pac::Net& eth;
   pac::Dumper log;
   void rx();
   void tx();
   Tester();
 };

 Tester Tester::tester;

 Tester::Tester():
 eth(Host::getNet()),
 log(sys::msg)
 {
  if (sys::argCnt()!=2)
     {
      sys::msg<<"usage "<<sys::argAt(0)<<" rx|tx\n"
                "   rx: receive\n"
		"   tx: transmit\n";
      sys::exit(1);		
     }
  if (util::Str::equal("rx",sys::argAt(1)))
     {
      rx();
      return; 
     }
  if (util::Str::equal("tx",sys::argAt(1)))
     {
      tx();
      return; 
     }
     
//  tx(); 
 }

 void Tester::rx()
 {
  sys::msg<<"------------- Net-Test rx\n";
  eth::Packet pac;
//  eth.setLogger(log);
//  eth.logon();
  while(true)
  {
//   pac.setTimeout(4000);
   eth>>pac;
   if (pac.isTimeout()) sys::msg<<"timeout\n";
      else              sys::msg<<pac<<"\n";
  }
 }

 void Tester::tx()
 {
  sys::msg<<"------------- Net-Test tx\n";
  static unsigned char Data[]={1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
  unsigned short typ=0xffff;
  eth::Packet pac(Default::ETH);
  pac.dst="0:1:2:b4:da:07";
  pac.payload.set(Data,sizeof(Data));
  while(true)
  {
   pac.type=typ--;
   sys::msg<<pac<<"\n";
   eth<<pac;
   sys::deb::get();
  }
 }
 
}
