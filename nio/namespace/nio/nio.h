//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#ifndef NIO_H
#define NIO_H
//--------------------------------
//nio
//(c) H.Buchmann FHSO 2003
//$Id: nio.h 193 2006-01-11 15:21:16Z buchmann $
//--------------------------------
INTERFACE(nio_collection,$Revision: 137 $)
#include "nio/host.h"
#include "nio/default.h"
#include "nio/pac/pac.h"
#include "nio/eth/pac.h"
#include "nio/ip4/pac.h"
#include "nio/tcp/pac.h"
#include "nio/icmp/pac.h"
#include "nio/udp/pac.h"
#include "nio/arp/pac.h"
#include "nio/test.h"
using namespace nio;
#ifdef NIO_SCENARIO
class Scenario:public nio::Test
{
 static Scenario scenario;
 Scenario();
};
#endif
#endif
