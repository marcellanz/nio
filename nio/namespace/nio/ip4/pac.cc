//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//--------------------------------
//ip4
//(c) H.Buchmann FHSO 2003
//$Id: pac.cc 193 2006-01-11 15:21:16Z buchmann $
//--------------------------------
#include "sys/sys.h"
IMPLEMENTATION(nio_ip4_pac,$Revision: 137 $)
#include "nio/ip4/pac.h"
#include "sys/msg.h"
#include "sys/host.h"
#include "util/endian.h"
#include "util/str.h"
#include <sys/types.h>
namespace nio
{
 namespace ip4
 {
 //--------------------------------------------- Address
  Address::Address(pac::Packet* p):
  Type(p)
  {
   raw[0]=0;raw[1]=0;raw[2]=0;raw[3]=0;
  }

  Address::Address(pac::Packet* p,const Address& addr):
  Type(p)
  {
   set(addr.raw);
  }

  io::ascii::Writer& Address::show(io::ascii::Writer& out,const RAW& r)
  {
   out.dec()<<(unsigned)r[0]<<"."
	    <<(unsigned)r[1]<<"."
	    <<(unsigned)r[2]<<"."
	    <<(unsigned)r[3];
   return out;
  }

  bool Address::copy(const char s[],RAW& r)
  {
   unsigned i=0; //index in s
   unsigned ri=0; //index in r
   unsigned status=0;
   unsigned d=0;  //current digit
   while(true)
   {
    char ch=s[i++];
 //   sys::msg<<"'"<<ch<<"'  "<<status<<"  '"<<ri<<"'   "<<d<<"\n";
    switch(status)
    {
     case 0: //first digit
     {
      int dd=util::Str::isDecDigit(ch);
      if (dd<0) return false;
      d=(unsigned)dd;
      status=1;
     }  
     break;
     case 1: //end | . |digit
     {
      if (ch=='\0')
         {
	  if (ri==3){ r[ri]=d;return true;}
	  return false;
	 }
      if (ch=='.')
         {
	  if (ri<3){r[ri++]=d;status=0;break;}
	  return false;   
	 }
      int dd=util::Str::isDecDigit(ch);
      if (dd<0) return false;
      d=10*d+dd;
      if (d>=256) return false;	 
     } 	 
     break;
    }
   }
  }
  
  void Address::copyExit(const char s[],RAW& r)
  {
   if (!copy(s,r))
      {
       sys::msg.error()<<"'"<<s<<"' not an IP4 Addres\n";
       sys::exit(1);
      }
  }
  
  void  Address::copy(const RAW& src,RAW& dst)
  {
   dst[0]=src[0];
   dst[1]=src[1];
   dst[2]=src[2];
   dst[3]=src[3];
  }
  
  void Address::put(pac::stream::Output& out)
  {
   out.put(raw,4);
  }
  
  void Address::get(pac::stream::Input& in)
  {
   in.get(raw,4);
  }
  
  bool Address::equal(const Address& a) const
  {
   return *(unsigned*)raw==*(unsigned*)(a.raw);
  }

  bool Address::equal(const char s[]) const
  {
   RAW r;
   if (copy(s,r)) 
       {
        return equal(r);
       }
   return false; 
  }

  void Address::set(const unsigned char data[])
  {
   assigned=true;
   raw[0]=data[0];
   raw[1]=data[1];
   raw[2]=data[2];
   raw[3]=data[3];
  }
 
  void Address::get(unsigned char data[])
  {
   data[0]=raw[0];
   data[1]=raw[1];
   data[2]=raw[2];
   data[3]=raw[3];
  }
 
  bool Address::equal(const RAW& r) const
  {
   return (r[0]==raw[0]) &&
          (r[1]==raw[1]) &&
          (r[2]==raw[2]) &&
          (r[3]==raw[3]);
  }
  
  void Address::operator=(const char s[])
  {
   RAW r;
   if (!copy(s,r))
      {
       sys::msg<<"**** not an ip address '"<<s<<"'\n";
       sys::exit(1);
      }
   set(r);
   pac::Type::set();   
  }

 //--------------------------------------------- Packet
  Packet::Packet():
  pac::Packet("ip4",eth),
  ver(this,VER),   
  ihl(this,IHL),
  tos(this,TOS),
  len(this),
  fid(this,FID),
  res(this,0u),
   df(this,0u),
   mf(this,0u),
  frg(this,FRG),
  tol(this,TOL),
  pro(this,PRO),
  chk(this),
  src(this),
  dst(this),
  pld(this),
  end(this)
  {
   init();
  }

  Packet::Packet(const Packet& p):
  pac::Packet("ip4(p)",eth),
  eth(p.eth),
  ver(this,p.ver),   
  ihl(this,p.ihl),
  tos(this,p.tos),
  len(this,p.len),
  fid(this,p.fid),
  res(this,p.res),
   df(this,p.df),
   mf(this,p.mf),
  frg(this,p.frg),
  tol(this,p.tol),
  pro(this,p.pro),
  chk(this,p.chk),
  src(this,p.src),
  dst(this,p.dst),
  pld(this,p.pld),
  end(this)
  {
   init();
  }
  
  Packet::~Packet()
  {
  }

  void Packet::init()
  {
   eth.type=ETHER_TYPE;
   chk.add(ver).   
       add(ihl).
       add(tos).
       add(len).
       add(fid).
       add(res).
       add( df).
       add( mf).
       add(frg).
       add(tol).
       add(pro).
       add(chk). 
       add(src).
       add(dst);
   len.add(ver).   
       add(ihl).
       add(tos).
       add(len).
       add(fid).
       add(res).
       add( df).
       add( mf).
       add(frg).
       add(tol).
       add(pro).
       add(chk). 
       add(src).
       add(dst);
  }
  
  io::ascii::Writer& Packet::show(io::ascii::Writer& out)const
  {
   static const char* RES[]={"","RES"};
   static const char* DF[] ={"" ,"DF"};
   static const char* MF[] ={"" ,"MF"};
   eth.show(out)<<"\n";
   out<<"ver: "<<ver  <<" "
	"ihl: "<<ihl  <<" "
	"tos: "<<tos  <<" "
	"len: "<<len  <<" "
      <<RES[res.get()]<<" "
      << DF[df.get()] <<" "
      << MF[mf.get()] <<" "
	"fid: "<<fid  <<" "
	"frg: "<<frg  <<" "
	"tol: "<<tol  <<" "
	"pro: "<<pro  <<" "
	"chk: "<<chk  <<"\n"
	       <<src  <<"->"
	       <<dst  <<" "
	       <<pld;
   return out;     
  }
  
  void Packet::onRaw()
  {
   //adjust payload lenght
   pld.adjustLen(len-4*ihl);
  }
 }
}
