//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#ifndef NIO_IP4_PAC_H
#define NIO_IP4_PAC_H
//--------------------------------
//ip4
//(c) H.Buchmann FHSO 2003
//$Id: pac.h 193 2006-01-11 15:21:16Z buchmann $
//TODO: options
//--------------------------------
INTERFACE(nio_ip4_pac,$Revision: 137 $)
#include "nio/pac/pac.h"
#include "nio/eth/pac.h"
#include "nio/pac/stream/checksum.h"
#include "nio/pac/stream/stream.h"
#include "nio/pac/stream/length.h"
#include "io/ascii/out.h"
#include "sys/msg.h"

namespace nio
{
 namespace ip4
 {
  class Address:public pac::Type
  {
   public:
    typedef unsigned char RAW[4];

   private:
    RAW raw;
    Address(const Address&);
    
   public:
    Address(pac::Packet*);
    Address(pac::Packet*,const Address&);
    
    void set(const unsigned char data[]);
    void get(unsigned char data[]);
    
    static io::ascii::Writer& show(io::ascii::Writer&,const RAW& r);
    io::ascii::Writer& show(io::ascii::Writer& out) const
                       {return show(out,raw);}
    void put(pac::stream::Output& out);
    void get(pac::stream::Input& in);
    bool equal(const Address& a) const;
    bool equal(const char s[]) const;
    bool equal(const RAW& a) const;
    bool operator==(const Address& a)const{return  equal(a);}
    bool operator==(const RAW& r)const{return equal(r);}
    bool operator==(const char s[])const{return equal(s);}
    bool operator!=(Address& a)const{return !equal(a);}
    bool operator!=(const char s[])const{return !equal(s);}
    bool operator!=(const RAW& r)const{return !equal(r);}
    void operator=(const unsigned char data[]){set(data);pac::Type::set();}
    void operator=(const char s[]);

    static bool copy(const char s[],RAW& r);
    static void copyExit(const char s[],RAW& r);
       //parse s as IP4 address and exits if not well formed
    static void copy(const RAW& src,RAW& dst);
  };
  
 
  class Packet:public pac::Packet
  {
   private:
    bool is(){return eth.type==ETHER_TYPE;}
    void init();
    void onRaw();
    
   public:
    static const unsigned short ETHER_TYPE = 0x0800;
    static const unsigned   IHL=5;
    static const unsigned   VER=4;
    static const unsigned   TOS=0;
    static const unsigned short FID=0;
    static const unsigned  FRG=0;
    static const unsigned   TOL=0xff;
    static const unsigned   PRO=0;
    static const unsigned short CHK=0;

    eth::Packet eth;
    pac::Bits<4>  ver;   
    pac::Bits<4>  ihl;
    pac::Byte     tos;
    pac::Len      len;
    pac::Short    fid; //fragmentation
    pac::Flag     res;
    pac::Flag     df;
    pac::Flag     mf;
    pac::Bits<13> frg;
    pac::Byte     tol;
    pac::Byte     pro;
    pac::Checksum chk;
    Address       src;
    Address       dst;
    pac::Payload  pld;
    pac::End      end;
    
    io::ascii::Writer& show(io::ascii::Writer&)const;
             Packet();
	     Packet(const Packet& p);
    virtual ~Packet();
    bool co(const Packet& p) const
         {
	  return eth.co(p.eth)&&(src==p.src) && (dst==p.dst);
	 }
    bool aco(const Packet& p) const
         {
	  return eth.aco(p.eth)&&(dst==p.src) && (src==p.dst);
	 }
    
  };
 }
}
#endif
