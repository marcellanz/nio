//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#include "sys/sys.h"
IMPLEMENTATION(foo,$Revision: 137 $)
#include "nio.h"

Scenario::Scenario()
{
//Begin iteration on 3 commands
/*<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<Services StartStop="10" Type="EC_UDP"><PortClient>1815</PortClient><PortServer>53</PortServer><IPAdd>195.176.96.46</IPAdd></Services>

*/
// Agent Command
// Agent command type : EC_UDP

//   agent::Agent agent1
   bool bResult=agent1.send("1004;10;53;1815;c3b0602e;");
   if (bResult) sys::msg<<"Send OK\n"; else sys::msg<<"Send error\n";
/*<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<Message MessID="1"><ICMPv4Mess><EthernetHeader DestAddr="00:20:ed:43:f4:38" Type="2048"/><IPv4Header TOSPrecedence="0" TOSDelay="false" TOSReserved="0" Protocol="1" Identifier="0" FragOffset="0" TimeToLive="64" TOSReliability="false" Version="4" FlagMoreFrag="false" HeaderLength="20" TOSThroughput="false" TotalLength="84" FlagDontFrag="false" FlagReserved="false" DestAddr="157.26.97.236"/><IPv4Options/><ICMPv4Header ICMPType="8" ICMPIdentifier="4879" ICMPData="8a06c63fdd03020008090a0b0c0d0e0f101112131415161718191a1b1c1d1e1f202122232425262728292a2b2c2d2e2f3031323334353637" ICMPCode="0" ICMPSequenceNumber="2"/></ICMPv4Mess></Message>

*/
// ICMP v4 send
// IP destination address : null
// IP source address : 157.26.97.236
// ICMP type : 8
   icmp::Packet pacSendICMP2(Default::ICMP);
   pacSendICMP2.ip4.eth.dst="00:20:ed:43:f4:38";
   pacSendICMP2.ip4.eth.type=2048;
   pacSendICMP2.ip4.ver=4;
   pacSendICMP2.ip4.ihl=20;
   pacSendICMP2.ip4.tos=28;
   pacSendICMP2.ip4.len=20;
   pacSendICMP2.ip4.fid=0;
   pacSendICMP2.ip4.res=false;
   pacSendICMP2.ip4.df=false;
   pacSendICMP2.ip4.mf=false;
   pacSendICMP2.ip4.frg=0;
   pacSendICMP2.ip4.tol=64;
   pacSendICMP2.ip4.pro=1;
   pacSendICMP2.ip4.dst="157.26.97.236";
//*****************Warning, IP options not implemented !*********************
   pacSendICMP2.type=8;
   pacSendICMP2.code=0;
   pacSendICMP2.pld.set("0x130f00028a06c63fdd03020008090a0b0c0d0e0f101112131415161718191a1b1c1d1e1f202122232425262728292a2b2c2d2e2f3031323334353637");
//******************WARNING, other ICMP type are not implemented (4,12-18)**********************
   eth<<pacSendICMP2;
   sys::msg<<pacSendICMP2<<"\n";
/*<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<Message MessID="2"><ICMPv4Mess><EthernetHeader/><IPv4Header SrcAddr="157.26.97.236"/><IPv4Options/><ICMPv4Header ICMPType="0"/></ICMPv4Mess></Message>

*/
// ICMPv4 receive
// IP destination address required : null
// IP source address required : 157.26.97.236
   icmp::Packet packet3;
   eth.logon();
   packet3.setTimeout(5000);
   do{
      eth>>packet3;
      if (packet3.isTimeout())
         break;
      sys::msg<<packet3<<"\n";
   }while (!(
             (packet3.ip4.src=="157.26.97.236")&& 
	     (packet3.type==0)));
//Warning, in receive, only address are tested in layer 2-3-4
}
