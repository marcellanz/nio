//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//---------------------------
//device-test
//(c) H.Buchmann FHSO 2004
//$Id: device-test.cc 193 2006-01-11 15:21:16Z buchmann $
//---------------------------
#include "sys/sys.h"
IMPLEMENTATION(device_test,$Revision: 137 $)
#include "nio/pac/rawsocket.h"
#include "sys/msg.h"
#include "sys/thread.h"

namespace nio
{
 class Device:public sys::Runnable,
              public pac::RawSocket
 {
  private:
   sys::Thread thread;
   void run();
  public:
            Device();
   virtual ~Device(){}
 };
 
 Device::Device():
 pac::RawSocket("eth0"),
 thread(*this)
 {
  thread.start();
 }

 void Device::run()
 {
  static const unsigned BUF_SIZE = 1600;
  unsigned char buffer[BUF_SIZE];
  pac::Device::Result res;
  while(true)
  {
   receive(buffer,BUF_SIZE,0,res);
   if (res.timeout) sys::msg<<"timeout\n";
      else          sys::msg<<"--- len = "<<res.len<<"\n";
  }
 }
  
 class DeviceTest
 {
  static DeviceTest deviceTest;
  Device dev;
  pac::RawSocket device;
  DeviceTest();
  void rx();
  void tx();
 };

 DeviceTest DeviceTest::deviceTest;

 DeviceTest::DeviceTest():
 device("eth0")
 {
  rx();
 }

 void DeviceTest::rx()
 {
  static const unsigned BUF_SIZE = 1600;
  unsigned char buffer[BUF_SIZE];
  pac::Device::Result res;
  while(true)
  {
   device.receive(buffer,BUF_SIZE,0,res);
   sys::msg<<"len = "<<res.len<<"\n";
  }
 }

 void DeviceTest::tx()
 {
  static const unsigned BUF_SIZE=100;
  unsigned char buffer[BUF_SIZE];
  unsigned idx=0;
  const char* mac=device.getMAC();
  for(unsigned i=0;i<6;i++)buffer[idx++]=0xff;
  for(unsigned i=0;i<6;i++)buffer[idx++]=mac[i];
  buffer[idx++]=0xff;
  buffer[idx++]=0xff;
  unsigned i=0;
  while(idx<BUF_SIZE)
  {
   buffer[idx++]=i++;
  }
  device.send(buffer,BUF_SIZE);
 }
}
