//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

// ------- This is the header lines of the generated code --------
// Name of the scenario        : test
// Description of the scenario : 
// ---------------------------------------------------------------


#include "sys/sys.h"
IMPLEMENTATION(foo,$Revision: 137 $)
#include "nio.h"
//#include "xmlresult.h"

Scenario::Scenario()
{
//   xml::XMLResult results;
   const time_t t=time(0);
//   results.header("/home/pem/tmp/test.xml","test", ctime(&t), "", "","/home/pem/tmp/test.cc.dump",0);
   bool bResult;
//Begin iteration on 3 commands
/*<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<Message MessID="1"><ARPMess><EthernetHeader DestAddr="ff:ff:ff:ff:ff:ff"/><ARPHeader AddLogEmt="157.26.101.46" AddPhyEmt="00:01:02:35:c8:B8" AddLogRec="157.26.96.15"/></ARPMess></Message>

*/
// ARP send
// Ethernet destination address : ff:ff:ff:ff:ff:ff
// Ethernet source address : null
// Ethernet type : 0
   arp::Packet packet1(Default::ARP);
   packet1.eth.dst="ff:ff:ff:ff:ff:ff";
   packet1.src.pr="157.26.101.46";
   packet1.tar.pr="157.26.96.15";
   packet1.src.hw="00:01:02:35:c8:B8";
   eth<<packet1;
   sys::msg<<packet1<<"\n";
//   results.addFrameSend("Send : ARP 1",packet1.getTimeStampSec(),packet1.getTimeStampUSec() );
/*<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<Message MessID="2"><ARPMess><EthernetHeader/><ARPHeader AddLogEmt="157.26.96.15"/></ARPMess></Message>

*/
// ARP receive
// Ethernet destination address required : null
// Ethernet source address required : null
// Ethernet type required : 0
   arp::Packet packet2;
   eth.logon();
   packet2.setTimeout(5000);
   do{
      eth>>packet2;
      if (packet2.isTimeout())
         break;
      sys::msg<<packet2<<"\n";
   }while (!((true)));
//Warning, in receive, only address are tested in layer 2-3-4
   if (packet2.isTimeout())
      sys::msg<<"timeout\n";
//      results.addFrameNotReceive("Receive : ARP 2",packet2.getTimeStampSec(),packet2.getTimeStampUSec() );
   else
      sys::msg<<"Receive : ARP 2"<<(unsigned)packet2.getTimestamp_us()<<"\n";
//      results.addFrameReceive("Receive : ARP 2",packet2.getTimeStampSec(),packet2.getTimeStampUSec() );
   if (! (packet2.isTimeout())){
       sys::msg<<"***********************************************************************************";
       sys::msg<<"WARNING, no timeout occurs but it was expected !";
       sys::msg<<"***********************************************************************************\n";}
   bResult=agent1.send("4010");
//   results.footer("Success");
   if (bResult) sys::msg<<"Success\n"; else sys::msg<<"Send error\n";
    exit(0);
//   results.footer("results comments...");
}

