//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//----------------------------
//default
//(c) H.Buchmann FHSO 2003
//$Id: default.cc 193 2006-01-11 15:21:16Z buchmann $
//----------------------------
#include "sys/sys.h"
IMPLEMENTATION(nio_default,$Revision: 137 $)
#include "nio/default.h"
#include "nio/host.h"
#include "util/plist.h"

#include "sys/msg.h"

namespace nio
{
 Default Default::default_;
 
 const eth::Packet&  Default::ETH(default_.eth);
 const ip4::Packet&  Default::IP4(default_.ip4);
 const arp::Packet&  Default::ARP(default_.arp);
 const icmp::Packet& Default::ICMP(default_.icmp);
 const udp::Packet& Default::UDP(default_.udp);
 const tcp::Packet& Default::TCP(default_.tcp);
 
 Default::Default()
// :agent(0)
 {
  util::PList& p=Host::getPList();  
  eth.dst=(p.isDefined("IP_BENCH_ETH_DST"))?
             p.valueOf("IP_BENCH_ETH_DST"):
             "ff:ff:ff:ff:ff:ff";
  eth.src=Host::getMAC();
  eth.type=p.valueOf("IP_BENCH_ETH_TYPE",0u);
  
  ip4.eth=eth;
  ip4.eth.type=ip4::Packet::ETHER_TYPE;
  ip4.pro=p.valueOf("IP_BENCH_IP4_PRO",0u);
  ip4.dst=(p.isDefined("IP_BENCH_IP4_DST"))?
              p.valueOf("IP_BENCH_IP4_DST"):
	      "255.255.255.255"; 
  ip4.src=Host::getIP4();

  icmp.ip4=ip4;
  icmp.ip4.pro=icmp::Packet::IP_PROT;
  arp.eth=eth;
  udp.ip4=ip4;
  udp.ip4.pro=udp::Packet::PROT;
  tcp.ip4=ip4;
  tcp.ip4.pro=tcp::Packet::PROT;
//  agent=new agent::Agent(p.valueOf("IP_BENCH_IP4_DST"));
 }

 Default::~Default()
 {
//  if (agent) delete agent;
 }

}
