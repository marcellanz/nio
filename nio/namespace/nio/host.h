//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#ifndef NIO_HOST_H
#define NIO_HOST_H
//---------------------------------
//host
//(c) H.Buchmann FHSO 2003
//$Id: host.h 193 2006-01-11 15:21:16Z buchmann $
//---------------------------------
INTERFACE(nio_host,$Revision: 137 $)
#include "nio/pac/rawsocket.h"
#include "nio/pac/device.h"
#include "nio/eth/pac.h"
#include "nio/ip4/pac.h"
#include "io/ascii/file/file.h"
#include "util/pfile.h"

namespace nio
{
 class Host
 {
  public:
   static const char NIO[]; //environment
  private:
  
   static Host host;
   util::PFile           para;   
   pac::Device*          device;
   pac::Net*             net;
   pac::Logger*          logger;
   io::ascii::file::Writer* msg;
   ip4::Address::RAW	  ip4;
   void close();
   Host(const Host&);
   Host();
    
  public:
   virtual ~Host();
   enum Loggers {PCAPLOGGER=1};
   static util::PList& getPList(){return host.para;}
   static pac::Net& getNet(){return *host.net;}
   static const eth::Address::RAW& getMAC()
   {
    return *(const eth::Address::RAW*)host.device->getMAC();
   }
   static ip4::Address::RAW& getIP4(){return host.ip4;}
   static io::ascii::Writer& getMsgWriter(){return *host.msg;}
 };
}
#endif
