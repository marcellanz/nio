//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//---------------------------------
//layer-test
//(c) D.Tschanz FHSO 2004
//$Id: layer-test.cc 193 2006-01-11 15:21:16Z buchmann $
//---------------------------------
#include "sys/sys.h"
IMPLEMENTATION(nio_layer_test,$Revision: 137 $)
#include "layer.h"
#include "sys/msg.h"
#include "tcp/tcp.h"
#include "host.h"
#include "pac/pac.h"
#include "ip4/ip4.h"
#include "default.h"

#include <unistd.h>

namespace nio
{
 class Tester
 {
  private:
   static Tester tester;
   pac::Net& eth;
   layer::Connection connection;
   layer::Listener listener;
   static void callBack(tcp::Packet& aPacket);
   void rx();
   void tx();   
   Tester();
 };
 
 Tester Tester::tester;

 void Tester::callBack(tcp::Packet& aPacket)
 {
  sys::msg<<"Data received: "<<(char*)aPacket.pld.getData()<< "\n";
 }

 void Tester::rx()
 {
  tcp::Packet pac;
  while(true)
  {  
   eth>>pac;
   sys::msg<<pac<<"\n";
   pac.ip4.chk.isOK();
   pac.chk.isOK();
  }
 }

 void Tester::tx()
 {
  static unsigned char Data[]={1,2,3,4,5,6,7,8,9,10};
  tcp::Packet pac;
  pac.ip4.eth.dst="1:2:3:4:5:6";
  pac.ip4.eth.src=Host::getMAC();
  pac.ip4.dst="192.168.1.55";
  pac.ip4.src=Host::getIP4();
  pac.prt.src=1234;
  pac.prt.dst=7;
  pac.pld.set(Data,sizeof(Data));
  eth<<pac;
 }
  
 Tester::Tester():
 eth(Host::getNet()),
 listener(callBack)
 {
  const ip4::Address::RAW targetIP = {10,88,65,59};
  tcp::Packet pac(Default::TCP);
  tcp::Packet data(Default::TCP);
//  eth.setLogger(logger);
  eth.startServices();
  pac.ip4.dst = targetIP;
  pac.prt.src = 1234;
  pac.prt.dst = 7;
  layer::Connection connection;
  connection.addListener(listener);
  connection.open(&pac);
  connection.printLastError();
sys::msg<<"Connectet to peer\n";
  data.ip4.dst = targetIP;
  data.prt.dst = 1;
  data.pld.set("Hello World\0");

sleep(1);
connection.dumpState();
  connection.send(&data);
//connection.dumpState();
//  connection.printLastError();
//sleep(1);
//connection.dumpState();
  connection.close();
connection.dumpState();
//  connection.printLastError();
sleep(1);
 }
}
