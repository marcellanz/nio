//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#ifndef NIO_ICMP_PAC_H
#define NIO_ICMP_PAC_H
//-----------------------------------
//icmp
//(c) H.Buchmann FHSO 2003
//$Id: pac.h 193 2006-01-11 15:21:16Z buchmann $
//-----------------------------------
INTERFACE(nio_icmp_pac,$Revision: 137 $)
#include "nio/ip4/pac.h"
#include "io/ascii/out.h"

namespace nio
{
 namespace icmp
 {
 //------------------------------------------ Packet  
  class Packet:public pac::Packet
  {
   private:
    void onTX(){}
    bool is(){return ip4.pro==(unsigned)IP_PROT;}
    void init();
    
   public:
    static const unsigned char IP_PROT=1;
    ip4::Packet ip4;
    pac::Byte type;
    pac::Byte code;
    pac::Checksum chk;
    pac::Payload pld;
    pac::End end;
    io::ascii::Writer& show(io::ascii::Writer&) const;
    
             Packet();
	     Packet(const Packet&);
    virtual ~Packet();
  };

//------------------------------------------ Echo
  class Echo:public pac::Packet
  {
   public:
    static const unsigned short REQUEST=8;
    static const unsigned short REPLY  =0;

   private:
    void onTX(){}
    bool is(){return (icmp.type==REQUEST)||(icmp.type==REPLY);}
    void init();
    
   public:
    icmp::Packet icmp;
    pac::Short id;
    pac::Short seq;
    pac::Payload pld;
    pac::End end;
    
    io::ascii::Writer& show(io::ascii::Writer&) const;
             Echo();
	     Echo(const Echo&);
    virtual ~Echo();
  };
 }
}
#endif
