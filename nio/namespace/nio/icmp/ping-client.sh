#-------------------------
#ping-client.sh 
#(c) H.Buchmann FHSO 2004
#$Id: ping-client.sh 193 2006-01-11 15:21:16Z buchmann $
#-------------------------
PACKET_COUNT=10
PERIOD_MS=64
ICMP_ECHO_ID=0x1234
PAYLOAD_SIZE="16 32 48 64 96 128 160 192 224 256 320 384 448 512 \
              640 768 896 1024"
RESULT="xxx.data"

#-----------------------------------------------
#enter your date here
# see ping-client.config for more info
#    ip4         mac
DST="192.168.1.5   0:e0:7d:7d:ef:fd \
     192.168.1.138 00:90:D0:03:AF:DA\
     192.168.1.55  1:2:3:4:5:6\
     192.168.1.20  00:40:49:FA:31:42"

#--------------------
#DST=\
#"10.88.65.20      00:06:5B:11:C8:05\
# 10.88.65.38      00:30:65:68:21:70
# 10.88.65.1       00:00:0C:07:AC:01\
#"
# 10.88.65.73      00:08:74:11:17:48\
# 10.88.65.65      00:06:C6:00:10:A2"
#-----------------------------------------------

function doPing 
{
#$1 ip4 address
#$2 mac address
#$3 payload size
 TMP=$(mktemp p.XXXXXX)
 echo "PING_CLIENT_IP4_DST = $1">>$TMP
 echo "PING_CLIENT_MAC_DST = $2">>$TMP
 echo "PING_CLIENT_COUNT   = $PACKET_COUNT">>$TMP
 echo "PING_CLIENT_TIME_TICK_MS = $PERIOD_MS">>$TMP
 echo "PING_CLIENT_ID		= 0x1234">>$TMP
 echo "PING_CLIENT_VERBOSE	= NO">>$TMP
 echo "PING_CLIENT_RESULT_FILE  = $RESULT">>$TMP
 echo "PING_CLIENT_RESULT_FILE_MODE   = APPEND">>$TMP
 ./ping-client $TMP $3
 rm $TMP
}

function main
{
rm $RESULT
echo "#PACKET_COUNT= $PACKET_COUNT">>$RESULT
echo "#PERIOD_MS   = $PERIOD_MS   ">>$RESULT

STATUS=0
for s in $DST;do
 if [ $STATUS = 0 ]
    then IP4=$s
         STATUS=1
    else MAC=$s
         STATUS=0
         echo "#$IP4 $MAC">>$RESULT
	 doPing $IP4 $MAC "$PAYLOAD_SIZE" 
	 echo >>$RESULT
	 echo >>$RESULT
 fi
done 
}

#------------------------------- main
main
