//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//--------------------------
//ping-client
//(c) H.Buchmann FHSO 2004
//$Id: ping-client.cc 193 2006-01-11 15:21:16Z buchmann $
//--------------------------
#include "sys/sys.h"
IMPLEMENTATION(nio_ping_client,$Revision: 137 $)
#include "nio/icmp/pac.h"
#include "nio/host.h"
#include "nio/pac/dump.h"

#include "io/ascii/file/file.h"
#include "util/pfile.h"
#include "util/str.h"
#include "sys/timeout.h"
#include "sys/ticker.h"
#include "sys/thread.h"
#include "sys/host.h"
#include "sys/msg.h"
#include "sys/time.h"

namespace nio
{
 class PingClient:public sys::Timeout::Listener
 {
  static const char Version[];
  
  struct Payload
  {
   sys::Time::Stamp_us timestamp;
   unsigned len;
   unsigned char payload[0];
  };
  
  static PingClient pC;
  pac::Dumper log;
  pac::Net& net;
  sys::Ticker ticker;  
  sys::Timeout::Entry timeout;
  icmp::Echo request;
  io::ascii::file::Writer* result;
  unsigned packetSizeLen;
  unsigned* packetSize;
  bool verbose;
  unsigned count;
  unsigned id;
  unsigned seqTX,seqRX;
  unsigned missing;
  unsigned min,max;
  double avg;  

  void setup();
  void readPacketSize(util::PFile& para); //read parameters 
  void readResultFile(util::PFile& para);
  void init();
  void run();
  void run(unsigned pSize);
  void wrResult();
  void onTimeout(sys::Timeout::Entry&);
  PingClient();
  ~PingClient();
 };

 const char PingClient::Version[]=
 "\nipBench: ping-client (c) H.Buchmann FHSO 2004\n$Id: ping-client.cc 193 2006-01-11 15:21:16Z buchmann $\n";
 
 PingClient PingClient::pC;

 PingClient::PingClient()
 :log(sys::msg),
  net(Host::getNet()),
  timeout(this),
  result(0),
  packetSizeLen(0),packetSize(0),
  verbose(false),
  id(0x1234u),
  count(5)
 {
  if (sys::argCnt()<2)
     {
      sys::msg<<Version<<"\n"
                "usage: "<<sys::argAt(0)<<" configFile packetSize*\n"
                "       packetSize number\n";
     		
      sys::exit(1);
     }
  net.setLogger(log);
  setup();
  run();
 }

 PingClient::~PingClient()
 {
  if (packetSize) delete [] packetSize;
  if (result) 
     {
      result->info(sys::msg<<"data written to ")<<"\n";
      delete result;
     }
 }

 void PingClient::readPacketSize(util::PFile& para)
 {
  packetSizeLen=sys::argCnt()-2; //first two parameters are prog config
  if (packetSizeLen==0) 
      {
       packetSizeLen=1;
       packetSize=new unsigned[1];
       packetSize[0]=para.valueOf("PING_CLIENT_PAYLOAD_SIZE",50u);
       return;
      }
     
  packetSize=new unsigned[packetSizeLen];
  unsigned argI=2; 
  for(unsigned i=0;i<packetSizeLen;i++)
  {
   if (!util::Str::to(sys::argAt(argI),packetSize[i]))
      {
       delete [] packetSize;
       packetSize=0;
       sys::msg<<"parameter '"<<sys::argAt(argI)<<"' no an number\n";
       sys::exit(1);
      }
   argI++;   
  }
 }

 void PingClient::readResultFile(util::PFile& para)
 {
  if (!para.isDefined("PING_CLIENT_RESULT_FILE")) return;
  unsigned mode=para.valueOf("PING_CLIENT_RESULT_FILE_MODE",
                             io::byte::file::Output::ModeName,
			     io::byte::file::Output::CREATE);
  result=new io::ascii::file::Writer(para.valueOf("PING_CLIENT_RESULT_FILE"),
                                    (io::byte::file::Output::Mode)mode);
 }

 void PingClient::setup()
 {
  util::PFile para(1,0);
  
  verbose=para.valueIsYes("PING_CLIENT_VERBOSE"); 
  count=para.valueOf("PING_CLIENT_COUNT",count);
  id=para.valueOf("PING_CLIENT_ID",id);
  request.icmp.ip4.eth.dst=para.valueOf("PING_CLIENT_MAC_DST");
  request.icmp.ip4.src=Host::getIP4();
  request.icmp.ip4.dst=para.valueOf("PING_CLIENT_IP4_DST");
  request.icmp.type=icmp::Echo::REQUEST;

  request.id=id;
  readPacketSize(para);
  ticker.start(para.valueOf("PING_CLIENT_TIME_TICK_MS",1000u));
  readResultFile(para);
 }

 void PingClient::init()
 {
  min=0xffffffff;
  max=0;
  avg=0;
  seqTX=0;
  seqRX=0;
  missing=0;   
 }
 
 void PingClient::run()
 {
  if (verbose) sys::msg<<Version<<"\n";
     
  for(unsigned i=0;i<packetSizeLen;i++)
  {
   ticker.add(timeout,0);
   init();
   run(packetSize[i]);
   timeout.remove(); //the timeout
   wrResult();
  }
 }

 void PingClient::run(unsigned pSize)
 {
  if (verbose) sys::msg<<"packet size "<<pSize<<"\n";
  ((Payload*)(request.pld.set(sizeof(Payload)+pSize)))->len=pSize;
  
  icmp::Echo reply;
  for(unsigned i=0;i<count;i++)
  {
   net>>reply;
   sys::Time::Stamp_us now=sys::Time::stamp();
   if (
       (reply.icmp.type!=icmp::Echo::REPLY) ||
       (reply.id!=id)
      ) continue;
   if (reply.seq!=seqRX++) missing++;
   unsigned d=sys::Time::delta(((Payload*)reply.pld.get())->timestamp,now);
   if (verbose)
      {
       sys::msg<<"id= "<<reply.id  <<" "
                 "seq= "<<reply.seq<<" "
	       <<d<<" us\n";
      }
   min=min<?d;
   max=max>?d;
   avg+=d;   
  }
 }
   
 void PingClient::wrResult()
 {
  sys::msg<<"packet# "<<seqTX<<"("<<missing<<") "
            "len(ip) "<<request.icmp.ip4.len<<" "
            "min/avg/max= "
	 <<min<<"/"
	 <<(unsigned)(avg/count)<<"/"
	 <<max<<"\n";
  if (result==0) return;
  *result<<request.pld.getLen()<<"\t"
         <<request.icmp.ip4.len<<"\t"
  	 <<min<<"\t"<<(unsigned)(avg/count)<<"\t"<<max<<"\n";
 }
 
 void PingClient::onTimeout(sys::Timeout::Entry& e)
 {
  ((Payload*)request.pld.get())->timestamp=sys::Time::stamp();
  request.seq=seqTX++;
  net<<request;
  ticker.add(timeout,1);
 }
}
