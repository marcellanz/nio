//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//-----------------------------------
//icmp
//(c) H.Buchmann FHSO 2003
//$Id: pac.cc 193 2006-01-11 15:21:16Z buchmann $
//-----------------------------------
#include "sys/sys.h"
IMPLEMENTATION(nio_icmp_pac,$Revision: 137 $)
#include "nio/icmp/pac.h"
#include "util/endian.h"
#include "sys/msg.h"

namespace nio
{
 namespace icmp
 {
//------------------------------------------ Packet  
  io::ascii::Writer& Packet::show(io::ascii::Writer& out) const
  {
   return ip4.show(out)<<"\n"
	      "ICMP: "<<type<<" "
	      "code: "<<code<<" "
	      "chk: " <<chk<<" "<<pld;
  }

  Packet::Packet():
  pac::Packet("icmp",ip4),
  type(this,0u),
  code(this,0u),
  chk(this), //chk(this,0) results in curious error
  pld(this),
  end(this)
  {
   ip4.pro=IP_PROT;
   init();
  }
  
  Packet::Packet(const Packet& p):
  pac::Packet("icmp(p)",ip4),
  ip4(p.ip4),
  type(this,p.type),
  code(this,p.code),
  chk(this,p.chk),
  pld(this,p.pld),
  end(this)
  {
   init();
  }
  
  Packet::~Packet()
  {
  }

  void Packet::init()
  {
   chk.add(type) 
      .add(code) 
      .add(chk) 
      .add(pld);
       
   ip4.len.add(type)
          .add(code)
	  .add(chk)
	  .add(pld);
  }

 //------------------------------------------ Echo 
  io::ascii::Writer& Echo::show(io::ascii::Writer& out) const
  {
   return icmp.show(out)<<" ECHO "
          "id: "<<id<<" "
	  "seq: "<<((seq>>8)&0xff)<<":"<<(seq&0xff) //seq format hh:ll
	         <<" "<<pld;      
  }

  Echo::Echo()
  :pac::Packet("icmp-echo",icmp),
   id(this,(unsigned short)0),
   seq(this,(unsigned short)0),
   pld(this),
   end(this)
  {
   init();
  }

  Echo::Echo(const Echo& p)
  :pac::Packet("icmp-echo",icmp),
   icmp(p.icmp),
   id(this,p.id),
   seq(this,p.seq),
   pld(this,p.pld),
   end(this)
  {
   init();
  }

  Echo::~Echo()
  {
  }
 
  void Echo::init()
  {
   icmp.ip4.len.add(id)
               .add(seq)
	       .add(pld);
   icmp.chk.add(id) 
           .add(seq) 
	   .add(pld);
  }
 }
}
