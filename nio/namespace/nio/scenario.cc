//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//----------------------------
//scenario
//(c) H.Buchmann FHSO 2003
//$Id: scenario.cc 193 2006-01-11 15:21:16Z buchmann $
//----------------------------
//<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
//<Scenario>
// <Name>scenario</Name>
#include "sys/sys.h"
IMPLEMENTATION(scenario,$Revision: 137 $)
#include "nio.h"
#if 1
// <LastMessID>2</LastMessID>
// <LastCommandID>2</LastCommandID>
// <Description>My first IPBench</Description>
// <Type>Ethernet</Type>

//  <Defaults>
//   <EthernetDef/>
//   <IPv4Def/>
//   <IPv4OptionsDef/>
//   <ARPDef/>
//   <UDPDef/>
//   <TCPDef/>
//   <ICMPDef/>
//   <PayloadDef/>
//  </Defaults>
  
//  <Code>
Scenario::Scenario()
{
//   <Commands>
 {
//    <Send CommandID="1">
//     <Message MessID="1">
//      <EthernetMess>
      eth::Packet pac(Default::ETH);
//       <EthernetHeader Type="1234" DestAddr="ff:ff:ff:ff:ff:ff" SrcAddr="00:20:E0:67:86:D9"/>
         pac.type=1234;
	 pac.dst="ff:ff:ff:ff:ff:ff";
	 pac.src="00:20:E0:67:86:D9";
//       <EthernetPayload>Hello</EthernetPayload>
	 pac.payload.set("Hello");
//      </EthernetMess>
//     </Message>
//    </Send>
     eth<<pac;
//   </Commands>
 }  
//   <Commands>
 {
//    <Receive Timer="5000" CommandID="2">
//     <Message MessID="2">
//      <EthernetMess>
      eth::Packet pac;
      pac.setTimeout(5000);
//       <EthernetHeader Type="1234" DestAddr="00:20:E0:67:86:D9" SrcAddr="01:02:03:04:05:05"/>
//       <EthernetPayload>Hello too</EthernetPayload>
//      </EthernetMess>
//     </Message>
      sys::msg<<pac<<"\n";
      eth>>pac;
      if (pac.isTimeout())              fail("timeout");
      if (pac.type!=1234)               fail("type");
      if (pac.dst!="00:20:E0:67:86:D9") fail("dst");
      if (pac.src!="01:02:03:04:05:05") fail("src");
      success();
//    </Receive>
//   </Commands>
 }  
//  </Code>
}
//</Scenario>
#endif

