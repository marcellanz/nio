//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//-----------------------------------
//udp-test
//(c) D.Tschanz FHSO 2003
//$Id: udp-test.cc 193 2006-01-11 15:21:16Z buchmann $
//-----------------------------------
#include "sys/sys.h"
IMPLEMENTATION(ip4_test,$Revision: 137 $)
#include "eth/eth.h"
#include "udp/udp.h"
#include "host.h"
#include "sys/msg.h"

namespace nio
{
 class Tester
 {
  private:
   static Tester tester;
   pac::Net& eth;
   pac::Dumper logger;
   void tx();
   Tester();
 };
 
 Tester Tester::tester;

 void Tester::tx()
 {
  const unsigned char data[11]={0,1,2,3,4,5,6,7,8,9,10};
  udp::Packet pac;
  pac.port.src=0x0005;
  pac.port.dst=0x0006;
  pac.payload.set(data,sizeof(data));
  sys::msg<<pac<<"\n";
  eth<<pac;
 }
 
 Tester::Tester():
 logger(sys::msg),
 eth(Host::getNet())
 {
  eth.logon();
  tx();
 }
}
