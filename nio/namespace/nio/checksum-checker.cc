//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//------------------------------
//checksum-checker
//(c) H.Buchmann FHSO 2003
//$Id: checksum-checker.cc 193 2006-01-11 15:21:16Z buchmann $
//reads from sdtin 
//------------------------------
#include "sys/sys.h"
IMPLEMENTATION(checksum_checker,$Revision: 137 $)
#include "util/str.h"
#include "sys/host.h"
#include "sys/msg.h"
#include "pac/stream/checksum.h"
#include <iostream>

namespace nio
{
 class ChecksumChecker
 {
  private:
   static ChecksumChecker checker;

   static const char Delim[]; 
   
   static bool isDelim(char ch);
   static void error();
   
   pac::stream::Checksum cs;
   
   void read();
   ChecksumChecker();
 };
 
 const char ChecksumChecker::Delim[]=" \t\n"; 
 ChecksumChecker ChecksumChecker::checker;
 
 bool ChecksumChecker::isDelim(char ch)
 {
  unsigned i=0;
  while(true)
  {
   char chd=Delim[i++];
   if (chd=='\0') return false;
   if (chd==ch)   return true;
  }
 }
 
 void ChecksumChecker::error()
 {
  sys::msg<<"\n**** not a hex digit sequence\n";
  sys::exit(1);
 }
 
 ChecksumChecker::ChecksumChecker()
 {
  sys::msg.hex();
  read();
  sys::msg<<"Checksum = "<<cs.get()<<"\n";
 }
 
 void ChecksumChecker::read()
 {
  unsigned status=0;
  unsigned char val=0;
  
  while(true)
  {
   int ch=std::cin.get();
//   sys::msg<<(char)ch<<" "<<status<<"\n";
   switch(status)
   {
    case 0: //blank or first hex digit
     {
      if (ch==-1) return; 
      if (isDelim(ch)) break;
      int dd=util::Str::isHexDigit(ch);
      if (dd<0) error(); 
      val=dd;
      status=1;    
     }
    break;
    
    case 1: // second digit | blank | end
     {
      if (ch==-1) return; 
      if (isDelim(ch)) {cs.put(val);break;} 
      int dd=util::Str::isHexDigit(ch);
      if (dd<0) error();
      val=(val<<4)+dd;
      cs.put(val);
      status=0;
     }
    break;
    
   }
  }
 }
 
}
