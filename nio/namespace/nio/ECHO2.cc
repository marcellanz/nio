//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

// ------- This is the header lines of the generated code --------
// Name of the scenario        : ECHO2
// Description of the scenario : 
// ---------------------------------------------------------------


#include "sys/sys.h"
IMPLEMENTATION(foo,$Revision: 137 $)
#include "nio.h"

//#include "xmlresult.h"

Scenario::Scenario()
{
//   xml::XMLResult results;
   const time_t t=time(0);
//   results.header("/home/rvo/tmp/ECHO2.xml","ECHO2", ctime(&t), "", "","/home/rvo/tmp/ECHO2.cc.dump",0);
   msg<<"/home/rvo/tmp/ECHO2.xml\n";
   bool bResult;
//Begin iteration on 8 commands
/*<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<Message MessID="1"><UDPv4Mess><EthernetHeader SrcAddr="00:20:ed:6b:e9:62" DestAddr="00:d0:59:1b:3c:A9"/><IPv4Header DestAddr="157.26.103.11" SrcAddr="157.26.103.20"/><IPv4Options/><UDPHeader SrcPort="2555" DstPort="7"/><UDPPayload>essaiasdfasdfasdf</UDPPayload></UDPv4Mess></Message>

*/
// UDP send
// IP destination address : 157.26.103.20
// IP source address : 157.26.103.11
   eth.logon(); //<------------------- I set it here

   udp::Packet packet1;
   packet1.ip4.eth.dst= "00:20:E0:67:86:D9";//"00:d0:59:1b:3c:A9";
   packet1.ip4.eth.src= "00:80:C7:C2:89:27";//"00:20:ed:6b:e9:62";
   packet1.ip4.src="192.168.1.55";         //"157.26.103.20";
   packet1.ip4.dst="192.168.1.197";
//*****************Warning, IP options not implemented !*********************
   packet1.port.dst=7;
   packet1.port.src=2555;


   packet1.payload.set("essaiasdfasdfasdf");
   eth<<packet1;
   sys::msg<<packet1<<"\n";
//   results.addFrameSend("Send : UDPv4 1",packet1.getTimestamp_us());
   msg<<"Send : UDPv4 1 "<<packet1.getTimestamp_us()<<"\n";
/*<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<Message MessID="2"><UDPv4Mess><EthernetHeader/><IPv4Header SrcAddr="157.26.103.11"/><IPv4Options/><UDPHeader/></UDPv4Mess></Message>

*/
// UDPv4 receive
// IP destination address required : null
// IP source address required : 157.26.103.11
// UDP source port required : 0
// UDP destination port required : 0
   udp::Packet packet2;
//   eth.logon(); <<--------------------- you set it here so you dont catch
//                                        the first packet                
   packet2.setTimeout(5000);
   do{
      eth>>packet2;
      if (packet2.isTimeout())
         break;
      sys::msg<<packet2<<"\n";
   }while (!((true)&& (packet2.ip4.src=="192.168.1.197")));
//Warning, in receive, only address are tested in layer 2-3-4
   if (packet2.isTimeout())
//      results.addFrameNotReceive("Receive : UDPv4 2",packet2.getTimestamp_us());
        msg<<"Receive Not: UDPv4 2 "<<packet2.getTimestamp_us()<<"\n";
   else
//      results.addFrameReceive("Receive : UDPv4 2",packet2.getTimestamp_us());
        msg<<"Receive : UDPv4 2 "<<packet2.getTimestamp_us()<<"\n";
   if (! (packet2.isTimeout())){
       sys::msg<<"***********************************************************************************";
       sys::msg<<"WARNING, no timeout occurs but it was expected !";
       sys::msg<<"***********************************************************************************\n";}
/*<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<Message MessID="3"><UDPv4Mess><EthernetHeader SrcAddr="00:20:ed:6b:e9:62" DestAddr="00:d0:59:1b:3c:a9"/><IPv4Header DestAddr="157.26.103.11" SrcAddr="157.26.103.20"/><IPv4Options/><UDPHeader SrcPort="2555" DstPort="7"/><UDPPayload>deuxième</UDPPayload></UDPv4Mess></Message>

*/
// UDP send
// IP destination address : 157.26.103.20
// IP source address : 157.26.103.11
   udp::Packet packet4;
   packet4.ip4.eth.dst="00:d0:59:1b:3c:a9";
   packet4.ip4.eth.src="00:20:ed:6b:e9:62";
   packet4.ip4.src="157.26.103.20";
   packet4.ip4.dst="157.26.103.11";
//*****************Warning, IP options not implemented !*********************
   packet4.port.dst=7;
   packet4.port.src=2555;
   packet4.payload.set("deuxi�me");
   eth<<packet4;
   sys::msg<<packet4<<"\n";
//   results.addFrameSend("Send : UDPv4 4",packet4.getTimestamp_us());
   msg<< "Send : UDPv4 4"<<packet4.getTimestamp_us()<<"\n";
/*<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<Message MessID="4"><UDPv4Mess><EthernetHeader/><IPv4Header SrcAddr="157.26.103.11"/><IPv4Options/><UDPHeader/></UDPv4Mess></Message>

*/
// UDPv4 receive
// IP destination address required : null
// IP source address required : 157.26.103.11
// UDP source port required : 7
// UDP destination port required : 0
   udp::Packet packet5;
   eth.logon();
   packet5.setTimeout(5000);
   do{
      eth>>packet5;
      if (packet5.isTimeout())
         break;
      sys::msg<<packet5<<"\n";
   }while (!((true)&& (packet5.ip4.src=="157.26.103.11")));
//Warning, in receive, only address are tested in layer 2-3-4
   if (packet5.isTimeout())
//      results.addFrameNotReceive("Receive : UDPv4 5",packet5.getTimestamp_us());
      msg<<"Receive : UDPv4 5 "<<packet5.getTimestamp_us()<<"\n";
   else
//      results.addFrameReceive("Receive : UDPv4 5",packet5.getTimestamp_us());
      msg<<"Receive : UDPv4 5 "<<packet5.getTimestamp_us()<<"\n";
   if (! (packet5.isTimeout())){
       sys::msg<<"***********************************************************************************";
       sys::msg<<"WARNING, no timeout occurs but it was expected !";
       sys::msg<<"***********************************************************************************\n";}
/*<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<Message MessID="7"><EthernetMess><EthernetHeader DestAddr="00:11:22:33:44:55"/></EthernetMess></Message>

*/
// Ethernet receive header
// Ethernet destination address required : 00:11:22:33:44:55
// Ethernet source address required : null
// Ethernet type required : 0
   eth::Packet packet8;
   eth.logon();
   packet8.setTimeout(5000);
   do{
      eth>>packet8;
      if (packet8.isTimeout())
         break;
      sys::msg<<packet8<<"\n";
   }while (!((true)&& (packet8.dst=="00:11:22:33:44:55")));
   if (packet8.isTimeout())
//      results.addFrameNotReceive("Receive : Ethernet 8",packet8.getTimestamp_us());
      msg<<"Receive Not : Ethernet 8 "<<packet8.getTimestamp_us()<<"\n";
   else
//      results.addFrameReceive("Receive : Ethernet 8",packet8.getTimestamp_us());
      msg<<"Receive : Ethernet 8 "<<packet8.getTimestamp_us()<<"\n";
   if (packet8.isTimeout()){
       sys::msg<<"***********************************************************************************";
       sys::msg<<"WARNING, a timeout occurs while waiting for receiving the packet and it shouldn't !";
       sys::msg<<"***********************************************************************************\n";}
/*<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<Message MessID="5"><UDPv4Mess><EthernetHeader SrcAddr="00:20:ed:6b:e9:62" DestAddr="00:d0:59:1b:3c:a9"/><IPv4Header DestAddr="157.26.103.11" SrcAddr="157.26.103.20"/><IPv4Options/><UDPHeader SrcPort="3333" DstPort="7"/><UDPPayload>3ieme</UDPPayload></UDPv4Mess></Message>

*/
// UDP send
// IP destination address : 157.26.103.20
// IP source address : 157.26.103.11
   udp::Packet packet6;
   packet6.ip4.eth.dst="00:d0:59:1b:3c:a9";
   packet6.ip4.eth.src="00:20:ed:6b:e9:62";
   packet6.ip4.src="157.26.103.20";
   packet6.ip4.dst="157.26.103.11";
//*****************Warning, IP options not implemented !*********************
   packet6.port.dst=7;
   packet6.port.src=3333;
   packet6.payload.set("3ieme");
   eth<<packet6;
   sys::msg<<packet6<<"\n";
//   results.addFrameSend("Send : UDPv4 6 ",packet6.getTimestamp_us());
   msg<<"Send : UDPv4 6 "<<packet6.getTimestamp_us()<<"\n";
/*<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<Message MessID="6"><UDPv4Mess><EthernetHeader/><IPv4Header SrcAddr="157.26.103.11"/><IPv4Options/><UDPHeader/></UDPv4Mess></Message>

*/
// UDPv4 receive
// IP destination address required : null
// IP source address required : 157.26.103.11
// UDP source port required : 7
// UDP destination port required : 0
   udp::Packet packet7;
   eth.logon();
   packet7.setTimeout(5000);
   do{
      eth>>packet7;
      if (packet7.isTimeout())
         break;
      sys::msg<<packet7<<"\n";
   }while (!((true)&& (packet7.ip4.src=="157.26.103.11")));
//Warning, in receive, only address are tested in layer 2-3-4
   if (packet7.isTimeout())
//      results.addFrameNotReceive("Receive : UDPv4 7",packet7.getTimestamp_us());
      msg<<"Receive not : UDPv4 7 "<<packet7.getTimestamp_us()<<"\n";
   else
//      results.addFrameReceive("Receive : UDPv4 7",packet7.getTimestamp_us());
     msg<<"Receive : UDPv4 7 "<<packet7.getTimestamp_us()<<"\n";
   if (! (packet7.isTimeout())){
       sys::msg<<"***********************************************************************************";
       sys::msg<<"WARNING, no timeout occurs but it was expected !";
       sys::msg<<"***********************************************************************************\n";}
   bResult=agent1.send("4010");
//   results.footer("Success");
   if (bResult) sys::msg<<"Success\n"; else sys::msg<<"Send error\n";
    exit(0);
//   results.footer("results comments...");
}
