//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//---------------------------------
//tcp-test
//(c) H.Buchmann FHSO 2003
//$Id: tcp-test.cc 193 2006-01-11 15:21:16Z buchmann $
//---------------------------------
#include "sys/sys.h"
IMPLEMENTATION(nio_tcp_test,$Revision: 137 $)
#include "nio/pac/pac.h"
#include "nio/ip4/pac.h"
#include "nio/tcp/pac.h"
#include "nio/pac/dump.h"
#include "nio/pac/stream/raw.h"
#include "nio/default.h"
#include "nio/host.h"
#include "sys/msg.h"

namespace nio
{
 class Tester
 {
  private:
   static Tester tester;
   pac::Dumper logger;
   pac::Net& eth;
   void rx();
   void tx();
   void connect();
   void dump();
   Tester();
 };
 
 Tester Tester::tester;

 void Tester::connect()
 {
#if 0
  tcp::Packet pacTX(Default::TCP);
  sys::msg<<"tcp = "<<pacTX<<"\n";
  tcp::Packet pacRX;
#endif  
 }
 
 void Tester::dump()
 {
  static unsigned char RAW1[]=
  {
   0x00,0x12,0x34,0xaa,0xbb,0xcc,0x00,0x01,0x02,0xb4,0xda,0x07,0x08,0x00,
   
   0x45,0x00,0x00,0x28, 
   0x00,0x00,0x00,0x00,
   0xff,0x06,0x24,0x9b,
   0x0a,0x58,0x41,0x4a, //E..(......$..XAJ
   0x0a,0x58,0x41,0x3b,
   
   0x04,0xb6,0x00,0x07,
   0x03,0x49,0xaa,0x2b,
   0x00,0x00,0x00,0x00, //.XA;.....I.+....
   0x50,0x02,0x03,0xe8,
   0x62,0x94,0x00,0x00                                          //P...b...
  };

  static unsigned char RAW2[]=
  {
  
   0x00,0x01,0x02,0xb4,
   0xda,0x07,0x00,0x12,
   0x34,0xaa,0xbb,0xcc,
   0x08,0x00,
   
   0x45,0x00,0x00,0x2c,
   0x00,0x00,0x40,0x00,
   0x40,0x06,0xa3,0x97,
   0x0a,0x58,0x41,0x3b,
   0x0a,0x58,0x41,0x4a,
   
   0x00,0x07,0x04,0xb6,
   0x85,0xb1,0xc4,0x31,
   0x03,0x49,0xaa,0x2c,
   0x60,0x12,0x16,0xd0,
   0xed,0xfb,0x00,0x00,

   0x02,0x04,0x05,0xb4,

   0x00,0x00
  };
  
  sys::msg<<"--------------- dump\n";
  pac::stream::RawInput in;
  in.set(RAW2,sizeof(RAW2));
  tcp::Packet pac;
  if (!pac.fromRaw(in)){sys::msg<<"not a TCP Packet\n";return;}
  sys::msg<<pac.ip4.eth<<"\n";
  sys::msg<<pac<<"\n"; 
 }
 
 void Tester::rx()
 {
  static const unsigned SIZE = 10; 
  tcp::Packet pac[SIZE];
  unsigned idx=0;  
  while(true)
  {
   if (idx==SIZE) break;  
   eth>>pac[idx];
   idx++;
//   pac.showSeq(sys::msg)<<" pld= "<<pac.pld.getLen()<<"\n";
  }
  for(unsigned i=0;i<idx;i++) pac[i].showSeq(sys::msg)<<"\n";
 }

 void Tester::tx()
 {
  static unsigned char Data[]={1,2,3,4,5,6,7,8,9,10};
  tcp::Packet pac;
  pac.ip4.eth.dst="0:1:2:b4:da:07";
  pac.ip4.eth.src=Host::getMAC();
  pac.ip4.dst="10.88.65.59";
  pac.ip4.src=Host::getIP4();
  pac.prt.src=0x1234;
  pac.prt.dst=7;
  pac.pld.set(Data,sizeof(Data));
  pac::Dumper log(sys::msg);
  eth.setLogger(log);
  eth.logon();
  sys::msg<<pac<<"\n";
  eth<<pac;
 }
  
 Tester::Tester():
 logger(sys::msg),
 eth(Host::getNet())
 {
 
  sys::msg<<"-------------------- TCP Tester\n";
  
//  eth.setLogger(logger).logon();
// tx();
  rx();
//  connect();
//  dump();
 }
}
