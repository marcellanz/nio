//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//---------------------------------
//host
//(c) H.Buchmann FHSO 2003
//$Id: host.cc 193 2006-01-11 15:21:16Z buchmann $
//---------------------------------
#include "sys/sys.h"
IMPLEMENTATION(nio_host,$Revision: 137 $)
#include "nio/host.h"
#include "nio/pac/pcap.h"

namespace nio
{
 const char Host::NIO[]="NIO";
 
 Host Host::host;

 Host::Host():
 para(0,NIO),
 device(0),
 net(0),
 msg(0),
 logger(0)
 {
  para.isDefinedExit("NIO_INTERFACE");
  device=new pac::RawSocket(para.valueOf("NIO_INTERFACE"));
  net=new pac::Net(*device);
  if (para.isDefined("NIO_LOGFILE"))
     {
      logger=new pac::PcapLogger(para.valueOf("NIO_LOGFILE"),
                                 pac::Logger::INANDOUT);
      net->setLogger(*logger);	  			 
     }				 
  
  para.isDefinedExit("NIO_MSGFILE");
  msg=new io::ascii::file::Writer(para.valueOf("NIO_MSGFILE"));
  
  ip4::Address::copy("255.255.255.255",ip4);
  if (para.isDefined("NIO_HOST_IP4"))
     {
      if (!ip4::Address::copy(para.valueOf("NIO_HOST_IP4"),ip4))
         {
	  sys::msg.error()<<"'"<<para.valueOf("NIO_HOST_IP4")
	          <<"' is not an ip address\n";
//	  sys::msg.halt();	  
	 }
	 
     }
 }
 
 Host::~Host()
 {
  close();
 }
 
 void Host::close()
 {
  if (msg) delete msg;
  if (net) delete net;
  if (logger) delete logger;
  if (device) delete device;
 }
}

