//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//--------------------------------
//timeout-test
//(c) D.Tschanz FHSO 2003
//$Id: timeout-test.cc 193 2006-01-11 15:21:16Z buchmann $
//--------------------------------
#include "sys/sys.h"
IMPLEMENTATION(timeout_test,$Revision: 137 $)
#include "arp/arp.h"
#include "sys/msg.h"
#include "host.h"

namespace nio
{
 class Tester
 {
  private:
   static Tester tester;
   static const ip4::Address::RAW my;
   pac::Net& eth;
  public:
            Tester();
   virtual ~Tester();
 };
 
 Tester Tester::tester;

 Tester::Tester():
 eth(Host::getNet())
 {
  sys::msg<<"---------------------- Timeout Test\n";
  unsigned timeout = 100;
  arp::Packet pacTX;
  arp::Packet pacRX;
  
  pacTX.src.pr=Host::getIP4();
  pacTX.src.hw=Host::getMAC();
  pacTX.eth.dst=eth::Address::Broadcast;
  pacTX.eth.src=Host::getMAC();
  ip4::Address::RAW target={192,168,1,174};
  while(1)
  {
   pacTX.tar.pr=target;
   sys::msg<<pacTX<<"\n";
   eth<<pacTX;
   sys::msg<<"Timestamp secs: "<<(int)pacTX.getTimeStampSec()<<" usecs:"<<(int)pacTX.getTimeStampUSec() <<"\n";

   pacRX.setTimeout(timeout);
   sys::msg<<"Waiting for Packet timeout.\n";

   eth>>pacRX;
   sys::msg<<"Timestamp secs: "<<(int)pacRX.getTimeStampSec()<<" usecs:"<<(int)pacRX.getTimeStampUSec() <<"\n";

   if(pacRX.isTimeout())
   sys::msg<<"Packet timeouted. Time:"<<timeout <<"\n";
   timeout+=100;
   sys::msg<<pacRX<<"\n";

   target[3]++;
   pacTX.tar.pr=target;
   sys::msg<<pacTX<<"\n";
   eth<<pacTX;
   pacRX.setTimeout(0);
   eth>>pacRX;
   sys::msg<<pacRX<<"\n";
   target[3]++;
  }   
 }

 Tester::~Tester()
 {
 }
}
