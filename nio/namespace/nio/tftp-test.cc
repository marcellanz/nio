//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//-----------------------------------
//tftp-test
//(c) D.Tschanz FHSO 2003
//$Id: tftp-test.cc 193 2006-01-11 15:21:16Z buchmann $
//-----------------------------------
#include "sys/sys.h"
IMPLEMENTATION(ip4_test,$Revision: 137 $)
#include "eth/eth.h"
#include "udp/udp.h"
#include "host.h"
#include "sys/msg.h"
#include <string.h>
#include <sstream>
#include <sys/types.h>
#include <sys/time.h>
#include <stdlib.h>

namespace nio
{
 class Tester
 {
  enum OPCode {RRQ=1,WRQ,DATA,ACK,ERROR};//According to the field in the protocol
  enum Mode {READ};

  unsigned char blockNrHB;
  unsigned char blockNrLB;
  unsigned short portRemote;
  unsigned short portLocal;
  static const char ipRemote[];
  static const char macRemote[];
  std::ostringstream out;

  static Tester tester;
  pac::Net& eth;
  static const char File[];
  static const char TransferMode[];
  void tx();
  bool connect(Mode);
  bool readPacket();
  void buildHeader(udp::Packet& p);
  void sendAck();
  Tester();
 };
 
 Tester Tester::tester;
 const char Tester::TransferMode[] = "netascii";

 //Parameters for TFTP Mode
 const char Tester::ipRemote[] = "10.88.65.59";
 const char Tester::macRemote[] = "00:01:02:B4:DA:07";
 const char Tester::File[] = "test.txt";
 //End of Parameters


 bool Tester::readPacket()
 {
  udp::Packet pac;
  sys::msg<<"Tester::readPacket\n";
  static bool ack = false;
  static bool setTarget = false;
  char *data;
  unsigned len;
  short errorCode;

  eth>>pac;
  if(pac.port.dst!=portLocal)
  {
   sys::msg<<"Tester::readPacket wrong port\n";
   return true;
  }
  if(!setTarget){
   portRemote = pac.port.src.get();
   setTarget=true;
  }
  if(pac.port.src!=portRemote)
  {
   sys::msg<<"Tester::readPacket wrong sender port\n";
   return true;
  }
  data = (char*)pac.payload.getData();
  len = pac.payload.getLen();
  if(len<4)return false;
  OPCode opcode = OPCode((data[0])+data[1]);
  switch(opcode)
  {
   case DATA:
    sys::msg<<"DATA:\n";
    blockNrHB = data[2];
    blockNrLB = data[3];
    sys::msg<<"DATA Block: "<<(blockNrHB*256)+blockNrLB<<"\n";
    len-=4; // Discard TFTP header length
    out.write(&data[4],len);
    if(len!=512){//Last Block received
     sys::msg<<"Blocksize: "<<len<<"\n";
     sendAck();
     return false;
    } else {
     sendAck();
     return true;
    }
    break;
   case ACK:
    blockNrHB = data[2];
    blockNrLB = data[3];
    sys::msg<<"ACK: "<< (blockNrHB*256)+blockNrLB <<"\n";
    return true;
    break;
   case ERROR:
    errorCode = (data[2]<<16)+data[3];
    sys::msg<<"ErrorNumber: "<<errorCode<< " " << data+4 <<"\n";
    return false;
    break;
   default:
     sys::msg<<"Unknown Opcode: "<<opcode<<"\n";
     return true;
     break;
  }
 }

 void Tester::sendAck() {
  udp::Packet pac;
  buildHeader(pac);
  unsigned char ack[4];
  ack[0] = 0;
  ack[1] = ACK;
  ack[2] = blockNrHB ;
  ack[3] = blockNrLB;
  pac.payload.set(ack,4);
  sys::msg<<"ACK: "<< (256*blockNrHB+blockNrLB) <<"\n";
  eth<<pac;
 }

 void Tester::buildHeader(udp::Packet& p){
  p.ip4.eth.src=Host::getMAC();
  p.ip4.eth.dst=&macRemote[0];
  p.ip4.src=Host::getIP4();
  p.ip4.dst=&ipRemote[0];
  p.ip4.df=1;
  p.ip4.tol=64;
  p.port.src=portLocal;
  p.port.dst=portRemote;
 }

 bool Tester::connect(Mode m){
  udp::Packet pac;
  timeval tv;
  gettimeofday(&tv,NULL);
  eth.logon();
  eth.startServices();
  srandom(tv.tv_usec);
  portLocal = (0x2000)|random();
  portRemote = 69;
  buildHeader(pac);
  int sizePayload = 2+2+strlen(File)+strlen(TransferMode);//2 for opcode and strs 
  unsigned char payLoad[sizePayload];
  memset(payLoad,0,sizePayload);
  strcpy((char*)&payLoad[2],File);
  strcpy((char*)&payLoad[2+sizeof(File)],TransferMode);

  switch(m){
   case READ:
    payLoad[1]=RRQ;
    break;
   default:
     sys::msg<<"Tester::connect Mode not supported.\n";
     return false;
    break;
  }
  pac.payload.set(payLoad,sizeof(payLoad));
  
  sys::msg<<pac<<"\n";
  eth<<pac;
  return true;
 }

 void Tester::tx() {
  std::string buffer;
  connect(READ);
  while(readPacket());
  buffer = out.str(); 
  sys::msg<<buffer.c_str()<<"\n";
 }

 Tester::Tester(): eth(Host::getNet()),blockNrHB(0),blockNrLB(1) {
  tx();
 }
}
