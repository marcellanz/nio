#-----------------------
#install source files
#(c) H.Buchmann FHSO 2005
#$Id: install-source-files.sh 193 2006-01-11 15:21:16Z buchmann $
#$1 src directory
#$2 dst directory
#stdin filelist
#-----------------------
while [ 1 ]
do
read file
if [ -z $file ];then break;fi
 install --mode 644 -D $1/$file $2/$file
done
