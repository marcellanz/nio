//----------------------------------------
// Parameterlist former PFile
// (c) H.Buchmann FHSO 2003
// $Id: plist.cc 193 2006-01-11 15:21:16Z buchmann $
// todo: garantee uniqueness of key
//----------------------------------------
//#define DEBUG
#include "sys/sys.h"
IMPLEMENTATION(util_plist,$Revision: 137 $)
#include "util/plist.h"
#include "util/str.h"

namespace util
{
 template<typename typ>
 bool PList::valueOf(const char key[],typ def,typ& val) const
 {
  const char* s;
  if (valueOf(key,s)) return Str::to(s,val);
  val=def;
  if (verbose)msg<<" default: "<<val<<"\n";
  return true;
 }

 template<typename typ>
 const typ PList::valueOf(const char key[],typ def) const
 {
  typ v;
  if (!valueOf(key,def,v)) notANumber(key);
  return v;
 }


 const char PList::Delim[]=" \t";
 bool PList::Cmp::operator()(const char* s0,const char* s1) const
 {
  return Str::cmp(s0,s1)<0;
 }

 PList::PList(io::ascii::Writer& msg,bool verbose)
 :dict(cmp),
  msg(msg),
  size(0),
  state(0),
  verbose(verbose)
 {
  init();
 // showIt(cout);
 }

 PList::~PList()
 {
 // cerr<<"~PFile()..";
  for(Dict::iterator i=dict.begin();i!=dict.end();++i)
  {
   delete [] i->first;
   delete [] i->second;
  }
  // cerr<<"done\n";
 }

 void PList::init()
 {
  size=0;
  for(unsigned i=0;i<SIZE;i++){name[i]=0;value[i]=0;}
 }

 bool PList::isDelim(char ch)
 {
  unsigned i=0;
  char chD;
  while(true)
   {
    chD=Delim[i++];
    if (!chD) return false;	
    if (chD==ch) return true;
   }
 }


 void PList::addEntry()
 {
  name[nameI++]=0;
  char* key=new char[nameI];
  Str::copy(name,key, nameI);
  value[valueI++]=0;
  char* val=new char[valueI];
  Str::copy(value,val,valueI);
  dict[key]=val;
 }

 void PList::put(char ch)
 {
 //   cerr<<ch<<"\t"<<state<<"\n";
    switch(state)
    {
     case 0:
      if (isDelim(ch)) break;
      if (ch=='\n') break;
      if (ch=='#'){state=8;break;} //skip line
      nameI=0;
      name[nameI++]=ch;
      state=1;
     break;
     
     case 1: //name
      if (isDelim(ch)){state=2;break;}
      if (ch=='='){state=3;break;}
      if (ch=='\n'){state=0;break;}
      if (ch=='#') {state=8;break;}//skip line
      name[nameI++]=ch;
     break;
     
     case 2: //wait for =
      if (isDelim(ch)) break;
      if (ch=='='){state=3;break;}
      state; //skip line
     break;
     
     case 3: //wait for value
      if (isDelim(ch)) break;
      if (ch=='\n') {state=0;break;}
      if (ch=='#') {state=8;break;}
      if (isQuote(ch)){valueI=0;state=7;break;} //quote
      valueI=0;
      value[valueI++]=ch;
      state=4;
     break;
     
     case 4:
      if (isDelim(ch)){state=5;break;}
      if (ch=='\n'){state=0;addEntry();break;}
      if (ch=='#'){state=6;break;}
      value[valueI++]=ch;
     break;
     
     case 5:
      if (isDelim(ch)) break;
      if (ch=='\n'){state=0;addEntry();break;}
      if (ch=='#'){state=6;break;}
      state=8; //skip line
     break;
     
     case 6:
      if (ch=='\n'){state=0;addEntry();break;}
     break;
     
     case 7: //quote
      if (isQuote(ch)){state=5;break;}
      if (ch=='\\')   {state=9;break;}
      value[valueI++]=ch;
     break;
     
     case 8: //skip
      if(ch=='\n') {state=0;break;}
     break;
     
     case 9: //escape character
      value[valueI++]=ch;
      state=7;
     break;
    }  
 }


 void PList::showIt(io::ascii::Writer& out)
 {
  out<<"PFile "<<dict.size()<<" entries\n";
  for(Dict::iterator i=dict.begin();i!=dict.end();++i)
   out<<i->first<<" = "<<i->second<<"\n";
 }

 bool PList::isDefined(const char n[])
 {
  return dict.find(n)!=dict.end();
 }

 void PList::isDefinedExit(const char key[])
 {
  if (!isDefined(key))
     {
      msg<<"'"<<key<<"' not defined\n";
      onError(msg);
     }
 }

 bool PList::valueOf(const char n[],const char*& s) const
 {
  Dict::const_iterator key=dict.find(n);
  if (key==dict.end()){s=0;return false;}
  s=key->second;
  if (verbose) msg<<n<<" = "<<s<<"\n";
  return true;
 }


 bool PList::valueOf(const char key[],int def,int& val) const
 {
  return valueOf<int>(key,def,val);
 } 

 bool PList::valueOf(const char key[],unsigned def,unsigned& val) const
 {
  return valueOf<unsigned>(key,def,val);
 } 

 bool PList::valueOf(const char key[],unsigned short def,unsigned short& val) const
 {
  return valueOf<unsigned short>(key,def,val);
 } 

 bool PList::valueOf(const char key[],double def,double& val) const
 {
  return valueOf<double>(key,def,val);
 } 


 bool PList::valueIs(const char key[],const char val[]) const
 {
  const char* s;
  if (valueOf(key,s)) return Str::equal(s,val);
  return false;
 }


 int PList::valueOf(const char key[],const char* selection[],int defaultVal) const
 {
  const char* s;
  if (!valueOf(key,s)) return defaultVal;
  const char** sel=selection;
  int cnt=0;
  while(*sel)
   {
    if (Str::equal(s,*sel)) return cnt;
    sel++;
    cnt++;
   }
  return defaultVal;
 }

 int PList::valueOf(const char key[],const char* selection[]) const
 {
  const char* s;
  if (!valueOf(key,s)) return -1;
  const char** sel=selection;
  int cnt=0;
  while(*sel)
   {
    if (Str::equal(s,*sel)) return cnt;
    sel++;
    cnt++;
   }
  return -1;
 }

 bool PList::valueIsYes(const char key[]) const
 {
  const char* s;
  if (!valueOf(key,s)) return false;
  return Str::equal(s,"YES");
 }

 void  PList::notANumber(const char key[]) const
 {
  msg<<"value of '"<<key<<"' not a number\n";
  onError(msg);
 }

 const char* PList::valueOf(const char key[]) const
 {
  const char* s;
  if (!valueOf(key,s))
     {
      msg<<"key '"<<key<<"' not defined\n";
      onError(msg);     
     }
  return s;
 }

 const int PList::valueOf(const char key[],int def) const
 {
  return valueOf<int>(key,def);
 }
 
 const unsigned PList::valueOf(const char key[],unsigned def) const
 {
  return valueOf<unsigned>(key,def);
 }

 const unsigned short PList::valueOf(const char key[],unsigned short def) const
 {
  return valueOf<unsigned short>(key,def);
 }
 
 const double PList::valueOf(const char key[],double def) const
 {
  return valueOf<double>(key,def);
 }

}
