//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#ifndef UTIL_ENDIAN_H
#define UTIL_ENDIAN_H
INTERFACE(util_endian,$Revision: 137 $)
//--------------------------
//endian stuff
//(c) H.Buchmann FHSO 2002
//$Id: endian.h 193 2006-01-11 15:21:16Z buchmann $
//--------------------------
namespace util
{
 class Endian
 {
  public:
   enum Type {BIG,LITTLE};
#ifdef  __ARMEL__ 
   static const Type SystemEndian=LITTLE;
#define __LITTLE_ENDIAN__   
#endif
#ifdef  __ARMEB__ 
#define __BIG_ENDIAN__   
   static const Type SystemEndian=BIG;
#endif
#ifdef  i386 
#define __LITTLE_ENDIAN__   
   static const Type SystemEndian=LITTLE;
#endif

  private:
   static Endian endian;
   
//--------------------------- get endianess from defines
   
   template<class type,Type to>static type changeVal(type val)
   {return (SystemEndian==to)?val:change(val);}
   
   template<class type,Type to>static type changeRef(type* val)
   {return (SystemEndian==to)?*val:change(val);}
    
   void checkEndian();
   Endian();

  public:      
   static const char* Name[];

   static const Type get(){return SystemEndian;}

   static unsigned       change(unsigned        v);
   static unsigned       change(unsigned*       v){*v=change(*v);return *v;}
   static unsigned short change(unsigned short  v);
   static unsigned short change(unsigned short* v){*v=change(*v);return *v;}
   
   static unsigned little(unsigned v)
          {return changeVal<unsigned,LITTLE>(v);}
   static unsigned little(unsigned*v)
          {return changeRef<unsigned,LITTLE>(v);}

   static unsigned little(int v)
          {return changeVal<unsigned,LITTLE>((unsigned) v);}
   static unsigned little(int*v)
          {return changeRef<unsigned,LITTLE>((unsigned*)v);}

   static unsigned little(unsigned short v)
          {return changeVal<unsigned short,LITTLE>(v);}
   static unsigned little(unsigned short *v)
          {return changeRef<unsigned short,LITTLE>(v);}

   static unsigned little(int short v)
          {return changeVal<unsigned short,LITTLE>((unsigned short)v);}
   static unsigned little(int short *v)
          {return changeRef<unsigned short,LITTLE>((unsigned short*)v);}

   static unsigned char little(unsigned char v)
          {return v;}		   
   static unsigned char little(unsigned char* v)
          {return *v;}

   static unsigned big(unsigned  v)
          {return changeVal<unsigned,BIG>(v);}
   static unsigned big(unsigned* v)
          {return changeRef<unsigned,BIG>(v);}

   static unsigned big(int  v)
          {return changeVal<unsigned,BIG>((unsigned)v);}
   static unsigned big(int* v)
          {return changeRef<unsigned,BIG>((unsigned*)v);}
     
   static unsigned short big(unsigned short v)
          {return changeVal<unsigned short,BIG>(v);}
   static unsigned short big(unsigned short*v)
          {return changeRef<unsigned short,BIG>(v);}

   static unsigned short big(int short v)
          {return changeVal<unsigned short,BIG>((unsigned short)v);}
   static unsigned short big(int short*v)
          {return changeRef<unsigned short,BIG>((unsigned short*)v);}

   static unsigned char big(unsigned char v)
          {return v;}		   
   static unsigned char big(unsigned char* v)
          {return *v;}
			 
 };

#ifdef __arm__
//-----------------------------
//code taken from DDI0100E_ARM_ARM.pdf 9.1.4 
 inline unsigned Endian::change(unsigned v)
 {
  unsigned v0;
//  unsigned res;
  asm volatile 
  (
   "\n@-------------Endian::change(unsigned v)\n\t"
   "eor %1,%0,%0,ror #16\n\t"
   "bic %1,%1,#0xff0000\n\t"
   "mov %0,%0,ror #8\n\t"
   "eor %0,%0,%1,lsr #8\n"
   :"+r"(v)
   :"r"(v0)
  );
  return v;
 }
 
//-----------------------------
//my own version not yet perfect 
 inline unsigned short Endian::change(unsigned short v)
 {
  asm volatile 
  (
   "\n@----------------Endian::change(unsigned short v)\n\t"    // xycd
   "mov %0,%0,lsl #16\n\t"      // %0 <- cd00
   "orr %0,%0,%0,lsr #16\n\t"   // %0 <- %0 | %0>>16 cdcd
   "mov %0,%0,lsr #8\n\t"
   :"+r"(v)
   :
  );
  return v;
 }
#endif 
}
#endif
