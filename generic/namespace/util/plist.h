//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#ifndef PLIST_H
#define PLIST_H
INTERFACE(util_plist,$Revision: 137 $)
//----------------------------------------
// Parameterfile former PFile
// (c) H.Buchmann FHSO 2003 
// $Id: plist.h 193 2006-01-11 15:21:16Z buchmann $
//----------------------------------------
// format BNF
// (parameter)*
// parameter 	= name '=' value
// name		= string
// value	= string
// comment: # to eol 
// delimiters: blank tab 

#include "io/ascii/write.h"
#include <map>

namespace util
{
 class PList
 {
  private:
   static const char	Delim[];
   PList(const PList&); //no cloning
//STL stuff
   struct Cmp{bool operator()(const char*,const char*) const;};
   typedef std::map<const char*,char*,Cmp> Dict;
   Cmp     cmp;
   Dict    dict;  

   static const unsigned SIZE = 1024; //of current value/name
   io::ascii::Writer& msg;
   unsigned size;
   unsigned state;
   char     name[SIZE];  //current name
   unsigned nameI;
   char     value[SIZE]; //current value
   unsigned valueI;
   
//helpers for parsing
   static bool  isDelim(char ch);
   bool  isQuote(char ch){return (ch=='\'')||(ch=='"');}
   void  addEntry();
   void  init();
   void  notANumber(const char key[]) const;//writes message and exit

   template<typename typ> bool valueOf(const char key[],typ def,typ& val) const;
   template<typename typ> const typ  valueOf(const char key[],typ def) const;
   
  protected:
            PList(io::ascii::Writer& msg,bool verbose);
   void put(char ch); //for building
   bool verbose;      //can be set from subclass

//error handler
   virtual void onError(io::ascii::Writer& msg) const=0; //message already written
          
  public:
   virtual ~PList();
   void showIt(io::ascii::Writer& out);

   bool isDefined(const char key[]);
   void isDefinedExit(const char key[]); //exit if not defined
   bool valueOf(const char key[],const char*& val) const;
   bool valueOf(const char key[],int def,int& val) const;
   bool valueOf(const char key[],unsigned def,unsigned& val) const;
   bool valueOf(const char key[],unsigned short def,unsigned short& val) const;
   bool valueOf(const char key[],double def,double& val) const;
   bool valueIs(const char key[],const char val[]) const;
   int valueOf(const char key[],const char* selection[],int def) const;
   int valueOf(const char key[],const char* selection[]) const;
       //returns <0 if not found
   bool valueIsYes(const char key[]) const;

 //-----------------easier access
  const char*    valueOf(const char key[]) const;
  const int      valueOf(const char key[],int def) const;
  const unsigned valueOf(const char key[],unsigned def) const;
  const unsigned short valueOf(const char key[],unsigned short def) const;
  const double   valueOf(const char key[],double def) const;


 };
}
#endif

