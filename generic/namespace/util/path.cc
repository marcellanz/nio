//-----------------------------
//path of files
//(c) H.Buchmann FHSO 2005
//$Id: path.cc 221 2006-03-11 17:57:03Z buchmann $
//-----------------------------
#include "sys/sys.h"
IMPLEMENTATION(util_path,$Revision: 140 $)
#include "util/path.h"
//#include "sys/msg.h"

//#define UTIL_PATH_DEBUG

namespace util
{
 const std::string Path::Empty;
//------------------------------------------------ Path::Value
 Path::Value::Value(bool deleteIt)
 :deleteIt(deleteIt)
 {
 }
 
 Path::Value::~Value()
 {
 }
 
//------------------------------------------------ Path::Tree::Node
 Path::Tree::Node::Node()
 :value(0)
 {
 }
 
 Path::Tree::Node::Node(Path& path,const char part[],unsigned len)
 :value(0),
  part(part,len)
 {
  path.path.push_back(&this->part);
//  sys::msg<<"node "<<this->part.c_str()<<"\n";
 }
 
 Path::Tree::Node::~Node()
 {
  for(unsigned i=0;i<child.size();i++)
  {
   delete child[i];
  }
  if (value && value->deleteIt) delete value;
 }
 
 Path::Tree::Node* Path::Tree::Node::findChild(const char p[],unsigned len)
 {
  for(unsigned i=0;i<child.size();i++)
  {
   Node* nd=child[i];
   if (nd->part.compare(std::string(p,len))==0) return nd;
  }
  return 0;
 }

 void Path::Tree::Node::show(unsigned indent,io::ascii::Writer& out) const
 {
  out.blank(indent)<<part.c_str()<<"\n";
  for(unsigned i=0;i<child.size();i++)
  {
   child[i]->show(indent+1,out);
  }
 }

 void Path::Tree::Node::final(Path& path,const char p[],unsigned len)
 {
  Node* nd=findChild(p,len);
  if (nd==0) 
     {
      nd=new Node(path,p,len);
      child.push_back(nd);
     }
  path.position=nd;   
 }
 
 void Path::Tree::Node::parse(Path& path,const char p[])
 {
//  sys::msg<<"    '"<<part.c_str()<<"'  '"<<p<<"'\n";
//p[0] != delim  
  unsigned i=1;
  unsigned len=1;
  unsigned status=0;
  while(true)
  {
   char ch=p[i];
   switch(status)
   {
    case 0:
     if (ch=='\0') {final(path,p,len);return;} 
     if (ch==path.tree.delim) 
        {
	 i++; 
	 status=1;break;
	}
     i++;len++;
    break;
    
    case 1:
     if (ch==path.tree.delim){i++;break;} //remain
     if (ch=='\0') {final(path,p,len);return;}
     Node* nd=findChild(p,len);
     if (nd==0) 
        {
	 nd=new Node(path,p,len);
	 child.push_back(nd);
        }
     nd->parse(path,p+i);
    return; 
   }
  }
 }

//------------------------------------------------ Path::Tree
 Path::Tree::Tree(const char delim)
 :delim(delim),
  root(new Node)
 {
 }
 
 Path::Tree::~Tree()
 {
  delete root;
 }
 
 io::ascii::Writer& Path::Tree::show(io::ascii::Writer& out) const
 {
  root->show(0,out);
  return out;
 }
 
 bool Path::Tree::add(const Path& path) 
 {
  return true;//root->add(path,0);
 }
 
//------------------------------------------------ Path
 void Path::parse(Tree& tree,const char path[])
 {
  this->path.clear();
  this->fullPath.clear(); 
  absolute=false;
  unsigned status=0;
  unsigned i=0;
  while(true)
  {
   char ch=path[i];
   switch(status)
   {
    case 0: //root* | x*
     if (ch==tree.delim){absolute=true;status=1;break;}
    return tree.root->parse(*this,path+i);
    
    case 1:
     if (ch==tree.delim) break; //remain
    return tree.root->parse(*this,path+i);
   }
   i++; //next char
  }
 }

 Path::Path(Tree& tree,const char pathName[])
 :tree(tree),
  absolute(false),
  position(0)
 {
  parse(tree,pathName);
 }

 Path::~Path()
 {
 }

 bool Path::less(const Path& path)const
 {
  unsigned i=0;
  while(true)
  {
   if ((i==this->path.size())||(i==path.path.size())) 
      {
       return this->path.size()<path.path.size();
      }
   int res=this->path[i]->compare(*path.path[i]);
   if (res!=0) return res<0;  
   i++;
  }
 }

 io::ascii::Writer& Path::show(io::ascii::Writer& out) const
 {
  if (absolute) out<<tree.delim;
  for(unsigned i=0;i<path.size();i++)
  {
   if (i>0) out<<tree.delim;
   out<<path[i]->c_str();
  }
  return out;
 }

#ifdef UTIL_PATH_DEBUG
 class Tester
 {
  static Tester tester;
  Tester();
 };

 Tester Tester::tester;

 Tester::Tester()
 {
  Path::Tree tree;

  Path p0(tree,"sys/time-1");
  Path p1(tree,"sys/time");
  tree.show(sys::msg);
 }
#endif   
}//namespace io
