//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//--------------------------
//endian stuff
//(c) H.Buchmann FHSO 2002
//$Id: endian.cc 193 2006-01-11 15:21:16Z buchmann $
//--------------------------
#include "sys/sys.h"
IMPLEMENTATION(util_endian,$Revision: 137 $)
#include "util/endian.h"
#include "sys/msg.h"

namespace util
{
 Endian Endian::endian;
 
 Endian::Endian()
 {
  checkEndian();
 }
 
 const char* Endian::Name[]={"BIG","LITTLE"};

 void Endian::checkEndian()
 {
  static const unsigned char Nbr1[]={1,2,3,4};
  static const unsigned Nbr2=0x01020304;
  Type t=(*(unsigned*)Nbr1==Nbr2)?BIG:LITTLE;
  if (t!=SystemEndian)
     sys::msg.error()<<"endian error (compiled != current)\n";
 }

#ifndef __arm__
 unsigned Endian::change(unsigned v)
 {
  return   ((v     & 0xff)<<24)|
          (((v>> 8)& 0xff)<<16)|
	  (((v>>16)& 0xff)<< 8)|
	   ((v>>24)& 0xff);    
 }
 
 unsigned short Endian::change(unsigned short v)
 {
  return ((v    & 0xff)<<8) | 
         ((v>>8)& 0xff);
 }
#endif
 
}
