//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#ifndef UTIL_ALIGN_H
#define UTIL_ALIGN_H
//------------------------
//align
//(c) H.Buchmann FHSO 2004
//$Id: align.h 217 2006-02-05 17:55:51Z buchmann $
//------------------------
INTERFACE(util_align,$Revision: 137 $)
namespace util
{
 template<typename out,typename in>
 struct Align
 {
  inline static out* up(void* val)
  {
   static const unsigned MASK=sizeof(in)-1;
   return (out*)((((unsigned)val-1)&~MASK)+sizeof(in));
  }
  
  inline static out* down(void* val)
  {
   static const unsigned MASK=sizeof(in)-1;
   return (out*)((unsigned)val&~MASK);
  }
 };
}
#endif
