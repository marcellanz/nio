#ifndef UTIL_PATH_H
#define UTIL_PATH_H
//-----------------------------
//path of files
//(c) H.Buchmann FHSO 2005
//$Id: path.h 193 2006-01-11 15:21:16Z buchmann $
//-----------------------------
INTERFACE(util_path,$Revision: 172 $)
#include "io/ascii/write.h"
#include <string>
#include <vector>
//TODO look for a faster solution
//TODO write it as template
namespace util
{
//------------------------------------------------ Path
 class Path
 {
  public:
   class Value;
//------------------------------------------------ Path::Tree
   class Tree
   {
    private:
     friend class Value;
     friend class Path;
     
    public:
//------------------------------------------------ Path::Tree::Node
     class Node
     {
      public:
       typedef std::vector<Node*> List;
       
      private:
       friend class Path;
       friend class Tree;
       Value* value;
       const std::string part;
       List child;
        	Node(); //for the root
        	Node(Path& path,const char part[],unsigned len);
       void show(unsigned indent,io::ascii::Writer& out) const;
       void parse(Path& path,const char p[]);
       Node*  findChild(const char p[],unsigned len);
       void final(Path& path,const char p[],unsigned len);

      public: 
       virtual ~Node();
       bool isLeaf()               const {return child.size()==0;}
       bool hasValue()             const {return value!=0;}
       const List& getChildren()   const {return child;} 
       const std::string& getPart()const {return part;}
     };
     const char delim;
     Node* root;

    public:
              Tree(const char delim='/');
     virtual ~Tree();
     bool add(const Path& path); //returns true if path not yet in tree
     io::ascii::Writer& show(io::ascii::Writer& out) const;
     const Node* getRoot()const{return root;} 
     const char getDelim()const{return delim;}
   };//class Tree

//------------------------------------------------ Path::Value
   class Value
   {
    private:
     friend class Tree::Node;
     bool deleteIt;
    public:
              Value(bool deleteIt);
     virtual ~Value();
   };//class Value
   
  private:
   friend class Tree;
   static const std::string Empty;
   Tree& tree;
   bool absolute;
   Tree::Node* position; //in tree
   std::vector<const std::string*> path;
   std::string fullPath;
   void parse(Tree& tree,const char path[]);

  public:
	    Path(Tree&,const char pathName[]);
   virtual ~Path();
   Value* getValue() const{return position->value;}
   void setValue(Value* val){position->value=val;}

   const bool isAbsolute()const
          {return absolute;}
   io::ascii::Writer& show(io::ascii::Writer& out) const;
   friend io::ascii::Writer& operator<<(io::ascii::Writer& out,const Path& p)
          {return p.show(out);}
   const std::string& getName()const
          {return fullPath;}
   const std::string& operator[](unsigned i)const
          {return (i<path.size())?*path[i]:Empty;}
   const unsigned getLength()const
          {return path.size();} 
   bool less(const Path& path)const;	
 };
}//namespace io
#endif
