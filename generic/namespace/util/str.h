//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#ifndef UTIL_STR_H
#define UTIL_STR_H
INTERFACE(util_str,$Revision: 174 $)
//---------------------------
//str
//(c) H.Buchmann FHSO 2002
//$Id: str.h 193 2006-01-11 15:21:16Z buchmann $
//---------------------------
namespace util
{
 class Str
 {
  public:
  //useful in STL
   struct Less
   {
    bool operator()(const char s0[],const char s1[]) const;
   };
   
  private:
   typedef int (*To)(unsigned, char*,int);
   static const char Digits[];
   static const To Conv[];
   template<typename typ,unsigned base,unsigned sLen>
   static int toBase(typ,char s[],int len);
   template<typename typ,unsigned shift,unsigned sLen>
   static int toShift(typ,char s[],int len);
   
  public:
   enum BASE  {OCT,DEC,HEX};
   static const int      MIN_INT      =  1<<(sizeof(unsigned)*8-1);
   static const int      MAX_INT      = ~MIN_INT;
   static const unsigned MAX_UNSIGNED = (unsigned)-1;
   
   static int len(const char s[]);
   static int copy(const char src[],char dst[]);
   static int copy(const char src[],char dst[],int maxLen);
   static int concat(const char src[],char dst[],int maxLen);
   static char* trim(char s[]); //removes leading/preceding blanks

 //easy recogs
   static char cap(char ch){return (('a'<=ch)&&(ch<='z'))?ch-'a'+'A':ch;}
   static int isDecDigit(char ch)
              {return (('0'<=ch)&&(ch<='9'))?ch-'0':-1;}
   static int isOctDigit(char ch)
              {return (('0'<=ch)&&(ch<='7'))?ch-'0':-1;}
   static int isHexDigit(char ch)
              {if (('0'<=ch)&&(ch<='9')) return    ch-'0';
	       if (('a'<=ch)&&(ch<='f')) return 10+ch-'a';
	       if (('A'<=ch)&&(ch<='F')) return 10+ch-'A';
	       return -1;
	      }
   static bool isWhite(char ch){return (ch==' ')||(ch=='\t');}
   static bool isLetter(char ch)
              {return (('a'<=ch)&&(ch<='z'))||
	              (('A'<=ch)&&(ch<='Z'));} 
 //conversion to string
   static int toOct(unsigned val,char s[],int len);
   static int toDec(unsigned val,char s[],int len);
   static int toHex(unsigned val,char s[],int len);
   
   static int to(unsigned val,BASE b,char s[],int len)
         {return Conv[b](val,s,len);}
   static int to(unsigned val,char s[],int len)
         {return toDec(val,s,len);}
   static int to(int      val,char s[],int len);
 
 //conversion from string
 //return value of to(const char s[],type& val)
 // return value | overflow |assignement of val 
 //--------------|-------------------
 // 0            | false    |no   
 // 0            | true     |no
 // !=0 ok       | false    |yes points to first char not belonging to nbr
 
   static const char* to(const char s[],bool& overflow,unsigned&       val);
   static const char* to(const char s[],bool& overflow,int&            val);
 // ['+'|'-']digit+
   static const char* to(const char s[],bool& overflow,unsigned short& val);
    
   template<typename T>   
   static bool to(const char s[],T& val)
   {
    T v=0;
    bool overflow=false;
    const char* p=to(s,overflow,v);
    bool res=(p!=0)&&(*p=='\0');
    if (res) val=v;
    return res;
   }
 
   static bool to(const char s[],double& val);    
 // ['+'|'-']intPart[fracPart][exp]
 // intPart  = digit+
 // fracPart = '.'digit*
 // exp      = ('e'|'E')['+'|'-']digit+ 
 
 //comparing
   static int cmp(const char s0[],const char s1[]);
   static int cmpCAP(const char s0[],const char s1[]);
   static bool less(const char s0[],const char s1[]){return cmp(s0,s1)<0;}
   static bool equal(const char s0[],const char s1[]){return cmp(s0,s1)==0;}
   static bool prefix(const char prfx[],const char s[]); 
       //returns true iff s starts with prfx
 //index
   static int index(const char s[],char ch);
 // index of ch in s first occurence
   static int index(const char* ss[],const char s[]);
 //returns index of s in ss 
 //ss must be 0 termianted

 //as escape sequence
   static int asEsc(char ch,char s[],unsigned len);
   static int asEsc(const char src[],char dst[],unsigned len);  
 };
}
#endif
