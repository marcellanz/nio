//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#ifndef UTIL_RANDOM_NUMBER_H
#define UTIL_RANDOM_NUMBER_H
INTERFACE(util_random_number,$Revision: 137 $)
//------------------------------------
// random generator
// see D.Knuth kongruental method
// (c) H.Buchmann ISOe 1997
// $Id: number.h 193 2006-01-11 15:21:16Z buchmann $
// thanks to Marcel Lanz
//------------------------------------
#include "sys/msg.h"
namespace util
{
 namespace random
 {
  template<typename Typ>
  class Number
  {
   private:
    static const Typ Data[];
    unsigned i0;
    unsigned i1;
    Typ data[55];

   public:

              Number();
             ~Number(){}
    void     seed(Typ nbr){data[i0]=nbr;}

    Typ value()
    {
     Typ r=data[i0]+data[i1];
     data[i0]=r;
     if (i0) i0--;
	else i0=54;
     if (i1) i1--;
	else i1=54;
     return r;
    }

    Typ value(const Typ range){return value()%range;}
                   //only valid for small ranges
    const Typ* getData()const {return data;}
    void reset(){i0=54;i1=23;}
  };

 }//random
}//namespace util
#endif

