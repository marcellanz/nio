//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//------------------------------------
// random generator
// see D.Knuth kongruental method
// (c) H.Buchmann ISOe 1997
// $Id: number.cc 193 2006-01-11 15:21:16Z buchmann $
// thanks to Marcel Lanz
//------------------------------------
#include "sys/sys.h"
IMPLEMENTATION(util_random_number,$Revision: 137 $)
#include "util/random/number.h"
//#define RANDOM_NUMBER_BUILD 

#ifdef RANDOM_NUMBER_BUILD
#include <iostream>
using namespace std;
#endif

namespace util
{
 namespace random
 {
  template<> const unsigned Number<unsigned>::Data[]=
  {
   3466244725u,1920822196u,2818131059u,3008957077u,3188823435u,
   3140297936u, 291449621u, 171503502u,1925834481u,3804121035u,
   2677268127u, 168906386u,3035688601u,1227848305u,1768178179u,
   3808236046u,2885572592u,2759130258u, 566114268u, 636264092u,
     41314543u,1209089351u,3241517952u,2571831821u,1871875022u,
    588610232u,1001091467u,1894540331u,1472025133u,3460893078u,
    408461586u,3214118329u,1087531705u,2981896928u,2237738121u,
    253590788u,3177070623u, 969237992u, 189714326u,1030284321u,
   2467566442u,1621241520u,2878684909u,3584175948u,3401266816u,
   3425515610u,1681314993u,3361900704u,3725701607u,4083249858u,
   1424725228u,3040519534u,2158198184u,3623401019u,3012190611u
  };
  template<> const long long unsigned Number<long long unsigned>::Data[]=
  {
   0xd252a0d12030f509llu,0xe91564c536524aa2llu,0x943537fcfa600023llu,
   0x2b575bd2edc9ea3cllu,0x0d0503231cf042cdllu,0x3186e28d08e7aaa1llu,
   0xad3cac8d397ab1cbllu,0x5125bd3c9a27cf7allu,0x78e88bc01a5c26b9llu,
   0x759d691910a919dcllu,0x63960a65412fc1c1llu,0x3c6bb06ebf17f97allu,
   0x6e5e4c39da47ff64llu,0x07c7027e3daddee3llu,0xc538b42b36af6790llu,
   0xed927843bd9887ccllu,0x03f6659f3dbe382allu,0xbb8a348716637bdfllu,
   0x5d58f6945e873701llu,0x696c23c5bc5e3ab7llu,0x93b023e870a317fcllu,
   0x99199f952f1c60f6llu,0xdce6cd91e2058f5allu,0x64c1451155539265llu,
   0x95a0354b6bfc9524llu,0x02efbf06ffa4015ellu,0xff5f113919d37e91llu,
   0xdba90ada78873856llu,0x85b1d04c7e4a74f7llu,0x570cbf908829112bllu,
   0x94111c31abc3a0b4llu,0xff632f347611a8fdllu,0xe7147922ff3733afllu,
   0xe90b45d0b21ca070llu,0xbcc7312b54d67080llu,0xc2186b5281b54fa1llu,
   0x83f7148cada48af4llu,0xee6333d63e931e83llu,0x0fb5219104218fd2llu,
   0x63961664c80ba26allu,0x2c3689f9cae65b88llu,0xdedc34c037ea6724llu,
   0x739b38baf83dcc38llu,0xfe3fb1da75e16dfdllu,0x4707506da3e8204allu,
   0xe7d267af27dc4af1llu,0x2dd2fb12d8153c59llu,0xfc27c9511b76d34ellu,
   0x83bd4422cca7dab6llu,0x2589f2b54941433dllu,0x13cb7fcf22d5a324llu,
   0xea2754bf862dab84llu,0x39ba5274b1961bfallu,0xfd6faf2b9aaa328fllu,
   0x86685b00a907e0d4llu 
  };

  template<>Number<unsigned>::Number()
  :i0(54),i1(23)
  {
   for(unsigned i=0;i<55;i++)data[i]=Data[i];
  }

  template<>Number<long long unsigned>::Number()
  :i0(54),i1(23)
  {
   for(unsigned i=0;i<55;i++)data[i]=Data[i];
  }

 }//namespace random
}//namespace util

#ifdef RANDOM_NUMBER_BUILD
//--------------------------------
class RandomBuilder
{
 static RandomBuilder rb;
 static const unsigned  R=1<<5;
 static const unsigned  N=500000;
 static const unsigned  WARM_UP =1000000000; 
 static const unsigned  LEN=5;

 typedef unsigned Typ;
 util::random::Number<Typ> random;
 void warmUp();
 void chiTest(unsigned n);
 void wrData();
 unsigned r[R];
 
 RandomBuilder();
};

RandomBuilder RandomBuilder::rb;


RandomBuilder::RandomBuilder()
{
// warmUp();
// wrData();
 random.reset();
 for(unsigned n=1000;n<N;n+=1000) chiTest(n);
}

void RandomBuilder::wrData()
{
 const Typ* d=random.getData();
 for(unsigned i=0;i<55;i++)
 {
  if (i>0) cerr<<",\n";
  cerr<<hex<<"0x"<<d[i]<<"llu";
 }
 cerr<<"\n";
}

void RandomBuilder::warmUp()
{
 cerr<<"warming up ...";
 for(unsigned i=0;i<WARM_UP;i++) random.value();
 cerr<<"done\n";
}

void RandomBuilder::chiTest(unsigned n)
{
 for(unsigned i=0;i<R;i++) r[i]=0;
 for(unsigned i=0;i<N;i++)
 {
  r[random.value(R)]++;
 }
//evaluate 
 double q=(double)N/(double)R;
 double s=0;
 for(unsigned i=0;i<R;i++)
 {
  double v=(r[i]-q);
  s+=v*v;
 }
 cerr<<n<<"\t"<<s/q<<"\n";
// cerr<<"chi^2= "<<s/q<<"\n";
}
#endif

