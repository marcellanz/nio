//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#ifndef UTIL_RANDOM_DISTRIBUTION_H
#define UTIL_RANDOM_DISTRIBUTION_H
//--------------------------
//distribution
//(c) H.Buchmann FHSO 2004
//$Id: distribution.h 193 2006-01-11 15:21:16Z buchmann $
//--------------------------
INTERFACE(util_random_distribution,$Revision: 137 $)
#include "util/random/number.h"
namespace util
{
 namespace random
 {
  class Distribution
  {
   private:
    const unsigned* dist;
    const unsigned len;
    unsigned n;
    Number<unsigned>& random;

   public:
             Distribution(Number<unsigned>& number,
	                  const unsigned dist[],unsigned len);
    virtual ~Distribution();
    unsigned value(); //returns random distrubuted with dist
  };
 }//namespace random
}//namespace util
#endif
