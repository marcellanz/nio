//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//--------------------------
//distribution
//(c) H.Buchmann FHSO 2004
//$Id: distribution.cc 193 2006-01-11 15:21:16Z buchmann $
//--------------------------
#include "sys/sys.h"
IMPLEMENTATION(util_random_distribution,$Revision: 137 $)
#include "util/random/distribution.h"

//#define UTIL_RANDOM_DISTRIBUTION_TEST

#ifdef UTIL_RANDOM_DISTRIBUTION_TEST
#include <iostream>
#endif

namespace util
{
 namespace random
 {
  Distribution::Distribution(Number<unsigned>& random,
                             const unsigned dist[],unsigned len)
  :random(random),
   dist(dist),len(len),n(0)
  {
   for(unsigned i=0;i<len;i++) n+=dist[i];
  }

  Distribution::~Distribution()
  {
  }

  unsigned Distribution::value()
  {
   unsigned lim=1+random.value(n);
   unsigned s=0;
   unsigned v=0;
   while(true)
   {
    s+=dist[v];
    if (lim<=s) return v;
    v++; 
   }
  }
 }//namespace random
}//namespace util

#ifdef UTIL_RANDOM_DISTRIBUTION_TEST
class Tester
{
 static Tester tester;
 static const unsigned LEN=5;
 static const unsigned dist[LEN];
 static const unsigned N=1000;
 util::random::Number<unsigned> random;
 util::random::Distribution     d;
 Tester();
};

Tester Tester::tester;

const unsigned Tester::dist[LEN] = {1,4,6,4,1};
Tester::Tester()
:d(random,dist,LEN)
{
 unsigned h[LEN]; //the histogram
 for(unsigned i=0;i<LEN;i++)h[i]=0;
 for(unsigned i=0;i<N;i++) h[d.value()]++;
 for(unsigned i=0;i<LEN;i++) std::cerr<<h[i]/(double)N<<"\n"; 
}

#endif
