//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//------------------------------------
// random generator
// see D.Knuth kongruental method
// (c) H.Buchmann ISOe 1997
// $Id: random-bug.cc 193 2006-01-11 15:21:16Z buchmann $
//------------------------------------
#include "sys/sys.h"
IMPLEMENTATION(util_random,$Revision: 137 $)
namespace util
{
 template<typename Typ>
 class Random
 {
  private:
   static const unsigned Data[55];
   unsigned i0;
   unsigned i1;
   Typ data[55];
    
  public:
   
            Random()
	    :i0(54),i1(23)
	    {
	     for(unsigned i=0;i<55;i++)data[i]=Data[i];
	    }
   virtual ~Random(){}
   void     seed(Typ nbr){data[i0]=nbr;}
   
   Typ number()
   {
    Typ r=data[i0]+data[i1];
    data[i0]=r;
    if (i0) i0--;
       else i0=54;
    if (i1) i1--;
       else i1=54;
    return r;
   }
   
   Typ number(const Typ range){return number()%range;}
                  //only valid for small ranges
   const Typ* getData()const {return data;}
 };
 
}

#define RANDOM_BUILD 

#ifdef RANDOM_BUILD
#include <iostream>
using namespace std;
#endif

namespace util
{
 template<typename Typ>
 const unsigned Random<unsigned>::Data[55]=
 {
  3466244725u,1920822196u,2818131059u,3008957077u,3188823435u,
  3140297936u, 291449621u, 171503502u,1925834481u,3804121035u,
  2677268127u, 168906386u,3035688601u,1227848305u,1768178179u,
  3808236046u,2885572592u,2759130258u, 566114268u, 636264092u,
    41314543u,1209089351u,3241517952u,2571831821u,1871875022u,
   588610232u,1001091467u,1894540331u,1472025133u,3460893078u,
   408461586u,3214118329u,1087531705u,2981896928u,2237738121u,
   253590788u,3177070623u, 969237992u, 189714326u,1030284321u,
  2467566442u,1621241520u,2878684909u,3584175948u,3401266816u,
  3425515610u,1681314993u,3361900704u,3725701607u,4083249858u,
  1424725228u,3040519534u,2158198184u,3623401019u,3012190611u
 };
}

#ifdef RANDOM_BUILD
//--------------------------------
class RandomBuilder
{
 static RandomBuilder rb;
 static const unsigned  R=1<<8;
 static const unsigned  N=100000;
 static const unsigned  WARM_UP = 1000000;

 util::Random<unsigned> random;

 void warmUp();
 void chiTest();
 void wrData();
 unsigned r[R];
 
 RandomBuilder();
};

RandomBuilder RandomBuilder::rb;

RandomBuilder::RandomBuilder()
{
 warmUp();
 wrData();
 chiTest();
}

void RandomBuilder::wrData()
{
 const unsigned* d=random.getData();
 for(unsigned i=0;i<55;i++)
 {
  if (i>0) cerr<<",\n";
  cerr<<hex<<"0x"<<d[i]<<"llu";
 }
 cerr<<"\n";
}

void RandomBuilder::warmUp()
{
 cerr<<"warming up ...";
 for(unsigned i=0;i<WARM_UP;i++) random.number();
 cerr<<"done\n";
}

void RandomBuilder::chiTest()
{
 for(unsigned i=0;i<R;i++) r[i]=0;
 for(unsigned i=0;i<N;i++)
 {
  r[random.number(R)]++;
 }
 double q=(double)N/(double)R;
 double s=0;
 for(unsigned i=0;i<R;i++)
 {
  double v=(r[i]-q);
  s+=v*v;
 }
 cerr<<"chi^2= "<<s/q<<"\n";
}
#endif

