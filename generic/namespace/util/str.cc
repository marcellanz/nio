//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//---------------------------
//str
//(c) H.Buchmann FHSO 2002 
//$Id: str.cc 221 2006-03-11 17:57:03Z buchmann $
//---------------------------
#include "sys/sys.h"
IMPLEMENTATION(util_str,$Revision: 174 $)
//#define UTIL_STRING_TEST

#ifdef UTIL_STRING_TEST
//#include "sys/msg.h"
#endif

#include "util/str.h"


namespace util
{
 const char Str::Digits[]="0123456789abcdef";
 const Str::To Str::Conv[]={Str::toOct,Str::toDec,Str::toHex};
 
 int Str::len(const char s[])
 {
  if (s==0) return 0;
  int l=0;
  while(*s++)l++;
  return l;
 }
 
 int Str::copy(const char src[],char dst[])
 {
  int i=0;
  while(true)
  {
   char ch=src[i];
   dst[i]=ch;
   if (ch=='\0') return i;
   i++;
  }
 }
 
 int Str::copy(const char src[],char dst[],int maxLen)
 {
  int i=0;
  while(true)
  {
   if (i==maxLen) return -1;
   char ch=src[i];
   dst[i]=ch;
   if (ch=='\0') return i;
   i++;
  }
 }
 
 int Str::concat(const char src[],char dst[],int maxLen)
 {
  if (dst==0) return -1;
  int i=0;
  //go to end
  while(*dst){i++;dst++;}
  int j=0;
  if (src==0) return -1;
  while(true)
  {
   char ch=src[j++];
   if (i>=maxLen) return -1;
   *dst++=ch;
   if(ch=='\0') return i;
   i++;
  }
 }

 char* Str::trim(char s[])
 {
  if (s==0) return 0;
  //start of non blank;
  while(true)
  {
   if (*s=='\0') return 0;
   if (*s!=' ') break;
   s++;
  }
//*s!=0 s!=' '  
  unsigned i=0;//index in s
  unsigned rb=0; //last right blank
  while(true)
  {
   char ch=s[i++];
   if (ch=='\0') {s[rb]='\0';return s;}
   if (ch!=' ') rb=i;
  }
 }
 
 template<typename typ,unsigned base,unsigned sLen>
 int Str::toBase(typ val,char s[],int len)
 {
  if (s==0) return -1;
  char d[sLen];//space for 128 bit unsigned
  char* str=d;
  int l=0;
  do
  {
   *str++=Digits[val%base];
   l++;
   val/=base;
  }while(val);
  if (l>=len) return -1;
  do *s++=*--str;while(d!=str);
  *s='\0';
  return l;
 }
 
 template<typename typ,unsigned shift,unsigned sLen>
 int Str::toShift(typ val,char s[],int len)
 {
  if (s==0) return -1;
  char d[sLen]; //space for 128 bit unsigned
  char* str=d;
  unsigned mask=1;
  int l=0;
  while(mask)
  {
   *str++=Digits[val&((1<<shift)-1)];
   l++;
   val>>=shift;
   mask<<=shift;
  }
  if (l>=len) return -1;
  do *s++=*--str;while(d!=str);
  *s='\0';
  return l;
 }
   
//conversion
 int Str::toOct(unsigned val,char s[],int len)
 {
  return toShift<unsigned,3,43>(val,s,len);
 }

 int Str::toHex(unsigned val,char s[],int len)
 {
  return toShift<unsigned,4,32>(val,s,len);
 }
 
 int Str::toDec(unsigned val,char s[],int len)
 {
  return toBase<unsigned,10,39>(val,s,len);
 }
 
 int Str::to(int      val,char s[],int len)
 {
  if (s==0) return -1;
  if (val<0)
     {
      if (len<=1) return -1;
      *s++='-';
      int res=toDec((unsigned)(-val),s,len-1);
      return (res<0)?-1:res+1;
     }
  return toDec((unsigned)(val),s,len);
 }

 const char* Str::to(const char s[],bool& overflow,unsigned& val)
 {
  overflow=false;
  if (s==0) return 0;
  unsigned state=0;
  unsigned v=0;
  int d=0; //digitvalue
  while(true)
  {
   const char* pos=s;
   char ch=*s;
   switch(state)
   {
    case 0: //[0..9]
     if (ch=='0'){state=2;break;} //0 x|dec
     d=isDecDigit(ch);
     if(d>=0)   {state=1;v=d;break;}//dec dec*
    return 0;
    
    case 1: //digit | other
     d=isDecDigit(ch);
     if(d>=0) 
       {
        unsigned v1=10*v+d;
	if (v1<v) {overflow=true;return 0;}
	v=v1;
	break;
       } 
    val=v;    
    return pos;
    
    case 2: //x | dec | other
     if ((ch=='x')||(ch=='X')){state=3;break;}
     d=isDecDigit(ch);
     if (d>=0){state=1;v=d;break;}
     val=0;
    return pos; //number 0
    
    case 3: //hex+ 
     d=isHexDigit(ch);
     if (d>=0){state=4;v=d;break;}
    return 0;
    
    case 4: //hex*
     d=isHexDigit(ch);
     if (d>=0)
        {
	 unsigned v1=0x10*v+d;
	 if (v1<v) {overflow=true;return 0;}
	 v=v1;
	 break;
	}
    val=v;
    return pos;
   }
   s++;
  }
 }

 const char* Str::to(const char s[],bool& overflow,int& val)
 {
  overflow=false;
  if (s==0) return 0;
  unsigned state=0;
  int v=0;
  bool neg=false;
  while(true)
  {
   const char* pos=s;
   char ch=*s++;
   int dd=isDecDigit(ch);
   switch(state)
   {
    case 0:
     if (ch=='-'){neg=true;state=1;break;}
     if (ch=='+'){         state=1;break;}
     if (dd>=0)  {v=dd;    state=2;break;}
    return 0; 
    
    case 1:
     if (dd>=0)   {v=dd;   state=2;break;}
    return 0;
    
    case 2:
     if (dd>=0)
        {
	 int v1=10*v+dd;
	 if (v1<0) {overflow=true;return 0;}
	 v=v1;
	 break;
	}
    val=v;	
    return pos;
   }
  }
 }
 

 const char* Str::to(const char s[], bool& overflow,unsigned short& val)
 {
  unsigned v;
  const char* pos=to(s,overflow,v);
  if (pos==0) return 0;
  if (v<0x10000) {val=v;return pos;}
  overflow=true;
  return 0;
 }
 
 bool Str::to(const char s[],double& val)
 {
#if 0
  if (s==0) return false;
  unsigned state=0;
  bool neg=false;
  bool negExp=false;
  double v=0;
  while(true)
  {
   char ch=*s++;
   sys::msg<<ch<<" : "<<state<<"\n";
   switch(state)
   {
    case 0: //+|-|digit
     if (ch=='-'){neg=true;state=1;break;}
     if (ch=='+'){         state=1;break;}
     {
      int d=isDecDigit(ch);
      if (d>=0)  {state=2;break;} 
     }
    return false;
    case 1: //digit
    {
     int d=isDecDigit(ch);
     if (d>=0)   {state=2;break;}
    }
    return false;
    case 2: //digit | . | eos
     if (ch=='\0')
        {
	 //[+|-]digit+
	 return true;
	}
     if (ch=='.'){state=3;break;}
     {
      int d=isDecDigit(ch);
      if (d>=0)
         { //remain in state
	  break;
	 }
     }     	
    return false;
    case 3: //digit| e | E |eos
    if ((ch=='e')||(ch=='E')){state=4;break;}
    if (ch=='\0')
       {
        //[+|-]digit+.
	return true;
       }
    {
     int d=isDecDigit(ch);
     if (d>=0) 
        { //+|-digit+.digit+
	 break;
	}
    }   
    return false;
    case 4: //+|-|digit
     if (ch=='-'){negExp=true;state=5;break;}
     if (ch=='+'){            state=5;break;}
     {
      int d=isDecDigit(ch);
      if (d>=0){state=6;break;}
     }
    return false;
    case 5: //digit
     {
      int d=isDecDigit(ch);
      if (d>=0){state=6;break;}
     }
    return false;
    case 6: //digit|eos
     if (ch=='\0')
        {
	 return true;
	}
     {
      int d=isDecDigit(ch);
      if (d>=0) 
         {
	  break;
	 }
     }	
    return false;
   }//switch(state)
  }//while
#endif
  return false;
 }

 int Str::cmp(const char s0[],const char s1[])
 {
  unsigned i=0;
  while(true)
  {
   char ch0=s0[i];
   char ch1=s1[i];
   if (ch0!=ch1)  return ch0-ch1;
   if (ch0=='\0') return 0; 
   i++;  
  }
 }
  
 bool Str::prefix(const char prfx[],const char s[])
 {
  unsigned i=0;
  while(true)
  {
   char p=prfx[i];
   if (p=='\0') return true;
   char c=s[i];
   if (p!=c) return false;
   i++;
  }
 }
 
 int Str::cmpCAP(const char s0[],const char s1[])
 {
  unsigned i=0;
  while(true)
  {
   char ch0=cap(s0[i]);
   char ch1=cap(s1[i]);
   if (ch0!=ch1)  return ch0-ch1;
   if (ch0=='\0') return 0; 
   i++;  
  }
 }
 
 int Str::index(const char s[],char ch)
 {
  int i=0;
  while(true)
  {
   char c=s[i];
   if (c=='\0') return -1;
   if (c==ch)  return i;
   i++;
  }
 }
 
 int Str::index(const char* ss[],const char s[])
 {
  int i=0;
  while(true)
  {
   if (ss[i]==0) return -1;
   if (equal(ss[i],s)) return i;
   i++;
  }
 }
 
 int Str::asEsc(char ch,char s[],unsigned len)
 {
  static const char* SpecialChar[]=
  {
   "\\0",
   "\\x001",
   "\\x002",
   "\\x003",
   "\\x004",
   "\\x005",
   "\\x006",
   "\\a",
   "\\b",
   "\\t",
   "\\n",
   "\\v",
   "\\f",
   "\\r",
   "\\00e",
   "\\00f",
   "\\010",
   "\\011",
   "\\012",
   "\\013",
   "\\014",
   "\\015",
   "\\016",
   "\\017",
   "\\018",
   "\\019",
   "\\01a",
   "\\01b",
   "\\01c",
   "\\01d",
   "\\01e",
   "\\01f"
  };
#ifdef __arm__
  if(ch<' ') return copy(SpecialChar[ch],s,len);
#else
  if(('\0'<=ch)&&(ch<' ')) return copy(SpecialChar[ch],s,len);
#endif  
  switch(ch)
  {
   case '"':return copy("\\\"",s,len);
   case '\'':return copy("\\'",s,len);
   case '\\':return copy("\\\\",s,len);
  }
  if (ch>0)
  { 
   char s1[2]={ch,'\0'};
   return copy(s1,s,len);
  }
  char s1[]={'\\','x','0',Digits[(ch>>4)&0xf],Digits[ch&0xf],'\0'};
  return copy(s1,s,len);
 }
 
 int Str::asEsc(const char src[],char dst[],unsigned len)
 {
  unsigned i=0;
  unsigned j=0;
  while(true)
  {
   char ch=src[i++];
   int l=asEsc(ch,dst+j,len);
   if (l<0) return -1;
   len-=l;
   j+=l;
   if (ch=='\0') break;
  }
  return j;
 }

 bool Str::Less::operator()(const char s0[],const char s1[]) const
 {
  return less(s0,s1);
 }

#ifdef UTIL_STRING_TEST
//--------------------------------------------- Tester
 class Tester
 {
  static Tester tester;
  Tester();
 };
 
 Tester Tester::tester;
 
 Tester::Tester()
 {
  sys::msg<<"Tester\n";
  const char s[]="0x800";
  unsigned short v=0;
//  bool overflow=false;
  bool pos=Str::to(s,v);
  sys::msg<<"pos= "<<pos<<" v= "<<v<<"\n";
 }
#endif 
} //namespace util
