//-------------------------------
//byte stream 
//(c) H.Buchmann FHSO 2005
//$LastChangedRevision: 137 $
//-------------------------------
#include "sys/sys.h"
IMPLEMENTATION(io_byte_stream,$LastChangedRevision: 137 $)
#include "io/byte/stream/stream.h"
namespace io
{
 namespace byte
 {
  namespace stream
  {
   Input::Input()
   :idx(0),
    size(0),
    data(0)
   {
   }
   
   Input::Input(const unsigned char data[],unsigned size)
   :idx(0),
    size(size),
    data(data)
   {
   }
   
   Input::~Input()
   {
   }
  }//namespace stream
 }//namespace byte
}//namespace io
