#ifndef IO_BYTE_STREAM_H
#define IO_BYTE_STREAM_H
//-------------------------------
//byte stream 
//(c) H.Buchmann FHSO 2005
//$LastChangedRevision: 137 $
//todo: Output
//-------------------------------
INTERFACE(io_byte_stream,$LastChangedRevision: 137 $)
namespace io
{
 namespace byte
 {
  namespace stream
  {
//------------------------------------------------ Input
   class Input
   {
    private:
     unsigned idx;
     
    protected: 
     unsigned size;
     const unsigned char* data;
     Input();
     
    public:
              Input(const unsigned char data[],unsigned size);
     virtual ~Input();
     bool eos(){return idx>=size;}
     const unsigned char* getData() const{return data+idx;}
     void skip(unsigned delta){idx+=delta;}
     unsigned getPos()const{return idx;}
     unsigned getSize()const{return size;}
     unsigned char getByte(){return eos()?0:data[idx++];}
   };
   
//------------------------------------------------ Output
   class Output
   {
    //not yet done
   };
  }//namespace stream
 }//namespace byte
}//namespace io
#endif
