//
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#ifndef IO_BYTE_LOG_H
#define IO_BYTE_LOG_H
//------------------------------------------
//logging
//(c) H.Buchmann FHSO 2001
//$Id: log.h 212 2006-01-31 15:44:44Z buchmann $
//------------------------------------------
INTERFACE(io_byte_log,$Revision: 137 $)
#include "io/byte/byte.h"
#include "io/ascii/out.h"
namespace io
{
 namespace byte
 {
//------------------------------------------ Logger
//format
// repeat data in ascii
// {
//  src: see Switch::Listener::Source
//  len: number of bytes from src
//  char the data maximal capacity
// }

  class Logger:public Switch::Listener
  {
   private:
    ascii::Writer log;
    unsigned char* buffer;
    unsigned capacity;
    unsigned len;
    Switch::Listener::Source src; //not defined at start
    
    void write();    
    void onData(Switch::Listener::Source,unsigned char d);
    
   public:
     Logger(Output& out,unsigned capacity);
    ~Logger();
  };
 } //namespace byte
} //namespace io
#endif
