//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//---------------------------------
//uart 
//(c) H.Buchmann FHSO 2002
//$Id: uart.cc 193 2006-01-11 15:21:16Z buchmann $
//---------------------------------
#include "sys/sys.h"
IMPLEMENTATION(io_byte_uart,$Revision: 137 $)
#include "io/byte/uart.h"
#include "sys/msg.h"
namespace io
{
 namespace byte
 {
  sys::Device::List UART::dList;
  
  const unsigned UART::BpS[] = //see Baudrate *must* match
  {
   1200,  
   2400, 4800,
   9600,   19200, 38400, 57600, 115200,
   230400, 460800
  };

  const char* UART::WordLenName[]={"5","6","7","8",0};
  const char* UART::ParityName[] ={"NONE","EVEN","ODD",0};
  const char* UART::StopBitName[]={"1","2",0};
  const char* UART::FlowName[]   ={"NONE","XON_XOFF","CTS_RTS",0};

  bool UART::fromNumber(unsigned in,Baudrate& out)
  {
   unsigned len=B_unknown;
   unsigned i0=0;
   unsigned i1=len; 
   while(true)
   {
    if ((i1-i0)<=1) 
        {
	 if (in==BpS[i0])
	    {
	     out=(Baudrate)i0;return true;
	    }
	 return false;
	}
    unsigned im=(i0+i1)/2;
    if (in<BpS[im]) i1=im;
       else         i0=im;
   }
  }

  UART::Baudrate UART::fromNumber(unsigned br)
  {
   Baudrate out=B_unknown;
   if (fromNumber(br,out)) return out;
   sys::msg.error()<<"unknown Baudrate '"<<br<<"'\n";
   return out;
  }
  
  ascii::Writer& UART::Config::show(ascii::Writer& out) const
  {
   static const char* WL[]={"5","6","7","8"};
   static const char* SB[]={"1","2"};
   static const char* P[]={"N","E","O"};
   out<<UART::BpS[br]<<" "<<WL[wl]<<P[0]<<SB[sb];
   return out;
  }
    
  UART::UART(const char name[],bool default_):
  sys::Device(name,dList,default_)
  {
  }
  
  UART& UART::init(Baudrate br,WordLen wl,StopBit sb,Flow flow)
  {
   cfg.br=br;
   cfg.wl=wl;
   cfg.sb=sb;
   cfg.fl=flow;
   init();
   return *this;
  }

  ascii::Writer& UART::info(ascii::Writer& out) const
  {
   out<<getName()<<": ";
   cfg.show(out)<<"\n";
   return out;
  }

  UART& UART::init(const Config& cfg)
  {
   this->cfg=cfg; //copy
   init();
   return *this;
  }

  unsigned UART::div(unsigned f0,unsigned f1)  //without any mul
  {
   unsigned s0=0;
   unsigned z0=0;
   while(true)
   {
    unsigned s1=s0+f0;
    unsigned z1=z0+1;
    unsigned df=f0;
    unsigned dz=1;
    while(s1<f1)
    {
     z0=z1;
     s0=s1;
     df+=df;
     dz+=dz;
     s1+=df;
     z1+=dz;
    }
    if (df==f0) return ((f1-s0)<(s1-f1))?z0:z1;
   }
  }
  
 }
}
