//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    Foobar is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    Foobar is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Foobar; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//------------------------------------------
//basic single byte oriented input/output
//(c) H.Buchmann FHSO 2001
//$Id: byte.cc 193 2006-01-11 15:21:16Z buchmann $
//------------------------------------------
#include "sys/sys.h"
IMPLEMENTATION(io_byte_byte,$Revision: 137 $)
#include "io/byte/byte.h"
namespace io
{
 namespace byte
 {
  Switch::Sw::Sw(Channel& src,Channel& dst,Listener* li,Listener::Source src_)
  :li(li),src_(src_),
   pSrc(src.setRX(this)),
   src(src),
   dst(dst)
  {
  }
  
  Switch::Sw::~Sw()
  {
   src.setRX(pSrc);
  }

  Switch::Switch(Channel& first,Channel& second)
  :first_second(first,second,0,Listener::FIRST),
   second_first(second,first,0,Listener::SECOND)
  {
  }

  Switch::Switch(Channel& first,Channel& second,Listener& li)
  :first_second(first,second,&li,Listener::FIRST),
   second_first(second,first,&li,Listener::SECOND)
  {
  }
  
  Switch::~Switch()
  {
  }
   
 } //namespace byte
} //namespace io
