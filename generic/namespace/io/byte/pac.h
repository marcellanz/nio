#ifndef IO_BYTE_PAC_H
#define IO_BYTE_PAC_H
//-------------------------
//pac the basic Packet
//(c) H.Buchmann FHNW 2006
//$Id$
//------------------------
INTERFACE(io_byte_pac,$Revision$)
#include "io/ascii/write.h"
#include "sys/msg.h"

namespace io
{
 namespace byte
 {
//-------------------------------------- Packet
  class Packet
  {
   public:
    virtual ~Packet(){}
//-------------------------------------- Packet::Listener
    struct Listener
    {
     virtual ~Listener();
     virtual void onPacket(Packet& pac)=0;
    };
//-------------------------------------- some packets
    class Empty; //len=0
    class Full;  //len==capacity
    unsigned capacity; 
    unsigned len;   //0<=len<=capacity
    unsigned char* data;
    
    io::ascii::Writer& show(io::ascii::Writer& out) const
    {
     return out.dump(data,len);
    }
    
    friend io::ascii::Writer& operator<<(io::ascii::Writer& out,
                                         const Packet& pac)
           {return pac.show(out);}
   private:
    Packet(unsigned char d[],unsigned capacity,unsigned len)
    :capacity(capacity)
    ,len(len)
    ,data(d)
    {
    }
  };
  
  struct Packet::Empty:public Packet
  {
   Empty(unsigned char d[],unsigned capacity)
   :Packet(d,capacity,0){}
   virtual ~Empty(){}
  };
  
  struct Packet::Full:public Packet
  {
   Full(unsigned char d[],unsigned capacity)
   :Packet(d,capacity,capacity){}
   virtual ~Full(){}
  };
 }//namespace byte
}//namespace io
#endif
