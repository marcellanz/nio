//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    Foobar is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    Foobar is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Foobar; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#ifndef IO_BYTE_BYTE_H
#define IO_BYTE_BYTE_H
INTERFACE(io_byte_byte,$Revision: 172 $)
//------------------------------------------
//basic single byte oriented input/output
//(c) H.Buchmann FHSO 2001
//$Id: byte.h 193 2006-01-11 15:21:16Z buchmann $
//------------------------------------------
namespace io
{
 namespace byte //character
 {
//------------------------------------------ InputListener
  class InputListener
  {
   public:
    virtual void onData(unsigned char d)=0;
  } __attribute__((deprecated));

//------------------------------------------ InputListener
  class RXListener
  {
   public:
    virtual void onRX(unsigned char d)=0;
  };

//------------------------------------------ Output
  class Output
  {
   public:
    virtual void put(unsigned char d)=0;
  };

//------------------------------------------ Writer
  class Writer
  {
   protected:
    Output& out;
             Writer(Output& out):out(out){}
	     Writer(const Writer& wr):out(wr.out){}
    virtual ~Writer(){}
  };

//------------------------------------------ Channel
  class Channel:public Output
  {
   protected:
    
   public:
  	     Channel(){}
    virtual ~Channel(){}
//    virtual void setListener(InputListener* li)=0;
//    void setListener(InputListener& li){setListener(&li);}
    virtual RXListener* setRX(RXListener* li)=0; //returns prev one
    RXListener* setRX(RXListener& li){return setRX(&li);}
  };

//------------------------------------------ Switch
//connects two channels
  class Switch
  {
   public:
    struct Listener
    {
     enum Source {FIRST,SECOND};
     virtual void onData(Source,unsigned char)=0;
    };
    
   private:
    struct Sw:public RXListener
    {
     Listener* li;
     Listener::Source src_;
     RXListener* pSrc; //prev of src
     Channel& src;
     Channel& dst;
     Sw(Channel& src,Channel& dst,Listener* li,Listener::Source src_);
     virtual ~Sw();
     void onRX(unsigned char ch){dst.put(ch);if (li) li->onData(src_,ch);}
    };
    
    Sw first_second;
    Sw second_first;

   public:
     Switch(Channel& first,Channel& second);
     Switch(Channel& first,Channel& second,Listener& li);
    ~Switch();
  };
 } //namespace byte
} //namespace io
#endif
