//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#ifndef IO_BYTE_UART_H
#define IO_BYTE_UART_H
INTERFACE(io_byte_uart,$Revision: 137 $)
//---------------------------------
//uart 
//(c) H.Buchmann FHSO 2002
//$Id: uart.h 193 2006-01-11 15:21:16Z buchmann $
//---------------------------------
#include "io/byte/byte.h"
#include "io/ascii/out.h"
#include "sys/device.h"
namespace io
{
 namespace byte
 {
//-------------------------------------------------- UART
  class UART:public byte::Channel,
               public sys::Device
  {
   public: 
//-------------------------------------------------- some enums 
    enum Baudrate { //only type we use underscore here 
                    //for not conflicting with #define's in unix stuff    
		   B_1200,  
		   B_2400,  B_4800,
                   B_9600,  B_19200, B_38400, B_57600, B_115200,
                   B_230400,B_460800,
		   B_unknown
                  };
    static const unsigned BpS[]; //bits per second corresponding 
                                 // to Baudrate
    enum WordLen {W_5,W_6,W_7,W_8};
    static const char* WordLenName[]; 
    
    enum Parity  {P_NONE,P_EVEN,P_ODD};
    static const char* ParityName[];
    
    enum StopBit {SB_1,SB_2};
    static const char* StopBitName[];
    
    enum Flow    {F_NONE,F_XON_XOFF,F_CTS_RTS};
    static const char* FlowName[];
//-------------------------------------------------- Config
    struct Config
    {
     Baudrate br;
     WordLen  wl;
     StopBit  sb;
     Flow     fl;
     ascii::Writer& show(ascii::Writer& out) const; //format br wl par sb example 9600 8N1
    };

   private:
    UART(const UART&);
    UART& operator=(const UART&);
    static sys::Device::List dList;

   protected:
    UART(const char name[],bool default_); 
    Config cfg;
    virtual void init()=0; //config already set
    
//convenience method 
//used for for example calculating baudrate counter 
    static unsigned div(unsigned f0,unsigned f1);
           //f0<f1 
           //without any mul f0<f1
           //return z:  f0*z=f1

   public:
    static bool fromNumber(unsigned in,Baudrate& out);
                                 //returns true iff valid result in ouit 
    static Baudrate fromNumber(unsigned br);
                                 //terminates program if wrong value
    				 
    static unsigned getNbr(){return dList.getNbr();}
    static UART& get(unsigned id){return (UART&)dList.get(id);}
    static UART& get(const char name[]){return (UART&)dList.get(name);}
    static UART& get(){return (UART&)dList.get();}
    static bool exists(const char name[]){return dList.exists(name);}
//parity not yet done    
    UART& init(Baudrate br,
                 WordLen wl,
		 StopBit sb,
		 Flow flow);
    UART& init(Baudrate br){return init(br,W_8,SB_1,F_NONE);}
    UART& init(const Config& cfg);
    
    const Config& getConfig()const{return cfg;}
    ascii::Writer& info(ascii::Writer& out) const;
//debug
    virtual void debug(){} //overwrie this for debugging   
  };
 } //namespace byte
} //namespace io

#endif
