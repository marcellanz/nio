//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    Foobar is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    Foobar is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Foobar; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//------------------------------------------
//logging
//(c) H.Buchmann FHSO 2001
//$Id: log.cc 193 2006-01-11 15:21:16Z buchmann $
//------------------------------------------
#include "sys/sys.h"
IMPLEMENTATION(io_byte_log,$Revision: 137 $)
#include "io/byte/log.h"
#include "sys/msg.h"
namespace io
{
 namespace byte
 {
  
  Logger::Logger(Output& out,unsigned capacity)
  :log(out),
   buffer(new unsigned char[capacity]),
   capacity(capacity),
   len(0)
  {
  }
  
  Logger::~Logger()
  {
   write();
   delete [] buffer;
  } 

  void Logger::write()
  {
   static const char* SRC[]={"FIRST:","SECND:"};
   if (len!=0)
      {
       log<<SRC[src]<<len<<"\n";
       log.dump((char*)buffer,len);
       len=0;
      }
  }    

  void Logger::onData(Switch::Listener::Source src,unsigned char d)
  {
   if (len==0)
      {
       this->src=src;
       buffer[len++]=d;
       return;
      }
//len>0
   if (len>=capacity)
      {
       write();
       buffer[len++]=d;
       return;
      }
//0<len<capacity      
   if (this->src!=src) write();
   this->src=src;
   buffer[len++]=d;             
  }
 } //namespace byte
} //namespace io
