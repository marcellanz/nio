//------------------------
//usb general stuff
//(c) H.Buchmann FHSO 2004
//$Id: usb-0.cc 216 2006-02-05 12:56:16Z buchmann $
//------------------------
#include "sys/sys.h"
IMPLEMENTATION(io_usb_0,$Revision: 1.3 $)
#include "io/usb/usb-0.h"
#include "sys/msg.h"

namespace io
{
 namespace usb
 {
//------------------------------------------ USB
  USB* USB::usb=0;
  Device::Listener::Factory USB::root;
  
//------------------------------------------ USB::Listener
  USB::Listener::~Listener()
  {
  }

  USB::USB()
  {
   usb=this;
  }
  
  USB::~USB()
  {
   usb=0;
  }
    
  USB& USB::get()
  {
   if (usb==0) sys::msg.error()<<"no USB support available\n";
   usb->install();
   return *usb;      
  } 
  
  io::ascii::Writer& USB::show(io::ascii::Writer& out)
  {
   root.show(0,out);
   return out;
  }

  void USB::install()
  {
   struct Listener:public USB::Listener
   {
    bool onDevice(Device& d)
    {
     root.install(d);
     return false;
    }
   };
   Listener li;   
   enumerate(li);
  }

//------------------------------------------ Descriptor
 Desc::Factory::FMap Desc::Factory::factories;
 
  template<typename T>
  io::ascii::Writer& POD0::show(io::ascii::Writer& out)const
  {
   out<<"--------------------"<<T::Name
      <<":"<<T::POD::Type
      <<"\n"
        "bLength             "<<io::ascii::dec()<<bLength        <<"\n"
        "bDescriptorType     "<<io::ascii::dec()<<bDescriptorType<<"\n";
   return out;
  }
  
  Descriptor::POD::POD()
  {
   __builtin_memset(this,0,sizeof(*this));
  }

//------------------------------------------ Descriptor
  const char Descriptor::Name[]="Descriptor";

  Descriptor::Descriptor()
  :configuration(0)
  {
  };
  
  io::ascii::Writer& Descriptor::POD::show(io::ascii::Writer& out)const
  {
   POD0::show<Descriptor>(out)
   <<
    "bcdUSB              "<<io::ascii::hex()<<bcdUSB            <<"\n"
    "bDeviceClass        "<<io::ascii::dec()<<bDeviceClass      <<"\n"
    "bDeviceSubClass     "<<io::ascii::dec()<<bDeviceSubClass   <<"\n"
    "bDeviceProtocol     "<<io::ascii::dec()<<bDeviceProtocol   <<"\n"
    "bMbMaxPacketSize0   "<<io::ascii::dec()<<bMaxPacketSize0   <<"\n"
    "idVendor            "<<io::ascii::hex()<<idVendor          <<"\n"
    "idProduct           "<<io::ascii::hex()<<idProduct         <<"\n"
    "bcdDevice           "<<io::ascii::hex()<<bcdDevice         <<"\n"
    "iManufacturer       "<<io::ascii::hex()<<iManufacturer     <<"\n"
    "iProduct            "<<io::ascii::hex()<<iProduct          <<"\n"
    "iSerialNumber       "<<io::ascii::hex()<<iSerialNumber     <<"\n"
    "bNumConfigurations  "<<io::ascii::dec()<<bNumConfigurations<<"\n";
   return out;
  }

  io::ascii::Writer& Descriptor::show(io::ascii::Writer& out)const
  {
   pod.show(out);
   for(unsigned i=0;i<pod.bNumConfigurations;i++)
   {
    configuration[i]->show(out);
   }
   return out;
  }

//------------------------------------------ Desc::Reader
  Desc::Reader::Reader(unsigned char buffer[],unsigned len)
  :len(len)
  ,buffer(buffer)
  ,currentLen(len)
  ,current(buffer)
  {
//   sys::msg<<"Reader "<<len<<"\n";
  }
  
  Desc::Reader::~Reader()
  {
  }
  
  unsigned char* Desc::Reader::get(unsigned size)
  {
//   sys::msg<<"get "<<currentLen<<" "<<size<<"\n";
   if (currentLen<size) return 0;
   unsigned char* b=current;
   current+=size;
   currentLen-=size;
   return b;
  }

  unsigned Desc::Reader::getLen()const
  {
   return len;
  }

  unsigned char* Desc::Reader::getBuffer() const
  {
   return buffer;
  }
  bool Desc::Reader::empty()const
  {
   return currentLen==0;
  }
//------------------------------------------ Desc::Factory
  Desc::Factory::Factory(unsigned type)
  :type(type)
  {
   sys::msg<<"Factory type= "<<type<<"\n";
   factories[type]=this;
  }
  
  Desc::Factory::~Factory()
  {
  }

//------------------------------------------ Desc
  Desc::Desc(POD0* pod0)
  :pod0(pod0)
  {
  }  
  
  Desc::~Desc()
  {
  }
  
  Desc* Desc::create(Reader& rd)
  {
   POD0* pod0=rd.get<POD0>();
   Factory::FMap::iterator f
       =Factory::factories.find(pod0->bDescriptorType);
   return (f==Factory::factories.end())?new Generic(rd,pod0)
                                       :f->second->create(rd,pod0);    
  }
//------------------------------------------ Configuration
  const char Configuration::Name[]="Configuration";
  Configuration::Configuration(Desc::Reader& rd)
  :bufferLen(rd.getLen())
  ,buffer(rd.getBuffer())
  ,pod(rd.get<POD>())
  ,ifc(0)
  {
   ifc=new Ifc*[pod->bNumInterfaces];
   for(unsigned i=0;i<pod->bNumInterfaces;i++)
   {
    POD0* pod0=rd.get<POD0>();
    ifc[i]=new Ifc(rd,pod0);
   }
  }
  
  Configuration::~Configuration()
  {
   for(unsigned i=0;pod->bNumInterfaces;i++) delete ifc[i];
   delete [] ifc;
   delete [] buffer;
  }
  
  io::ascii::Writer& Configuration::POD::show(io::ascii::Writer& out)const
  {
   POD0::show<Configuration>(out)
   <<
    "wTotalLength        "<<io::ascii::dec()<<wTotalLength       <<"\n"
    "bNumInterfaces      "<<io::ascii::dec()<<bNumInterfaces     <<"\n"
    "bConfigurationValue "<<io::ascii::dec()<<bConfigurationValue<<"\n"
    "iConfiguration      "<<io::ascii::dec()<<iConfiguration     <<"\n"
    "bmAttributes        "<<io::ascii::hex()<<bmAttributes       <<"\n"
    "maxPower            "<<io::ascii::dec()<<maxPower           <<"\n";
   return out;
  }

  io::ascii::Writer& Configuration::show(io::ascii::Writer& out)const
  {
   out.indent(out.getIndent()+Desc::Indent);
   pod->show(out);
   for(unsigned i=0;i<pod->bNumInterfaces;i++)
   {
    ifc[i]->show(out);
   }
   out.indent(out.getIndent()-Desc::Indent);
   return out;
  }

//------------------------------------------ Ifc::Factory
  template<> Desc::FactoryT<Ifc> Desc::FactoryT<Ifc>::fact(1);

//------------------------------------------ Ifc

  const char Ifc::Name[]="Ifc";
  Ifc::Ifc(Desc::Reader& rd,POD0* pod0)
  :Desc(pod0)
  ,pod(rd.get<POD>())
  {
   while(!rd.empty())
   {
    desc.push_back(Desc::create(rd));
   }
  }
  
  Ifc::~Ifc()
  {
   for(unsigned i=0;i<desc.size();i++) delete desc[i];
  }
  
  io::ascii::Writer& Ifc::POD::show(io::ascii::Writer& out)const
  {
   out<<
    "bInterfaceNumber    "<<io::ascii::dec()<<bInterfaceNumber  <<"\n"
    "bAlternateSettings  "<<io::ascii::dec()<<bAlternateSettings<<"\n"
    "bNumEndpoints       "<<io::ascii::dec()<<bNumEndpoints     <<"\n"
    "bInterfaceClass     "<<io::ascii::dec()<<bInterfaceClass   <<"\n"
    "bInterfaceSubClass  "<<io::ascii::dec()<<bInterfaceSubClass<<"\n"
    "bInterfaceProtocol  "<<io::ascii::dec()<<bInterfaceProtocol<<"\n"
    "iInterface          "<<io::ascii::dec()<<iInterface        <<"\n";
   return out;
  }

  io::ascii::Writer& Ifc::show(io::ascii::Writer& out)const
  {
   out.indent(out.getIndent()+Desc::Indent);
   pod0->show<Ifc>(out);
   pod->show(out);
   for(unsigned i=0;i<desc.size();i++)
   {
    desc[i]->show(out);
   }
   out.indent(out.getIndent()-Desc::Indent);
   return out;
  }

//------------------------------------------ EndP::Factory
  template<> Desc::FactoryT<EndP> Desc::FactoryT<EndP>::fact(0);

//------------------------------------------ EndP
  const char EndP::Name[]="EndP";
  EndP::EndP(Desc::Reader& rd,POD0* pod0)
  :Desc(pod0)
  ,pod(rd.get<POD>())
  {
  }
  
  io::ascii::Writer& EndP::POD::show(io::ascii::Writer& out)const
  {
   static const char Dir[]="OI";
   static const char* Type[]={"Cntrl","Iso","Bulk","Int"};
   out<<
    "bEndpointAddress    "<<(bEndpointAddress&0xf)
    			  <<Dir[0x1&(bEndpointAddress>>7)]
                          <<"\n"
    "bmAttributes        "<<io::ascii::hex()<<bmAttributes    
                          <<" "<<Type[bmAttributes&0x3]<<"\n"
    "wMaxPacketSize      "<<io::ascii::dec()<<wMaxPacketSize  <<"\n"
    "bInterval           "<<io::ascii::dec()<<bInterval       <<"\n";
   return out;
  }

  io::ascii::Writer& EndP::show(io::ascii::Writer& out)const
  {
   out.indent(out.getIndent()+Desc::Indent);
   pod0->show<EndP>(out);
   pod->show(out);
   out.indent(out.getIndent()-Desc::Indent);
   return out;
  }

  bool EndP::is(Direction d)const
  {
   static const unsigned char Dir[]={
                                     0x00, //FROM_TD,
				     0x00, //OUT
				     0x80  //IN
                                    };
   return (0x80& pod->bEndpointAddress)==Dir[d];
  }
  
  bool EndP::is(TransType type) const
  {
   return (0x03&pod->bmAttributes)==type;
  }
  
//------------------------------------------ HID
  template<> Desc::FactoryT<HID> Desc::FactoryT<HID>::fact(0);
  
  const char HID::Name[]="HID";
  
  HID::HID(Desc::Reader& rd,POD0* pod0)
  :Desc(pod0)
  ,pod(rd.get<POD>())
  {
  }

  io::ascii::Writer& HID::POD::show(io::ascii::Writer& out) const
  {
   out<<
    "bcdHID              "<<io::ascii::hex()<<bcdHID           <<"\n"
    "bCountryCode        "<<io::ascii::dec()<<bCountryCode     <<"\n"
    "bNumDescriptors     "<<io::ascii::dec()<<bNumDescriptors  <<"\n"
    "bDescriptorType     "<<io::ascii::dec()<<bDescriptorType_ <<"\n"
    "wDescriptorLength   "<<io::ascii::dec()<<wDescriptorLength<<"\n";
   return out; 
  };

  io::ascii::Writer& HID::show(io::ascii::Writer& out)const
  {
   out.indent(out.getIndent()+Desc::Indent);
   pod0->show<HID>(out);
   pod->show(out);
   out.indent(out.getIndent()-Desc::Indent);
   return out;
  }
  
//------------------------------------------ Generic
  template<> Desc::FactoryT<Generic> Desc::FactoryT<Generic>::fact(0);  

  
  const char Generic::Name[]="Generic";
  Generic::Generic(Desc::Reader& rd,POD0* pod0)
  :Desc(pod0)
  ,pod((POD*)rd.get(pod0->bLength-sizeof(POD0)))
  {
  }
  
  Generic::~Generic()
  {
  }

  io::ascii::Writer& Generic::show(io::ascii::Writer& out)const
  {
   pod0->show<Generic>(out);
   out.dump(pod->data,pod0->bLength);
   return out;
  }

//------------------------------------------ Control::Listener
  Control::Listener::~Listener()
  {
  }
  
//------------------------------------------ Control
  Control::~Control()
  {
  }

//------------------------------------------ Bulk
  Bulk::Bulk(EndP& endP)
  :packetSize(endP.pod->wMaxPacketSize)
  ,eP((Endpoint)(0x0f&endP.pod->bEndpointAddress))
  {
  }

  Bulk::~Bulk()
  {
  }


//------------------------------------------ Bulk::Input
  Bulk::Input::Input(EndP& ep)
  :Bulk(ep)
  {
  }
  
  Bulk::Input::~Input()
  {
  }

//------------------------------------------ Bulk::Input
  Bulk::Output::Output(EndP& ep)
  :Bulk(ep)
  {
  }
  
  Bulk::Output::~Output()
  {
  }

//------------------------------------------ Device::Bulk
  Device::Bulk::~Bulk()
  {
  }
  
//------------------------------------------ Device::Listener::Factory
  Device::Listener::Factory::Factory()
  :name("/")
  ,parent(0)
  {
  }
  
  Device::Listener::Factory::Factory(const char name[],Factory& parent)
  :name(name)
  ,parent(&parent)
  {
   parent.children.push_back(this);
  }

  Device::Listener::Factory::~Factory()
  {
  }
  
  void Device::Listener::Factory::show(unsigned indent,
                                       io::ascii::Writer& out) const 
  {
   out.blank(indent)<<name<<"\n";
   indent++;
   for(unsigned i=0;i<children.size();i++)
   {
    children[i]->show(indent,out);
   }
  }

  void Device::Listener::Factory::install(Device& d)
  {
   for(unsigned i=0;i<children.size();i++)
   {
    Factory* f=children[i];
    if (f->match(d)) 
       {
        if (d.li) return; //listener assigned
	f->install(d);
       }
   }
  }
  
//------------------------------------------ Device::Listener
  Device::Listener::~Listener()
  {
  }
  
//------------------------------------------ Device
  Device::Device()
  :li(0)
  ,ifc(0)
  {
  }
  
  Device::~Device()
  {
  }
  
  Device::Listener* Device::assign(Listener& li)
  {
   Listener* l=this->li;
   this->li=&li;
   this->li->onAssign(this);
   return l;
  }
  
 }//namespace usb
}//namespace io

