//--------------------------
//test
//(c) H.Buchmann FHNW 2006
//$Id: test.cc 209 2006-01-24 13:49:16Z buchmann $
//--------------------------
#include "sys/sys.h"
IMPLEMENTATION(io_usb_test,$Revision$)
#include "io/usb/usb-0.h"
#include "sys/msg.h"

namespace io
{
 namespace usb
 {
#if 1 
  class Tester:public Device::Listener::Factory,
               public Device::Listener
  {
   static Tester tester;
   Device* device;
   Request::POD request;
   char buffer[255];
   Tester();
   bool match(io::usb::Device&);
   void onAssign(Device* dev);
   void onRequest(const Request::POD& req,unsigned char d[]);
   void onClaim(const Ifc* ifc);
   void onWrite(Device::Transfer&);
   void onRead(Device::Transfer&);
   
  };
  
  Tester Tester::tester;
  
  bool Tester::match(io::usb::Device& dev) 
  {
   const Descriptor& d=dev.getDescriptor();
   if ((d.pod.bDeviceClass)||(d.pod.bDeviceSubClass)) return false;
   const Ifc::POD* ifc=d.configuration[0]->ifc[0]->pod;
   if ((ifc->bInterfaceClass!=8)
        ||
       (ifc->bInterfaceSubClass!=5)
        ||
       (ifc->bInterfaceProtocol!=80)) return false;
//   dev.show(sys::msg);
   dev.assign(*this);    
   return true;
  }
  
  void Tester::onAssign(Device* dev)
  {
   device=dev;
   dev->show(sys::msg);
   device->claimIfc(0);
  }
  
  void Tester::onRequest(const Request::POD& req,unsigned char d[])
  {
   sys::msg<<"onRequest\n";
   sys::msg.dump((char*)d,req.wLength);
  }
  
  void Tester::onClaim(const Ifc* ifc)
  {
   sys::msg<<"onClaim\n";
   request.bmRequestType=0x80;
   request.bRequest     =0x06;
   request.wValue       =(0x03<<8)|1; //index
   request.wIndex       =0x409;
   request.wLength      =sizeof(buffer);
   device->request(request,(unsigned char*)buffer);


   return;
   
   static unsigned char cmd[]=
     {
      'U','S','B','C', //Signature
      0,0,0,0,         //tag
      0,0,0,0,         //transfer length
      0,               //flags
      0,               //lun
      1,               //length
      0,0,0,0, 
      0,0,0,0, 
      0,0,0,0, 
      0,0,0,0 
     };
   Device::Transfer t;
   t.ep=EP_1;
   t.capacity=sizeof(cmd);
   t.len     =sizeof(cmd);
   t.data    =cmd;
   device->bulkWrite(t);
  }
  
  void Tester::onWrite(Device::Transfer&)
  {
   static unsigned char data[256];
   sys::msg<<"onWrite\n";
   Device::Transfer t={EP_2,sizeof(data),0,data};
   device->bulkRead(t);
  }
  
  void Tester::onRead(Device::Transfer& t)
  {
   sys::msg<<"onRead\n";
   sys::msg.dump(t.data,t.len);
  }
  
  Tester::Tester()
  :Device::Listener::Factory("test",USB::root)
  ,device(0)
  {
   sys::msg<<"USB-Test\n";
//   device->claimIfc(0);
//   sys::msg<<"----------- end\n";
  }
#endif  
 }//namespace usb
}//namespace io

