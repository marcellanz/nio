#ifndef IO_USB_HID_H
#define IO_USB_HID_H
//------------------------
//usb Human Interface Device
//(c) H.Buchmann FHSO 2004
//$Id: hid.h 193 2006-01-11 15:21:16Z buchmann $
//------------------------
INTERFACE(io_usb_hid,$Revision: 1.1 $)
#include "io/ascii/write.h"
#include "io/usb/usb-0.h"

namespace io
{
 namespace usb
 {
 //------------------------------------------ HID
  struct HID
  {
   static const Byte Type=33;
   Byte   bLength;
   Byte   bDescriptorType;
   BCD    bcdHID;
   Byte	  bCountryCode;
   Byte   bNumDescriptors;
   Byte	  bDescriptorType_;
   Word	  wDescriptorLength;   
   io::ascii::Writer& show(io::ascii::Writer&)const;
  } __attribute__((packed));
  
  class Report
  {
   public:
    static const Byte Type = 34;
   private:
    unsigned len;
    unsigned char* data;

   public:
             Report(unsigned char data[],unsigned len);
    virtual ~Report();
    io::ascii::Writer& show(io::ascii::Writer&)const;
  };
 }//namespace usb
}//namespace io
#endif
