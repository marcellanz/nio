#ifndef IO_USB_STORAGE_FACTORY_H
#define IO_USB_STORAGE_FACTORY_H
//-----------------------------
//storage factory
//(c) H.Buchmann FHNW 2006
//$Id$
//-----------------------------
INTERFACE(io_usb_storage_factory,$Revision$)
#include "io/usb/usb-0.h"
namespace io
{
 namespace usb
 {
  namespace storage
  {
   class Factory:public Device::Listener::Factory
   {
    private:
     Factory();
     bool match(Device& dev);
    
    public:
     static Factory factory;
   };
  }//namespace storage
 }//namespace usb
}//namespace io
#endif
