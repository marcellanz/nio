//-----------------------------
//storage factory
//(c) H.Buchmann FHNW 2006
//$Id$
//-----------------------------
#include "sys/sys.h"
IMPLEMENTATION(io_usb_storage_factory,$Revision$)
#include "io/usb/storage/factory.h"

namespace io
{
 namespace usb
 {
  namespace storage
  {
   Factory Factory::factory;
   
   Factory::Factory()
   :Device::Listener::Factory("storage",USB::root)
   {
   }

   bool Factory::match(Device& dev)
   {
    return dev.getDescriptor().
               configuration[0]->ifc[0]->pod->bInterfaceClass==8;
   }
   
  }//namespace storage
 }//namespace usb
}//namespace io
