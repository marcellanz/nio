//--------------------------
//test
//(c) H.Buchmann FHNW 2006
//$Id$
//--------------------------
#include "sys/sys.h"
IMPLEMENTATION(io_usb_storage_bulk_test,$Revision$)
#include "sys/msg.h"
#include "io/usb/usb-0.h"
#include "io/fs/media.h"
#include "io/byte/pac.h"
#include "sys/thread.h"

namespace io
{
 namespace usb
 {
  namespace storage
  {
   namespace bulk
   {
    class Tester:public fs::Media::Listener
                ,public byte::Packet::Listener
    {
     static Tester tester;
     static const unsigned SIZE=512;
     unsigned char data[SIZE];
     byte::Packet::Empty pac;
     Tester();
     void onMedia(fs::Media& m);
     void onPacket(byte::Packet& pac);
    };
    
    Tester Tester::tester;
    
    Tester::Tester()
    :pac(data,SIZE)
    {
#if 0
     pac.capacity=SIZE;
     pac.len     =SIZE;
     pac.data    = data;
#endif     
//     sys::msg<<"USB-Storage-bulk\n";
//     USB::show(sys::msg);
     sys::Thread::wait();
    }
    
    void Tester::onPacket(byte::Packet& pac)
    {
     sys::msg<<"onPacket\n";
     sys::msg.dump((char*)pac.data,pac.len);
    }
    
    void Tester::onMedia(fs::Media& m)
    {
     sys::msg<<"onMedia\n";
     m.read(0,pac,*this);
    }
   }//namespace bulk
  }//namespace storage
 }//namespace usb
}//namespace io

