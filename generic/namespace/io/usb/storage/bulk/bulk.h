#ifndef IO_USB_STORAGE_BULK_H
#define IO_USB_STORAGE_BULK_H
//-------------------------
//bulk
//see: Universal Serial Bus
//     Mass Storage Class
//     Bulk-Only Transport
//(c) H.Buchmann FHNW 2006
//$Id: bulk.h 221 2006-03-11 17:57:03Z buchmann $
//-------------------------
INTERFACE(io_usb_storage_bulk,$Revision$)
#include "io/ascii/write.h"
#include "io/usb/usb-0.h"
#include "io/byte/pac.h"
#include "io/fs/media.h"
#include "io/fs/ifc.h"
namespace io
{
 namespace usb
 {
  namespace storage
  {
   namespace bulk
   {
//------------------------------------------ Factory
    class Factory:public Device::Listener::Factory
    {
     static Factory factory;
     Factory();
     bool match(Device& dev);
    };

    class Bulk:public Device::Listener
              ,public Control::Listener
	      ,public fs::Media
    {
     friend class bulk::Factory;
     private:
      typedef void (Bulk::*Action)();

//------------------------------------------ CBW
      struct CBW:public byte::Packet::Full //Command Block Wrapper&
                ,public fs::Ifc::Cmd	
      {
       static const unsigned CMD_SIZE=16;
       static const char Signature[];
       enum Direction {OUT,IN};
       struct POD
       {
	char          signature[4];
	unsigned      tag;
	unsigned      transferLen;
	unsigned char flags;
	unsigned char lun;
	unsigned char len;
	unsigned char cmd[CMD_SIZE];
	io::ascii::Writer& show(io::ascii::Writer&) const;
       }__attribute__((packed));
       POD pod;
       CBW();
      };

//------------------------------------------ CSW
      struct CSW:public byte::Packet::Full //Command Status Wrapper
      {
       static const char Signature[];
       struct POD
       {
	char          signature[4];
	unsigned      tag;
	unsigned      residue;
	unsigned char status;
	io::ascii::Writer& show(io::ascii::Writer&) const;
       }__attribute__((packed));
       POD pod;
       CSW();
      };
      class Transfer;

//------------------------------------------ MaxLUN
      struct MaxLUN:public Control::POD
      {
       unsigned char max;
       MaxLUN();
      }__attribute__((packed));

      static Bulk* anchor;
      static const unsigned SUPP_IFC=7; 
      static fs::Ifc::Factory* ifcFactory[SUPP_IFC];
      
      Bulk*         next;
      Device*       dev;
      Action        action;
      MaxLUN        maxLun;
      Control*      control;
      usb::Bulk::Input*  in;
      usb::Bulk::Output* out;
      fs::Ifc*      ifc; //the current
      
      Bulk();
      static Control::POD Reset;
   
//implementation Listener
      void onAssign(Device* dev);
      void onControl(Control::POD& ctl);
//------------------ actions
      void getMaxLun();
      void configure();
      void ready();
            
     public: 
//implementation fs::Media
      void read(Pos pos,
        	byte::Packet& p,
		byte::Packet::Listener& li);
      void write(Pos pos,
                 byte::Packet& p,
		 byte::Packet::Listener& li);
      
      virtual ~Bulk();
    };

//------------------------------------------ Bulk::Transfer
    struct Bulk::Transfer:public byte::Packet::Listener
    {
     struct Configuration;
     struct Output;
     struct Input;

     Bulk& bulk;
     fs::Media::Pos pos;
     CBW cbw;
     byte::Packet& pac;
     CSW csw;
     byte::Packet::Listener* li;
     bool sot(byte::Packet& pac){return &pac==&cbw;}
         //start of transmission     
     bool eot(byte::Packet& pac){return &pac==&csw;}
         //end of transmission     
     Transfer(Bulk&,
              fs::Media::Pos pos,
	      byte::Packet& pac,
	      byte::Packet::Listener* li
	     );
     virtual ~Transfer();
    };


//------------------------------------------ Bulk::Transfer::Configuration
    struct Bulk::Transfer::Configuration:public Transfer
    {
     void onPacket(byte::Packet& pac);
     Configuration(Bulk&);
     ~Configuration();
    };

//------------------------------------------ Bulk::Transfer::Output
    struct Bulk::Transfer::Output:public Transfer
    {
     Output(Bulk&,
            fs::Media::Pos pos,
	    byte::Packet& pac,
	    byte::Packet::Listener& li);
     ~Output();
    };

//------------------------------------------ Bulk::Transfer::Input
    struct Bulk::Transfer::Input:public Transfer
    {
     void onPacket(byte::Packet& pac);
     Input(Bulk&,
           fs::Media::Pos pos,
	   byte::Packet& pac,
	   byte::Packet::Listener& li);
     ~Input();
    };
   }//namespace bulk
  }//namespace storage
 }//namespace usb
}//namespace io
#endif
