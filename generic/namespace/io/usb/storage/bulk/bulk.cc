//-------------------------
//bulk
//see: Universal Serial Bus
//     Mass Storage Class
//     Bulk-Only Transport
//(c) H.Buchmann FHNW 2006
//$Id: bulk.cc 221 2006-03-11 17:57:03Z buchmann $
//TODO: use tag field in CBW/CSW as pointer
//-------------------------
#include "sys/sys.h"
IMPLEMENTATION(io_usb_storage_bulk,$Revision$)
#include "io/usb/storage/bulk/bulk.h"
#include "io/usb/storage/factory.h"
#include "io/fs/atapi/ifc.h"
#include "io/fs/scsi/ifc.h"

#include "sys/msg.h"

namespace io
{
 namespace usb
 {
  namespace storage
  {
   namespace bulk
   {
//------------------------------------------ Bulk::CBW
    const char Bulk::CBW::Signature[]="USBC";

    io::ascii::Writer& Bulk::CBW::POD::show(io::ascii::Writer& out) const
    {
     out.put(signature,sizeof(signature))
         <<" "<<io::ascii::hex()<<tag
	 <<" "<<io::ascii::hex()<<flags
	 <<" "<<io::ascii::setw(7)<<transferLen
	 <<" l";
	 out.hex(lun)
	 <<" " <<io::ascii::setw(3)<<len<<": ";
     for(unsigned i=0;i<len;i++) out.hex(cmd[i])<<" ";
     return out;	 
    }
    
    Bulk::CBW::CBW()
    :byte::Packet::Full((unsigned char*)&pod,sizeof(POD))
    ,fs::Ifc::Cmd(CMD_SIZE,pod.cmd)
    {
     static const POD Default={
                               {'U','S','B','C'},
			       0, //tag
			       0, //transferLen
			       0, //flags
			       0, //lun
			       1, //len
			       {0,0,0,0, 0,0,0,0,
			        0,0,0,0, 0,0,0,0}
                              };
     __builtin_memcpy(&pod,&Default,sizeof(Default));
    };

//------------------------------------------ Bulk::CSW
    const char Bulk::CSW::Signature[]={'U','S','B','S'};
    
    io::ascii::Writer& Bulk::CSW::POD::show(io::ascii::Writer& out) const
    {
     out.put(signature,sizeof(signature))
	 <<" "<<io::ascii::hex()<<tag
	 <<" "<<io::ascii::hex()<<status
	 <<" "<<io::ascii::setw(7)<<residue;
     return out;
    }

    Bulk::CSW::CSW()
    :byte::Packet::Full((unsigned char*)&pod,sizeof(POD))
    {
    }
    
    Bulk::MaxLUN::MaxLUN()
    {
     bmRequestType=0xa1;
     bRequest     =0xfe;
     wValue       =0;
     wIndex       =0;
     wLength      =1;
     max          =0;    
    }
//------------------------------------------ Bulk
    Control::POD Bulk::Reset =
                   {
                    0x21, //Type
		    0xff,
		    0,
		    0,
		    0
                   };
    
    Bulk* Bulk::anchor=0;
    
    fs::Ifc::Factory* Bulk::ifcFactory[SUPP_IFC]=
                    {
		     &fs::Ifc::getFactory(),//0: not supp
		     &fs::Ifc::getFactory(),//1: not supp
		     &fs::Ifc::getFactory(),//2: not supp
		     &fs::Ifc::getFactory(),//3: not supp
		     &fs::Ifc::getFactory(),//4: not supp
		     &fs::atapi::Ifc::getFactory(), //5: 
		     &fs::scsi::Ifc::getFactory(),  //6: 
		    };    
    Bulk::Bulk()
    :next(anchor)
    ,dev(0)
    ,action(0)
    ,control(0)
    ,in(0)
    ,out(0)
    ,ifc(0)
    {
     anchor=this;
    }
    
    Bulk::~Bulk()
    {
     if (control) delete control;
     if (ifc)     delete ifc;
    }

    void Bulk::onAssign(Device* dev)
    {
     this->dev=dev;
     control=dev->control();
     dev->claimIfc(0);
     unsigned ifcCode=dev->ifc->pod->bInterfaceSubClass;
     ifc=(ifcCode<SUPP_IFC)?ifcFactory[ifcCode]->create()
                           :0;
     if (ifc==0)
        {
	 sys::msg.error()<<"ifc  "<<ifcCode<<" not supported\n";
	}			   
     out=dev->bulk().output(*(EndP*)(dev->ifc->desc[1]));
     if (out==0) 
        {
	 sys::msg<<"no output\n";
	}
     in =dev->bulk().input (*(EndP*)(dev->ifc->desc[0]));
     if (in==0) 
        {
	 sys::msg<<"no input\n";
	}
     action=&Bulk::getMaxLun;
     control->transfer(Reset,*this);
    }

    void Bulk::getMaxLun()
    {
     sys::msg<<"getMaxLun\n";
     action=&Bulk::configure;
     control->transfer(maxLun,*this);
    }

    void Bulk::configure()
    {
     sys::msg<<"maxLun= "<<maxLun.max<<"\n";
     action=0;
     new Transfer::Configuration(*this);
    }
    
    void Bulk::ready()
    {
     action=0;
    }
    
    void Bulk::onControl(Control::POD& ctl)
    {
     sys::msg<<"onControl\n";
     if (action) (this->*action)();
    }
    
//------------------------------------------ implementation fd::Media
    void Bulk::read(Pos pos,
        	    byte::Packet& p,
		    byte::Packet::Listener& li)
    {
     new Transfer::Input(*this,
                         pos,
			 p,
			 li);
    }
    		    
    void Bulk::write(Pos pos,
        	     byte::Packet& p,
		     byte::Packet::Listener& li)
    {
    }
    
//------------------------------------------ Factory
    Factory Factory::factory;
       
    Factory::Factory()
    :Device::Listener::Factory("bulk",storage::Factory::factory)
    {
    }

    bool Factory::match(Device& d)
    {
     if (d.getDescriptor()
          .configuration[0]->ifc[0]->pod->bInterfaceProtocol==80)
	{
	 d.getDescriptor().configuration[0]->ifc[0]->show(sys::msg);
	 d.assign(*new Bulk);
	 return true; 
	}
     return false;	
    }
    
//------------------------------------------ Bulk::Transfer
    Bulk::Transfer::Transfer(
            Bulk& blk,
            fs::Media::Pos pos,
	    byte::Packet& pac,
	    byte::Packet::Listener* li)
    :bulk(blk)
    ,pos(pos)
    ,pac(pac)
    ,li(li)
    {
    }

    Bulk::Transfer::~Transfer()
    {
    }	   
//------------------------------------------ Bulk::Transfer::Configuration
    Bulk::Transfer::Configuration::Configuration(
              Bulk& blk
                                                )
    :Transfer(blk,0,blk.ifc->getConfig(),0)
    {
     cbw.pod.tag=2;
     cbw.pod.transferLen=pac.capacity;
     cbw.pod.lun=1;
     cbw.pod.flags=0;//0x80; //input
     cbw.pod.len=blk.ifc->configure(1,cbw);
     bulk.out->transfer(cbw,*this);
     bulk.in ->transfer(pac,*this);
     bulk.in ->transfer(csw,*this);
    }
 
    Bulk::Transfer::Configuration::~Configuration()
    {
    }
    
    void Bulk::Transfer::Configuration::onPacket(byte::Packet& pac)
    {
     sys::msg<<"Configuration::onPacket\n"
             <<pac<<"\n";
#if 1
     if (eot(pac))
        {
	 bulk.ifc->setup(this->pac);
         bulk.isReady();
	 delete this;
	 return;
	}
#endif
    }
       					
//------------------------------------------ Bulk::Transfer::Input
    Bulk::Transfer::Input::Input(
           Bulk& blk,
           fs::Media::Pos pos,
	   byte::Packet& pac,
	   byte::Packet::Listener& li)
    :Transfer(blk,pos,pac,&li)
    {
     cbw.pod.tag=3;
     cbw.pod.transferLen=pac.capacity;
     cbw.pod.lun=0;
     cbw.pod.flags=0x80; //input
     cbw.pod.len=blk.ifc->read(0,pos,pac.capacity,cbw);
     bulk.out->transfer(cbw,*this);
     
     pac.len=pac.capacity;
     unsigned char* data=pac.data;     
     unsigned pLen=bulk.in->getPacketSize();
     unsigned len=pac.capacity;
     while(len)
     {
      unsigned l=(len<=pLen)?len:pLen;
      byte::Packet* p=new byte::Packet::Empty(data,l);
      data+=l;
      len-=l;
      bulk.in ->transfer(*p,*this);
     }
     
     bulk.in ->transfer(csw,*this);
    }
    
    Bulk::Transfer::Input::~Input()
    {
    }

    void Bulk::Transfer::Input::onPacket(byte::Packet& pac)
    {
//     sys::msg<<"Input::onPacket\n";
//     sys::msg.dump(pac.data,pac.len);
     if (sot(pac)) return;
     if (eot(pac))
        {
	 if (li) li->onPacket(this->pac);
	 delete this;
	 return;
	}
//     sys::msg<<"delete pac\n";	
     delete &pac;	
    }


//------------------------------------------ Bulk::Transfer::Output
    Bulk::Transfer::Output::Output(
           Bulk& blk,
           fs::Media::Pos pos,
	   byte::Packet& pac,
	   byte::Packet::Listener& li)
    :Transfer(blk,pos,pac,&li)   
    {
    }
    
    Bulk::Transfer::Output::~Output()
    {
    }
   }//namespace bulk
  }//namespace storage
 }//namespace usb
}//namespace io
