#ifndef IO_USB_USB_0
#define IO_USB_USB_0
//------------------------
//usb general stuff
//(c) H.Buchmann FHSO 2004
//$Id: usb-0.h 219 2006-02-06 16:26:14Z buchmann $
//TODO parse descriptors
//POD: Plain Old Data
//------------------------
INTERFACE(io_usb_0,$Revision: 1.1 $)
#include "io/ascii/write.h"
#include <vector>
#include <map>
#include "sys/msg.h"
#include "io/byte/pac.h"

namespace io
{
 namespace usb
 {
  typedef unsigned char  Address;
  static const           Address AddressMask=(1<<7)-1;
  typedef unsigned char  Byte;
  typedef unsigned char  Bitmap;
  typedef unsigned short Word;
  typedef unsigned short BCD;
  typedef unsigned short ID;
  typedef unsigned char  Index;


  enum Endpoint  {EP_0,EP_1, EP_2, EP_3, EP_4, EP_5, EP_6, EP_7, 
                  EP_8,EP_9,EP_10,EP_11,EP_12,EP_13,EP_14,EP_15,
		  EP_Mask=0xf};

  enum Direction {FROM_TD,OUT,IN};

  enum Speed     {FULL,LOW};
 


//------------------------------------------ POD0
//the start of all POD's
//sometimes 'is a' 
//sometimes 'has a'
  struct POD0
  {
   Byte   bLength;
   Byte   bDescriptorType;
   template<typename T>
   io::ascii::Writer& show(io::ascii::Writer&)const;
  }__attribute__((packed));

//------------------------------------------ Descriptor
  struct Configuration;
  struct Descriptor
  {
   static const char Name[];
   struct POD:public POD0 
   {
    static const Byte Type=1;
    BCD    bcdUSB;
    Byte   bDeviceClass;
    Byte   bDeviceSubClass;
    Byte   bDeviceProtocol;
    Byte   bMaxPacketSize0;
    ID     idVendor;
    ID     idProduct;
    BCD    bcdDevice;
    Index  iManufacturer;
    Index  iProduct;
    Index  iSerialNumber;
    Byte   bNumConfigurations;
    io::ascii::Writer& show(io::ascii::Writer&)const;
    POD();
   } __attribute__((packed));
   POD pod;
   Configuration** configuration;
   Descriptor();
   io::ascii::Writer& show(io::ascii::Writer&)const;
  };

//------------------------------------------ Desc
  struct Desc
  {
//------------------------------------------ Desc::Reader
   class Reader
   {
    private:
     const unsigned len;
     unsigned char* const buffer; 
     unsigned currentLen;
     unsigned char* current;

    public:
      Reader(unsigned char buffer[],unsigned len);
     ~Reader();
     unsigned char* get(unsigned size);
     unsigned getLen()const;
     unsigned char* getBuffer()const;
     bool empty() const; 
     unsigned char* getRest() const;
     template<typename T>
     T* get(){return (T*)get(sizeof(T));}
   };
//------------------------------------------ Desc::Factory
   class Factory
   {
    private:
     friend class Desc;
     typedef std::map<unsigned,Desc::Factory*,std::less<unsigned> > FMap;
     static FMap factories;
     friend class USB;
     unsigned type;
     
    public:
              Factory(unsigned type);
     virtual ~Factory();
     virtual Desc* create(Reader& rd,POD0* pod0)=0;     
   };
   
//------------------------------------------ Desc::FactoryT
   template<typename T>
   class FactoryT:public Desc::Factory
   {
    private:
     static FactoryT fact;
    public:
     FactoryT(int id) //see template-static
     :Desc::Factory(T::POD::Type)
            {sys::msg<<"Factory "<<T::Name<<"\n";}
     ~FactoryT(){}
     Desc* create(Reader& rd,POD0* pod0){return new T(rd,pod0);}
   };
   
   static const unsigned Indent=2;
   
   POD0* pod0;
            Desc(POD0* pod0);
   virtual ~Desc();
   virtual io::ascii::Writer& show(io::ascii::Writer&) const=0;
   bool operator==(unsigned type)const
       {return pod0->bDescriptorType==type;}
   static Desc* create(Reader& rd);
  };

//------------------------------------------ Configuration
  struct Ifc;
  struct Configuration
  {
   static const char Name[];
   
//TODO 8 byte POD that it can be read with an 8Byte control pipe   

   struct POD:public POD0
   {
    static const Byte Type=2;
    Word   wTotalLength;
    Byte   bNumInterfaces;
    Byte   bConfigurationValue;
    Index  iConfiguration;
    Bitmap bmAttributes;
    Byte   maxPower;
    io::ascii::Writer& show(io::ascii::Writer&)const;
   } __attribute__((packed));
   
   unsigned bufferLen;
   unsigned char* buffer; //ower of this buffer 

   POD*  pod;
   Ifc** ifc;

    Configuration(Desc::Reader& rd);
   ~Configuration();
   io::ascii::Writer& show(io::ascii::Writer&)const;
  };

//------------------------------------------ EndP
  struct EndP:public Desc
  {
   static const char Name[];
   enum TransType {CONTROL,ISO,BULK,INTERRUPT};
   
   struct POD
   {
    static const Byte Type=5;
    Byte   bEndpointAddress;
    Bitmap bmAttributes;
    Word   wMaxPacketSize;
    Byte   bInterval;
    io::ascii::Writer& show(io::ascii::Writer&)const;
   } __attribute__((packed));
   
   POD* pod;
   EndP(Desc::Reader& rd,POD0* pod0);
   io::ascii::Writer& show(io::ascii::Writer&)const;
   bool is(Direction d)const;
   bool is(TransType type) const;
  };

//------------------------------------------ Ifc
  struct Generic;
  struct Ifc:public Desc
  {
   static const char Name[];
   
   struct POD
   {
    static const Byte Type=4;
    Byte   bInterfaceNumber;
    Byte   bAlternateSettings;
    Byte   bNumEndpoints;
    Byte   bInterfaceClass;
    Byte   bInterfaceSubClass;
    Byte   bInterfaceProtocol;
    Index  iInterface;
    io::ascii::Writer& show(io::ascii::Writer&)const;
   }__attribute__((packed));

   POD*  pod;
   std::vector<Desc*> desc;
    Ifc(Desc::Reader& rd,POD0* pod0);
   ~Ifc();
   io::ascii::Writer& show(io::ascii::Writer&)const;
  } ;

//------------------------------------------ HID
  struct HID:public Desc
  {
   static const char Name[];
   
   struct POD
   {
    static const Byte Type=33;
    BCD    bcdHID;
    Byte   bCountryCode;
    Byte   bNumDescriptors;
    Byte   bDescriptorType_;
    Word   wDescriptorLength;   
    io::ascii::Writer& show(io::ascii::Writer&)const;
   } __attribute__((packed));
   
   POD* pod;
   HID(Desc::Reader& rd,POD0* pod0);
   io::ascii::Writer& show(io::ascii::Writer&)const;
  };
  
//------------------------------------------ Generic
  struct Generic:public Desc
  {
   static const char Name[];
   struct POD
   {
    static const Byte Type=255;
    unsigned char data[0];
   }__attribute__((packed));
   
   POD* pod;
    Generic(Desc::Reader& rd,POD0* pod0);
   ~Generic();
   io::ascii::Writer& show(io::ascii::Writer&)const;
  };

//------------------------------------------ Control
  class Control
  {
   public:
    struct POD
    {
     Bitmap bmRequestType;
     Byte   bRequest;
     Word   wValue;
     Word   wIndex;
     Word   wLength;
     Byte   data[0];
    } __attribute__((packed));

    struct POD_:public POD
    {
     unsigned char t;
    }__attribute__((packed));
    
    struct Listener
    {
     virtual ~Listener();
     virtual void onControl(POD&)=0; 
    };


   virtual ~Control(); 
   virtual void transfer(POD&,Listener&)=0; 
    //POD:wLength set
    //   :data contains the data
  };
  
//------------------------------------------ Bulk
  class Bulk
  {
   private:
    unsigned packetSize;
    
   public:
    class Input;
    class Output;

   protected:
    Bulk(EndP& endP);
    Endpoint eP;
    
   public:
    virtual ~Bulk(); 
    virtual void transfer(byte::Packet& pac,byte::Packet::Listener&)=0;
    unsigned getPacketSize()const{return packetSize;}
  };
  
//------------------------------------------ Bulk::Input
  class Bulk::Input:public Bulk
  {
   protected:
    Input(EndP& endP);
    
   public:
    virtual ~Input();
  };

//------------------------------------------ Bulk::Output
  class Bulk::Output:public Bulk
  {
   protected:
    Output(EndP& endP);
    
   public:
    virtual ~Output();
  };
  
//------------------------------------------ Device
  class Device
  {
   public:
//------------------------------------------ Device::Listener
    class Listener
    {
     friend class Device;
      
     public:
//------------------------------------------ Device::Listener::Factory
      class Factory
      {
       friend class USB;
       private:
        const char*const name;
	Factory(const Factory&);
        Factory* parent;
	std::vector<Factory*> children;
	void install(Device&);
	void show(unsigned indent,io::ascii::Writer& out) const;
	Factory(); //used for root
	
       protected:
                 Factory(const char name[],Factory& parent);
	virtual ~Factory();
	virtual bool match(Device& dev){return false;}

      };
      virtual ~Listener();
      virtual void onAssign(Device*)=0;
    };

//------------------------------------------ Device::Bulk
    struct Bulk //the factory
    {
     virtual ~Bulk();
     virtual usb::Bulk::Input*  input(EndP& endP)=0;
     virtual usb::Bulk::Output* output(EndP& endP)=0;
    };
    
   protected:
    Descriptor descriptor; //shall be filled in subclass of Device
    Listener* li;
    Device();

   public:
    Ifc* ifc; //the claimed one
    virtual ~Device();
    Listener* assign(Listener& li); //return the previous

//be sure that Device is assigned			 
    virtual void claimIfc(int ifc)=0;
    virtual Bulk& bulk()=0;
    virtual Control* control()=0; //create one
    io::ascii::Writer& show(io::ascii::Writer& out)const
                      {return descriptor.show(out);}
    const Descriptor& getDescriptor()const{return descriptor;}
  };

//------------------------------------------ USB
  class USB
  {
   public:
//------------------------------------------ USB::Listener
    struct Listener
    {
     virtual ~Listener();
     virtual bool onDevice(Device&)=0;
    //return true if enumeration shall stop    
    };

   private:
    static USB* usb;
    
   protected:
    USB();
    void install();
   
   public:
    static Device::Listener::Factory root;
    virtual ~USB();
    virtual void enumerate(Listener& li)=0;
    static USB& get();
    static io::ascii::Writer& show(io::ascii::Writer& out);
  };

 }//namespace usb
}//namespace io
#endif
