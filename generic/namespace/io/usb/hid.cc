//------------------------
//usb Human Interface Device
//(c) H.Buchmann FHSO 2004
//$Id: hid.cc 193 2006-01-11 15:21:16Z buchmann $
//------------------------
#include "sys/sys.h"
IMPLEMENTATION(io_usb_hid,$Revision: 1.1 $)
#include "io/ascii/write.h"
#include "io/usb/hid.h"

namespace io
{
 namespace usb
 {
  io::ascii::Writer& HID::show(io::ascii::Writer& out) const
  {
   out<<
    "bLength             "<<io::ascii::dec()<<bLength          <<"\n"
    "bDescriptorType     "<<io::ascii::dec()<<bDescriptorType  <<"\n"
    "bcdHID              "<<io::ascii::hex()<<bcdHID           <<"\n"
    "bCountryCode        "<<io::ascii::dec()<<bCountryCode     <<"\n"
    "bNumDescriptors     "<<io::ascii::dec()<<bNumDescriptors  <<"\n"
    "bDescriptorType     "<<io::ascii::dec()<<bDescriptorType_ <<"\n"
    "wDescriptorLength   "<<io::ascii::dec()<<wDescriptorLength<<"\n";
   return out; 
  };
  
  Report::Report(unsigned char data[],unsigned len)
  :len(len),
   data(data)
  {
  }
  
  Report::~Report()
  {
  }
  
  io::ascii::Writer& Report::show(io::ascii::Writer& out)const
  {
   static const unsigned SIZE[]={0,1,2,4};
   static const char*    Type[]={"main",
                                 "glob",
				 "locl",
				 " res"};
   
   out.dump(data,len);
   unsigned i=0;
   while(i<len)
   {
    unsigned p=i;
    unsigned char prefix=data[i++];
    unsigned char siz=SIZE[prefix&0x3];
    unsigned char typ=0x3&(prefix>>2);
    unsigned char tag=0xf&(prefix>>4);
    if ((typ==3)&&(tag=0xf))
       {
        siz=data[i++];
	tag=data[i++];
       }
    out<<io::ascii::hex()<<p<<" "
            <<siz<<" "<<Type[typ]
            <<" "<<io::ascii::hex()<<tag<<": ";
    out.dump(data+i,siz); 
    i+=siz;
   }
   return out;
  }
  
 }//namespace usb
}//namespace io
