//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    Foobar is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    Foobar is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Foobar; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//---------------------------------
//progress-bar
//(c) H.Buchmann FHSO 2004
//$Id: progress-bar.cc 193 2006-01-11 15:21:16Z buchmann $
//---------------------------------
#include "sys/sys.h"
IMPLEMENTATION(io_ascii_progress_bar,$Revision: 137 $)
#include "io/ascii/progress-bar.h"
//#define PROGRESS_BAR_TEST
//---------------------------------
//principle bresenham
//for the calculation
//u=capacity/len = c/l 
//u_i   0<=i<len   u_i= i*u
//e_i   error      -u/2 < e_i <u/2
//step d
// first 0<d<u               
// a_i= e_(i-1)+d              <--> a_i * l = err+d*l
// -(1/2)*u < a_i < (3/2)*u    <--> -(1/2)*c < a_i*l < (3/2)*c 
// if a_i > (1/2)*u                 a_i*l> (1/2)*c
//    {
//     u_i=u_(i-1)+u	       <--> inc unit		
//     e_i=a_i-u               <--> e_i*l=a_i*l-c
//    }
//    else
//     e_i=a_i                 <--> e_i*l=a_i*l
//--------------------------------------
//format
//  xxx% |
//    ^   ^
//    |   |-------------P_START                                               |   
//    |-----------------P_POS

#ifdef PROGRESS_BAR_TEST
#include "sys/msg.h"
#endif

namespace io
{
 namespace ascii
 {
  ProgressBar::Bresenham::Bresenham(unsigned cap,unsigned len,unsigned val0)
  :c(cap),l(len),err(-c),val(val0)
  {
  }

  bool ProgressBar::Bresenham::step(unsigned d)
  {
   unsigned change=0;
   int dl=d*l;
   while(dl>=c) 
   {
    dl-=c;
    val++;
    change++;
   }
   err+=2*dl;
   if (err>0)
      {
       val++;
       change++;
       err-=2*c;
      }
   return change>0;   
  }

  ProgressBar::ProgressBar(io::ascii::Writer& out,
               unsigned len,          //in chars
	       unsigned capacity)
  :out(out),
   front(BAR_START),
   bar(capacity,len,BAR_START),
   percent(capacity,100,0),
   sum(0),capacity(capacity)
  {
   init();
  }

  ProgressBar::ProgressBar(io::ascii::Writer& out,
	       unsigned capacity)
  :out(out),
   front(BAR_START),
   bar(capacity,DEFAULT_LEN,BAR_START),
   percent(capacity,100,0),
   sum(0),capacity(capacity)
  {
   init();
  }

  ProgressBar::~ProgressBar()
  {
   out<<"\n";
  }

  void ProgressBar::init()
  {
   for(unsigned i=0;i<MAX_BAR_SIZE;i++) theBar[i]=' ';
   theBar[P_POS-1]='0';
   theBar[P_POS]='%';
   theBar[BAR_START-1]='[';
   theBar[BAR_START]='>';
   theBar[BAR_START+bar.l]=']';
   theBar[BAR_START+bar.l+1]='\0';
   out<<theBar;
  }

  void ProgressBar::wrPercent()
  {
   static const char Digits[]="0123456789";
   unsigned i=0;
   unsigned v=percent.val;
   unsigned bi=P_POS;
   theBar[bi]='%';
   do
   {
    theBar[--bi]=Digits[v%10];
    v/=10;
   }while(v>0);
   while(bi>0)
   {
    theBar[--bi]=' ';
   }
  }


  void ProgressBar::put(unsigned d)
  {
   sum+=d;
   if (sum>capacity) return; //at upper limit
   unsigned update=0;
   if (percent.step(d)) {wrPercent();update++;}
   if (bar.step(d))
      {
       while (front<bar.val) theBar[front++]='=';
       theBar[front]='>';
       theBar[BAR_START+bar.l]=']';
      }
   if (update) out<<"\n\e[A"<<theBar;
  }
 }//namespace ascii 
}//namespace io

#ifdef PROGRESS_BAR_TEST
//-------------------------------------------- Test
class Tester
{
 static Tester tester;
 static const unsigned LEN      =  5;
 static const unsigned CAPACITY = 13;
 Tester();
};

Tester Tester::tester;

Tester::Tester()
{
 io::ascii::ProgressBar pb(sys::msg,CAPACITY);
 for(unsigned i=0;i<CAPACITY;i++)
 {
  pb.put(1);
 }
 sys::msg<<"\n";
}

#endif

