//
//    ascii Writer
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//---------------------------
//out
//(c) H.Buchmann FHSO 2003
//$Id: write.cc 222 2006-03-17 12:58:28Z buchmann $
//---------------------------
#include "sys/sys.h"
IMPLEMENTATION(io_ascii_write,$Revision: 189 $)
#include "io/ascii/write.h"
#define LONG_LONG

//#define IO_ASCII_WRITE_TEST

#ifdef IO_ASCII_WRITE_TEST
//#include "sys/deb.h"
#endif
//#include <stdio.h>

//#include "sys/deb.h"

namespace io
{
 namespace ascii
 {
//------------------------------------------------- Writer
                             // 0123456789abcdef 
  const char Writer::Digits_[]="fedcba9876543210123456789abcdef";
                             //                ^
			     //                |---- entry
			     //see val%10 and val/10 if val<0
  const char* Writer::Digits=Digits_+15;			      
                                       //12345678|
  const char Writer::DefaultTabString[]="        |";
  const char Writer::Fill=' ';

//------------------------------------------------- StringFormatter
  const char* Writer::LeftAlign::put(const char s[]) const
  {
   return wr.putX_(s);
  }

  const char* Writer::LeftAlign::put(const char s[],unsigned len) const
  {
   return wr.putX_(s,len);
  }

  const char* Writer::RightAlign::put(const char s[]) const
  {
   return wr.put_X(s);
  }

  const char* Writer::RightAlign::put(const char s[],unsigned len) const
  {
   return wr.put_X(s,len);
  }
  
//------------------------------------------------- Dec.Formatter
  template<typename typ>Writer& Writer::Dec::putU(typ val) const
  {
   //strln(s) =round(8*sizeof(typ)*log(2)+1) 
   //        ~ (8*sizeof(typ)*3)/10+1
   //          
   char s[(8*sizeof(typ)*3)/10+2];
   unsigned l=0;
   do
   {
    s[l++]=Digits[val%10];
    val/=10;
   }while(val);
   return wr.putReverse(s,l);
  }

  template<typename typ>Writer& Writer::Dec::putS(typ val) const
  {
   char s[(8*sizeof(typ)*3)/10+4]; //plus sign -/+
   bool neg=val<0;
   unsigned l=0;
   do
   {
    s[l++]=Digits[val%10];
    val/=10;
   }while(val);
   if (neg) s[l++]='-';
   return wr.putReverse(s,l);
  }
  
  Writer& Writer::Dec::put(unsigned char val) const
  {
   return putU<unsigned>((unsigned)val);
  }
  
  Writer& Writer::Dec::put(unsigned val) const
  {
   return putU<unsigned>(val);
  }

  Writer& Writer::Dec::put(unsigned short val) const
  {
   return putU<unsigned short>(val);
  }

  Writer& Writer::Dec::put(unsigned long long val) const
  {
#ifdef LONG_LONG  
   return putU<unsigned long long>(val);
#else
   return wr.putReverse("gnol gnol",9);  //long long
#endif
   
  }
  
  Writer& Writer::Dec::put(int val) const
  {
   return putS<int>(val);
  }
  
  Writer& Writer::Dec::put(int long long val) const
  {
#ifdef LONG_LONG
   return putS<int long long>(val);
#else
   return wr.putReverse("gnol gnol",9);  //long long
#endif   
  }
  
//------------------------------------------------- Hex.Formatter
  template<typename typ>
  unsigned Writer::Hex::put(char s[],typ val) const
  {
   unsigned l=0;
   typ mask=1;
   while(mask)
   {
    s[l++]=Digits[val&0xf];
    val>>=4;
    mask<<=4;
   }
   return l;
  }
  
  template<typename typ>
  Writer& Writer::Hex::put(typ val) const
  {
   char s[2*sizeof(typ)+4];
        //^             ^--- 0x + spare ev terminating zer
	//|------------------ 2 nibbles per byte
   unsigned l=put<typ>(s,val);
   s[l++]='x';s[l++]='0';
   return wr.putReverse(s,l);
  }
 
  unsigned Writer::Hex::put(char s[],unsigned short val) const
  {
   return put<unsigned short>(s,val);
  }
   
  unsigned Writer::Hex::put(char s[],unsigned val) const
  {
   return put<unsigned>(s,val);
  }

  Writer& Writer::Hex::put(unsigned char val) const
  {
   return put<unsigned char>(val);
  }
    
  Writer& Writer::Hex::put(unsigned val) const
  {
   return put<unsigned>(val);
  }

  Writer& Writer::Hex::put(unsigned short val) const
  {
   return put<unsigned short>(val);
  }

  Writer& Writer::Hex::put(unsigned long long val) const
  {
   return put<unsigned long long>(val);
  }
  
//------------------------------------------------- Default
 void Writer::DefaultEoL::onEoL(Output& out)
 {
 }  

//------------------------------------------------- Writer
  Writer::Writer(Output& out)
  :out(out)
  ,chN(0)
  ,width(0)
  ,indentN(0)
  ,decF(*this)
  ,hexF(*this)
  ,leftAlign(*this)
  ,rightAlign(*this)
  ,cNumberF(&decF),cStringF(&leftAlign)
  ,eol(&defEoL)
  {
  }

  Writer::Writer(Output& out,EoLListener& eol)
  :out(out)
  ,chN(0)
  ,width(0)
  ,indentN(0)
  ,decF(*this)
  ,hexF(*this)
  ,leftAlign(*this)
  ,rightAlign(*this)
  ,cNumberF(&decF),cStringF(&leftAlign)
  ,eol(&defEoL)
  {
  }
  
  Writer::Writer(Output& out,const char tabString[])
  :out(out)
  ,chN(0)
  ,width(0)
  ,indentN(0)
  ,decF(*this)
  ,hexF(*this)
  ,leftAlign(*this)
  ,rightAlign(*this)
  ,cNumberF(&decF),cStringF(&leftAlign)
  ,eol(&defEoL)
  {
  }

  Writer::Writer(Output& out,EoLListener& eol,const char tabString[])
  :out(out)
  ,chN(0)
  ,width(0)
  ,indentN(0)
  ,decF(*this)
  ,hexF(*this)
  ,leftAlign(*this)
  ,rightAlign(*this)
  ,cNumberF(&decF),cStringF(&leftAlign)
  ,eol(&defEoL)
  {
  }
  
  Writer& Writer::newLine()
  {
   out.put('\n');
   eol->onEoL(out);
   width=0;
   chN=0;
   cStringF=&leftAlign;
   cNumberF=&decF;
   return *this;
  }

  Writer& Writer::putReverse(const char s[],unsigned len)
  {           // ____01234
              //         1
	      //         
   while(width>len) putChar(Fill);
   while(len>0) putChar(s[--len]);
   cNumberF=&decF;
   return *this;
  }


  const char* Writer::putX_(const char s[])  //xxxxxx_____
  {
   unsigned i=0;
   while(true)
   {
    char ch=s[i++];
    switch(ch)
    {
     case '\0':
      while(width>0) putChar(Fill);
     return 0;
     case '\n':
      newLine();
     return s+i;
    }
    putChar(ch);
   }
  }

  const char* Writer::putX_(const char s[],unsigned len)
  {
   for(unsigned i=0;i<len;i++)
   {
    char ch=s[i];
    if (ch=='\n') 
       {
        newLine();
	return s+i+1;
       }
    putChar(ch);   
   }
   while(width>0) putChar(Fill);
   return 0;
  }

  void Writer::put_XLine(const char s[],unsigned len)
  {
   while(width>len) putChar(Fill);
   for(unsigned i=0;i<len;i++) putChar(s[i]);
  }

  const char* Writer::put_X(const char s[],unsigned len)
  {         //_______xxxx
   while(width>len) putChar(Fill);
   for(unsigned i=0;i<len;i++)
   {
    char ch=s[i];
    switch(ch)
    {
     case '\n':
      put_XLine(s,i);
      newLine();
     return s+i+1;

     case '\t':
      put_XLine(s,i);
     return s+i;
     default:
      putChar(ch);
     break;
    }
   }
   return 0;
  }

  const char* Writer::put_X(const char s[])  //_______xxxx
  {
   if (width==0) return putX_(s); 
   unsigned i=0;
   while(true)
   {
    char ch=s[i];
    switch(ch)
    {
     case '\0':
      put_X(s,i);
     return 0;
     
     case '\n':
      put_XLine(s,i);
      newLine();
     return s+i+1;
     
     case '\t':
      put_XLine(s,i);
     return s+i;
    }
    i++;
   }
  }

  Writer& Writer::putChar(char ch)
  {
   if (chN==0)
      {
       for(unsigned i=0;i<indentN;i++) out.put(' ');
      }
   out.put(ch);
   if (width>0) width--;
   chN++;
   return *this;
  }
  
  Writer& Writer::put(char ch)
  {
   switch(ch)
   {
    case '\n':return newLine();
   }
   return putChar(ch);
  }

  Writer& Writer::put(const char s[])
  {
   if (s==0) return put("(null)"); //recursive call
   while(s&&s[0])
   {
    s=cStringF->put(s);
   }
   return *this;
  }

  Writer& Writer::put(const char s[],unsigned len)
  {
   if (s==0) return put("(null)"); //recursive call
   while(s[0])
   {
    const char* s1=cStringF->put(s,len);
    if (s1==0) break;
    len=len-(s1-s); //rest 
    s=s1;
   }
   return *this;
  }
  
  Writer& Writer::setw(int w)
  {
   if (w>=0) 
      { 
       width=w;
       cStringF=&leftAlign;
      }
      else
      {
       width=-w;
       cStringF=&rightAlign;
      }
   return *this;   
  }
  
  Writer& Writer::indent(unsigned w)
  {
   indentN=w;
   return *this;
  }
  
  template<typename typ> Writer& Writer::putAsHex(typ val)
  {
   char s[2*sizeof(typ)+2];
   unsigned l=hexF.put(s,val);
   putReverse(s,l);
   return *this;
  }

  Writer& Writer::hex(unsigned val)
  {
   return putAsHex<unsigned>(val);
  }
  
  Writer& Writer::hex(unsigned short val)
  {
   return putAsHex<unsigned short>(val);
  }

  Writer& Writer::hex(unsigned char val)
  {
   return putAsHex<unsigned char>(val);
  }

  template<typename typ>Writer& Writer::putNumber(typ val)
  {
   cNumberF->put(val);
   return *this;
  }

  Writer& Writer::put(unsigned char val)
  {
   return putNumber<unsigned char>(val);
  }

  Writer& Writer::put(unsigned val)
  {
   return putNumber<unsigned>(val);
  }

  Writer& Writer::put(unsigned short val)
  {return putNumber<unsigned short>(val);}
  
  Writer& Writer::put(int val)
  {return putNumber<int>(val);}
  
  Writer& Writer::put(unsigned long long val)
  {return putNumber<unsigned long long>(val);}

  Writer& Writer::put(int long long val)
  {return putNumber<int long long>(val);}
  
  Writer& Writer::put(const void* val)
  {
                         //0x12345678
   if (val==0) putReverse("  )llun(  ",10); //(null)
      else hexF.put((unsigned)val);
   return *this;   
  }

  Writer& Writer::put(double val)
  {
#warning not yet implemented
   put("not yet implemented");
   return *this; 
  }

  Writer& Writer::put(bool val)
  {
   static const char* Bool[]={"false","true "};
   return put(Bool[val]);
  }

  Writer& Writer::rep(unsigned cnt,const char s[])
  {
   while(cnt>0){put(s);cnt--;}
   return *this;
  }

  Writer::HexLine::HexLine(Writer& out,const unsigned perLine)
  :out(out),
   perLine(perLine<?MAX_LEN),
   lineIdx(0),
   offset(0)
  {
  }

  Writer::HexLine::~HexLine()
  {
   show();
  }
    
  void Writer::HexLine::put(char ch)
  {
   if (lineIdx==perLine) show();
   line[lineIdx++]=ch;      
  }
  
  void Writer::HexLine::show()
  {
   out.hex(offset)<<": ";
   unsigned i=0;
   for(;i<lineIdx;i++)
   {
    unsigned ch=line[i];
    if (i==8) out.put(' ');	
    out.put(Digits[(ch>>4)&0xf]).
        put(Digits[(ch   )&0xf]).
	put(' ');
    offset++;
   }
   for(;i<perLine;i++) out.put("   ");
   out.put("  ");
   for(unsigned i=0;i<lineIdx;i++)
   {
    char ch=line[i];
    out.put( ((' '<=ch)&&(ch<='~'))?ch:'.');
   };
   out.put('\n');
   lineIdx=0;
  }
  
  Writer& Writer::dump(const char d[],unsigned len,unsigned perLine)
  {
   HexLine line(*this,perLine);
   for(unsigned i=0;i<len;i++) line.put(d[i]);
   return *this;
  }

  template<typename typ,unsigned MAX_LINE,unsigned sLen> 
  Writer& Writer::dump(const typ d[],unsigned len,unsigned perLine)
  {
   unsigned offset=0;
   perLine=perLine<?MAX_LINE;
   unsigned cnt=perLine;
   hex(offset)<<": ";
   for(unsigned i=0;i<len;i++)
   {
    if (cnt==0) 
       {
        cnt=perLine;
	put('\n');
	hex(offset)<<": ";
       }
    char s[sLen];
    if (cnt==8) put(' ');
    unsigned l=hexF.template put<typ>(s,d[i]);
    while(l) put(s[--l]);
    put(' ');
    cnt--;
    offset++;
   }
   put('\n');
   return *this;
  }
  
  Writer& Writer::dump(const unsigned char d[],unsigned len,unsigned perLine)
  {
   return dump<unsigned char,16,4>(d,len,perLine);
  }

  Writer& Writer::dump(const unsigned      d[],unsigned len,unsigned perLine)
  {
   return dump<unsigned,8,34>(d,len,perLine);
  }  
  
  Writer& Writer::dump(const unsigned short d[],unsigned len,unsigned perLine)
  {
   return dump<unsigned short,8,17>(d,len,perLine);
  }

  Writer& Writer::dump(unsigned val,const BitDesc bitd[])
  {
   static const char Bool[]="01";
   unsigned i=0;
   while(true)
   {
    const BitDesc& bd=bitd[i++];
    if (bd.name==0) break;
    put(bd.name);
    put(Bool[0x1&(val>>bd.pos)]);
    put('\n');
   }
   return *this; 
  }
  
  void Writer::debug()
  {
//   fprintf(stderr,"\npos0= %d pos1= %d tPos= %d\n",
//                   pos0,    pos1,    tPos);
  }


#ifdef IO_ASCII_WRITE_TEST
  class Tester:public Output
  {
   static Tester tester;
   Writer out;
   void put(char ch){sys::deb::out(ch);}
   Tester();
  };
  
  Tester Tester::tester;
   
  Tester::Tester()
  :out(*this)
  {
   out<<"012345678901234567890123456789\n"
      <<setw(-10)<<"ABC"<<setw(-10)<<"jj"
	<<"\n";
  }
#endif
 }//namespace ascii
}//namespace io
