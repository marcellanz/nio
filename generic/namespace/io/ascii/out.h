//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#ifndef IO_ASCII_OUT_H
#define IO_ASCII_OUT_H
//---------------------------
//out
//(c) H.Buchmann FHSO 2003
//$Id: out.h 193 2006-01-11 15:21:16Z buchmann $
//---------------------------
INTERFACE(io_ascii_out,$Revision: 137 $)
namespace io
{
 namespace ascii
 {
 
//------------------------------------------------- Output
  struct Output
  {
   virtual ~Output();
   virtual void put(char ch)=0;
   virtual void put(const char s[]); 
           //overwrite this if you have a better solution   
  };
  
//------------------------------------------------- Entaber
//replaces sequence of whitespace with tab char if appropriate
  class Entaber:public Output
  {
   private:
    Output& layer;
    const unsigned tab;
    unsigned pos;         //|----->|
                          //t      pos
    unsigned blankN;      //the number of consecutive blanks
 
   public:
             Entaber(Output& layer,unsigned tab=8);
    virtual ~Entaber();
    void put(char ch);
  };
  
 }
}
#endif
