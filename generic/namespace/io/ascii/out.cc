//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//---------------------------
//out
//(c) H.Buchmann FHSO 2003
//$Id: out.cc 193 2006-01-11 15:21:16Z buchmann $
//---------------------------
#include "sys/sys.h"
IMPLEMENTATION(io_ascii_out,$Revision: 137 $)
#include "io/ascii/out.h"
#define LONG_LONG
//#define IO_ASCII_OUT_TEST
//#include "sys/deb.h"

namespace io
{
 namespace ascii
 {
//------------------------------------------------- Output
  Output::~Output()
  {
  }
  
  void Output::put(const char s[])
  {
   unsigned i=0;
   while(true)
   {
    char ch=s[i++];
    if (ch=='\0') break;
    put(ch);
   }
  }
  
//------------------------------------------------- Entaber
   Entaber::Entaber(Output& layer,unsigned tab)
   :layer(layer),
    tab(tab),
    pos(0),
    blankN(0)
   {
   }
   
   Entaber::~Entaber()
   {
   }
   
   void Entaber::put(char ch)
   {
    if (ch=='\n')
       {
        pos=0;
	blankN=0;
	layer.put('\n');
	return;
       }
    if (ch=='\t')
       {
        layer.put('\t');
	pos=0;
	blankN=0;
	return;
       }   
    if (pos==tab)
       {
        if (blankN>0) layer.put('\t');
	blankN=0;
	pos=0;
       }
    pos++;
    if (ch==' ')
       {
        blankN++;
       }
       else
       {
        while(blankN>0){layer.put(' ');blankN--;}
        blankN=0;
	layer.put(ch);
       }
   }

#ifdef IO_ASCII_OUT_TEST
  class Tester:public Output
  {
   static Tester tester;
   Entaber out;
   void put(char ch) {sys::deb::out(ch);}
   Tester();
  }; 
  
  Tester Tester::tester;
  
  Tester::Tester()
  :out(*this)
  {
  //               0123456701234567012345670123456701234567
  //               |       |       |       |
   sys::deb::out(  "0123456701234567012345670123456701234567\n"
                   "|       |       |       |       |\n");
   out.Output::put("xxxx     Hello\n");
  }
#endif  
 }//namespace ascii
}//namespace io
