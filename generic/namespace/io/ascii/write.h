#ifndef IO_ASCII_WRITE_H
#define IO_ASCII_WRITE_H
//
//    ascii write
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//$Id: write.h 193 2006-01-11 15:21:16Z buchmann $
//---------------------------
INTERFACE(io_ascii_write,$Revision: 189 $)
#include "io/ascii/out.h"
namespace io
{
 namespace ascii
 {
  class Writer
  {
   public:
//------------------------------------------------- EOLListener
//what to do in case of eol
    struct EoLListener
    {
     virtual ~EoLListener(){}
     virtual void onEoL(Output&)=0;
                     //the \n will be written by the writer
                     //avoid recursive call
    };
    
   private:
    Writer(const Writer&);
    Writer  operator=(const Writer&);

//------------------------------------------------- Formatter
    struct Formatter
    {
     Writer& wr;
              Formatter(Writer& wr):wr(wr){}
     virtual ~Formatter(){}
    };
    
//------------------------------------------------- StringFormatter
    struct StringFormatter:public Formatter
    {
              StringFormatter(Writer& wr):Formatter(wr){}
     virtual ~StringFormatter(){}
//both stops at eol     
     virtual const char* put(const char s[]) const=0; //zero terminated
     virtual const char* put(const char s[],unsigned len) const=0;
    };
    
    struct LeftAlign:public StringFormatter //xxxx___
    {
      LeftAlign(Writer& wr):StringFormatter(wr){}
     ~LeftAlign(){}
     const char* put(const char s[]) const; 
     const char* put(const char s[],unsigned len) const;
    };
    
    struct RightAlign:public StringFormatter //____xxx
    {
      RightAlign(Writer& wr):StringFormatter(wr){}
     ~RightAlign(){}
     const char* put(const char s[]) const; 
     const char* put(const char s[],unsigned len) const;
    };
    
//    typedef const char* (Writer::*StringFormat)(const char s[]);

//------------------------------------------------- NumberFormatter
    struct NumberFormatter:public Formatter
    {
              NumberFormatter(Writer& wr):Formatter(wr){}
     virtual ~NumberFormatter(){}
     virtual Writer& put(unsigned char) const=0;
     virtual Writer& put(unsigned short) const=0;
     virtual Writer& put(unsigned) const=0;
     virtual Writer& put(int) const=0;
     virtual Writer& put(unsigned long long) const=0;
     virtual Writer& put(int long long) const=0;
    };
    
//------------------------------------------------- Dec.Formatter
    struct Dec:public NumberFormatter
    {
     template<typename typ>Writer& putU(typ) const; //unsigned
     template<typename typ>Writer& putS(typ) const; //signed
     Dec(Writer& wr):NumberFormatter(wr){}
     Writer& put(unsigned char) const;
     Writer& put(unsigned short) const;
     Writer& put(unsigned) const;
     Writer& put(int) const;
     Writer& put(unsigned long long) const;
     Writer& put(int long long) const;
    };

//------------------------------------------------- Hex.Formatter
    struct Hex:public NumberFormatter
    {
     template<typename typ>unsigned put(char s[],typ) const;
     template<typename typ>Writer&  put(typ) const;
     
     Hex(Writer& wr):NumberFormatter(wr){}
//result in s in reversed order without prefix 0x
//return len
     unsigned put(char s[],unsigned short) const; 
     unsigned put(char s[],unsigned) const;     
     
     Writer& put(unsigned char) const;
     Writer& put(unsigned short) const;
     Writer& put(unsigned val) const;
     Writer& put(int val) const{return put((unsigned)val);}
     Writer& put( unsigned long long) const;
     Writer& put(int long long val) const{return put((unsigned long long)val);}
    };

//------------------------------------------------- Default.Formatter    
    struct DefaultEoL:public EoLListener
    {
     void onEoL(Output&); //does nothing
    };
    
//------------------------------------------------- HexLine
    struct HexLine //used in dump(const char d[],unsigned len)
    {
     static const unsigned MAX_LEN = 16;
     Writer& out;
     const unsigned perLine;
     char line[MAX_LEN];
     unsigned lineIdx;
     unsigned offset;
              HexLine(Writer& out,const unsigned perLine);
     virtual ~HexLine();
     void put(char ch);
     void show();
    };
     
    static const char  Digits_[];
    static const char* Digits;
    static const char DefaultTabString[];
    static const char TabChar;
    static const char Fill;
    static const unsigned MAX_TAB_NBR = 40;

    Output& out;
//------------------------------------------------- state
    unsigned chN;   //number of chars in current line
    unsigned width; //set by setw normally 0 
                    //set by putChar
    unsigned indentN; //the blanks after new line		    
    Dec        decF;
    Hex        hexF;
    LeftAlign  leftAlign;
    RightAlign rightAlign;
    DefaultEoL        defEoL;
              
    NumberFormatter*  cNumberF; //current 
    StringFormatter*  cStringF;
    
    EoLListener* eol;
    
    Writer& newLine();
    Writer& putChar(char ch);

    Writer& putReverse(const char s[],
                       unsigned len);    //_______xxxx
 
    const char* putX_(const char s[]); //xxxxxx_____
    const char* putX_(const char s[],unsigned len);

    const char* put_X(const char s[]); //_______xxxx
    const char* put_X(const char s[],unsigned len);
    void put_XLine(const char s[],unsigned len);
                  //s contains no \n char
      

    template<typename typ,unsigned MAX_LINE,unsigned sLen> 
    Writer& dump(const typ d[],unsigned len,unsigned perLine);
   
    template<typename typ> Writer& putNumber(typ val);
    template<typename typ> Writer& putAsHex(typ val);
    void debug();

   public:
	     Writer(Output& out);
	     Writer(Output& out,EoLListener&);
	     Writer(Output& out,const char tabString[]);
	     Writer(Output& out,EoLListener&,const char tabString[]);
    virtual ~Writer(){}
    Writer& setOutput(Output& new_){out=new_;return *this;}
    Output& getOutput()const{return out;}
    Writer& setTabString(const char ts[]);
    Writer& setEoLListener(EoLListener&  eol){this->eol=&eol;return *this;}
    const EoLListener& getEoLListener()const{return *eol;}
    Writer& dec(){cNumberF=&decF;return *this;}
    Writer& hex(){cNumberF=&hexF;return *this;}
    Writer& setw(int w);
         //w<0  strings right aligned ____ssss
	 //w>0          left  aligned ssss____ 
	 //numbers always right aligned
    Writer& indent(unsigned w);
    unsigned getIndent()const{return indentN;} 	 
    Writer& put(char ch);
    Writer& put(const char s[]); //terminating zero
    Writer& put(const char s[],unsigned len); //len chars
    Writer& put(unsigned char val);
    Writer& put(unsigned short val);
    Writer& put(unsigned val);
    Writer& put(int val);
    Writer& put(unsigned long long val);
    Writer& put(int long long val);
    Writer& put(double val);
    Writer& put(const void* val);
    Writer& put(unsigned long val){return put((unsigned)val);}
    Writer& put(bool val);
    Writer& rep(unsigned cnt,const char s[]);
    Writer& blank(unsigned cnt){return rep(cnt," ");}

//------------------------------------ stateless unformatted
//hex output without prefix 0x
    Writer& hex(unsigned val);
    Writer& hex(unsigned short val);
    Writer& hex(unsigned char val);
    Writer& dec(unsigned val)      {return decF.put(val);}
    Writer& dec(unsigned short val){return decF.put(val);}
    Writer& dec(int val)           {return decF.put(val);}

//------------------------------------ debug
    Writer& dump(const char d[],unsigned len,unsigned perLine);
    Writer& dump(const char d[],unsigned len)
                {return dump(d,len,HexLine::MAX_LEN);}

    Writer& dump(const unsigned char d[],unsigned len,
                 unsigned perLine);
    Writer& dump(const unsigned char d[],unsigned len)
                {return dump(d,len,16);}
    Writer& dump(const unsigned      d[],unsigned len,
                 unsigned perLine);
    Writer& dump(const unsigned      d[],unsigned len)
                {return dump(d,len,4);}
    Writer& dump(const unsigned short d[],unsigned len,
                 unsigned perLine);
    Writer& dump(const unsigned short d[],unsigned len)
		{return dump(d,len,8);}
		
    struct BitDesc
    {
     const char* name;
     unsigned pos;
    };
    Writer& dump(unsigned val,const BitDesc bd[]);  
                                   //bd.name=0 terminates list
#if 0
//try once this
    template<typename T>Writer& operator <<(T t){return put(t);}
#endif

#if 1
    Writer& operator <<(char ch){return put(ch);}
    Writer& operator <<(const char s[]){return put(s);}
    Writer& operator <<(unsigned val){return put(val);}   
    Writer& operator <<(unsigned char val){return put(val);}
    Writer& operator <<(unsigned short val){return put(val);}
    Writer& operator <<(int val){return put(val);}
    Writer& operator <<(unsigned long val){return put(val);}
    Writer& operator <<(unsigned long long val){return put(val);}
    Writer& operator <<(int long long val){return put(val);}
    Writer& operator <<(double val){return put(val);}
    Writer& operator <<(const void* val){return put(val);}
    Writer& operator <<(bool val){return put(val);}
#endif    
  };

//------------------------------------- Formatter 
//similar to iomanip
  class Formatter
  {
   protected:
    virtual Writer& doIt(Writer& out) const=0;
   public:
    friend Writer& operator<<(Writer& out,const Formatter& f)
                   {return f.doIt(out);}
  };
//------------------------------------- some Formatters
  class hex:public Formatter
  {
   private:
    Writer& doIt(Writer& out) const{out.hex();return out;}
   public:
    hex(){} 
  };

  class dec:public Formatter
  {
   private:
    Writer& doIt(Writer& out) const{out.dec();return out;}
   public:
    dec(){} 
  };
  
  class setw:public Formatter
  {
   private:
    int w;
    Writer& doIt(Writer& out) const{out.setw(w);return out;}
   public:
    setw(int w):w(w){} 
  };
  
  class indent:public Formatter
  {
   private:
    unsigned ind;
    Writer& doIt(Writer& out) const{out.indent(ind);return out;}
   public:
    indent(unsigned ind):ind(ind){} 
  };
  
 }//namespace ascii
}//namespace io
#endif
