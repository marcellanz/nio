//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    Foobar is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    Foobar is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Foobar; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#ifndef IO_ASCII_PROGRESS_BAR_H
#define IO_ASCII_PROGRESS_BAR_H
//---------------------------------
//progress-bar
//(c) H.Buchmann FHSO 2004
//$Id: progress-bar.h 193 2006-01-11 15:21:16Z buchmann $
//---------------------------------
INTERFACE(io_ascii_progress_bar,$Revision: 137 $)
#include "io/ascii/out.h"
namespace io
{
 namespace ascii
 {
  class ProgressBar
  {
   public:
    static const unsigned P_POS = 4;
    static const unsigned BAR_START=P_POS+3;
   private:
    static const unsigned MAX_BAR_SIZE=256;
    static const unsigned DEFAULT_LEN =50;

    struct Bresenham
    {
     const unsigned c;
     const unsigned l;
     int err;
     unsigned val;
     bool step(unsigned d); //returns true iff val changed
     Bresenham(unsigned cap,unsigned len,unsigned val0);    
    };

    ProgressBar(const ProgressBar&);
    ProgressBar& operator=(const ProgressBar&);
    io::ascii::Writer& out;

    char theBar[MAX_BAR_SIZE+1];
    unsigned front; //index of front bar
    Bresenham bar;
    Bresenham percent;
    unsigned sum;
    unsigned capacity;

    void init();
    void wrPercent();

   public:
     ProgressBar(io::ascii::Writer& out,
                 unsigned len,          //in chars
		 unsigned capacity);    
     ProgressBar(io::ascii::Writer& out,unsigned capacity);    
    ~ProgressBar();
    void put(unsigned delta);
  };
 } 
}
#endif
