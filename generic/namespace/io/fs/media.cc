//-------------------------
//media
//(c) H.Buchmann FHNW 2006
//$Id$
//-------------------------
#include "sys/sys.h"
IMPLEMENTATION(io_fs_media,$Revision$)
#include "io/fs/media.h"
namespace io
{
 namespace fs
 {
  Media* Media::anchor=0;
  Media::Listener* Media::Listener::anchor=0;
  
  Media::Media()
  :next(anchor)
  {
   anchor=this;
  }
  
  Media::~Media()
  {
  }

  void Media::isReady()
  {
   Listener* li=Media::Listener::anchor;
   while(li)
   {
    li->onMedia(*this);
    li=li->next;
   }
  }
  
  Media::Listener::Listener()
  :next(anchor)
  {
   anchor=this;
  }
  
  Media::Listener::~Listener()
  {
  }
  
 }////namespace fs
}//namespace io 
