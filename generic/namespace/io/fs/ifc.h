#ifndef IO_FS_IFC_H
#define IO_FS_IFC_H
//-------------------------
//cmd superclass of 
// -atapi
// -scsi
//(c) H.Buchmann FHNW 2006
//$Id$
//-------------------------
INTERFACE(io_fs_ifc,$Revision$)
#include "io/byte/pac.h"
#include "io/fs/media.h"
namespace io
{
 namespace fs
 {
//----------------------------------------- Ifc
  class Ifc
  {
   protected:
    Ifc();
    
   public:
//----------------------------------------- Ifc::Factory
     class Factory
     {
      protected:
       friend class Ifc;
       Factory();
      public:
       virtual Ifc* create();
       virtual ~Factory();
     };
//----------------------------------------- Ifc::Cmd
    static Factory factory;
    
    struct Cmd
    {
     unsigned capacity;
     unsigned char* cmd;
              Cmd(unsigned capacity,unsigned char cmd[]);
     virtual ~Cmd();
    };
    
    virtual ~Ifc();
    virtual unsigned read(unsigned lun,
                	  Media::Pos,
			  Media::Length,
			  Cmd& cmd)=0;
          //returns length of cmd
	  // cmd.capacity>=length_of_cmd
    virtual unsigned configure(unsigned lun,Cmd& cmd)=0;
    virtual byte::Packet& getConfig()=0;
    virtual void setup(byte::Packet&)=0;
    static Factory& getFactory(){return factory;}    
  };
 }//namespace fs
}//namespace io 
#endif
