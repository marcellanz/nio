//---------------------------
//fs filesystem
//(c) H.Buchmann FHSO 2006
//$Id: fs.cc 193 2006-01-11 15:21:16Z buchmann $
//---------------------------
#include "sys/sys.h"
IMPLEMENTATION(io_fs,$Revision$)
#include "io/fs/fs.h"
#include "sys/msg.h"

namespace io
{
 namespace fs
 {
//------------------------------------------------- Media
  Media::Media()
  :fs(0)
  {
  }
  
  Media::~Media()
  {
  }
 
  Media::Listener::~Listener()
  {
  }

//------------------------------------------------- FS
  FS::FS()
  :media(0),
   li(0)
  {
  }
  
  FS::~FS()
  {
   if (media)
      {
       sys::msg<<"---------- still mounted ----------\n";
      }
  }
  
  void FS::mount(Media& m,Listener& li)
  {
   if (media) sys::msg.error()<<"FS::mount already mounted\n";
   this->media=&m;
   this->li=&li;
   mountIt();      
  }
  
//------------------------------------------------- FS::Listener
  FS::Listener::~Listener()
  {
  }
  
//------------------------------------------------- FS::Entry
  FS::Entry::~Entry()
  {
  }
  
//------------------------------------------------- FS::Entry::Listener
  FS::Entry::Listener::~Listener()
  {
  }
  
//------------------------------------------------- Cache
  Cache::Cache()
  {
  }
  
  Cache::~Cache()
  {
   for(Map::iterator i=entries.begin();i!=entries.end();++i)
   {
    delete i->second;
   }
  }

  FS::Entry* Cache::add(FS::Entry* e)
  {
   std::pair<Map::iterator,bool> res=entries.insert(
                                   Map::value_type(e->getName(),e)
				                   );
   return (res.second)?0:e;
  }

  FS::Entry* Cache::find(const char name[])
  {
   sys::msg<<"find not yet implemented\n";
   return 0;
  }
  
  io::ascii::Writer& Cache::show(io::ascii::Writer& out) const
  {
   for(Map::const_iterator i=entries.begin();i!=entries.end();++i)
   {
    out<<*(i->second)<<"\n";
   }
   return out;
  }

 }//namespace fs
}//namespace io
