#ifndef IO_FS_SCSI_IFC_H
#define IO_FS_SCSI_IFC_H
//-----------------------------------
//atapi
//(c) H.Buchmann FHNW 2006
// see:SFF Committee
//     SFF-8070i Specification for
//     ATAPI Removable Rewritable Media Devices
//$Id: atapi.h 218 2006-02-06 08:29:40Z buchmann $
//-----------------------------------
INTERFACE(io_fs_scsi_ifc,$Revision$)
#include "io/fs/ifc.h"
#include "util/endian.h"
#include "io/ascii/write.h"

namespace io
{
 namespace fs
 {
  namespace scsi
  {
//----------------------------------------- Ifc
   class Ifc:public fs::Ifc
   {
    public:
     class Factory:public fs::Ifc::Factory
     {
      private:
       friend class Ifc;
       Factory();
      public:
       fs::Ifc* create();
       ~Factory();
     };
     
    private:
     struct Configuration
     {
      unsigned lastBlock;
      unsigned blockLen;
     }__attribute__((packed));
     
     static Factory factory;
     static const unsigned SIZE=256;
     unsigned char cfg[SIZE];
     byte::Packet::Empty config;

     
     unsigned blockN;
     unsigned blockLen;
     unsigned shift;
      
              Ifc();
    public:
     virtual ~Ifc();
     unsigned  read(unsigned lun,
        	    Media::Pos,
		    Media::Length,
		    Cmd& cmd);
     unsigned configure(unsigned lun,
                        Cmd& cmd);
     byte::Packet& getConfig();
     void setup(byte::Packet&);
     static fs::Ifc::Factory& getFactory(){return factory;}
   };

  }//namespace atapi
 }//namespace fs
}//namespace io
#endif
