//-----------------------------------
//atapi
//(c) H.Buchmann FHNV 2006
//$Id: atapi.cc 218 2006-02-06 08:29:40Z buchmann $
//-----------------------------------
#include "sys/sys.h"
IMPLEMENTATION(io_fs_scsi_ifc,$Revision$)
#include "io/fs/scsi/ifc.h"
#include "util/endian.h"
#include "sys/msg.h"

namespace io
{
 namespace fs
 {
  namespace scsi
  {
   Ifc::Factory Ifc::factory;

   Ifc::Factory::Factory()
   {
   }

   Ifc::Factory::~Factory()
   {
   }
   
   fs::Ifc* Ifc::Factory::create()
   {
    return new Ifc;
   }

   Ifc::Ifc()
   :config(cfg,SIZE)
   ,blockN(0)
   ,blockLen(0)
   ,shift(0)
   {
    sys::msg<<"SCSI\n";
   }
   
   Ifc::~Ifc()
   {
   }
   
   unsigned Ifc::read(unsigned lun,
        	      Media::Pos p,
		      Media::Length len,
		      Cmd& cmd)
   {
    struct POD
    {
     unsigned char cod;      //76543210
     unsigned char res0;      
     unsigned lba;           //logical block address
     unsigned char res1;     //reserved
     unsigned short tLen;
     unsigned char control; 
    }__attribute__((packed));
    
    POD* pod=(POD*)(cmd.cmd);
    pod->cod=0x28; 
    pod->lba    =util::Endian::big(p);
    pod->tLen   =util::Endian::big((unsigned short)(len>>shift));
    __builtin_memset(cmd.cmd+sizeof(POD),0,cmd.capacity-sizeof(POD));
    return sizeof(POD); 
   }
   
   unsigned Ifc::configure(unsigned lun,
                           Cmd& cmd)
   {
    struct POD
    {
     unsigned char cod;      //76543210
     unsigned char res[8];
     unsigned char control; 
    }__attribute__((packed));
    
    POD* pod=(POD*)(cmd.cmd);
    pod->cod=0x25;
    pod->control=0;
    __builtin_memset(cmd.cmd+sizeof(POD),0,cmd.capacity-sizeof(POD));
    return sizeof(POD);
   }
   
   byte::Packet& Ifc::getConfig()
   {
    return config;
   }
   
   void Ifc::setup(byte::Packet& pac)
   {
    sys::msg<<"Ifc::setup\n"
            <<pac;
    Configuration* cfg=(Configuration*)(pac.data);
    blockN=util::Endian::big(cfg->lastBlock)+1;
    blockLen=util::Endian::big(cfg->blockLen);
    if ((blockLen&-blockLen)!=blockLen)
       {
        sys::msg.error()<<"SCSI: " <<__PRETTY_FUNCTION__<<"\n";
       }
    shift=__builtin_ctz(blockLen);   
   }
   
  }//namespace atapi
 }//namespace fs
}//namespace io
