#ifndef IO_FS_H
#define IO_FS_H
//---------------------------
//fs filesystem
//(c) H.Buchmann FHSO 2006
//$Id: fs.h 193 2006-01-11 15:21:16Z buchmann $
//---------------------------
INTERFACE(io_fs,$Revision$)
#include "util/str.h"
#include "io/ascii/write.h"
#include <map>

namespace io
{
 namespace fs
 {
  class FS;
//---------------------------------------- Media
  class Media
  {
   private:
    FS* fs;

   protected:
    Media();
    
   public:
//---------------------------------------- Media::Data
    struct Data
    {
     unsigned capacity;
     unsigned len;
     unsigned char* data;
     Data(unsigned capacity,unsigned char data[])
     :capacity(capacity),len(0),data(data){}

     template<typename T>
     Data(T& t)
     :capacity(sizeof(T)),len(0),data((unsigned char*)&t){}

     template<typename T>
     Data(T t[],unsigned len)
     :capacity(len*sizeof(T)),len(0),data((unsigned char*)t){}
    };

//---------------------------------------- Media::Listener
    struct Listener
    {
     virtual ~Listener();
     virtual void onData(Data& d)=0;
     //TODO 
     //virtual onTimeout()=0; 
    };
   
    virtual ~Media();
    virtual void get(Data& d,Listener& li)=0;
    virtual void setPos(unsigned p)=0;
  };

//----------------------------------------
//TODO abstraction of FileName
//what means equal ?

//---------------------------------------- FS
//Filesystem
  class FS
  {
   public:
//---------------------------------------- FS::Entry
    struct Entry
    {
//---------------------------------------- FS::Entry::Listener
     struct Listener
     {
      virtual ~Listener();
      virtual void onEntry(Entry&)=0;
     };
     virtual ~Entry();
     virtual io::ascii::Writer& show(io::ascii::Writer&) const=0;
     friend io::ascii::Writer& operator<<(io::ascii::Writer& out,
                                          const Entry& e)
            {return e.show(out);}
     virtual const char* getName()const=0;
    };
//----------------------------------------
//TODO NullEntry
    
//---------------------------------------- FS::Listener
    struct Listener
    {
     virtual ~Listener();
     virtual void onMount(FS&)=0;
//     virtual void onUnmount(FS&)=0;
    };
    
   protected:
    Media*    media;
    Listener* li;

    FS();
    virtual void mountIt()=0; 
    
   public:
    virtual ~FS();
    virtual void find(const char s[],Entry::Listener& li)=0;
    void mount(Media& media,Listener& li);
  };
  
  
//---------------------------------------- Cache
  class Cache
  {
   private:
    typedef std::map<const char*,FS::Entry*,util::Str::Less> Map;
    Map entries;
    
   public:
             Cache();
    virtual ~Cache();
    FS::Entry* add(FS::Entry* e);
    //returns e if already in set
    //          otherwise 0
    FS::Entry* find(const char name[]);
    io::ascii::Writer& show(io::ascii::Writer&) const;
  };
 }//namespace fs
}//namespace io
#endif
