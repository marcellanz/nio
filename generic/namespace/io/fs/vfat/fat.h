#ifndef IO_FS_VFAT_FAT_H
#define IO_FS_VFAT_FAT_H 
//-----------------------------
//fat
//(c) H.Buchmann FHNW 2006
//$Id: fat.h 193 2006-01-11 15:21:16Z buchmann $
//-----------------------------
INTERFACE(io_fs_vfat_fat,$Revision$)
#include "io/fs/fs.h"
namespace io
{
 namespace fs
 {
  namespace vfat
  {
   class FAT
   {
    protected:
     typedef unsigned short Entry;
     unsigned len;
     Entry*   fat;
     FAT(unsigned len);

    public:
     virtual ~FAT(); 
   };
  }//namespace vfat
 }//namespace fs
}//namespace io 
#endif
