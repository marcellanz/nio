#ifndef IO_FS_VFAT_DIRECTORY_H
#define IO_FS_VFAT_DIRECTORY_H
//---------------------------
//directory 
//(c) H.Buchmann FHNW 2006
//$Id: directory.h 193 2006-01-11 15:21:16Z buchmann $
//info see: http://support.microsoft.com/kb/q140418/
//          http://home.teleport.com/~brainy/fat16.htm
//---------------------------
INTERFACE(io_fs_vfat_directory,$Revision$)
#include "io/fs/fs.h"
#include "io/fs/vfat/fat.h"
#include "io/fs/vfat/vfat.h"
#include <string>
namespace io
{
 namespace fs
 {
  namespace vfat
  {
   
   class Directory;
//-------------------------------------------------- Entry
   struct Entry:public fs::FS::Entry
   {
     Directory& dir;
     std::string  name_255;
     std::string  name_8_3;
     std::string& name;
     
     unsigned start;     //of sector
     unsigned size_byte;      
    
     Entry(Directory& dir,const char name[],Entry_8_3&);
     Entry(Directory& dir,Entry_8_3&);
     Entry(const char name[],unsigned start,unsigned size_byte);
    ~Entry();
    void mkName_8_3(const Entry_8_3& e); 
    io::ascii::Writer& show(io::ascii::Writer&) const;
    const char* getName() const;
   };

   class Directory
   {
    private:
//-------------------------------------------------- Directory::LFN
     struct LFN //Long File Name
     {
      typedef unsigned short Wide;
      unsigned char  id;
      Wide  name1[5];
      unsigned char  attr; 
      unsigned char  reserved;
      unsigned char  checkSum;
      Wide  name2[6];
      unsigned short start; 
      Wide   name3[2];
      unsigned toString(const Wide wide[],unsigned len,char*& s) const;
      unsigned toString(char s[]) const;
      io::ascii::Writer& show(io::ascii::Writer&) const;
     }__attribute__((packed));

//-------------------------------------------------- Directory::DirEntry
     union DirEntry
     {
      Entry_8_3 entry_8_3;
      LFN       lfn;
      io::ascii::Writer& show(io::ascii::Writer&) const;
      bool isLFN() const{return lfn.attr==0xf;}
      bool isDeleted()const{return entry_8_3.name[0]==(char)0xe5;}
      bool isEmpty()const{return lfn.attr==0;}
     }__attribute__((packed));
     
     Cache cache;
     unsigned  len;
     DirEntry* dir;

    public:
	       Directory(Directory* parent,unsigned len);
      virtual ~Directory();
      void find(const char name[],Entry::Listener& li);
      Media::Data set(unsigned len);
      void build();
   };
  }//namespace vfat
 }//namespace fs
}//namespace io 
#endif
