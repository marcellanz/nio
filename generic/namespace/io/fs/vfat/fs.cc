//---------------------------
//vfat 
//(c) H.Buchmann FHNW 2006
//$Id: fs.cc 193 2006-01-11 15:21:16Z buchmann $
//info see: http://support.microsoft.com/kb/q140418/
//          http://home.teleport.com/~brainy/fat16.htm
//---------------------------
#include "sys/sys.h"
IMPLEMENTATION(io_fs_vfat,$Revision$)
#include "io/fs/vfat/fs.h"
#include "sys/msg.h"
#include "util/str.h"
#include <string>

namespace io
{
 namespace fs
 {
  namespace vfat
  {
//-------------------------------------------------- FAT
   FS::FAT::FAT(FS* vfat,unsigned len,unsigned nbr)
   :vfat::FAT(len),
    Media::Data(fat,len),
    vfat(*vfat),
    nbr(nbr),
    cnt(0)
   {
    vfat->media->get(*this,*this);
   }
   
   FS::FAT::~FAT()
   {
   }

   void FS::FAT::onData(Media::Data& d)
   {
    
#if 0
    sys::msg<<"fat# "<<cnt++<<"\n";
    if (cnt<2) vfat.med.get(*this,*this);
       else    {  
                vfat.fat=this;
		vfat.root=new Root(&vfat);
		Media::Data d=vfat.root->set(vfat.bootSector->data.rootDirN);
		vfat.med.get(d,*vfat.root);
               }
#endif
   }

#if 0
//-------------------------------------------------- Root
   FS::Root::Root(FS* vfat)
   :vfat::Directory(0)
   ,vfat(*vfat)
   {
   }
   FS::Root::~Root()
   {
   }   

   void FS::Root::onData(Media::Data& d)
   {
//    vfat.root=this;
//    build();
   }
#endif

   FS::FS()
   :fat(0)
//    root(0)
   {
    sys::msg<<"size= "<<sizeof(Entry_8_3)<<"\n";
   }
   
   FS::~FS()
   {
    if (fat)        delete fat;
   }

   void FS::onData(Media::Data& d)
   {
    bootSector.show(sys::msg);
    li->onMount(*this);
   }
   
   void FS::mountIt()
   {
    Media::Data d(bootSector);
    media->get(d,*this);
   }
   
   void FS::find(const char id[],Entry::Listener& li)
   {
//    root->find(id,li);
   }   
  }//namespace vfat
 }//namespace fs
}//namespace io 
