#ifndef IO_FS_VFAT_VFAT_H
#define IO_FS_VFAT_VFAT_H
//------------------------------
//boot
//(c) H.Buchmann FHNW 2006
//$Id: vfat.h 193 2006-01-11 15:21:16Z buchmann $
//------------------------------
INTERFACE(io_fs_vfat_vfat,$Revision$)
#include "io/fs/fs.h"
#include "sys/msg.h"
#include "io/ascii/write.h"
namespace io
{
 namespace fs
 {
  namespace vfat
  {
//-------------------------------------------------- BootSector
   struct BootSector
   {
    unsigned char  jmp[3];
    char           oemName[8];
    unsigned short bps;         //bytes per sector
    unsigned char  spc;         //sectors per cluster
    unsigned short reservedSectorCnt;
    unsigned char  fatN;        //number of file allocation table
    unsigned short rootN;       //number of root directory entries
    unsigned short sector0N;    //total sectors
    unsigned char  mediaDesc;
    unsigned short spf;        //sector per fat
    unsigned short spt;        //sector per track
    unsigned short headN;      //number of heads
    unsigned       hiddenSectors;
    unsigned       sector1N;
//     unsigned       spfat;    //sectors per FAT
    unsigned char  driveN;     //physical drive nmbr;
    unsigned char  currHead;
    unsigned char  signature;
    unsigned       id;
    char           labelName[11];
    char           type[8];
    unsigned char  code[448];
    unsigned short end;
    io::ascii::Writer& show(io::ascii::Writer&) const;
    unsigned rootSector()const 
            {return 1+fatN*spf;}
   }__attribute__((packed));

//-------------------------------------------------- Entry_8_3
   struct Entry_8_3                                 //POD
   {
    char           name[8];    // file name 
    char           ext[3];     // file extension	       
    unsigned char  attr;       // attribute byte	       
    unsigned char  lcase;      // Case for base and extension  
    unsigned char  ctime_ms;   // Creation time, milliseconds  
    unsigned short ctime;      // Creation time 	       
    unsigned short cdate;      // Creation date 	       
    unsigned short adate;      // Last access date	       
    unsigned short reserved;   // reserved values (ignored)    
    unsigned short time;       // time stamp 
    unsigned short date;       // date stamp 
    unsigned short start;      // starting cluster number 
    unsigned       size;       // size of the file 
    io::ascii::Writer& show(io::ascii::Writer&) const;
    unsigned toString(char s[]) const;
   }__attribute__((packed));


  }//namespace vfat
 }//namespace fs
}//namespace io 
#endif
