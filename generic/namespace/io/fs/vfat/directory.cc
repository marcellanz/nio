//---------------------------
//directory 
//(c) H.Buchmann FHNW 2006
//$Id: directory.cc 193 2006-01-11 15:21:16Z buchmann $
//info see: http://support.microsoft.com/kb/q140418/
//          http://home.teleport.com/~brainy/fat16.htm
//---------------------------
#include "sys/sys.h"
IMPLEMENTATION(io_fs_vfat_directory,$Revision$)
#include "io/fs/vfat/directory.h"
namespace io
{
 namespace fs
 {
  namespace vfat
  {

//-------------------------------------------------- Entry
   Entry::Entry(Directory& dir,const char name[],Entry_8_3& e)
   :dir(dir),
    name_255(name),
    name(name_255)
   {
   }

   Entry::Entry(Directory& dir,Entry_8_3& e)
   :dir(dir),
    name(name_8_3)
   {
   }
   
   
   Entry::~Entry()
   {
   }
   
   void Entry::mkName_8_3(const Entry_8_3& e)
   {
    char n[15];
    e.toString(n);
    name_8_3=n;
   } 

   io::ascii::Writer& Entry::show(io::ascii::Writer& out) const
   {
    out<<name.c_str();
    return out;
   } 

   const char* Entry::getName() const
   {
    return name.c_str();
   }
   
//-------------------------------------------------- Directory
   Directory::Directory(Directory* parent,unsigned len)
   :len(0)
   ,dir(0)
   {
   }
   
   Directory::~Directory()
   {
    if (dir) delete [] dir;
   }
   
   
   Media::Data Directory::set(unsigned len)
   {
    this->len=len;
    dir=new DirEntry[len];
    return Media::Data(dir,len);
   }
   
   void Directory::find(const char name[],Entry::Listener& li)
   {
    FS::Entry* e=cache.find(name);
    if (e) {li.onEntry(*e);return;}
   }

   void Directory::build()
   {
    const unsigned NAME_CAPACITY=260;
    char     name[NAME_CAPACITY+1];
    unsigned nameI=NAME_CAPACITY;
    name[nameI]='\0'; //final terminating zero
    DirEntry* d=dir;
    for(unsigned i=0;i<len;i++)
    {
     if (d->isEmpty()) continue;
     if (d->isLFN())
        {
	 char s[50];
	 unsigned l=d->lfn.toString(s);
	 while(l>0) name[--nameI]=s[--l];
 	}
	else
	{
	 fs::FS::Entry* e=
	  cache.add((nameI==NAME_CAPACITY)
	            ?new Entry(*this,d->entry_8_3)
	            :new Entry(*this,name+nameI,d->entry_8_3));
	 if (e) delete e; //already in list
	 nameI=NAME_CAPACITY;
	}
     d++;	
    }
   }
   
   unsigned Directory::LFN::toString(const Wide wide[],unsigned len,char*& s) const
   {
    unsigned sI=0;
    for(unsigned i=0;i<len;i++)
    {
     unsigned short ch=wide[i];
     s[sI]=(char)(ch&0xff); //the lower bytes
     if (ch==0) {s=0;return sI;}  //end of string
     sI++;
    }
    s[sI]='\0';
    s=s+sI;
    return sI;
   }

   unsigned Directory::LFN::toString(char s[]) const
   {
    struct {
            const Wide*    name;
	    unsigned size;
           } n[]={
	           {name1,sizeof(name1)/sizeof(Wide)},
	           {name2,sizeof(name2)/sizeof(Wide)},
	           {name3,sizeof(name3)/sizeof(Wide)}
	          };
    char* s1=s;	
    unsigned len=0;	  
    for(unsigned i=0;(i<3)&&s1;i++)
    {
     len+=toString(n[i].name,n[i].size,s1);
    }
    return len;
   } 
   
   io::ascii::Writer& Directory::LFN::show(io::ascii::Writer& out) const
   {
    char s[50];
    toString(s);
    out.hex(id)<<" "<<s<<"\n";
    return out;
   }

   io::ascii::Writer& Directory::DirEntry::show(io::ascii::Writer& out) const
   {
    return (isLFN())?lfn.show(out):entry_8_3.show(out);
   } 

     
  }//namespace vfat
 }//namespace fs
}//namespace io 
