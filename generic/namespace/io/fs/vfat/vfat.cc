//------------------------------
//boot
//(c) H.Buchmann FHNW 2006
//$Id: vfat.cc 193 2006-01-11 15:21:16Z buchmann $
//------------------------------
#include "sys/sys.h"
IMPLEMENTATION(io_fs_vfat_vfat,$Revision$)
#include "io/fs/vfat/vfat.h"
namespace io
{
 namespace fs
 {
  namespace vfat
  {
//-------------------------------------------------- Boot::Sector
   io::ascii::Writer& BootSector::show(io::ascii::Writer& out) const
   {
    out<<"----------------------------------- BootSector\n"
         "jmp               ";
    for(unsigned i=0;i<sizeof(jmp);i++)
    {
     out.hex(jmp[i])<<" ";
    }
    out<<"\n";
    (out<<"oemName           ").put(oemName,sizeof(oemName))<<"\n";
    out<<
    "bytes/sector      "<<bps<<"\n"      //bytes per sector
    "sectors/cluster   "<<spc<<"\n"       //sectors per cluster
    "reservedSectorCnt "<<reservedSectorCnt<<"\n"
    "fatN              "<<fatN<<"\n"     //number of file allocation table
    "rootN             "<<rootN<<"\n" //number of root directory entries
    "sector0N          "<<sector0N<<"\n"  //total sectors
    "mediaDesc         "<<io::ascii::hex()<<mediaDesc<<"\n"
    "sector/fat        "<<spf<<"\n"      //sector per fat
    "sector/track      "<<spt<<"\n"      //sector per track
    "headN             "<<headN<<"\n"    //number of heads
    "hiddenSectors     "<<hiddenSectors<<"\n"
    "sector1N          "<<sector1N<<"\n"
//     "spfat             "<<spfat<<"\n"    //sectors per FAT
    "driveN            "<<driveN<<"\n"   //physical drive nmbr<<"\n"
    "currHead          "<<currHead<<"\n"
    "signature         "<<signature<<"\n"
    "id                "<<io::ascii::hex()<<id<<"\n";

    (out<<"labelName         ").put(labelName,sizeof(labelName))<<"\n";
    (out<<"type              ").put(type,sizeof(type))<<"\n";
    (out<<"end               ").hex(end)<<"\n"
          "-----------------------------------\n";
    return out;
   }

//-------------------------------------------------- Entry_8_3
   io::ascii::Writer& Entry_8_3::show(io::ascii::Writer& out) const
   {
    out.put(name,sizeof(name)).put(".").put(ext,sizeof(ext))
       <<" "<<io::ascii::hex()<<attr<<"\n";
    return out;
   }

   unsigned Entry_8_3::toString(char s[]) const
   {
    unsigned i=0;
    for(unsigned k=0;k<sizeof(name);k++)
    {
     char ch=name[k];
     if (ch==' ') break;
     s[i++]=ch;
    }
    for(unsigned k=0;k<sizeof(ext);k++)
    {
     char ch=ext[k];
     if (ch==' ') break;
     if (k==0) s[i++]='.';
     s[i++]=ch;
    } 
    s[i]='\0';
    return i;   
   }
  }//namespace vfat
 }//namespace fs
}//namespace io 
