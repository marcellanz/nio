//-----------------------------
//fat
//(c) H.Buchmann FHNW 2006
//$Id: fat.cc 193 2006-01-11 15:21:16Z buchmann $
//-----------------------------
#include "sys/sys.h"
IMPLEMENTATION(io_fs_vfat_fat,$Revision$)
#include "io/fs/vfat/fat.h"
namespace io
{
 namespace fs
 {
  namespace vfat
  {
   FAT::FAT(unsigned len)
   :len(len),
    fat(new Entry[len])
   {
   }
   
   FAT::~FAT()
   {
    delete [] fat;
   }
  }//namespace vfat
 }//namespace fs
}//namespace io 
