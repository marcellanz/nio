#ifndef IO_FS_VFAT_H
#define IO_FS_VFAT_H
//---------------------------
//vfat 
//(c) H.Buchmann FHNW 2006
//$Id: fs.h 193 2006-01-11 15:21:16Z buchmann $
//info see: http://support.microsoft.com/kb/q140418/
//          http://home.teleport.com/~brainy/fat16.htm
//---------------------------
INTERFACE(io_fs_vfat,$Revision$)
#include "io/ascii/write.h"
#include "io/fs/fs.h"
#include "io/fs/vfat/vfat.h"
#include "io/fs/vfat/fat.h"
#include "io/fs/vfat/directory.h"
#include <string>
namespace io
{
 namespace fs
 {
  namespace vfat
  {

//-------------------------------------------------- FS
   class FS:public fs::FS,
            public fs::Media::Listener
   {
    private:
#if 0
//-------------------------------------------------- Root
     struct Root:public vfat::Directory,
                 public Media::Listener
     {
       FS& vfat;
       Root(FS* vfat);
      ~Root();
      void onData(Media::Data& d); //for the root dir
     };
#endif
     
//-------------------------------------------------- FAT
     struct FAT:public vfat::FAT,
                public Media::Listener,
                public Media::Data
     {
      FS& vfat;
      unsigned nbr; //of fats
      unsigned cnt;
       FAT(FS* vfat,unsigned len,unsigned nbr);
      ~FAT();
      void onData(Media::Data& d);
     };
 
          
     enum Attr {
        	RO    =1<<0,
        	HIDDEN=1<<1,
		SYS   =1<<2,
		VOLUME=1<<3,
		DIR   =1<<4,
		ARCH  =1<<5
	       };

     BootSector  bootSector;
     FAT*        fat;
     Entry*      rootEntry;     
     Directory*  root;
     
     void onData(Media::Data& d); //for the BootSector
     void mountIt();
     
     
    public:     
              FS();
     virtual ~FS();
     void find(const char id[],Entry::Listener& li);
   };
  }//namespace vfat
 }//namespace fs
}//namespace io 
#endif
