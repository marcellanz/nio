#ifndef IO_FS_MEDIA_H
#define IO_FS_MEDIA_H
//-------------------------
//media
//(c) H.Buchmann FHNW 2006
//$Id$
//-------------------------
INTERFACE(io_fs_media,$Revision$)
#include "io/byte/pac.h"
namespace io
{
 namespace fs
 {
//----------------------------------------- Media
  class Media
  {
   private:
    static Media* anchor;
    Media* next;

   protected:
    Media();
     
   public:
//----------------------------------------- Media::Listener
    class Listener
    {
     private:
      friend class Media;
      static Listener* anchor;
      Listener* next;
      
     protected:
      Listener();

     public:
      virtual ~Listener();
      virtual void onMedia(Media& m)=0;
    };
    
    typedef unsigned Pos;
    typedef unsigned Length;
    
    virtual ~Media();
    void isReady(); //calls the listener
    
    virtual void read(Pos pos,
                      byte::Packet& p,
		      byte::Packet::Listener& li)=0;
    virtual void write(Pos pos,
                       byte::Packet& p,
		       byte::Packet::Listener& li)=0;
  };
 }//namespace fs
}//namespace io 
#endif
