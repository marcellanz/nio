//-------------------------
//cmd superclass of 
// -atapi
// -scsi
//(c) H.Buchmann FHNW 2006
//$Id$
//-------------------------
#include "sys/sys.h"
IMPLEMENTATION(io_fs_ifc,$Revision$)
#include "io/fs/ifc.h"
namespace io
{
 namespace fs
 {
//----------------------------------------- Ifc
  Ifc::Factory Ifc::factory;
  
  Ifc::Factory::Factory()
  {
  }
  
  Ifc::Factory::~Factory()
  {
  }

  Ifc* Ifc::Factory::create()
  {
   return 0;
  }
  
  Ifc::Ifc()
  {
  }

  Ifc::~Ifc()
  {
  }

  Ifc::Cmd::Cmd(unsigned capacity,unsigned char cmd[])
  :capacity(capacity)
  ,cmd(cmd)
  {
  }
  
  Ifc::Cmd::~Cmd()
  {
  }

 }////namespace fs
}//namespace io 
