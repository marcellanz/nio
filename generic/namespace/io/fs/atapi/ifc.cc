//-----------------------------------
//atapi
//(c) H.Buchmann FHNV 2006
//$Id: ifc.cc 221 2006-03-11 17:57:03Z buchmann $
//-----------------------------------
#include "sys/sys.h"
IMPLEMENTATION(io_fs_atapi_ifc,$Revision$)
#include "io/fs/atapi/ifc.h"
#include "util/endian.h"
#include "sys/msg.h"

namespace io
{
 namespace fs
 {
  namespace atapi
  {
   Ifc::Factory Ifc::factory;

   Ifc::Factory::Factory()
   {
   }

   Ifc::Factory::~Factory()
   {
   }
   
   fs::Ifc* Ifc::Factory::create()
   {
    return new Ifc;
   }

   Ifc::Ifc()
   :config(cfg,SIZE)
   ,blockN(0)
   ,blockLen(0)
   ,shift(0)
   {
   }
   
   Ifc::~Ifc()
   {
   }
   
   unsigned Ifc::Configuration::Entry::getBlockLen() const
   {
    return blockLen[2]+0x100*(blockLen[1]+0x100*blockLen[0]);
   }
   
   io::ascii::Writer& Ifc::Configuration::Entry::show(io::ascii::Writer& out)const
   {
    static const char* Desc[]={
                               "xxx",
			       "Unformatted Media",
			       "Formatted Media",
			       "No Cartridge in Drive"
                              };
    out<<util::Endian::big(blockN)
       <<io::ascii::setw(4)<<getBlockLen()
       <<" "<<Desc[res_desc&0x3];
    return out;
   }


   io::ascii::Writer& Ifc::Configuration::show(io::ascii::Writer& out)const
   {
    int s=util::Endian::big(size);
    for(unsigned i=0;s>0;i++)
    {
     entry[i].show(out)<<"\n";
     s-=sizeof(Entry);
    }
    return out;
   }
   
   unsigned Ifc::read(unsigned lun,
        	      Media::Pos p,
		      Media::Length len,
		      Cmd& cmd)
   {
    struct POD
    {
     unsigned char cod;      //76543210
     unsigned char lun_res;  //lll00000  l:lub
     unsigned lba;           //logical block address
     unsigned char res0;     //reserved
     unsigned short tLen;
     unsigned char res1[3]; 
    }__attribute__((packed));
    
    POD* pod=(POD*)(cmd.cmd);
    pod->cod=0x28; 
    pod->lun_res=(lun&0x7)<<5;
    pod->lba    =util::Endian::big(p);
    pod->tLen   =util::Endian::big((unsigned short)(len>>shift));
    __builtin_memset(cmd.cmd+sizeof(POD),0,cmd.capacity-sizeof(POD));
    return sizeof(POD); 
   }
   
   unsigned Ifc::configure(unsigned lun,
                           Cmd& cmd)
   {
    struct POD
    {
     unsigned char cod;      //76543210
     unsigned char lun_res;  //lll00000  l:lub
     unsigned char res0[5]; 
     unsigned short allocLen;
     unsigned char res1[3]; 
    }__attribute__((packed));
    
    POD* pod=(POD*)(cmd.cmd);
    pod->cod=0x23;
    pod->lun_res=(lun&0x7)<<5;
    pod->allocLen=util::Endian::big((unsigned short)0);
    __builtin_memset(cmd.cmd+sizeof(POD),0,cmd.capacity-sizeof(POD));
    return sizeof(POD);
   }
   
   byte::Packet& Ifc::getConfig()
   {
    return config;
   }
   
   void Ifc::setup(byte::Packet& pac)
   {
    sys::msg<<"Ifc::setup\n"
            <<pac;
    	    
    Configuration* cfg=(Configuration*)(pac.data);
    cfg->show(sys::msg);
    Configuration::Entry& e=cfg->entry[0];
    blockN=util::Endian::big(e.blockN);
    blockLen=e.getBlockLen();
    if ((blockLen&-blockLen)!=blockLen)
       {
        sys::msg.error()<<"ATAPI: " <<__PRETTY_FUNCTION__<<"\n";
       }
    shift=__builtin_ctz(blockLen);
   }
   
  }//namespace atapi
 }//namespace fs
}//namespace io
