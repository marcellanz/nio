#ifndef IO_FS_ATAPI_IFC_H
#define IO_FS_ATAPI_IFC_H
//-----------------------------------
//atapi
//(c) H.Buchmann FHNW 2006
// see:SFF Committee
//     SFF-8070i Specification for
//     ATAPI Removable Rewritable Media Devices
//$Id: ifc.h 221 2006-03-11 17:57:03Z buchmann $
//-----------------------------------
INTERFACE(io_fs_atapi_ifc,$Revision$)
#include "io/fs/ifc.h"
#include "util/endian.h"
#include "io/ascii/write.h"

namespace io
{
 namespace fs
 {
  namespace atapi
  {
//----------------------------------------- Ifc
   class Ifc:public fs::Ifc
   {
    public:
     class Factory:public fs::Ifc::Factory
     {
      private:
       friend class Ifc;
       Factory();
      public:
       fs::Ifc* create();
       ~Factory();
     };
     
    private:
     static Factory factory;
     static const unsigned SIZE=256;
     unsigned char cfg[SIZE];
     byte::Packet::Empty config;

//----------------------------------------- Ifc::Configuration
     struct Configuration
     {
      struct Entry
      {
       unsigned blockN;           //76543210
       unsigned char res_desc;    //000000dd
       unsigned char blockLen[3]; // big endian
       io::ascii::Writer& show(io::ascii::Writer&)const;
       unsigned getBlockLen() const;
      }__attribute__((packed));
      
      unsigned size;
      Entry entry[0];
      io::ascii::Writer& show(io::ascii::Writer&)const;
     }__attribute__((packed));
     
     unsigned blockN;
     unsigned blockLen;
     unsigned shift;
      
              Ifc();
    public:
     virtual ~Ifc();
     unsigned  read(unsigned lun,
        	    Media::Pos,
		    Media::Length,
		    Cmd& cmd);
     unsigned configure(unsigned lun,
                        Cmd& cmd);
     byte::Packet& getConfig();
     void setup(byte::Packet&);
     static fs::Ifc::Factory& getFactory(){return factory;}
   };

  }//namespace atapi
 }//namespace fs
}//namespace io
#endif
