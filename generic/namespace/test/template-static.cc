//---------------------
//template-static
//(c) H.Buchmann FHNW 2006
//$Id$
//---------------------
#include <iostream>

#define INSTANCE
//#define NO_INSTANCE

namespace test
{
 template<typename T>
 class Template
 {
  static Template temp;

  Template()
  {
   std::cerr<<__PRETTY_FUNCTION__<<" "<<"\n";
  }

  Template(T id)
  {
   std::cerr<<__PRETTY_FUNCTION__<<" "<<"\n";
  }
  
  ~Template()
  {
   std::cerr<<__PRETTY_FUNCTION__<<" "<<"\n";
  }
 };

#ifdef  INSTANCE
//calls non default constructor
 template<> Template<unsigned> Template<unsigned>::temp(6);
// template<> Template<int> Template<int>::temp(-6);
#endif

#ifdef  NO_INSTANCE
//should call default constructor  
 template<> Template<unsigned> Template<unsigned>::temp;
// template<> Template<int> Template<int>::temp;
#endif
 
}

int main(unsigned argc,char* args[])
{
 return 0;
}
