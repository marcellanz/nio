//-----------------------
//list-test (valgrind)
//(c) H.Buchmann FHNW 2006
//$Id$
//-----------------------
#include "sys/sys.h"
IMPLEMENTATION(stl_list_test,$Revision$)
#include "sys/msg.h"
#include <list>

namespace test
{
 class Tester
 {
  struct Val
  {
   unsigned val;
            Val(unsigned val);
   virtual ~Val();
  };
  
  static Tester tester;
  typedef std::list<Val*> List;
  List list;
  Tester();
  ~Tester();
 };
 
 Tester::Val::Val(unsigned val)
 :val(val)
 {
 }
 
 Tester::Val::~Val()
 {
 }
 
 Tester Tester::tester;
 
 Tester::Tester()
 {
  for(unsigned i=0;i<10;i++)
  {
   list.push_back(new Val(i));
  }
 }
 
 Tester::~Tester()
 {
  for(List::iterator i=list.begin();i!=list.end();++i)
  {
   delete *i;
  }
 }
 
}//namespace test
