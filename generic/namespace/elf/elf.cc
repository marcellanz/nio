//-----------------------------
//elf
//(c) H.Buchmann FHSO 2002
//$Id: elf.cc 221 2006-03-11 17:57:03Z buchmann $
//-----------------------------
#include "sys/sys.h"
IMPLEMENTATION(elf_elf,$Revision: 1.1 $)
#include "elf/elf.h"
//#include "sys/deb.h"
namespace elf
{
 bool Header::isElf()
 {
//  static const unsigned char Signature[] __attribute__ ((aligned(__alignof__(const unsigned))))={0x7f,'E','L','F'};
  static const unsigned char Signature[]={0x7f,'E','L','F'};
  return (*(unsigned*)e_ident)== (*(unsigned*)Signature); //independent of endian
 }

 Endian Header::getTargetEndian()
 {
  return (e_ident[5]==1)?LITTLE:BIG;
 }

 Endian Header::getHostEndian()
 {
//  static const unsigned char Data0[4]__attribute__ ((aligned(__alignof__(unsigned))))={0x12,0x34,0x56,0x78};
  static const unsigned char Data0[4]={0x12,0x34,0x56,0x78};
  static const unsigned      Data1   =0x12345678;
  return (Data1==*(unsigned*)Data0)?BIG:LITTLE;  
 }  

#if 1 

                          //0123456789abcdef
 const char Signature[16]= "eo3sELF100   HB"; //16 chars incl termiating zero

 void ElfletDesc::copy(const ProgHeader& ph,unsigned codeOffset)
 {
  unsigned pos=ph.p_offset.idx+codeOffset;
  if (pos==ph.p_vaddr) return; //already at rigth pos

  unsigned* dst=(unsigned*)ph.p_vaddr;
  const unsigned* src=(unsigned*)pos;
  unsigned  len= ph.p_files;
  unsigned cnt=0;
  while(cnt<len)
  {
   *dst++=*src++;
   cnt+=sizeof(unsigned);
  }
 }

 void ElfletDesc::relocate(unsigned codeOffset) const
 {
  copy(text,codeOffset);
  copy(data,codeOffset);
 }
 
 void ElfletDesc::startIt(unsigned codeOffset) const//__attribute__ ((noreturn))
 {
//  sys::deb::hex("desc       ",(unsigned)this);
//  sys::deb::hex("codeoffset ",codeOffset);
  typedef void (*Start)(const ElfletDesc* ed,unsigned) __attribute__ ((noreturn));
  Start start=(Start)(entry+codeOffset);
//  sys::deb::hex("start ",(unsigned)start);
  start(this,codeOffset); //relocation done in startup
 }

 unsigned ElfletDesc::clone(ElfletDesc* dst,unsigned maxLen)
 {
  if (maxLen<sizeof(ElfletDesc)) return 0;
  *dst=*this; //copy fixed part
//copy string
  unsigned len=sizeof(ElfletDesc);
  unsigned i=0;
  while(true)
  {
   if (len>=maxLen) return maxLen;
   char ch=desc[i];
   dst->desc[i]=ch;
   len++;
   i++;
   if (ch=='\0') return len;
  }
 }
#endif

}
