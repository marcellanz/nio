//-------------------------
//output operators for elf data structures
//(c) H.Buchmann FHSO 2001
//$Id: show.cc 193 2006-01-11 15:21:16Z buchmann $
//-------------------------
#include "sys/sys.h"
IMPLEMENTATION(elf_show,$Revision: 1.3 $)
#include "elf/show.h"
#include "sys/msg.h"

extern "C" 
char *__cxa_demangle (const char *__mangled_name,
		      char *__output_buffer,
		      __SIZE_TYPE__ *__length,
		      int *__status);

namespace elf
{
 io::ascii::Writer& operator<<(io::ascii::Writer& out,const Header& hdr)
 {
  out<<"------------- Header "<<sizeof(Header)<<"\n"<<
  "hdr.e_type       "<<hdr.e_type      <<"\n"
  "hdr.e_machine    "<<hdr.e_machine   <<"\n"
  "hdr.e_version    "<<hdr.e_version   <<"\n"
  "hdr.e_entry      "<<io::ascii::hex()<<hdr.e_entry    <<"\n"
//  "hdr.e_phoff      "                  <<hdr.e_phoff    <<"\n"
//  "hdr.e_shoff      "                  <<hdr.e_shoff    <<"\n"
  "hdr.e_flags      "<<io::ascii::hex()<<hdr.e_flags    <<"\n"
  "hdr.e_ehsize     "<<io::ascii::hex()<<hdr.e_ehsize   <<" ("<<hdr.e_ehsize<<")\n"
  "hdr.e_pentsize   "<<io::ascii::hex()<<hdr.e_pentsize <<" ("<<hdr.e_pentsize<<")\n"
  "hdr.e_phnum      "<<io::ascii::hex()<<hdr.e_phnum    <<" ("<<hdr.e_phnum<<")\n"
  "hdr.e_shentsize  "<<io::ascii::hex()<<hdr.e_shentsize<<" ("<<hdr.e_shentsize<<")\n"
  "hdr.e_shnum      "<<io::ascii::hex()<<hdr.e_shnum    <<" ("<<hdr.e_shnum<<")\n"
  "hdr.e_shstrndx   "<<io::ascii::hex()<<hdr.e_shstrndx <<" ("<<hdr.e_shstrndx<<")\n";
  return out;
 }

 io::ascii::Writer& operator<<(io::ascii::Writer& out,const SectionHeader& sh)
 {
  out<<"------------- SectionHeader "<<sizeof(SectionHeader)<<"\n";
  "sh.sh_name      ";out.dec()<< sh.sh_name.ref      << "\n"
  "sh.sh_type      "<<      sh.sh_type      << "\n"
  "sh.sh_flags     "<<      sh.sh_flags     << "\n"
  "sh.sh_addr      ";out.hex()<< sh.sh_addr      << "\n"
//  "sh.sh_offset    "          << sh.sh_offset.ref-data    << "\n"
  "sh.sh_size      "  ;out.dec()<<sh.sh_size      << "\n"
  "sh.sh_link      "<<      sh.sh_link      << "\n"
  "sh.sh_info      "<<      sh.sh_info      << "\n"
  "sh.sh_addralign "<<      sh.sh_addralign << "\n"
  "sh.sh_entsize   "<<      sh.sh_entsize   << "\n";
  return out;
 }

 io::ascii::Writer& operator<<(io::ascii::Writer& out,const ProgHeader& ph)
 {
  const char* Type[]=
              {
               "PT_NULL   ", // 0x0,
	       "PT_LOAD   ", // 0x1,
	       "PT_DYNAMIC", // 0x2,
	       "PT_INTERP ", // 0x3,
	       "PT_NOTE   ", // 0x4,
	       "PT_SHLIB  ", // 0x5,
	       "PT_PHDR   ", // 0x6,
	       "PT_TLS    ", // 0x7,
	       "PT_LOOS   ", // 0x60000000,
	       "PT_HIOS   ", // 0x6fffffff,
	       "PT_LOPROC ", // 0x70000000,
	       "PT_HIPROC "  // 0x7fffffff
	      };
	      
   out<<"------------- ProgHeader "<<sizeof(ProgHeader)<<"\n";
   if (ph.p_type<=PT_TLS) out<<Type[ph.p_type];
      else if ((PT_LOOS<=ph.p_type)&&(ph.p_type<=PT_HIOS))
               out<<"PT_LOOS "<<io::ascii::hex()<<ph.p_type;
      else if ((PT_LOPROC<=ph.p_type)&&(ph.p_type<=PT_HIPROC))
               out<<"PT_LOPROC "<<io::ascii::hex()<<ph.p_type;
	   else out<<io::ascii::hex()<<ph.p_type;
   out<<"\n"
//  "ph.p_offset  "<<io::ascii::hex()<<(ph.p_offset.ref-data)<<"\n"
  "ph.p_vaddr   "<<io::ascii::hex()<<ph.p_vaddr  <<"\n"
  "ph.p_paddr   "<<io::ascii::hex()<<ph.p_paddr  <<"\n"
  "ph.p_files   "<<io::ascii::hex()<<ph.p_files  <<" ("<<ph.p_files<<")\n"
  "ph.p_memsz   "<<io::ascii::hex()<<ph.p_memsz  <<" ("<<ph.p_memsz<<")\n"
  "ph.p_flags   "<<io::ascii::hex()<<ph.p_flags  <<"\n"
  "ph.p_align   "<<io::ascii::hex()<<ph.p_align  <<"\n";
  return out;
 }

 io::ascii::Writer& operator<<(io::ascii::Writer& out,const Symbol& s)
 {
  static const char* Bind[]=
  {
   "loc", //Local
   "glb", //Global
   " wk", //Weak
   "  3",
   "  4",
   "  5",
   "  6",
   "  7",
   "  8",
   "  9",
   "  a",
   "  b",
   "  c",
   "lpr", //Loproc
   "  e",
   "hpr"  //Hiproc
  };
  static const char Defined[]="-d";
  int status;
  char* name=__cxa_demangle(s.st_name.ref,0,0,&status);
  out<<io::ascii::hex()  <<s.st_value<<" "<<Bind[s.bind()]<<" "
     <<Defined[s.isDefined()]
     <<io::ascii::setw(4)<<s.st_shndx<<" "<<name;
  if (name) delete [] name;
  return out;
 }
}//namespace elf
