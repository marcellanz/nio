#ifndef ELF_SHOW_H
#define ELF_SHOW_H
//-------------------------
//output operators for elf data structures
//(c) H.Buchmann FHSO 2001
//$Id: show.h 193 2006-01-11 15:21:16Z buchmann $
//-------------------------
INTERFACE(elf_show,$Revision: 1.3 $)
#include "elf/elf.h"
#include "io/ascii/write.h"

namespace elf
{
 io::ascii::Writer& operator<<(io::ascii::Writer& out,const Header&);
 io::ascii::Writer& operator<<(io::ascii::Writer& out,const SectionHeader&);
 io::ascii::Writer& operator<<(io::ascii::Writer& out,const ProgHeader&);
 io::ascii::Writer& operator<<(io::ascii::Writer& out,const Symbol&);
}//namespace elf
#endif
