#ifndef ELF_ELF_H
#define ELF_ELF_H
//-------------------------
//elf basic data structures
//see elf1 
//(c) H.Buchmann FHSO 2001
//$Id: elf.h 221 2006-03-11 17:57:03Z buchmann $
//-------------------------
INTERFACE(elf_elf,$Revision: 1.3 $)
#include "util/endian.h"
namespace elf
{
//------------------------- information for elf file
 typedef unsigned       Elf32_Addr;
 typedef unsigned short Elf32_Half;
 typedef unsigned       Elf32_Off;
 typedef int            Elf32_Sword;
 typedef unsigned       Elf32_Word;
 
 enum Endian {LITTLE,BIG};
 
 enum E_Type   {
        	ET_NONE  = 0,
        	ET_REL   = 1,
		ET_EXEC  = 2,
		ET_DYN   = 3,
		ET_CORE  = 4,
		ET_LOPROC= 0xff00, 
		ET_HIPROC= 0xffff
               };
	       
 enum SHN_INDEX {
                SHN_UNDEF     =      0,
		SHN_LORESERVE = 0xff00,
		SHN_LOPROC    = 0xff00,
		SHN_HIPROC    = 0xff1f,
		SHN_ABS       = 0xfff1,
		SHN_COMMON    = 0xfff2,
		SHN_HIRESERVE = 0xffff
                };
	       
 enum SH_TYPE  {
		SHT_NULL     =  0,
		SHT_PROGBITS =  1,
		SHT_SYMTAB   =  2,
		SHT_STRTAB   =  3,
		SHT_RELA     =  4,
		SHT_HASH     =  5,
		SHT_DYNAMIC  =  6,
		SHT_NOTE     =  7,
		SHT_NOBITS   =  8,
		SHT_REL      =  9,
		SHT_SHLIB    = 10,
		SHT_DYNSYM   = 11,
		SHT_LOPROC   = 0x70000000,
		SHT_HIPROC   = 0x7fffffff,
		SHT_LOUSER   = 0x80000000,
		SHT_HIUSER   = 0xffffffff     
               };
	       
 enum SH_FLAGS {
                SHF_WRITE     =0x1,
		SHF_ALLOC     =0x2,
		SHF_EXECINSTR =0x4,
		SHF_MASKPROC  =0xf0000000
               };
	       
 enum PH_TYPE {
               PH_NULL    = 0x0,
	       PH_LOAD    = 0x1,
	       PH_DYNAMIC = 0x2,
	       PH_INTERP  = 0x3,
	       PH_NOTE    = 0x4,
	       PH_SHLIB   = 0x5,
	       PH_PHDR    = 0x6,
	       PH_ELF     = 0x7   //private for elf header
              };
	      
 enum PT_TYPE {
               PT_NULL    = 0x0,
	       PT_LOAD    = 0x1,
	       PT_DYNAMIC = 0x2,
	       PT_INTERP  = 0x3,
	       PT_NOTE    = 0x4,
	       PT_SHLIB   = 0x5,
	       PT_PHDR    = 0x6,
	       PT_TLS     = 0x7,
	       PT_LOOS    = 0x60000000,
	       PT_HIOS    = 0x6fffffff,
	       PT_LOPROC  = 0x70000000,
	       PT_HIPROC  = 0x7fffffff
              };
	      	      
 enum PH_FLAGS{
               PH_X = 0x1,       //executable
	       PH_W = 0x2,       //writable
	       PH_R = 0x4        //readable
              };
 

 static void chgEndian(Elf32_Addr&);
 static void chgEndian(Elf32_Half&);
//  static Elf32_Off   changeEndian(Elf32_Off);
 static void chgEndian(Elf32_Sword&);
//  static Elf32_Word  changeEndian(Elf32_Word);

 template<typename E>
 union Ref
 {
  Elf32_Word idx; //as index
  E*         ref; //as pointer
  template<typename F>
  void toRef(F table[]){ref= (idx)?(E*)(table+idx):0;}
  void toIdx(E table[]){idx= (ref)?    (ref-table):0;}
 };
 
 typedef Ref<const char> Name;
 
//--------------------------------------------- Header
 class ProgHeader;
 class SectionHeader;
 struct Header
 {
  unsigned char      e_ident[16];
  Elf32_Half         e_type;
  Elf32_Half         e_machine;
  Elf32_Word         e_version;
  Elf32_Addr         e_entry;
  Ref<ProgHeader>    e_phoff;
  Ref<SectionHeader> e_shoff;
  Elf32_Word         e_flags;
  Elf32_Half         e_ehsize;
  Elf32_Half         e_pentsize;
  Elf32_Half         e_phnum;
  Elf32_Half         e_shentsize;
  Elf32_Half         e_shnum;
  Elf32_Half         e_shstrndx;
  
  void changeEndian();
  bool isElf();
  Endian getHostEndian();
  Endian getTargetEndian();
 };

//--------------------------------------------- SectionHeader
 struct SectionHeader
 {
  Name                     sh_name;
  Elf32_Word               sh_type;
  Elf32_Word               sh_flags;
  Elf32_Addr               sh_addr;
  Ref<unsigned char>       sh_offset;
  Elf32_Word               sh_size;
  Elf32_Word               sh_link;
  Elf32_Word               sh_info;
  Elf32_Word               sh_addralign;
  Elf32_Word               sh_entsize;
  
  void changeEndian();
  unsigned length(){return sh_size/sh_entsize;}
 };
 
//--------------------------------------------- ProgHeader
 struct ProgHeader
 {
  Elf32_Word         p_type;
  Ref<unsigned char> p_offset;
  Elf32_Addr         p_vaddr;
  Elf32_Addr         p_paddr;
  Elf32_Word         p_files;
  Elf32_Word         p_memsz;
  Elf32_Word         p_flags;
  Elf32_Word         p_align;
  
  void changeEndian();
 };

//--------------------------------------------- Symbol
 struct Symbol
 {
  enum Bind {STB_LOCAL=0,
             STB_GLOBAL=1,
	     STB_WEAK=2,
	     STB_LOPROC=13,
	     STB_HIPROC=15
	    };
	     
  enum Type {STT_NOTYPE = 0,
             STT_OBJECT = 1,
	     STT_FUNC   = 2,
	     STT_SECTION= 3,
	     STT_FILE   = 4,
	     STT_LOPROC =13,
	     STT_HIPROC =15
	    };

  Name          st_name;
  Elf32_Addr    st_value;
  Elf32_Word    st_size;
  unsigned char st_info;
  unsigned char st_other;
  Elf32_Half    st_shndx;

  void changeEndian();
  Bind bind()const{return (Bind)((st_info>>4)&0xf);}
  Type type()const{return (Type)((st_info   )&0xf);}
  bool isDefined()const{return st_shndx!=::elf::SHN_UNDEF;}

  bool isGlobal()const
  {
   Bind b=bind();
   return (b==STB_GLOBAL)||(b==STB_WEAK);
  }

  bool isRelocable() const
  {
   return (::elf::SHN_UNDEF<st_shndx)&&
          (st_shndx< ::elf::SHN_LORESERVE);
  }
 };

//--------------------------------------------- Rel
 struct Rel
 {
  Elf32_Addr r_offset;
  Elf32_Word r_info;
  void changeEndian();
  unsigned symbol()const{return r_info>>8;}
  unsigned char type()const{return (unsigned char)r_info;}
 };
 
//--------------------------------------------- RelA
 struct RelA:public Rel
 {
  Elf32_Sword r_addend;
  void changeEndian();
 };

//--------------------------------------------- Hash
 struct Hash
 {
  Elf32_Word nbucket;
  Elf32_Word nchain;
  Elf32_Word data[0];
 };

//------------------------- inline stuff
 inline void chgEndian(Elf32_Addr& v)
 {
  v=util::Endian::change((unsigned)v);
 }
 
 inline void chgEndian(Elf32_Half& v)
 {
  v=util::Endian::change((unsigned short)v);
 }
 
 inline void chgEndian(Elf32_Sword& v)
 {
  v=util::Endian::change((unsigned)v);
 }


 inline void Header::changeEndian()
 {
  chgEndian(e_type);
  chgEndian(e_machine);
  chgEndian(e_version);
  chgEndian(e_entry);
  chgEndian(e_phoff.idx);
  chgEndian(e_shoff.idx);
  chgEndian(e_flags);
  chgEndian(e_ehsize);
  chgEndian(e_pentsize);
  chgEndian(e_phnum);
  chgEndian(e_shentsize);
  chgEndian(e_shnum);
  chgEndian(e_shstrndx);
 }

 inline void SectionHeader::changeEndian()
 {
  chgEndian(sh_name.idx);
  chgEndian(sh_type);
  chgEndian(sh_flags);
  chgEndian(sh_addr);
  chgEndian(sh_offset.idx);
  chgEndian(sh_size);
  chgEndian(sh_link);
  chgEndian(sh_info);
  chgEndian(sh_addralign);
  chgEndian(sh_entsize);
 }

 inline void ProgHeader::changeEndian()
 {
  chgEndian(p_type);
  chgEndian(p_offset.idx);
  chgEndian(p_vaddr);
  chgEndian(p_paddr);
  chgEndian(p_files);
  chgEndian(p_memsz);
  chgEndian(p_flags);
  chgEndian(p_align);
 }
 
#if 1
 extern const char Signature[16];
 
 struct ElfletDesc
 {                      //'0123456789abcdef
  char  signature[16];  //      
  unsigned start;
  unsigned len;
  unsigned self;        //its onw position 
  unsigned entry;
  ProgHeader text;
  ProgHeader data;
  char desc[0];           //open ended string
  static void copy(const ProgHeader& ph,unsigned codeOffset);
  void relocate(unsigned codeOffset) const;
  void startIt(unsigned codeOffset) const __attribute__ ((noreturn));
  unsigned clone(ElfletDesc* dst,unsigned maxLen);
                           //because of open ended string 
			   //returns size in bytes
 };
#endif
}//namespace elf
#endif
