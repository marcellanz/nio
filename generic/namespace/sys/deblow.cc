//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#include "sys/sys.h"
IMPLEMENTATION(sys_deblow,$Revision: 137 $)
#include "sys/deb.h"
namespace sys
{
 namespace deb
 {
  void waitForKey(char ch)
  {
   out("press key '");out(ch);out("' ...\n\r");
   while(true)
   {
    if (get()==ch) break;
   }
  }
  
  void out(const char s[])
  {
   if (s==0) {out("(null)");return;}
   unsigned i=0;
   for(;;)
   {
    char ch=s[i++];
    if (ch=='\0') break;
    out(ch);
   }
  }

  void out(unsigned val)
  {
   static const char Digits[]="0123456789";
   char nbr[25]; //enough for 64 bit unsigned
   char* s=nbr; 
   do
   {
    *s++=Digits[val%10];
    val/=10;
   } while(val);
   do out(*--s); while(s!=nbr);
  }

  void out(int val)
  {
   if (val<0){out('-');out((unsigned)-val);}
	else {out((unsigned)val);}
  }

  void out(const void* val)
  {           //    0x12345678 
   if (val==0){out("  (null)  ");}
      else {out('0');out('x');hex((unsigned)val);}
  }
  
  void out(const char key[],unsigned val)
  {
   out(key);out(' ');out(val);newln();
  }

  void out(const char key[],const void* val)
  {
   out(key);out(' ');out(val);newln();
  }

  void out(const char key[],const unsigned val[],unsigned len)
  {
   out(key);out(' ');
   for(unsigned i=0;i<len;i++)
   {
    if (i>0) out('\t');
    out(val[i]);
   }
   newln();
  }

  void out(const char key[],const char val[]) 
  {
   out(key);
   out(' ');
   out(val);
   newln();
  }
  
  void newln()
  {
   out("\n\r");
  }

  void hexByte(unsigned char ch)
  {
   static const char Digits[]="0123456789abcdef";
   out(Digits[(ch>>4)&0xf]);
   out(Digits[(ch   )&0xf]);
  }
 
  void hexShort(unsigned short val)
  {
   static const char Digits[]="0123456789abcdef";
   out(Digits[(val>>12)&0xf]);
   out(Digits[(val>> 8)&0xf]);
   out(Digits[(val>> 4)&0xf]);
   out(Digits[(val    )&0xf]);
  }
   
  void hex(unsigned val)
  {
   static const char Digits[]="0123456789abcdef";
   char nbr[16]; //enough for 64 bit unsigned
   char* s=nbr;
   unsigned mask=1;
   while(mask)
   {
    *s++=Digits[val&0xf];
    val>>=4;
    mask<<=4;
   }
   do out(*--s);while(s!=nbr);
  }

  void hex(const char key[],unsigned val)
  {
   out(key);hex(val);newln();
  }

  void hex(const char key[],unsigned* val,unsigned len)
  {
   out(key);
   for(unsigned i=0;i<len;i++)
   {
    if (i>0) out(' ');
    hex(val[i]);
   }
   newln();
  }
  
  void dump(const unsigned char data[],unsigned len)
  {
   static const unsigned PER_LINE = 16;
   unsigned perLine=0;
   for(unsigned i=0;i<len;i++)
   {
    if (perLine==0)
       {
        newln();perLine=PER_LINE;
       }
       else out(' ');
    hexByte(data[i]);
    perLine--;    
   }
   newln();
  }

  void dump(const char s[],unsigned len)
  {
   for(unsigned i=0;i<len;i++) out(s[i]);
  }

  void test()
  {
  }  
 } 
}
