//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#ifndef SYS_SYS_H
#define SYS_SYS_H
//--------------------------
//sys startup 
//(c) H.Buchmann FHSO 2003
//$Id: sys.h 193 2006-01-11 15:21:16Z buchmann $
//--------------------------
#define IMPLEMENTATION(name,info) \
static void __code__(){} \
sys::Mod name  = {sys::Dep::MOD,__FILE__,#info,false,false,0,0,0,__code__,0,0,0,0};\
static sys::Mod* __m __attribute__((section(".depend"),used)) = &name; \
static void* __dso_handle = (void*)&__dso_handle;

#define INTERFACE(name,info) \
extern sys::Mod name; \
static sys::Inc  __inc__##name = {sys::Dep::INC,__INCLUDE_LEVEL__,&name};\
static sys::Inc* __inc__##name##_ __attribute__((section (".depend"),used)) = \
&__inc__##name;
//#define SYS_SYS_DEBUG

namespace sys
{
 struct Dep 
 {
  enum Type {MOD,INC};
  Type typ;
 };
 
 class Mod;
 struct Inc 
 {
  Dep::Type typ;
  unsigned includeLevel; 
  Mod* m;
 };

 
 class Mod 
 {
  public:
   typedef void (*global)();
   enum ErrorCode {OK,CONSTRUCTOR,DESTRUCTOR,CIRCULAR};
   
   class Iterator 
   {
    private:
     const Mod* root;
     const Inc* self; //as included
     unsigned includeLevel;
     int pos;
     
    public:
      Iterator(const Mod& mod);
     ~Iterator();
     void reset(const Mod& mod); //same like constructor
     void reset();               //same like constructor with the same mod
     bool hasNext() const;
     bool next();
     const char* getModName() const;
     unsigned getIncludeLevel() const;
   };
   
  private:
   static const char* ErrorCodeText[];
   static bool started;
   static ErrorCode errorCode; //0 means ok
   static Mod* first;          //anchor for link
   static Mod* last;           //Module in link list
   static Mod* cStart;         //for contruction
   static Mod* cEnd;           //for destruction
   static Mod* lastStarted;    //last started module
   static const char* prefix;
   static unsigned prefixSize;

//------------------------------------------ instance methods   
   void sort();
   void setConstructor(global g);
   void setDestructor(global g);
   void setInclude(Inc** inc,unsigned len);
   void linkIn();
#ifdef SYS_SYS_DEBUG
   void show();
   void showInc();
   void showTSort();
   static void showGlobal();
   static void showInclude();
   static void showModules();
   static void showForTsort();
   static void showStartupSequence();
#endif
      
   static void linkModules0(); // regex: (mod inc*)* see L0,L1 for decision
   static void linkModules1(); //        (inc* mod)*
   static void mkPrefix();
   static void matchGlobalConstructor();
   static void matchGlobalDestructor();
   static void sortModules();
   static void callConstructors();
   static void thumbStart();

   typedef void (Mod::*SetGlobal)(global g);
   static void match(global* g,SetGlobal s,ErrorCode cod);
   
  public: 
//must be all public because no constructor can be used
   const Dep::Type typ;
   const char* name;
   const char* info;
   bool visited;
   bool onStack;
   Mod* link;
   Mod* next; 
   Mod* prev; 
   global  code;
   global  constructor;
   global  destructor;
   unsigned incLen;
   Inc** inc;

   const char* getName() const; 
//------------------------------------------ class methods
   static void start();
   static void stop();
   static ErrorCode getErrorCode();
   static const char* getErrorCodeText();
   static const Mod* getFirst(){return first;}
   static const Mod* getStart(){return cStart;}  
 };
}
#endif
