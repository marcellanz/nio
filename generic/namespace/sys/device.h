//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#ifndef SYS_DEVICE_H
#define SYS_DEVICE_H
INTERFACE(sys_device,$Revision: 143 $)
//------------------------------
//Device/List
//(c) H.Buchmann FHSO 2003
//$Id: device.h 193 2006-01-11 15:21:16Z buchmann $
//------------------------------ 
#include "io/ascii/write.h"
namespace sys
{
//------------------------------------------------ Device
  class Device
 {
  public:
//------------------------------------------------ List   
   class List //of all available devices
   {
    private:
     List(const List&);
     List& operator=(const List&);

     friend class Device;
     Device*   anchor;
     Device*   default_;
     
     unsigned  nbr;
     Device* getDev(const char name[]) const;
     Device* getDev(const unsigned id) const;
          
    public:
              List();
     virtual ~List();
     Device& get(const char name[]) const;      //stops if not found
     Device& get(unsigned idx) const;      //stops if not found
     Device& get() const;   //default device stops if not found 
     bool exists(const char name[]) const{return getDev(name)!=0;}
     bool exists(unsigned id) const{return getDev(id)!=0;}
     
     unsigned getNbr()const {return nbr;}
     void info(io::ascii::Writer& out) const;
   };

   
  private:
   Device(const Device&);
   Device& operator=(const Device&);

   const char* name;
   List& lst;    //(perhaps) used in destructor
   Device* next; //doubly linked list useful for deleting
   Device* prev;

  protected:
            Device(const char name[],List& lst,bool default_);
   virtual ~Device();
   const char* getName() const{return name;}

  public:
   virtual io::ascii::Writer& info(io::ascii::Writer& out) const=0;
       //chaining of ASCIIWriter possible
   friend io::ascii::Writer& operator <<(io::ascii::Writer& out,Device& d)
                             {return d.info(out);}
 };
}
#endif
