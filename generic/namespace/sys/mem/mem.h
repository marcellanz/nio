//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#ifndef SYS_MEM_MEM_1_H
#define SYS_MEM_MEM_1_H
//----------------------
//mem-1 yet a new mem
//(c) H.Buchmann FHSO 2004
//$Id: mem.h 193 2006-01-11 15:21:16Z buchmann $
//----------------------
INTERFACE(sys_mem_mem,$Revision: 137 $)
#include "sys/mem/chunk.h"
#include "io/ascii/out.h"
extern bool debug;
namespace sys
{
 namespace mem
 {
#ifdef __arm__
  typedef unsigned long size_t;
#else
  typedef unsigned size_t;
#endif
  enum Align  { A_0 = (1<< 0),
        	A_1 = (1<< 1),
		A_2 = (1<< 2),
        	A_3 = (1<< 3),
        	A_4 = (1<< 4),
		A_5 = (1<< 5),
		A_6 = (1<< 6),
		A_7 = (1<< 7),
		A_8 = (1<< 8),
		A_9 = (1<< 9),
	       A_10 = (1<<10),
	       A_11 = (1<<11),
	       A_12 = (1<<12),   //4kb
	       A_13 = (1<<13),
	       A_14 = (1<<14),
	       A_15 = (1<<15),
	       A_16 = (1<<16),  
	       A_17 = (1<<17),
	       A_18 = (1<<18),
	       A_19 = (1<<19),
	       A_20 = (1<<20)
	     };

  class Memory
  {
   public:
   private:
    Memory(const Memory&);
    Memory& operator=(const Memory&);

//--------------------------------------------------- Block
    class Free;
    struct Block
    {
     Block* lo; //lower in memory
     Block* hi; //higer in memory
     Block(); //do nothing
     Block(unsigned size_byte);
     Block(Block* encl); //works only if encl ist *not* last
                         //block in chunk
     Block(const Block&);//not implemented
     unsigned getLen_byte()const{return (unsigned char*)hi-(unsigned char*)this;}
     void* operator new(size_t siz,void* data);
     virtual Block* mergeWithHi(Free* anchor); 
                //default: assuming Block is not a Free block
		//         return hi->hi
     virtual void mergeWithLo(); 
                //default: assuming Block is not a Free block
		//do nothing         
     
//--------------------------------------------------- debug
     virtual io::ascii::Writer& show(io::ascii::Writer&) const;
     virtual bool test() const;
     friend io::ascii::Writer& operator<<(io::ascii::Writer& out,
                                          const Block& bl)
     {return bl.show(out);}					  
    };
    
//--------------------------------------------------- Free
    class Chunk;
    class Alloc; 
    struct Free:public Block
    {
     Free* prev; //ring 
     Free* next;
     Free(Free* lnk,Chunk* encl);
     Free(Free* lnk);
     Free(); //used only for anchor free0
     unsigned getAvailableSize()const{return getLen_byte()-ALLOC_SIZE;}
     void unlink();
     Block* mergeWithHi(Free* anchor);
     void mergeWithLo();
     void* alloc(unsigned size_byte_incl); //inclusive admin overhead
     void* alloc(unsigned size_byte,Align align);
            //returns != null if done
//--------------------------------------------------- debug
     io::ascii::Writer& show(io::ascii::Writer&) const;
     bool test() const;
    };

//--------------------------------------------------- Alloc
    struct Alloc:public Block
    {
//--------------------------------------------------- Alloc::Reference
     struct Reference
     {
      Alloc* alloc;
      unsigned char data[0];
     };
     
     Reference  ref[0];

     Alloc(Block*encl);
     Alloc(); 
//could be subclassed
     Alloc(Block*encl,Reference* ref);
     Alloc(Reference* ref);
     io::ascii::Writer& show(io::ascii::Writer& out) const; 
    };    
    
//--------------------------------------------------- Chunk
    struct Chunk:public Block
    {
     Chunk* cPrev;
     Chunk* cNext;
     Block* end;
     unsigned  blk[0];  //lfree space
     Chunk();
     Chunk(Chunk* lnk,unsigned size_byte);
     io::ascii::Writer& show(io::ascii::Writer&) const;
     unsigned getChunkLen_byte() const;
     bool test() const;
     bool testAll() const; //the chunk and all block in it
    };

//--------------------------------------------------- ChunkEnd
    struct ChunkEnd:public Block
    {
     ChunkEnd(Free* encl);
     io::ascii::Writer& show(io::ascii::Writer&) const;
     bool test() const;
    };
    
//--------------------------------------------------- some constants
//all values in bytes
    static const unsigned ALLOC_SIZE = sizeof(Alloc)+sizeof(Alloc::Reference);
    static const unsigned MIN_ALLOC  = sizeof(Block);
    static const unsigned GAP        = (ALLOC_SIZE>?sizeof(Free))+MIN_ALLOC;
    static const Align    MIN_ALIGN  = A_2;
    
//--------------------------------------------------- instance variables
   private: 
    Free  free0;    //free  list never empty
    Chunk chunk0;   //chunk list never empty

   protected:
//--------------------------------------------------- error handling
    virtual void onFull(); //will be called if no memory available
    
   public:
    Memory();
//--------------------------------------------------- building memory
    void add(const mem::Chunk* c);
    
//--------------------------------------------------- getting/releasing memory
    void* alloc(unsigned size_byte);
    void* alloc(unsigned size_byte,Align align);
                                 //align power of 2
    void dealloc(void*);
//--------------------------------------------------- info
    unsigned getLargestBlock() const;
    
//--------------------------------------------------- debug
    io::ascii::Writer& showChunk(io::ascii::Writer&) const;
    io::ascii::Writer& showFree(io::ascii::Writer&) const;
    io::ascii::Writer& show(io::ascii::Writer&) const;
    bool testChunk() const;
    bool testFree() const;
    bool check() const;    
  };
 }//namespace mem
}//namespace sys
#endif
