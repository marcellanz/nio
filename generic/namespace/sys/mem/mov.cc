//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//------------------------------------
//mem-move 
//(c) H.Buchmann FHSO 2003
//$Id: mov.cc 193 2006-01-11 15:21:16Z buchmann $
//------------------------------------
#include "sys/sys.h"
IMPLEMENTATION(sys_mem_mov,$Revision: 137 $)
#include "sys/mem/mov.h"

#ifdef MEM_MOVER_DEBUG
#include "sys/msg.h"
#endif

namespace sys
{

 namespace mem
 {
#if !((defined __LITTLE_ENDIAN__ || defined  __BIG_ENDIAN__))
#error no endian defined
#else
#warning not yet in final form
#endif 
//  template<unsigned v>unsigned change=v;
  const unsigned Mover::Mask[]=
  {             //              d 
#ifdef __LITTLE_ENDIAN__
   0x00000000,  // 00 00 00 00  0 never used
   0xff000000,  // 00 00 00 ff  1
   0xffff0000,  // 00 00 ff ff  2
   0xffffff00   // 00 ff ff ff  3
#endif
#ifdef __BIG_ENDIAN__
   0x00000000,  // 00 00 00 00  0 never used
   0x000000ff,  // 00 00 00 ff  1
   0x0000ffff,  // 00 00 ff ff  2
   0x00ffffff   // 00 ff ff ff  3
#endif
  };


  const unsigned Mover::Lower[]=
  {             //              d 
#ifdef __LITTLE_ENDIAN__
   0x00000000,  // 00 00 00 00  0
   0xff000000,  // 00 00 00 ff  1
   0xffff0000,	// 00 00 ff ff  2
   0xffffff00	// 00 ff ff ff  3
#endif   
#ifdef __BIG_ENDIAN__
   0x00000000,  // 00 00 00 00  0
   0x000000ff,  // 00 00 00 ff  1
   0x0000ffff,	// 00 00 ff ff  2
   0x00ffffff	// 00 ff ff ff  3
#endif
  };

  const unsigned Mover::Upper[]=
  {
#ifdef __LITTLE_ENDIAN__
   0x00000000,  // 00 00 00 00  0
   0x000000ff,	// ff 00 00 00  1
   0x0000ffff,	// ff ff 00 00  2 
   0x00ffffff	// ff ff ff 00  3
#endif   
#ifdef __BIG_ENDIAN__
   0x00000000,  // 00 00 00 00  0
   0xff000000,	// ff 00 00 00  1
   0xffff0000,	// ff ff 00 00  2 
   0xffffff00	// ff ff ff 00  3
#endif
  };
  

  Mover::Move_d Mover::CpyFWD[]=
  {
   cpyFWD0,    //<--------------
   cpyFWD<1>, //LITTLE
   cpyFWD<2>,
   cpyFWD<3>
  };

  Mover::Move_d Mover::CpyBWD[]=
  {
   cpyBWD0,    //<--------------
   cpyBWD<1>, //BIG
   cpyBWD<2>,
   cpyBWD<3>
  };
  
  template<int d>
  inline unsigned Mover::ror(unsigned v)
  {
   static const int sh=(util::Endian::SystemEndian==util::Endian::LITTLE)?
                       -d:d; 
   static const int shift=(sh>0)?(8*sh):(32+8*sh);
  #ifdef i386 
   asm volatile
   (
    "\t#-------------- ror\n\t"
    "rorl %[d],%[output]\n\t"
     :[output] "+r"(v)
    :[input]  "r"(v),[d] "i"(shift)
   );
   return v;
  #endif 
  #ifdef __arm__
   asm volatile
   (
    "\t@-------------- ror\n\t"
    "mov %[input],%[output],ROR %[d]\n\t"
    :[output] "+r"(v)
    :[input]  "r"(v),[d] "i"(shift)
   );
   return v;
  #endif
  }
  
//-------------------------- forward - FWD - section
  inline void Mover::cpyFWD(unsigned char* dst,unsigned char* src,unsigned n)
  {
   while(n>0)
   {
    n--;
    *dst++=*src++;
   }
  }

  void Mover::cpyFWD0(unsigned* dst,unsigned* src,unsigned n)
  {
#ifndef __arm__
   Chunk* ccDst=(Chunk*)dst;
   Chunk* ccSrc=(Chunk*)src;
#endif
   while(n>=sizeof(Chunk))
   {
#ifdef __arm__
    register unsigned r0 asm("r0");
    register unsigned r1 asm("r1");
    register unsigned r2 asm("r2");
    register unsigned r3 asm("r3");
    
    asm volatile
    (
     "@----------- copyBWD Chunk --------------\n\t"
     "ldmia %[src]!,{%[r0],%[r1],%[r2],%[r3]}\n\t" 
     "stmia %[dst]!,{%[r0],%[r1],%[r2],%[r3]}\n\t"
     "ldmia %[src]!,{%[r0],%[r1],%[r2],%[r3]}\n\t" 
     "stmia %[dst]!,{%[r0],%[r1],%[r2],%[r3]}\n\t"
     "ldmia %[src]!,{%[r0],%[r1],%[r2],%[r3]}\n\t" 
     "stmia %[dst]!,{%[r0],%[r1],%[r2],%[r3]}\n\t"
     "ldmia %[src]!,{%[r0],%[r1],%[r2],%[r3]}\n\t" 
     "stmia %[dst]!,{%[r0],%[r1],%[r2],%[r3]}\n\t"
     :[dst]"=r"(dst),
      [r0] "+r"(r0),
      [r1] "+r"(r1),
      [r2] "+r"(r2),
      [r3] "+r"(r3)
     :[src]"r"(src)
    );
#else
//    *((Chunk*)dst)++=*((Chunk*)src)++;
      *ccDst++=*ccSrc++;   
#endif 
    n-=sizeof(Chunk);
   }
#ifndef __arm__
   src=(unsigned*)ccSrc;
   dst=(unsigned*)ccDst;
#endif
   
   while(n>=sizeof(unsigned))
   {
    *dst++=*src++;
    n-=sizeof(unsigned);
   }
   unsigned char* cDst=(unsigned char*)dst;
   unsigned char* cSrc=(unsigned char*)src;
   while(n-->0) //0<=n<4
   {
    *cDst++=*cSrc++;
   }
   
  }

  template<int d>
  void Mover::cpyFWD(unsigned* dst,unsigned* src,unsigned n)
  {
// dst aligned  points to end (one word pos beyond last)
// src aligned  src+d  points to end 
// d>0
   unsigned s=ror<-d>(*src++);
   while(n>=4)
   {
    *dst=s&~Mask[d];
    s=ror<-d>(*src++);
    *dst++|=s&Mask[d];
    n-=4;    
   }
   *dst&=~Upper[n];
   *dst|=(s&~Mask[d]|(ror<-d>(*src)&Mask[d]))&Upper[n];
  }

  
  void* Mover::cpyFWD(void *dst, const void *src, unsigned n)
  {
   if ((dst==0)||(src==0)) return dst;
   void* dst0=dst; //for return
   
   unsigned char* cDst=(unsigned char*)dst;
   unsigned char* cSrc=(unsigned char*)src;
   while(((unsigned)cDst&0x3)&&(n>0))
   {
    n--;
    *cDst++=*cSrc++;
   }
   dst=cDst;
   src=cSrc;
   if (n<=4) 
      {
       cpyFWD((unsigned char*)dst,(unsigned char*)src,n);
       return dst0;
      }
   CpyFWD[(unsigned)src&0x3]((unsigned*)dst,
                             (unsigned*)((unsigned)src&~0x3),
			      n);
   return dst0;
  }


//-------------------------- backward - BWD - section
  inline void Mover::cpyBWD(unsigned char* dst,unsigned char* src,unsigned n)
  {
   while(n>0)
   {
    n--;
    *--dst=*--src;
   }
  }
  
  void  Mover::cpyBWD0(unsigned* dst,unsigned* src,unsigned n)
  {
//   sys::msg<<"d = 0 n= "<<n<<"\n";
//copy chunks only for arm
#ifdef __arm__
   while(n>=sizeof(Chunk))
   {
//    sys::msg<<".\n";
    register unsigned r0 asm("r0");
    register unsigned r1 asm("r1");
    register unsigned r2 asm("r2");
    register unsigned r3 asm("r3");
    
    asm volatile
    (
     "@----------- copyBWD Chunk --------------\n\t"
     "ldmdb %[src]!,{%[r0],%[r1],%[r2],%[r3]}\n\t" 
     "stmdb %[dst]!,{%[r0],%[r1],%[r2],%[r3]}\n\t"
     "ldmdb %[src]!,{%[r0],%[r1],%[r2],%[r3]}\n\t" 
     "stmdb %[dst]!,{%[r0],%[r1],%[r2],%[r3]}\n\t"
     "ldmdb %[src]!,{%[r0],%[r1],%[r2],%[r3]}\n\t" 
     "stmdb %[dst]!,{%[r0],%[r1],%[r2],%[r3]}\n\t"
     "ldmdb %[src]!,{%[r0],%[r1],%[r2],%[r3]}\n\t" 
     "stmdb %[dst]!,{%[r0],%[r1],%[r2],%[r3]}\n\t"
     :[dst]"=r"(dst),
      [r0] "+r"(r0),
      [r1] "+r"(r1),
      [r2] "+r"(r2),
      [r3] "+r"(r3)
     :[src]"r"(src)
    );
    n-=sizeof(Chunk);
   }
#endif
   while(n>=4)
   {
    *--dst=*--src;
    n-=4;
   }
//   sys::msg<<"n= "<<n<<"\n";
//copy bytes 
#if 0  
   while(n>0)
   {
//    sys::msg<<"-\n";
    *--(unsigned char*)dst=*--(unsigned char*)src;
    n--;
   }
#endif   
   unsigned char* cDst=(unsigned char*)dst;
   unsigned char* cSrc=(unsigned char*)src;
   while(n>0)
   {
//    sys::msg<<"-\n";
    *--cDst=*--cSrc;
    n--;
   }
  }
  
  template<int d>
  void Mover::cpyBWD (unsigned* dst,unsigned* src,unsigned n)
  {
// dst aligned
// src aligned src+d  pos
// d>0
   unsigned s=ror<-d>(*src--);
   dst--;
   while(n>=4)
   {
    *dst=(s&Mask[d]);
    s=ror<-d>(*src--);
    *dst--|=s&~Mask[d];
    n-=4;
   }
   *dst&=~Lower[n];
   *dst|=((s&Mask[d]) | (ror<-d>(*src)&~Mask[d]))& Lower[n];
  } 

  void* Mover::cpyBWD(void *dst, const void *src, unsigned n)
  {
   if ((dst==0)||(src==0)) return dst;
   unsigned char* dst1=(unsigned char*)dst+n;
   unsigned char* src1=(unsigned char*)src+n;
   while(((unsigned)dst1&0x3)&&(n>0))
   {
    *--dst1=*--src1;
    n--;
   }
   if (n<=4) 
      {
       cpyBWD(dst1,src1,n);
       return dst;
      }
//dst1 aligned   
   CpyBWD[(unsigned)src1 & 0x3]((unsigned*)dst1,
                               (unsigned*)((unsigned)src1&~0x3),
				n);
   return dst;
  }

//-------------------------- memset
  void* Mover::set(void *s, int c, unsigned n)
  {
   if (s==0) return 0;
   unsigned char* cs=(unsigned char*)s;
   while(((unsigned)cs&0x3)&&(n>0))
   {
    n--;
    *cs++=(unsigned char)c;
   }
   unsigned* us=(unsigned*)cs;
//s aligned
   unsigned v=(unsigned)((c<<24)|(c<<16)|(c<<8)|c);
   while(n>=4)
   {
    *us++=v;
    n-=4;
   }
   cs=(unsigned char*)us;
   while(n>0)
   {
    n--;
    *cs++=(unsigned char)c;
   }
   return s;
  }

  void* Mover::mov(void *dst, const void *src, unsigned n)
  {
   return (dst<=src)?cpyFWD(dst,src,n):cpyBWD(dst,src,n);
  }

#ifdef MEM_MOVER_DEBUG
  void Mover::byteOut(const char title[],unsigned v)
  {
   sys::msg.hex()<<title<<" '";
   static char Digits[]="0123456789abcdef";
   unsigned char* bv=(unsigned char*)&v;
   for(unsigned i=0;i<4;i++)
   {
    unsigned val=bv[i];
    sys::msg<<Digits[(val>>4)&0xf]<<Digits[val&0xf];
   }
   sys::msg<<"' "<<v<<"\n";
  }
#endif
 }  
}

//------------------------------- interface ISO 9899
extern "C" void* memcpy(void *dst, const void *src, unsigned n) 
{
// sys::msg<<"memmove dst: "<<dst<<" src: "<<src<<" n: "<<n<<"\n";
 return sys::mem::Mover::cpyFWD(dst,src,n);
}

extern "C" void* memmove(void *dst, const void *src, unsigned n) 
{
// sys::msg<<"memmove dst: "<<dst<<" src: "<<src<<" n: "<<n<<"\n";
 return sys::mem::Mover::mov(dst,src,n);
}

extern "C" void* memset(void *s,int c, unsigned n) 
{
 return sys::mem::Mover::set(s,c,n);
}

#ifdef MEM_MOVER_DEBUG
//------------------------------------
class Tester
{
 static Tester tester;
 Tester();
};

Tester Tester::tester;

Tester::Tester()
{
 sys::msg<<__PRETTY_FUNCTION__<<"\n";
 static const unsigned short Data[]={1,2,3};
 
 unsigned short snk=100;
 sys::mem::Sink<unsigned short> sink(&snk);
 sink.put(Data,0);
 sys::msg<<"snk "<<snk<<"\n"; 
}
#endif
