//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#ifndef SYS_MEM_CHUNK_H
#define SYS_MEM_CHUNK_H
//------------------------------
//chunk 
// used at various places
//(c) H.Buchmann FHSO 2003
//$Id: chunk.h 221 2006-03-11 17:57:03Z buchmann $
//------------------------------
INTERFACE(sys_mem_chunk,$Revision: 137 $)
#include "io/ascii/write.h"
namespace sys
{
 namespace mem
 {
  struct Chunk
  {
   Chunk(const Chunk& ch); //copyable
   Chunk(unsigned*const start,unsigned len_word);
   unsigned*const   start;
   const unsigned   len_word;    // size in *word*
   io::ascii::Writer& show(io::ascii::Writer& out) const;
   friend io::ascii::Writer& operator <<(io::ascii::Writer& out,
                                         const Chunk& ch)
			     {return ch.show(out);}		 
  };
 }
}
#endif

