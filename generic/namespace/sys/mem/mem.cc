//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//----------------------
//mem-1 yet a new mem
//(c) H.Buchmann FHSO 2004
// must be compiled with -fno-strict-aliasing
// see Alloc(Free&)
//$Id: mem.cc 193 2006-01-11 15:21:16Z buchmann $
//----------------------
#include "sys/sys.h"
IMPLEMENTATION(sys_mem_mem,$Revision: 137 $)
#include "sys/mem/mem.h"
#include "sys/mem/chunk.h"
#include "sys/msg.h"
namespace sys
{
 namespace mem
 {
//--------------------------------------------------- Block
  inline Memory::Block::Block()
  {
  }

  inline Memory::Block::Block(unsigned size_byte)
  :lo(this),hi((Block*)((unsigned char*)this+size_byte))
  {
  }
  
  inline Memory::Block::Block(Block* encl)
  :lo(encl),hi(encl->hi)
  {
   encl->hi=this;
//   hi->lo  =this; see Alloc(Block* encl)
  }
  
  Memory::Block* Memory::Block::mergeWithHi(Free* anchor)
  {
   Block* b=hi;
   return (new (b) Free(anchor))->hi;    
  }

  void Memory::Block::mergeWithLo()
  {
  }
  
  io::ascii::Writer& Memory::Block::show(io::ascii::Writer& out) const
  {
   out<<this<<" "<<(void*)lo<<" "<<(void*)hi
      <<io::ascii::setw(7)<<getLen_byte()<<" "<<test();
   return out;
  }

  inline void* Memory::Block::operator new(size_t siz,void* data)
  {
   return data;
  }
  
  bool Memory::Block::test() const
  {
   return (lo->hi==this) && (hi->lo==this);
  }

//--------------------------------------------------- Free
  inline Memory::Free::Free()
  :Block(sizeof(Free)/4),
   next(this),   prev(this)
  {
  }
  
  inline Memory::Free::Free(Free* lnk,Chunk* encl)
  :Block(encl),
   prev(lnk),
   next(lnk->next)
  {
   lnk->next->prev=this;
   lnk->next=this;
  }
  
  inline Memory::Free::Free(Free* lnk)
  :Block(), //dont change lo/hi
   prev(lnk),next(lnk->next)
  {
   lnk->next->prev=this;
   lnk->next=this;
  }

  inline void Memory::Free::unlink()
  {
   next->prev=prev;
   prev->next=next;
  }
  
  inline void* Memory::Free::alloc(unsigned size_byte_incl)
  {
   unsigned siz=getLen_byte();
   if  (siz < size_byte_incl) return 0;
   if ((siz - size_byte_incl)<=GAP)
      { //whole block
        unlink();
	Alloc* a=new(this)Alloc();
	return a->ref[0].data;
      }
   Alloc* a=new ((unsigned char*)hi-size_byte_incl) Alloc(this);
   return a->ref[0].data; 
  }
  
  inline void* Memory::Free::alloc(unsigned size_byte,Align align)
  {
   Alloc* a0= (Alloc*)this;           //the lowest possible value free 
   unsigned d=(unsigned)((unsigned char*)hi-size_byte);
          //^should be optimized away
   Alloc*  a1=(Alloc*)((d&~(align-1))-ALLOC_SIZE);
   
   if (a1<a0) return 0; //no space 
   
   if (((unsigned char*)a1-(unsigned char*)a0)<=GAP)
      {//whole block
       unlink();
       Alloc* a=new(this)Alloc(a1->ref);
       return a1->ref[0].data;       
      }
   Alloc* a=new(a1)Alloc(this,a1->ref);   
   return a1->ref[0].data;
  }

  Memory::Block* Memory::Free::mergeWithHi(Free* anchor)
  {
   hi->hi->lo=this;
   hi=hi->hi;
   return hi;
  }
  
  void Memory::Free::mergeWithLo()
  {
   unlink();
   lo->hi=hi;
   hi->lo=lo;
  }
  
  bool Memory::Free::test() const
  {
   return Block::test()&&(prev->next==this)&&(next->prev==this);
  }
  
  io::ascii::Writer& Memory::Free::show(io::ascii::Writer& out) const
  {
   return Block::show(out<<" F ")<<io::ascii::setw(7)<<getAvailableSize()
                                 <<" "<<(void*)prev<<" "<<(void*)next;
  }
  
//--------------------------------------------------- Alloc
  inline Memory::Alloc::Alloc(Block* encl)
  :Block(encl)
  {
   hi->lo=this; 
   ref[0].alloc=this;
  }

  inline Memory::Alloc::Alloc(Block* encl,Reference* ref)
  :Block(encl)
  {
   hi->lo=this;
   ref->alloc=this;
  }
  
  inline Memory::Alloc::Alloc()
  {
   ref[0].alloc=this;
  }

  inline Memory::Alloc::Alloc(Reference* ref)
  {
   ref->alloc=this;
  }

  io::ascii::Writer& Memory::Alloc::show(io::ascii::Writer& out) const
  {
   return Block::show(out<<" A ");
  }


//--------------------------------------------------- Chunk
  inline Memory::Chunk::Chunk()
  :Block(0u),
   cPrev(this),cNext(this),
   end(this)
  {
  }

  inline Memory::Chunk::Chunk(Chunk* lnk,unsigned size_byte)
  :Block(size_byte),
   cPrev(lnk),
   cNext(lnk->cNext),
   end((Block*)((unsigned char*)this+size_byte))
  {
   lnk->cNext->cPrev=this;
   lnk->cNext=this;
  }

  unsigned Memory::Chunk::getChunkLen_byte() const
  {
   return (unsigned)((unsigned char*)end-(unsigned char*)this);
  }
  
  io::ascii::Writer& Memory::Chunk::show(io::ascii::Writer& out) const
  {
   Block::show(out<<"C0 ")
                  <<io::ascii::dec()<<io::ascii::setw(7)
		  <<getChunkLen_byte()
		  <<" "<<(void*)end<<"\n";
   unsigned siz=getLen_byte();             
   const Block* b=hi;
   while(b!=end)
   {
    siz+=b->getLen_byte();
    out<<*b<<"\n";
    b=b->hi;
   }
   out<<"total size= "<<siz<<"\n";
   return out;
  }

  bool Memory::Chunk::test() const
  {
   return (lo==this)&&(hi->lo==this);
   
  }
  
  bool Memory::Chunk::testAll() const
  {
   
   if (!((lo==this)&&(hi->lo==this))) return false;
   unsigned siz=getLen_byte();
   const Block* b=hi;
   while(b!=end)
   {
    if (!b->test()) return false;
    siz+=b->getLen_byte();
    b=b->hi;
   }
   return siz==getChunkLen_byte();
  }

//--------------------------------------------------- ChunkEnd
  inline Memory::ChunkEnd::ChunkEnd(Free* encl)
  :Block(encl)
  {
  }
    
  io::ascii::Writer& Memory::ChunkEnd::show(io::ascii::Writer& out) const
  {
   return Block::show(out<<"C1 ");
  }
  
  bool Memory::ChunkEnd::test() const
  {
   return (lo->hi==this);
  }
  
   
//--------------------------------------------------- Memory
  Memory::Memory()
  {
   if (MIN_ALIGN!=sizeof(unsigned))
      sys::msg.error()<<__FILE__<<":"<<__LINE__<<" align\n";
  }
  
  void Memory::add(const mem::Chunk* c)
  {
   unsigned len_byte=4*c->len_word;
   Chunk*    start=new(c->start)Chunk(&chunk0,len_byte);
   Free*     first=new(start->blk)Free(&free0,start);
   ChunkEnd* end  =new((unsigned char*)c->start+(len_byte-sizeof(ChunkEnd)))ChunkEnd(first);
  }

  void* Memory::alloc(unsigned size_byte)
  {
//align and add admin ALLOC_SIZE
   unsigned size_byte_incl=ALLOC_SIZE+
                          (MIN_ALLOC>?(4+(~0x3&(size_byte-1))));
// must be calculated only once  
 
   Free* f=free0.next; //free0 only anchor
   while(f!=&free0)
   {
    void* r=f->alloc(size_byte_incl);
    if (r) return r;
    f=f->next;
   }
   onFull(); //no memory available
   return 0; 
  }

  void* Memory::alloc(unsigned size_byte,Align align)
  {
   if (align<=MIN_ALIGN) return alloc(size_byte);
   size_byte=MIN_ALLOC>?(4+(~0x3&(size_byte-1)));
   Free* f=free0.next; //free0 only anchor
   while(f!=&free0)
   {
    void* r=f->alloc(size_byte,align);
    if (r) return r;
    f=f->next;
   }
   onFull(); //no memory available
   return 0; 
  }

  void Memory::dealloc(void* m)
  {
   Alloc* a=((Alloc::Reference*)
             ((unsigned char*)m-sizeof(Alloc::Reference)))->alloc;
   a->lo->mergeWithHi(&free0)->mergeWithLo();
  }

  unsigned Memory::getLargestBlock() const
  {
   unsigned siz=0;
   const Free* f=free0.next;
   while(f!=&free0)
   {
    siz=siz>?f->getAvailableSize();
    f=f->next;
   }
   return siz;
  }
  
//--------------------------------------------------- error handling
  void Memory::onFull()
  {
   sys::msg.error()<<"no more memory available\n";
  }

  io::ascii::Writer& Memory::showChunk(io::ascii::Writer& out) const
  {
   out<<"-------------------------------- chunks\n";
   const Chunk* ch=chunk0.cNext;
   while(ch!=&chunk0)
   {
    out<<*ch;
    ch=ch->cNext;
   }
   return out;
  }
  
  io::ascii::Writer& Memory::showFree(io::ascii::Writer& out) const
  {
   out<<"-------------------------------- free blocks\n";
   const Free* f=free0.next;
   while(f!=&free0)
   {
    out<<*f<<"\n";
    f=f->next;
   }
   return out;
  }

  io::ascii::Writer& Memory::show(io::ascii::Writer& out) const
  {
   showChunk(out);
   showFree(out);
   return out<<"Largest Block "<<getLargestBlock()<<"\n";
  }
  
  bool Memory::testChunk() const
  {
   const Chunk* ch=chunk0.cNext;
   while(ch!=&chunk0)
   {
    if (!ch->testAll()) return false;
    ch=ch->cNext;
   }
   return true;
  }
  
  bool Memory::testFree() const
  {
   const Free* f=free0.next;
   while(f!=&free0)
   {
    if (!f->test()) return false;
    f=f->next;
   }
   return true;
  }
  
  bool Memory::check() const
  {
   return testChunk()&&testFree();
  }
 }//namespace mem
}//namespace sys
