//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//------------------------------------
//mem-mov
//(c) H.Buchmann FHSO 2003
//$Id: mov.h 193 2006-01-11 15:21:16Z buchmann $
//ToDo: better endian recognition without defines
//    : test of speed  
//------------------------------------
INTERFACE(sys_mem_mov,$Revision: 137 $)
#include "util/endian.h"
//#define MEM_MOVER_DEBUG
#include "sys/msg.h"
namespace sys
{
 namespace mem
 {
//---------------------------------------------- Mem
//implements classical UNIX mem* stuff
  class Mover
  {
   private:
    Mover(const Mover&);
    Mover();
    static const unsigned Mask[];
    static const unsigned Lower[];
    static const unsigned Upper[];
    
    typedef void (*Move_d)(unsigned* dst,unsigned* src,unsigned n);

    static Move_d  CpyFWD[];
    static Move_d  CpyBWD[];

    template<int d>static unsigned ror(unsigned v);
//--------------------------------- 
//FWD forward
//BWD backward
//--------------------------------- FWD
    struct Chunk{unsigned d[16];}; //used for block copy
                          //^dont change this number
    static void cpyFWD(unsigned char* dst,unsigned char* src,
			  unsigned n);  //bytewise n small
                  static void cpyFWD0(unsigned* dst,unsigned* src,
		                       unsigned n);
    template<int d>static void cpyFWD (unsigned* dst,unsigned* src,
                                       unsigned n); 



//--------------------------------- BWD
    static void cpyBWD(unsigned char* dst,unsigned char* src,
                       unsigned n);  //bytewise n small
                   static void cpyBWD0(unsigned* dst,unsigned* src,
		                       unsigned n);
    template<int d>static void cpyBWD (unsigned* dst,unsigned* src,
                                       unsigned n); 

#ifdef MEM_MOVER_DEBUG
    static void byteOut(const char title[],unsigned v);
#endif
    
   public:
    typedef void* (*Move)(void* dst,const void* src,unsigned n);
    
    static void* cpyBWD(void *dst, const void *src, unsigned n);
    static void* cpyFWD(void *dst, const void *src, unsigned n);
    static void* mov(void *dst, const void *src, unsigned n);
    static void* set(void *s, int c, unsigned n);
  }; //Mem
  
//---------------------------------------------- Sink
  template<typename Typ>
  class Sink
  {
   private:
    Sink(const Sink&);
    volatile Typ*const port;
    
   public:
             Sink(Typ*const port):port(port){}
    virtual ~Sink(){}
    void put(const Typ src[],unsigned siz)
                                      //siz in Typ
    {
     while(siz--)*port=*src++;
    }
  };
  
 } //namespace mem
} //namespace sys

//------------------------------- interface ISO 9899
extern "C" void* memcpy(void *dst, const void *src, unsigned n) 
                 __attribute__((weak));
extern "C" void* memmove(void *dst, const void *src, unsigned n) 
                 __attribute__((weak));
extern "C" void* memset(void *s,int c, unsigned n) 
                 __attribute__((weak));
		 
