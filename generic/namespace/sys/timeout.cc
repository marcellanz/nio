//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//-----------------------------
//timeout
//(c) H.Buchmann FHSO 2002
//$Id: timeout.cc 193 2006-01-11 15:21:16Z buchmann $
//-----------------------------
#include "sys/sys.h"
IMPLEMENTATION(sys_timeout,$Revision: 137 $)
#include "sys/timeout.h"
//#include "sys/msg.h"

namespace sys
{
 const char* Timeout::StateName[]=
             {
	      "INACTIVE",
	      "ACTIVE",
	      "TIMED_OUT"
	     };
	     
 void Timeout::Entry::remove()
 {
  state=INACTIVE;
  if (owner==0) return;
  if (owner->anchor==0) return; //never happens but
  if (prev) {prev->next=next;prev=0;}
     else   owner->anchor=next;  
  if (next) 
     {
      next->dt+=dt;
      next->prev=prev;next=0;
     }
 }

 Timeout::Timeout():
 anchor(0)
 {
 }
 
 Timeout::~Timeout()
 {
 }
 
 void Timeout::add(Entry& e,unsigned at)
 {
  if (e.state==ACTIVE) e.remove(); 
  e.owner=this;
  e.state=ACTIVE;
  if (anchor==0)
     {
      e.dt=at;
      e.prev=0;
      e.next=0;
      anchor=&e;
      return;      
     }
  unsigned t0=0;
  Entry* e0=0;
  unsigned t1=0;
  Entry* e1=anchor;   
  while(true)
  {
   if (e1==0)
      {
       e.dt=at-t0;
       e.prev=e0;
       e.next=0;
       e0->next=&e;
       return;
      }
   t1=t0+e1->dt;
   if (at<t1)
      {
       e.dt=at-t0;
       e.prev=e0;
       e.next=e1;
       if (e0) e0->next=&e;
          else anchor=&e;
       e1->prev=&e;
       e1->dt=t1-at;
       return;
      }
   t0=t1;
   e0=e1;
   e1=e1->next;
  }     
 }

 void Timeout::tick()
 {
  
  while(anchor)
  {
   if ((anchor->dt)==0)
      {
       Entry* e=anchor;
       anchor->owner=0;
       anchor->state=TIMED_OUT;
       anchor=anchor->next;
       if (e->li) e->li->onTimeout(*e); //so add can be called in onTimeout
      }
      else
      {
       anchor->dt--;
       break;
      }
  }
 }

 void Timeout::enumerate(Entry::Listener& li)
 {
  Entry* e=anchor;
  unsigned t=0;
  while(e)
  {
   t+=e->dt;
   li.onEntry(*e,t);
   e=e->next;
  }
 }
}
