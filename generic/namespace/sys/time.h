//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#ifndef SYS_TIME
#define SYS_TIME
//----------------------------
//time.h
//(c) H.Buchmann FHSO 2004
//$Id: time.h 193 2006-01-11 15:21:16Z buchmann $
//----------------------------
INTERFACE(sys_time,$Revision: 137 $)
namespace sys
{
 class Time
 {
  public:
   static const unsigned SECOND = 1000000;
   typedef long long Stamp_us; //resolution microseconds
   
  private:
   static Time* time;
   static Time this_;
   Time(const Time&);
   Time& operator=(const Time&);

  protected:
   Time();
   virtual Stamp_us stamp_();
   
  public:
   static Stamp_us stamp(){return time->stamp_();}
   static unsigned sec (Stamp_us ts){return ts/SECOND;}
   static unsigned usec(Stamp_us ts){return ts%SECOND;}
   static unsigned delta(Stamp_us ts0,Stamp_us ts1)
          {return (unsigned)(ts1-ts0);}
   static unsigned delta(Stamp_us ts)
          {return stamp()-ts;};	  
 };
}
#endif
