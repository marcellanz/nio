#ifndef SYS_MSG_T_H
#define SYS_MSG_T_H
INTERFACE(sys_msg,$Revision: 160 $)
//-----------------------
//system wide messages (cerr)
//(c) H.Buchmann FHSO 2003
//$Id: msg.h 193 2006-01-11 15:21:16Z buchmann $
//-----------------------
#include "io/ascii/write.h"
#include "io/ascii/out.h"
namespace sys
{
 class Msg:public io::ascii::Writer,
	   io::ascii::Writer::EoLListener
 {
  private:
   Msg(const Msg&);
   Msg& operator=(const Msg&);
   void onEoL(io::ascii::Output&);
   void halt();    //display halt and halts

  protected:
   Msg(io::ascii::Output& out);
  
  public:
   Msg& error();   //display error prefix   **** error: and halts after eol 
   Msg& warning(); //display warning prefix **** warning:
   Msg& debug();   //enter debug call of sys::deb::enter
 };
 
 extern Msg& msg;
}
#endif
