//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#ifndef SYS_INFO_H
#define SYS_INFO_H
//-----------------------------
//info
//(c) H.Buchmann FHSO 2003
//$Id: info.h 193 2006-01-11 15:21:16Z buchmann $
//-----------------------------
INTERFACE(sys_info,$Revision: 137 $)
#include "io/ascii/out.h"
namespace sys
{
 class Info
 {
  public:
//------------------------------------------ Mod see sys::Mod
   class Mod
   {
    public:
     static void showGlobal(io::ascii::Writer& out); 
           //addresses of global constructors/destructors
     static void showInclude(io::ascii::Writer& out);
     static void showModules(io::ascii::Writer& out);
     static void showStartupSequence(io::ascii::Writer& out);
     static void showForTsort(io::ascii::Writer& out);
           //see UNIX command tsort
     static void showForDot(io::ascii::Writer& out);
           //see GraphViz www.graphviz.org dot file	   
   };
   
 };
}
#endif
