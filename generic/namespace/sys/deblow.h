//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#ifndef SYS_DEB_LOW_H
#define SYS_DEB_LOW_H
INTERFACE(sys_deblow,$Revision: 137 $)
//-----------------------
//debug low level 
//should be implemented in 'board/sys'
//(c) H.Buchmann FHSO 2001
//$Id: deblow.h 193 2006-01-11 15:21:16Z buchmann $
//-----------------------
namespace sys
{
 namespace deb //debug
 {
//elementary signal methods 
//display a signal and loops   
//coding for two leds green red 

//signal 0: green red blinking
//signal 1: green     blinking
//signal 2: red       blinking
//signal 3: (green red) alternating blinking

  void signal0();
  void signal1();     
  void signal2();     
  void signal3();

//low level
  void init();
  void halt();       //the program halts (whatever it means)
  void enter();      //enters debug mode normally all interrupts disabled
  void out(char ch); //transmits a char
  char get();        //waits until char ready
  bool avail();      //returns true if char available
  void test();	     //for tests
  void waitUntil();  //something happens
 }
}
#endif
