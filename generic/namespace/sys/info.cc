//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//-----------------------------
//info
//(c) H.Buchmann FHSO 2003
//$Id: info.cc 193 2006-01-11 15:21:16Z buchmann $
//-----------------------------
#include "sys/sys.h"
IMPLEMENTATION(sys_info,$Revision: 137 $)
#include "sys/info.h"
namespace sys
{
 extern "C" sys::Mod::global*  __GLOBAL__CONSTRUCTORS[];
 extern "C" sys::Mod::global*  __GLOBAL__DESTRUCTORS[];

 void Info::Mod::showGlobal(io::ascii::Writer& out)
 {
  out<<"----------------------------------------- __GLOBAL__CONSTRUCTORS\n";
  sys::Mod::global** g=__GLOBAL__CONSTRUCTORS;
  while(*g)out<<(void*)*g++<<"\n";

  out<<"----------------------------------------- __GLOBAL__DESTRUCTORS\n";
  sys::Mod::global** d=__GLOBAL__DESTRUCTORS;
  while(*d)out<<(void*)*d++<<"\n";
 }

 void Info::Mod::showInclude(io::ascii::Writer& out)
 {
  const sys::Mod* m=sys::Mod::getFirst();
  out<<"----------------------------------------- Include List\n";
  while(m)
  {
   out<<m->getName()<<"\n";
   sys::Mod::Iterator mi(*m);
   while(mi.next())
   {
    (out<<"\t").blank(mi.getIncludeLevel())<<mi.getModName()<<"("
         <<mi.getIncludeLevel()<<")\n";
   }
   m=m->link;
  }
 }

 void Info::Mod::showModules(io::ascii::Writer& out)
 {
  const sys::Mod* m=sys::Mod::getFirst();
  out.setTabString(
    "          x           |          |"
  )
  <<"----------------------------------------- Module List\n"
    "       code constructor destructor  Name\n";
  while(m)
  {
   out<<"\t"<<(unsigned)m->code
      <<"\t"<<(void*)m->constructor
      <<"\t"<<(void*)m->destructor
      <<"  "<<m->getName()<<"\n";
   m=m->link;
  }
 }

 void Info::Mod::showForTsort(io::ascii::Writer& out)
 {
  out<<"----------------------------------------\n"
       "for UNIX command 'tsort'\n"
       "----------------------------------------\n";
  const sys::Mod* m=sys::Mod::getFirst();
  while(m)
  {
   for(unsigned i=0;i<m->incLen;i++)
   {
    if(m!=m->inc[i]->m) out<<m->getName()<<" "<<m->inc[i]->m->getName()<<"\n";
   }
   m=m->link;
  }
 }

 void Info::Mod::showForDot(io::ascii::Writer& out)
 {
  out<<"//------------------------------------\n"
       "//dot file www.graphviz.org\n"
       "//------------------------------------\n";
  const sys::Mod* m=sys::Mod::getFirst();
  out<<"digraph moduleList \n"
       "{\n"
       "node [shape=plaintext]\n";
  while(m)
  {
   sys::Mod::Iterator mi(*m);
   while(mi.next())
   {
    if (mi.getIncludeLevel()==1)
       out<<"\""<<m->getName()<<"\"->\""<<mi.getModName()<<"\";\n";
   }
   m=m->link;
  }
  out<<"}\n";
 }
 
 void Info::Mod::showStartupSequence(io::ascii::Writer& out)
 {
  out<<"----------------------------------------- startup sequence\n";
  const sys::Mod* m=sys::Mod::getStart();
  while(m)
  {
   if (m->constructor) out<<m->getName()<<"\n";
   m=m->next;
  }
 }
 
}
