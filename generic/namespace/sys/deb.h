//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#ifndef SYS_DEB_H
#define SYS_DEB_H
INTERFACE(sys_deb,$Revision: 137 $)
//-----------------------
//sysdebug 
//see deblow.h
//(c) H.Buchmann FHSO 2001
//$Id: deb.h 193 2006-01-11 15:21:16Z buchmann $
//-----------------------
#include "sys/deblow.h"
namespace sys
{
 namespace deb //debug
 {
  void out(const char s[]); //zero terminated
  void out(unsigned val);  
  void out(int val);
  void out(const void* val);
  void out(const char key[],unsigned val);
  void out(const char key[],const unsigned val[],unsigned len);
  void out(const char key[],const char val[]); //zero terminated
  void out(const char key[],const void* val);
  void newln();
  void hexShort(unsigned short val);
  void hexByte(unsigned char ch);
  void hex(unsigned val);
  inline void hex(void* val){out(val);}
  void hex(const char key[],unsigned val);
  void hex(const char key[],unsigned* val,unsigned len);
  inline void hex(const char key[],void* val){out(key,val);}
  void dump(const unsigned char data[],unsigned len);
  void dump(const char s[],unsigned len);
  void waitForKey(char ch); //useful for starting
 }
}
#endif
