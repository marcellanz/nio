//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//------------------------------
//Device/List
//(c) H.Buchmann FHSO 2003
//$Id: device.cc 193 2006-01-11 15:21:16Z buchmann $
//------------------------------ 
#include "sys/sys.h"
IMPLEMENTATION(sys_device,$Revision: 137 $)
#include "sys/device.h"
#include "util/str.h"
#include "sys/msg.h"

namespace sys
{
 Device::Device(const char name[],List& lst,bool default_)
 :name(name),
  lst(lst),
  next(0),prev(0)
 { //add new devices *before* the existing ones
  next=lst.anchor; //last device will be 'first' 
  if (next) next->prev=this;
  lst.nbr++;
  lst.anchor=this;
  if (default_) lst.default_=this;
 }
 
 Device::~Device() 
 { //remove from list
  if (next) next->prev=prev;
  if (prev) prev->next=next;
  if (this==lst.anchor) lst.anchor=lst.anchor->next;
 }
 
 
 Device::List::List()
 :anchor(0),default_(0),nbr(0)
 {
 }
 
 Device::List::~List()
 {
 }
  
 Device* Device::List::getDev(const char name[]) const
 {
  Device* d=anchor;
  while(d)
  {
   if (util::Str::cmp(d->name,name)==0) return d;
   d=d->next;
  }
  return 0;
 }

 Device* Device::List::getDev(unsigned idx) const
 {
  if (idx>=nbr) return 0;
  unsigned di=nbr;
  Device* d=anchor;
  while(--di>idx) d=d->next;
  return d;
 }

 Device& Device::List::get(const char name[]) const
 {
  Device* d=getDev(name); //already in list
  if (d==0) msg.error()<<"device '"<<name<<"' dont exists\n";
  return *d;   
 }

 Device& Device::List::get(unsigned id) const
 {
  Device* d=getDev(id);
  if (d==0) msg.error()<<"device id='"<<id<<"' dont exists\n";
  return *d;   
 }

 Device& Device::List::get() const
 {
  if (default_==0) msg.error()<<"default device not available\n";
  return *default_;   
 }

 void Device::List::info(io::ascii::Writer& out) const
 {
  Device* d=anchor;
  while(d)
  {
   d->info(out);
   d=d->next;
  }
  if (default_)
     {
      out<<"default '"<<default_->name<<"'\n";
     }   
 }
}
