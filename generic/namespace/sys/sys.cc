//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

//--------------------------
//sys
//(c) H.Buchmann FHSO 2003
//$Id: sys.cc 221 2006-03-11 17:57:03Z buchmann $
//not yet in final form
// detection of circular import done
// but only rudimentary display of circular import 
//--------------------------
#include "sys/sys.h"
IMPLEMENTATION(sys_sys,$Revision: 143 $)
#include "sys/deb.h"

//#define SYS_SYS_SHOW_STARTUP

namespace sys
{
 extern "C" Mod::global  __GLOBAL__CONSTRUCTORS[];
 extern "C" Mod::global  __GLOBAL__DESTRUCTORS[];
 extern "C" Dep*         __DEPEND[];
 
//--------------------------------
//see -funit-at-a-time
 static const unsigned L0=0;
 static const unsigned L1=1;
 
 const char* Mod::ErrorCodeText[]=
 {
  "OK","CONSTRUCTOR","DESTRUCTOR","CIRCULAR"
 };
 
 bool           Mod::started=false;
 Mod::ErrorCode Mod::errorCode=Mod::OK;
 Mod*           Mod::first=0;
 Mod*           Mod::last=0;
 Mod*           Mod::cStart=(Mod*)0; 
 Mod*           Mod::cEnd=0;
 Mod*		Mod::lastStarted=0;
 const char*    Mod::prefix=0;
 unsigned       Mod::prefixSize=0;

 void Mod::sort()
 {
  if (errorCode!=OK) return;
  if (onStack)
     {
      errorCode=CIRCULAR;
      return;
     }
  if (visited) return;
  visited=true;
  onStack=true;
  for(unsigned i=0;i<incLen;i++)
  {
   Mod* m=inc[i]->m;
   if (m!=this) m->sort();
  }

//link in start list
  if (cEnd)
     {
      cEnd->next=this;
      prev=cEnd;
     }
     else
     {
      cStart=this;
     }
  cEnd=this;
  onStack=false;
 }
 
 void Mod::setConstructor(global g)
 {
  global* c=(global*)&constructor;
  *c=g;
 }
 
 void Mod::setDestructor(global g)
 {
  global* d=(global*)&destructor;
  *d=g;
 }
 
 const char* Mod::getName() const
 {
  return name+prefixSize;
 } 

 void Mod::setInclude(Inc** inc,unsigned len)
 {
  this->inc=inc;
  incLen=len;
 }

 void Mod::linkIn()
 {
  if (last) last->link=this;
     else   first=this;
  last=this;   
 }

//-------------------------------
//depending on cmpiler version the sequence is
//mod inc inc inc | mod inc inc inc preMOD

 void Mod::linkModules0()
 {
  Dep** d=__DEPEND;
  Mod* mod=0;
  while(true)
  {
   if (*d==0) break;
   switch((*d)->typ)
   {
    case Dep::MOD:
     mod=(Mod*)*d;
     mod->linkIn();
   
    break;
    case Dep::INC:
    {
     if (mod->incLen==0) mod->inc=(Inc**)d;
     mod->incLen++;
    }
    break;
   }
   ++d;
  }
 }

//-------------------------------
//   inc_i inc_(i-1) .. inc_0 mod | inc inc inc mod postMOD
//   i: include number        
 void Mod::linkModules1()
 {
  Dep** d=__DEPEND; 
  Inc** inc0=0; //the address of first include module
  unsigned incLen=0;
  while(true)
  {
   if (*d==0) break;
   switch((*d)->typ)
   {
    case Dep::MOD:
    {
//insert in main list 
     Mod* m=(Mod*)*d;
     m->linkIn();
     m->setInclude(inc0,incLen);
     inc0=0;
     incLen=0;      
    }
    break;

    case Dep::INC:
    {
     if (inc0==0) inc0=(Inc**)d;
     incLen++;
    }
    break;
   }
   ++d;
  }
 } 

#if (__GNU_MAJOR__==3) && (__GNUC_MINOR__<4)
#error wrong compiler version revisit code 
#endif

 Mod::Iterator::Iterator(const Mod& mod)
 :root(0),
  self(0),
  includeLevel(0),
  pos(0)
 {
  reset(mod);
 }
 
 Mod::Iterator::~Iterator()
 {
 }
 
 void Mod::Iterator::reset(const Mod& mod)
 {
  this->root=&mod;
  self=0;
  includeLevel=0;
  pos=mod.incLen;
 }
 
 void Mod::Iterator::reset()
 {
  includeLevel=0;
 }
 
 bool Mod::Iterator::hasNext() const
 {
  return pos>0;
 }
 
 bool Mod::Iterator::next()
 {
  pos--;
  if (pos>=0) 
  {
   Inc* inc=root->inc[pos];
   if (inc->m==root)
      {
       self=inc;
       return next();
      }
   if (self && (self->includeLevel==inc->includeLevel)) self=0;
  }
  return pos>=0;
 }
 
 const char* Mod::Iterator::getModName() const
 {
  return root->inc[pos]->m->getName();
 }
 
 unsigned Mod::Iterator::getIncludeLevel() const
 {
  unsigned lvl=root->inc[pos]->includeLevel;
  return (self)?lvl-1:lvl;
 }

 void Mod::mkPrefix()
 {
  prefix=first->name;
  unsigned i=0;
  prefixSize=0;
  while(true)
  {
   char ch=prefix[i];
   i++;
   if (ch=='\0') break;
   if (ch=='/') prefixSize=i;
  }
  Mod* m=first;
  
  while(m)
  {
   unsigned i=0;
   unsigned pfs=0;
   while(true)
   {
    if (i==prefixSize) break;
    char ch=m->name[i];
    char chp=prefix[i];
    i++;
    if (chp=='/') pfs=i;
    if (chp!=ch) break;
   }
   prefixSize=pfs;
   m=(Mod*)m->link;     
  }  
 }

 void Mod::match(global g[],SetGlobal s,ErrorCode cod)
 {
  if (errorCode!=OK) return;
  Mod* m=first;
  Mod* m0=0;
  int balance=0;
  
  while(true)
  {
//cases
// A: *g==0  m!=0 same as C
// B: *g!=0  m==0 same as D
// C:  m->code<*g
// D:  m->code>*g
   if (balance>0) {errorCode=cod;return;}
   if ((*g==0)&&(m==0)) break;
   if ((*g==0)||((m!=0)&&(m->code<*g)))
      {
#ifdef SYS_SYS_DEBUG     
       deb::out("\t\t");
       deb::out((void*)m->code);
       deb::out("\t");
       deb::out(m->getName());
       deb::newln();
#endif
       m0=m;
       m=(Mod*)m->link;
       balance=-1;
       continue;
      }
   if  ((m==0)||((*g!=0)&&(*g<m->code)))
       {
#ifdef SYS_SYS_DEBUG     
	deb::out((void*)*g);
	deb::newln();
#endif
	if (m0) (m0->*s)(*g);
	m0=0;
	g++;
	balance++;
	continue;
       }
  }
 }
 
 void Mod::matchGlobalConstructor()
 {
#ifdef SYS_SYS_DEBUG
  deb::out("----------------------------------------- Constructor\n\r"
           "Global\t\tModule\n\r"
           "----------------------\n\r");
#endif
  match(__GLOBAL__CONSTRUCTORS,&Mod::setConstructor,CONSTRUCTOR);
 }

 void Mod::matchGlobalDestructor()
 {
#ifdef SYS_SYS_DEBUG
  deb::out("----------------------------------------- Destructor\n\r"
           "Global\t\tModule\n\r"
           "----------------------\n\r");
#endif
  match(__GLOBAL__DESTRUCTORS,&Mod::setDestructor,DESTRUCTOR);
 }

 void Mod::sortModules()
 {
#ifdef SYS_SYS_DEBUG
  deb::out("----------------------------------------- Sorting\n\r");
#endif
  Mod* m=first;
  while(m)
  {
   m->sort();  
   m=(Mod*)m->link;
  }
 }

 void Mod::thumbStart()
 {
  global* c=__GLOBAL__CONSTRUCTORS;
  while(c)
  {
   (*c++)();
  }
 }
 
#ifdef SYS_SYS_DEBUG    
 void Mod::show()
 {
  deb::out((void*)code);deb::out('\t');
  deb::out((void*)constructor);deb::out('\t');
  deb::out((void*)destructor);deb::out('\t');
  deb::out(name+prefixSize);deb::newln();
 }   

 void Mod::showInc()
 {
  deb::out(getName());deb::newln();
  for(unsigned i=0;i<incLen;i++)
  {
   Mod* m=inc[i]->m;
   if (m!=this) deb::out("\t",m->name+prefixSize);
  }
 }

 void Mod::showInclude()
 {
  Mod* m=first;
  deb::out("----------------------------------------- Include List\n\r");
  while(m)
  {
   m->showInc();
   m=(Mod*)m->link;
  }
 }

 void Mod::showModules()
 {
  Mod* m=first;
  deb::out("----------------------------------------- Module List\n\r"
           "code\t\tconstructor\tdestructor\tName\n\r");
  while(m)
  {
   m->show();
   m=(Mod*)m->link;
  }
 }

 void Mod::showTSort()
 {
  for(unsigned i=0;i<incLen;i++)
  {
   deb::out(name+prefixSize);deb::out(" ");
   deb::out(inc[i]->m->name+prefixSize);
   deb::newln();
  }
 }

 void Mod::showForTsort()
 {
  deb::out("----------------------------------------\n\r"
           "for program tsort\n\r"
	   "----------------------------------------\n\r"
           );
  Mod* m=first;
  while(m)
  {
   m->showTSort();
   m=(Mod*)m->link;
  }
 }
 
 void Mod::showGlobal()
 {
  deb::hex("----------------------------- __GLOBAL__CONSTRUCTORS",
            __GLOBAL__CONSTRUCTORS);
  global* g=__GLOBAL__CONSTRUCTORS;
  while(*g)
  {
   deb::out("",(void*)*g);
   g++;
  }

  deb::out("----------------------------- __GLOBAL__DESTRUCTORS",
           __GLOBAL__DESTRUCTORS);
  global* d=__GLOBAL__DESTRUCTORS;
  while(*d)
  {
   deb::out("",(void*)*d);
   d++;
  }
 }

 void Mod::showStartupSequence()
 {
  deb::out("----------------------------------------- startup sequence\n\r");
  Mod* m=cStart;
  while(m)
  {
   if (m->constructor) {deb::out(m->getName());deb::newln();}
   m=(Mod*)m->next;
  }
 }

#endif

 Mod::ErrorCode Mod::getErrorCode(){return errorCode;}
 
 const char* Mod::getErrorCodeText(){return ErrorCodeText[errorCode];}

 void Mod::callConstructors()
 {
  if (errorCode!=OK)
     {
      deb::out("**** thumbStart because of",ErrorCodeText[errorCode]);
      deb::halt();
      thumbStart();return;
     }
  Mod* m=cStart;
  while(m)
  {
   lastStarted=m;
#ifdef SYS_SYS_SHOW_STARTUP
   if (m->constructor)
      {
       deb::out("+++++++++++  ");
       deb::out(m->getName());
       deb::newln();
//       deb::waitUntil();
       (*m->constructor)();
      }
#else
   if (m->constructor) (*m->constructor)();
#endif   
   m=m->next;
  }
#ifdef SYS_SYS_DEBUG
  deb::out("----------------------------------------- lastStarted",lastStarted->name);
#endif  
 }

 void Mod::start()
 {
  if (started)
     {
      deb::out("**** already started\n\r");
      return;
     }
  started=true;
  if (&L0<&L1) linkModules0(); // -funit-at-a-time disabled
     else      linkModules1(); // -funit-at-a-time enabled
  
  
  
  mkPrefix();
//  showInclude();

   
//  showGlobal();
  
  
  matchGlobalConstructor();
  matchGlobalDestructor();
  
  
//  showModules();
//  showForTsort();
  sortModules();
 
//  showStartupSequence();  
//  return;
//  deb::signal0();

  callConstructors();
 }
 
 void Mod::stop() 
 {
  Mod* m=lastStarted;
  while(m)
  {
   if (m->destructor) 
   {
#ifdef SYS_SYS_SHOW_STARTUP
    deb::out("-----------  ");
    deb::out(m->getName());
    deb::newln();
#endif   
    (*m->destructor)();
   }
   m=m->prev;
  }
 }
}
