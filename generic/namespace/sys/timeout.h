//
//    nio: network io
//
//    Copyright (C) 2005 FHSO, Hans Buchmann <hans.buchmann@fhso.ch>
//
//    This file is part of nio
//
//    nio is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    nio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with nio; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#ifndef SYS_TIMEOUT_H
#define SYS_TIMEOUT_H
INTERFACE(sys_timeout,$Revision: 137 $)
//-----------------------------
//timeout
//(c) H.Buchmann FHSO 2002
//$Id: timeout.h 193 2006-01-11 15:21:16Z buchmann $
//-----------------------------
namespace sys
{
//--------------------------------------- Timeout
 class Timeout
 {
  private:
   Timeout(const Timeout&);
   Timeout operator=(const Timeout&);
   
  public:
   enum State {INACTIVE,ACTIVE,TIMED_OUT};
   static const char* StateName[];
   
   class Entry;
//--------------------------------------- Listener
   class Listener
   {
    public:
     virtual void onTimeout(Entry& e)=0;
   };

//--------------------------------------- Entry
   class Entry
   {
    private:
     Entry(const Entry&);
     Entry operator=(const Entry&);
     
    public:
     class Listener //used for enumerating
     {
      public:
       virtual void onEntry(Entry& e,unsigned t)=0;
     };
     
    private:
     friend class Timeout;
     Timeout::Listener* li;
     Timeout* owner;
     Entry* prev;
     Entry* next;
     State  state;
     unsigned dt;

    public:
              Entry(Timeout::Listener* li):
	      li(li),owner(0),prev(0),next(0),state(INACTIVE),dt(0){}
     virtual ~Entry(){}
     bool isActive()const {return state==ACTIVE;}
     bool isTimedOut()const {return state==TIMED_OUT;}
     State getState()const{return state;}
     const char* getStateName(){return StateName[state];}
     void remove();
     unsigned getDt()const{return dt;}
   };
  
  private:
   friend class Entry;
   Entry* anchor;
  
  public:
            Timeout();
   virtual ~Timeout();
   void add(Entry& e,unsigned at); 
//if the entry is already in the queue it will be removed an again inserted 
   void tick();
   void enumerate(Entry::Listener& li);
   bool isEmpty(){return anchor==0;}
 };
}
#endif
