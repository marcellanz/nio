#ifndef PIX_IMAGE_H
#define PIX_IMAGE_H
//---------------------------------
//image
//(c) H.Buchmann FHSO 2005
//$LastChangedRevision: 137 $
//---------------------------------
INTERFACE(pix_image,$LastChangedRevision: 137 $)
#include <vector>
namespace pix
{
 typedef unsigned char RGB8;
  
 template<typename Pixel>
 class Image
 {
  private:
    Image();

  public:
   struct Listener
   {
    virtual void onDimension(unsigned wi,unsigned he)=0;
    virtual void onPixel(Pixel p)=0;
    virtual ~Listener(){}
   };
 };
}//namespace pix
#endif
