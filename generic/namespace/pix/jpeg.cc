//----------------------------
//jpeg
//(c) H.Buchmann FHSO 2005
//$LastChangedRevision: 137 $
//----------------------------
#include "sys/sys.h"
IMPLEMENTATION(pix_jpeg,$LastChangedRevision: 137 $)
#include "pix/jpeg.h"
#include "sys/msg.h"
#include "io/byte/file/file.h"
#include "util/endian.h"
#include "util/str.h"
#include "pix/jpeg/dct.h"

namespace pix
{

 JPEG JPEG::jpeg;

 const unsigned JPEG::ZIG_ZAG[64]=
 {
   0, 1, 5, 6,14,15,27,28,  // 0
   2, 4, 7,13,16,26,29,42,  // 8
   3, 8,12,17,25,30,41,43,  //16
   9,11,18,24,31,40,44,53,  //24
  10,19,23,32,39,45,52,54,  //32
  20,22,33,38,46,51,55,60,  //40
  21,34,37,47,50,56,59,61,  //48 
  35,36,48,49,57,58,62,63   //56
 };

 const unsigned JPEG::DE_ZIG_ZAG[64]=
 {
   0, 1, 8,16, 9, 2, 3,10,
  17,24,32,25,18,11, 4, 5,
  12,19,26,33,40,48,41,34,
  27,20,13, 6, 7,14,21,28,
  35,42,49,56,57,50,43,36,
  29,22,15,23,30,37,44,51,
  58,59,52,45,38,31,39,46,
  53,60,61,54,47,55,62,63
 };

 
 io::ascii::Writer& JPEG::bitOut(io::ascii::Writer& out,unsigned len,unsigned v)
 {
  unsigned mask=1<<len;
  for(unsigned i=0;i<len;i++)
  {
   static const char Digit[]="01";
   mask>>=1;
   out<<Digit[(v&mask)!=0];
  }
  return out;
 }

 io::ascii::Writer& JPEG::blockOut(io::ascii::Writer& out,int v[])
 {
  unsigned i=0;
  for(unsigned x=0;x<8;x++)
  {
   for(unsigned y=0;y<8;y++)
   {
    out<<io::ascii::setw(8)<<v[i++];
   }
   out<<"\n";
  }
  return out;
 }
 
//-------------------------------------- JPEG::BitReader
 JPEG::BitReader::BitReader(io::byte::stream::Input& src)
 :src(src),
  bits(src.getByte()),
  mask(0x80)
 {
 }

 bool JPEG::BitReader::get()
 {
  bool v=bits&mask;
  mask>>=1;
  if (mask==0)
     {
      mask=0x80;
      unsigned char v=src.getByte();
      if (bits==0xff)
         {
	  if (v==0) bits=src.getByte();
	     else   sys::msg.error()<<"Marker "<<(jpeg::Marker::Code)v
	                            <<" at "<<io::ascii::hex()<<src.getPos()
				    <<"\n";
	 }
	 else
	 {
	  bits=v;
	 }
     }
  return v;   
 }

 void JPEG::BitReader::skip(unsigned k)
 {
  while(k>0)
  {
   get();
   k--;
  }
 }

 int JPEG::BitReader::val(unsigned l)
 {
  int v=0;
  bool pos=false;
  for(unsigned k=0;k<l;k++)
  {
   v=(v<<1)|get();
   if (k==0) pos=(v==1); 
  }
  int r=(pos)?v:(v+1-(1<<l));
#if 0  
  sys::msg<< "l= "<<io::ascii::hex()<<l
                  <<" v= "<<io::ascii::hex()<<v
		  <<" r= "<<io::ascii::hex()<<r<<"\n";
#endif
  return r;
 }

 io::ascii::Writer& JPEG::BitReader::show(io::ascii::Writer& out) const
 {
  out<<"pos "<<io::ascii::hex()<<src.getPos()
     <<" "<<io::ascii::hex()<<(unsigned char)mask<<"\n";
  return out;
 }

//-------------------------------------- JPEG::Segment
 JPEG::Segment::Segment(JPEG* jpeg,jpeg::Marker::Code m)
 :jpeg(*jpeg),
  marker(m),
  len(jpeg->len()),
  data(jpeg->src.getData())
 {
  jpeg->src.skip(len-2);
 }
 
//-------------------------------------- JPEG::APP_0
 JPEG::APP_0::APP_0(JPEG* jpeg)
 :Segment(jpeg,jpeg::Marker::APP_0)
 {
 }

//-------------------------------------- JPEG::SOF
 JPEG::SOF::SOF(JPEG* jpeg,jpeg::Marker::Code c)
 :Segment(jpeg,c),
  sof((Data*)data)
 {
  x=util::Endian::big(sof->x);
  y=util::Endian::big(sof->y);
  jpeg->dRow=8+(x-1)&~0xf; //aligned to 8 
  jpeg->pixel=new double[8*jpeg->dRow];
  sof->show(sys::msg);
 }
 
 io::ascii::Writer& JPEG::SOF::Data::show(io::ascii::Writer& out) const
 {
  out<<"p= "<<p<<" y= "<<util::Endian::big(y)
               <<" x= "<<util::Endian::big(x)
	       <<" nf= "<<nf<<"\n";
  for(unsigned i=0;i<nf;i++)
  {
   out<< "ci= "<<io::ascii::setw(3)<<comp[i].ci
      <<" hi_vi= "<<io::ascii::hex()<<comp[i].hi_vi
      <<" tqi= "<<io::ascii::setw(3)<<comp[i].tqi<<"\n";
  }
  return out;
 }

//-------------------------------------- JPEG::DHT
 JPEG::DHT::DHT(JPEG* jpeg)
 :Segment(jpeg,jpeg::Marker::DHT),
 dht((Data*)data),
 root(new Inner)
 {
  switch(dht->tC_tH>>4)
  {
   case 0:jpeg->dc[dht->tC_tH&0xf]=this;break;
   case 1:jpeg->ac[dht->tC_tH&0xf]=this;break;
   default:
    sys::msg.error()<<"unexpected tC value\n";
   break; 
  }
  dht->show(sys::msg);
  build();
  root->show(0);
 } 

 JPEG::DHT::~DHT()
 {
//FIXME delete tree
 }
 
 io::ascii::Writer& JPEG::DHT::Data::show(io::ascii::Writer& out) const
 {
  static const unsigned PER_LINE=16;
  out<<"Tc/Th= "<<io::ascii::hex()<<tC_tH<<"\n";
  unsigned vi=0;
  for(unsigned i=0;i<16;i++)
  {
   unsigned n=l[i];
   out<<io::ascii::setw(3)<<(i+1)<<io::ascii::setw(4)<<n<<" : ";
   unsigned perLine=0;
   for(unsigned k=0;k<n;k++)
   {
    unsigned char vv=v[vi++];
    if (perLine==PER_LINE)
       {
        out<<"\n            ";
	perLine=0;
       }
    out.hex(vv)<<" ";
    perLine++;
   }
   out<<"\n";
  }
  showCode(out);
  return out;  
 }

 void JPEG::DHT::Data::showCode(io::ascii::Writer& out) const
 {
  unsigned short code=0;
  unsigned vi=0;
  for(unsigned i=0;i<16;i++)
  {
   unsigned n=l[i];
   for(unsigned k=0;k<n;k++)
   {
    out<<io::ascii::setw(4)<<io::ascii::hex()<<v[vi++]
       <<io::ascii::setw(3)<<(i+1)<<" ";
    JPEG::bitOut(out,i+1,code)<<"\n";
    code++;
   }
   code*=2;
  }
 }

 JPEG::DHT::Inner::Inner()
 :le(0),ri(0)
 {
 }

 JPEG::DHT::Leaf::Leaf(unsigned val)
 :val(val)
 {
 }

 void JPEG::DHT::Leaf::build(unsigned mask,unsigned code,unsigned val)
 {
 }

 unsigned JPEG::DHT::Leaf::decode(BitReader& br)
 {
  return val;
 }

 void JPEG::DHT::Leaf::show(unsigned indent)
 {
  sys::msg.blank(indent)<<val<<"\n";
 }
 
 void JPEG::DHT::Inner::build(unsigned mask,unsigned code,unsigned val)
 {
  if (mask&code)
     { //ri
      if (mask==1) 
         {
	  ri=new Leaf(val);
	 }
	 else
	 {
	  if (ri==0) ri=new Inner;
	  ri->build(mask>>1,code,val);
	 }
     }
     else
     { //le
      if (mask==1)
        {
	 le=new Leaf(val);
	}
	else
	{
	 if (le==0) le=new Inner;
	 le->build(mask>>1,code,val);
	}
     }
 }
 
 void JPEG::DHT::Inner::show(unsigned indent)
 {
  sys::msg.blank(indent)<<"o\n";
  if (le) le->show(indent+1);
  if (ri) ri->show(indent+1);
 }

 unsigned JPEG::DHT::Inner::decode(BitReader& br)
 {
  bool bit=br.get();
  if (bit) 
     {
      if (ri==0) {
                  br.show(sys::msg);
                  sys::msg.error()<<"decode ri==0\n";
		 }
      return ri->decode(br);
     }
     else  
     {
      if (le==0) {
                  br.show(sys::msg);
		  sys::msg.error()<<"decode ri==0\n";
		 }
      return le->decode(br);
     }
 }

 void JPEG::DHT::build()
 {
  unsigned code=0;
  unsigned vi=0;
  unsigned mask=1;
  for(unsigned i=0;i<16;i++)
  {
   unsigned n=dht->l[i];
   for(unsigned k=0;k<n;k++)
   {
    root->build(mask,code,dht->v[vi++]);
    code++;
   }
   code*=2;
   mask<<=1;
  }
 }

 unsigned JPEG::DHT::decode(BitReader& br)
 {
  return root->decode(br);
 }

//-------------------------------------- JPEG::DQT
 io::ascii::Writer& JPEG::DQT::Data::show(io::ascii::Writer& out) const
 {
  out<<"pQ_tQ= "<<io::ascii::hex()<<pQ_tQ<<"\n";
  unsigned idx=0;
  for(unsigned y=0;y<8;y++)
  {
   for(unsigned x=0;x<8;x++)
   {
    sys::msg<<io::ascii::setw(3)<<q[DE_ZIG_ZAG[idx++]];
   }
   sys::msg<<"\n";
  }
  return out;
 }
 
 JPEG::DQT::DQT(JPEG* jpeg)
 :Segment(jpeg,jpeg::Marker::DQT),
  dqt((Data*)data)
 {
  unsigned tQ=dqt->pQ_tQ&0xf;
  if (0xf&(dqt->pQ_tQ>>4))
     {
      sys::msg.error()<<"not yet supported\n";
     }
  if(tQ>3)
    {
     sys::msg.error()<<"Tq too big ("<<tQ<<")\n"; 
    }
  jpeg->dqt[tQ]=this;  
  dqt->show(sys::msg);
  build();
 }


 void JPEG::DQT::build()
 {
  static const double Scale[8]=  //1                    k=0
                                 //cos(k*pi/16)*sqrt(2) k>0 
  {
   1.0,1.38703984532215E+00,1.30656296487638E+00,1.17587560241936E+00,
   1.0,7.85694958387102E-01,5.41196100146197E-01,2.75899379282943E-01
  };
  unsigned idx=0;
  for(unsigned y=0;y<8;y++)
  {
   for(unsigned x=0;x<8;x++)
   {
    coeff[idx]=Scale[x]*Scale[y]*dqt->q[JPEG::ZIG_ZAG[idx]];
//    coeff[idx]=Scale[x]*Scale[y]*dqt->q[idx];
    idx++;
   }
  }
 }

//-------------------------------------- JPEG::SOS
 io::ascii::Writer& JPEG::SOS::Data::show(io::ascii::Writer& out) const
 {
  out<<"Ns= "<<nS<<"\n";
  for(unsigned i=0;i<nS;i++)
  {
   out<<io::ascii::setw(3)<<i<<": Cs "<<comp[i].cS<<"  Td/Ta "<<io::ascii::hex()<<comp[i].tD_tA<<"\n";
  }
  return out;
 }
 
 JPEG::SOS::SOS(JPEG* jpeg)
 :Segment(jpeg,jpeg::Marker::SOS),
  sos((Data*)data)
 {
  sos->show(sys::msg);
 }

 void JPEG::SOS::scan()
 {
 }
  
//-------------------------------------- JPEG::JPEG
 JPEG::JPEG()
 ://src("doc/jpeg-6b/testimg.jpg"),
  //src("nio-logo.jpg"),
  //src("test.jpg"),
  src("src/generic/pix/jpeg/test-black-and-white.jpg"),
  sof(0),
  dcVal(0),
  dRow(0),
  pixel(0)
 {
  for(unsigned i=0;i<4;i++){dqt[i]=0;dc[i]=0;ac[i];}
  marker();isExit(jpeg::Marker::SOI);
  frame();
 }

 JPEG::~JPEG()
 {
  for(unsigned i=0;i<4;i++)
  {
   if (dqt[i]) delete dqt[i];
   if  (ac[i]) delete  ac[i];
   if  (dc[i]) delete  dc[i];
  }
  if (sof) delete sof;
  if (pixel) delete pixel;
 }

 void JPEG::marker()
 {
  if (src.getByte()!=0xff) sys::msg.error()<<"marker expected "
                                         <<io::ascii::hex()<<src.getPos()<<"\n";
  code=(jpeg::Marker::Code)src.getByte();
  sys::msg<<code<<"\n";
 }

 void JPEG::isExit(jpeg::Marker::Code c)
 {
  if (code!=c) sys::msg.error()<<"marker "<<c<<" excpected\n";
 }

 unsigned JPEG::len()
 {
  unsigned short h=(unsigned short)src.getByte();
  unsigned short l=(unsigned short)src.getByte();
  return (h<<8)+l;
 }

 void JPEG::skip()
 {
  src.skip(len()-2);
 }
 
 void JPEG::frame()
 {
  marker();
  tableMisc();
  frameHeader();
  while(code!=jpeg::Marker::EOI)
  {
   scan();
  }
 }
 
 void JPEG::tableMisc()
 {
  while(true)
  {
   switch(code)
   {
    case jpeg::Marker::DQT:
     {
      new DQT(this);
     }
     marker();
    break;
    case jpeg::Marker::DHT:
     new DHT(this);
     marker();
    break;
    case jpeg::Marker::DAC:
     skip();
     marker();
    break;
    case jpeg::Marker::COM:
     skip();
     marker();
    break;
    case jpeg::Marker::APP_0:
     skip();
     marker();
    break;
    case jpeg::Marker::APP_1:
    case jpeg::Marker::APP_2:
    case jpeg::Marker::APP_3:
    case jpeg::Marker::APP_4:
    case jpeg::Marker::APP_5:
    case jpeg::Marker::APP_6:
    case jpeg::Marker::APP_7:
    case jpeg::Marker::APP_8:
    case jpeg::Marker::APP_9:
    case jpeg::Marker::APP_10:
    case jpeg::Marker::APP_11:
    case jpeg::Marker::APP_12:
    case jpeg::Marker::APP_13:
    case jpeg::Marker::APP_14:
    case jpeg::Marker::APP_15:
     skip();
     marker();
    break;

    default:
    return;
   }//switch(code)
  }
 }
 
 void JPEG::frameHeader()
 {
  switch(code)
  {
   case jpeg::Marker::SOF_0:
    sof=new SOF(this,jpeg::Marker::SOF_0);
    marker();
   return; 
   
   case jpeg::Marker::SOF_1: 
   case jpeg::Marker::SOF_2: 
   case jpeg::Marker::SOF_3: 

   case jpeg::Marker::SOF_5: 
   case jpeg::Marker::SOF_6: 
   case jpeg::Marker::SOF_7: 

   case jpeg::Marker::SOF_9: 
   case jpeg::Marker::SOF_10:
   case jpeg::Marker::SOF_11:

   case jpeg::Marker::SOF_13:
   case jpeg::Marker::SOF_14:
   case jpeg::Marker::SOF_15:
    skip();
    marker();
   return;
   default:
    sys::msg<<"SOF_x expected\n";
   break;
  }
 }
 
 void JPEG::scan()
 {
  tableMisc();
  isExit(jpeg::Marker::SOS);
  SOS sos(this);
  BitReader br(src);
  io::byte::file::Output dst("xxx.pnm");
  const char* hdr="P5\n448 312 255\n";
  dst.put(hdr,util::Str::len(hdr));
  {
   unsigned y=0;
   while(y<sof->y)
   {
    double* upperLeft=pixel;
    unsigned x=0;
    while(x<sof->x)
    {
     block(br,upperLeft);
     x+=8;
     upperLeft+=8;
    }
    {
     unsigned pi=0;
     double* row=pixel;
     for(unsigned r=0;r<8;r++)
     {
      for(unsigned c=0;c<sof->x;c++)
      {
       int v=(int)row[c];
       unsigned char pv=(v<0)?0:((v>255)?255:v);
       dst.put(&pv,1);
      }
      row+=dRow;
     }
    }
    y+=8;
    sys::msg<<"\n";
   }
   br.show(sys::msg)<<"\n";
  }
  sys::msg<<"\n";
  marker();
 }
 
 void JPEG::block(BitReader& br,double upperLeft[])
 {
  int coeff[64];
  unsigned idx=0;
  dcVal+=br.val(dc[0]->decode(br));
  coeff[DE_ZIG_ZAG[idx++]]=dcVal;
  while(idx<64)
  {
   unsigned v=ac[0]->decode(br);
   if (v==0)
      {
       for (;idx<64;idx++) coeff[DE_ZIG_ZAG[idx]]=0;
       break; 
      }
   if (v==0xf0)
      {
       for(unsigned i=0;i<16;i++) coeff[DE_ZIG_ZAG[idx++]]=0;
      }
      else
      {
       unsigned zeroCnt=0xf&(v>>4);
       for(unsigned i=0;i<zeroCnt;i++) coeff[DE_ZIG_ZAG[idx++]]=0;
       if (idx>=64) sys::msg.error()<<idx<<" out of range\n";
       coeff[DE_ZIG_ZAG[idx++]]=br.val(0xf&v);
      }  
  }
  jpeg::DCT::inverse(coeff,upperLeft,dRow,dqt[0]->coeff);
 }
 
}
