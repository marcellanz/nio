#ifndef PIX_JPEG_H
#define PIX_JPEG_H
//----------------------------
//jpeg
//(c) H.Buchmann FHSO 2005
//$Id: jpeg.h 193 2006-01-11 15:21:16Z buchmann $
//----------------------------
INTERFACE(pix_jpeg,$Revision: 137 $)
#include "io/ascii/write.h"
#include "io/byte/file/file.h"
#include "pix/jpeg/marker.h"

namespace pix
{
//-------------------------------------- JPEG
 class JPEG
 {
//-------------------------------------- convenience
  static io::ascii::Writer& bitOut(io::ascii::Writer&,unsigned len,unsigned v);
  static io::ascii::Writer& blockOut(io::ascii::Writer&,int v[]);
  
//-------------------------------------- JPEG::BitReader
  class BitReader
  {
   private:
    io::byte::stream::Input& src;
    unsigned char  bits;
    unsigned char  mask;

   public:
    BitReader(io::byte::stream::Input& src);
    bool get();
    void skip(unsigned k);
    int val(unsigned l);
    io::ascii::Writer& show(io::ascii::Writer&) const;
  };

//-------------------------------------- JPEG::Segment
  struct Segment
  {
   JPEG& jpeg;
   jpeg::Marker::Code marker;
   unsigned len;
   const unsigned char* data;
   Segment(JPEG* jpeg,jpeg::Marker::Code m);
  };

//-------------------------------------- JPEG::APP_0
  struct APP_0:public Segment
  {
   APP_0(JPEG*);
  };

//-------------------------------------- JPEG::DHT
//Hufmantable
  struct DHT:public Segment
  {
   struct Node
   {
    virtual void build(unsigned mask,unsigned code,unsigned val)=0;
    virtual void show(unsigned indent)=0;
    virtual unsigned decode(BitReader& br)=0;
   };
   
   struct Inner:public Node
   {
    Node* le;
    Node* ri;
    Inner();
    void build(unsigned mask,unsigned code,unsigned val);
    void show(unsigned indent);
    unsigned decode(BitReader& br);
   };
   
   struct Leaf:public Node
   {
    unsigned val;
    Leaf(unsigned val);
    void build(unsigned mask,unsigned code,unsigned val);
    void show(unsigned indent);
    unsigned decode(BitReader& br);
   };
   
   struct Data
   {
    unsigned char tC_tH;
    unsigned char l[16];
    unsigned char v[0];
    io::ascii::Writer& show(io::ascii::Writer& out) const;
    void showCode(io::ascii::Writer& out) const;
   }__attribute__((packed));
   
   Data* dht;
   Node* root;
   void build();
   unsigned decode(BitReader& br);
            DHT(JPEG*);
   virtual ~DHT();
  };

//-------------------------------------- JPEG::DQT
  struct DQT:public Segment
  {
   struct Data
   {
    unsigned char pQ_tQ;
    unsigned char q[];
    io::ascii::Writer& show(io::ascii::Writer& out) const;
   }__attribute__((packed));
   
   Data* dqt;
   double coeff[64];
   DQT(JPEG*);
   void build();
  };
    
//-------------------------------------- JPEG::SOF
  struct SOF:public Segment
  {
   struct Data
   {
    unsigned char p;
    unsigned short y;
    unsigned short x;
    unsigned char nf;
    struct 
    {
     unsigned char ci;
     unsigned char hi_vi;
     unsigned char tqi;
    } comp[0];
    io::ascii::Writer& show(io::ascii::Writer& out) const;
   }__attribute__((packed));
   int x,y; //taken from Data
   SOF(JPEG* jpeg,jpeg::Marker::Code c);
   Data* sof;
  };
  
//-------------------------------------- JPEG::SOS
  struct SOS:public Segment
  {
   struct Data
   {
    unsigned char nS;
    struct 
    {
     unsigned char cS;
     unsigned char tD_tA;
    } comp[0];
    io::ascii::Writer& show(io::ascii::Writer& out) const;
   }__attribute__((packed));
   
   Data* sos;
   void scan();
   SOS(JPEG* jpeg);
  };
  
  static JPEG jpeg;
  static const unsigned ZIG_ZAG[64];
  static const unsigned DE_ZIG_ZAG[64];
  io::byte::file::Input::Stream src;  
  jpeg::Marker::Code code;
  DQT* dqt[4];
  DHT* dc[4];
  DHT* ac[4];
  SOF* sof;
  double* pixel; //the last 8 rows
  unsigned dRow;
  int dcVal;
  
  void marker();
  
  JPEG();
  ~JPEG();
  void read(const char fName[]);
  unsigned char* getData(); //current data byte
  void frame();
  void tableMisc();
  void frameHeader();
  void scan();
  void block(BitReader& br,double* upperLeft);
  unsigned len();
  void skip();   //len .....
  bool is(jpeg::Marker::Code c){return code==c;}
  void isExit(jpeg::Marker::Code c);
 }; 
}//namespace pix
#endif
