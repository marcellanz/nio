//-----------------------------
//DCT 8x8 discrete cosinus transformation
//(c) H.Buchmann 
//$LastChangedRevision: 137 $
//see JPEGDEC.C and jidctflt.c
//-----------------------------
#include "sys/sys.h"
IMPLEMENTATION(pix_jpeg_dct,$LastChangedRevision: 137 $)
#include "pix/jpeg/dct.h"
namespace pix
{
 namespace jpeg
 {
  double DCT::range(double v)
  {
   double v1=0.125*v;
   return (v1<0)?0:((v1>255)?255:v1);
  }
  
  void DCT::inverse(int in[],double out[],unsigned dy,double dqt[])
  {
   double workspace[64];
   int* col=in; //current col
   double* ws=workspace;
   double* q =dqt;
   for(unsigned y=0;y<8;y++)
   {
    if ((col[ 8]==0) &&
        (col[16]==0) &&
        (col[24]==0) &&
        (col[32]==0) &&
        (col[40]==0) &&
        (col[48]==0) &&
        (col[56]==0)) //all zero
       {
        double dc=col[0]*q[0];
	ws[ 0]=dc;
	ws[ 8]=dc;
	ws[16]=dc;
	ws[24]=dc;
	ws[32]=dc;
	ws[40]=dc;
	ws[48]=dc;
	ws[56]=dc;
	col++;
	q++;
	ws++;
	continue;
       }
       double t0 = col[0] *q[0];
       double t1 = col[16]*q[16];
       double t2 = col[32]*q[32];
       double t3 = col[48]*q[48];

       double t10 = t0 + t2;// phase 3
       double t11 = t0 - t2;

       double t13 = t1 + t3;// phases 5-3
       double t12 = (t1 - t3) * 1.414213562 - t13; // 2*c4

       t0 = t10 + t13;// phase 2
       t3 = t10 - t13;
       t1 = t11 + t12;
       t2 = t11 - t12;

       // Odd part
       double t4 = col[8] *q[8];
       double t5 = col[24]*q[24];
       double t6 = col[40]*q[40];
       double t7 = col[56]*q[56];

       double z13 = t6 + t5;// phase 6
       double z10 = t6 - t5;
       double z11 = t4 + t7;
       double z12 = t4 - t7;

       t7 = z11 + z13;// phase 5
       t11= (z11 - z13) * 1.414213562f; // 2*c4

       double z5 = (z10 + z12) * 1.847759065f; // 2*c2
       t10 = 1.082392200f * z12 - z5; // 2*(c2-c6)
       t12 = -2.613125930f * z10 + z5;// -2*(c2+c6)

       t6 = t12 - t7;// phase 2
       t5 = t11 - t6;
       t4 = t10 + t5;

       ws[0]  = t0 + t7;
       ws[56] = t0 - t7;
       ws[8]  = t1 + t6;
       ws[48] = t1 - t6;
       ws[16] = t2 + t5;
       ws[40] = t2 - t5;
       ws[32] = t3 + t4;
       ws[24] = t3 - t4;
       col++;
       q++;
       ws++;//advance pointers to the next column
   }//for(unsigned y=0;y<8;y++)
   double* row=out;
   ws=workspace;
   for(unsigned x=0;x<8;x++)
   {
   // Even part
	double t10 = ws[0] + ws[4];
	double t11 = ws[0] - ws[4];

	double t13 = ws[2] + ws[6];
	double t12 =(ws[2] - ws[6]) * 1.414213562f - t13;

	double t0 = t10 + t13;
	double t3 = t10 - t13;
	double t1 = t11 + t12;
	double t2 = t11 - t12;

   // Odd part
	double z13 = ws[5] + ws[3];
	double z10 = ws[5] - ws[3];
	double z11 = ws[1] + ws[7];
	double z12 = ws[1] - ws[7];

	double t7 = z11 + z13;
	       t11= (z11 - z13) * 1.414213562f;

	double z5 = (z10 + z12) * 1.847759065f; // 2*c2
	t10 = 1.082392200f * z12 - z5;  // 2*(c2-c6)
	t12 = -2.613125930f * z10 + z5; // -2*(c2+c6)

	double t6 = t12 - t7;
	double t5 = t11 - t6;
	double t4 = t10 + t5;

	row[0] = range(t0 + t7);
	row[7] = range(t0 - t7);
	row[1] = range(t1 + t6);
	row[6] = range(t1 - t6);
	row[2] = range(t2 + t5);
	row[5] = range(t2 - t5);
	row[4] = range(t3 + t4);
	row[3] = range(t3 - t4);
#if 0
 // Final output stage: scale down by a factor of 8
	row[0] = range_limit[(DESCALE((int) (t0 + t7), 3)) & 1023L];
	row[7] = range_limit[(DESCALE((int) (t0 - t7), 3)) & 1023L];
	row[1] = range_limit[(DESCALE((int) (t1 + t6), 3)) & 1023L];
	row[6] = range_limit[(DESCALE((int) (t1 - t6), 3)) & 1023L];
	row[2] = range_limit[(DESCALE((int) (t2 + t5), 3)) & 1023L];
	row[5] = range_limit[(DESCALE((int) (t2 - t5), 3)) & 1023L];
	row[4] = range_limit[(DESCALE((int) (t3 + t4), 3)) & 1023L];
	row[3] = range_limit[(DESCALE((int) (t3 - t4), 3)) & 1023L];
#endif
	ws+=8;//advance pointer to the next row
	row+=dy;
   }
  }
 }//namespace jpeg
}//namespace pix 
