//--------------------
//marker
//(c) H.Buchmann FHSO 2005
//$Id: marker.h 193 2006-01-11 15:21:16Z buchmann $
//--------------------
INTERFACE(pix_jpeg_marker,$LastChangedRevision: 137 $)
#include "io/ascii/write.h"

namespace pix
{
 namespace jpeg
 {
  class Marker
  {
   public:
    enum Code {  
               SOF_0  = 0xc0,
               SOF_1  = 0xc1,
               SOF_2  = 0xc2,
               SOF_3  = 0xc3,
	       
               SOF_5  = 0xc5,
               SOF_6  = 0xc6,
               SOF_7  = 0xc7,
	       
               SOF_9  = 0xc9,
               SOF_10 = 0xca,
               SOF_11 = 0xcb,

               SOF_13 = 0xcd,
               SOF_14 = 0xce,
               SOF_15 = 0xcf,
	       
	       DHT    = 0xc4,
	       DAC    = 0xcc,
	       RST_0  = 0xd0,
	       RST_1  = 0xd1,
	       RST_2  = 0xd2,
	       RST_3  = 0xd3,
	       RST_4  = 0xd4,
	       RST_5  = 0xd5,
	       RST_6  = 0xd5,
	       RST_7  = 0xd7,
               SOI    = 0xd8,
	       EOI    = 0xd9,
	       SOS    = 0xda,
	       APP_0  = 0xe0,
	       APP_1  = 0xe1,
	       APP_2  = 0xe2,
	       APP_3  = 0xe3,
	       APP_4  = 0xe4,
	       APP_5  = 0xe5,
	       APP_6  = 0xe6,
	       APP_7  = 0xe7,
	       APP_8  = 0xe8,
	       APP_9  = 0xe9,
	       APP_10 = 0xea,
	       APP_11 = 0xeb,
	       APP_12 = 0xec,
	       APP_13 = 0xed,
	       APP_14 = 0xee,
	       APP_15 = 0xef,
	       DQT    = 0xdb,
	       COM    = 0xfe,
              };
   private:
    static Marker marker;
    Marker();
    const char* name[256];
    
   public:
    static const char* getName(Code cod){return marker.name[cod];}
    static io::ascii::Writer& show(io::ascii::Writer&,Code);
    friend io::ascii::Writer& operator<<(io::ascii::Writer& out,Code c)
           {return show(out,c);}
    static bool isSegment(Code code);   
  };
 }//namespace jpeg
}//pix
