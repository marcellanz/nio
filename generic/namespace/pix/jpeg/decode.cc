//---------------------------------
//decoder
//(c) H.Buchmann FHSO 2005
//$LastChangedRevision: 137 $
//---------------------------------
#include "sys/sys.h"
IMPLEMENTATION(pix_jpeg_decoder,$LastChangedRevision: 137 $)
#include "pix/jpeg/decode.h"
#include "sys/msg.h"

#define PIX_JPEG_DECODE_TEST

#ifdef PIX_JPEG_DECODE_TEST
#include "sys/host.h"
#include "io/byte/file/file.h"
#endif

namespace pix
{
 namespace jpeg
 {
  Decoder::Decoder(Image<RGB8&>::Listener&)
  :soi(this),
   consumer(&soi)
  {
  }

  Decoder::~Decoder()
  {
  }

  Consumer* Decoder::onConsume(Consumer*)
  {
   sys::msg<<"Consumed\n";
   return 0;
  }

  bool Decoder::put(unsigned char c)
  {
   if (consumer==0) return false;
   consumer=consumer->onByte(c);
   return consumer!=0;
  }
 
 
#ifdef PIX_JPEG_DECODE_TEST
//--------------------------------------------------- Test
  class Tester:public Decoder,
               public Image<RGB8&>::Listener
  {
   static Tester tester;
   Tester();
   void onDimension(unsigned wi,unsigned he);
   void onPixel(RGB8& pixel);
  };
  
  Tester Tester::tester;

  Tester::Tester()
  :Decoder((Image<RGB8&>::Listener&)*this)
  {
   if (sys::argCnt()!=2)
      {
       sys::msg<<"usage "<<sys::argAt(0)<<" jpegFile\n";
       return;
      }
   io::byte::file::Input::Stream src(sys::argAt(1));
   while(!src.eos())
   {
    put(src.getByte());
   }   
  }
  
  void Tester::onDimension(unsigned wi,unsigned he)
  {
  }
  
  void Tester::onPixel(RGB8& pixel)
  {
  }
#endif
 }//namespace jpeg
}//namespace pix
