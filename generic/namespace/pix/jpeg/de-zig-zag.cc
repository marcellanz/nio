//------------------------
//de-zig-zag
//(c) H.Buchmann FHSO 2005
//$LastChangedRevision: 137 $
//------------------------
#include "sys/sys.h"
IMPLEMENTATION(pix_jpeg_de_zig_zag,$LastChangedRevision: 137 $)
#include "sys/out.h"

namespace pix
{
 namespace jpeg
 {
  class DeZigZagger
  {
   static DeZigZagger deZigZagger;
   static const unsigned ZIG_ZAG[64];
   DeZigZagger();
  };
  
  DeZigZagger DeZigZagger::deZigZagger;
  
  const unsigned DeZigZagger::ZIG_ZAG[64]=
  {
    0, 1, 5, 6,14,15,27,28,  // 0
    2, 4, 7,13,16,26,29,42,  // 8
    3, 8,12,17,25,30,41,43,  //16
    9,11,18,24,31,40,44,53,  //24
   10,19,23,32,39,45,52,54,  //32
   20,22,33,38,46,51,55,60,  //40
   21,34,37,47,50,56,59,61,  //48 
   35,36,48,49,57,58,62,63   //56
  };
  
  DeZigZagger::DeZigZagger()
  {
   unsigned de_zig_zag[64];
   for(unsigned i=0;i<64;i++)
   {
    de_zig_zag[ZIG_ZAG[i]]=i; 
   }
   
   unsigned idx=0;
   for(unsigned y=0;y<8;y++)
   {
    for(unsigned x=0;x<8;x++)
    {
     sys::out<<io::ascii::setw(3)<<de_zig_zag[idx++];
     if (idx<64) sys::out<<",";
    }
    sys::out<<"\n";
   }
  }
  
 }//namespace jpeg
}//namespace pix
