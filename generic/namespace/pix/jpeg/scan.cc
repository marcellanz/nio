//--------------------
//scan
//(c) H.Buchmann FHSO 2005
//$Id: scan.cc 193 2006-01-11 15:21:16Z buchmann $
//--------------------
#include "sys/sys.h"
IMPLEMENTATION(pix_jpeg_scan,$LastChangedRevision: 137 $)
#include "pix/jpeg/marker.h"
#include "io/byte/file/file.h"
#include "sys/host.h"
#include "sys/msg.h"
namespace pix
{
 namespace jpeg
 {
  class Scanner
  {
   static Scanner scanner;
   unsigned size;
   unsigned char* data;
    Scanner();
   ~Scanner();
   void scan();
  };

  Scanner Scanner::scanner;

  Scanner::Scanner()
  :size(0),
   data(0)
  {
   if (sys::argCnt()!=2)
      {
       sys::msg<<"usage "<<sys::argAt(0)<<" jpgFile\n";
       return;
      }
   {
    io::byte::file::Input src(sys::argAt(1));
    size=src.getSize();
    data=new unsigned char[size];
    src.get(data,size);
   }
   scan();
  }


  Scanner::~Scanner()
  {
   if (data) delete [] data;
  }


  void Scanner::scan()
  {
   unsigned status=0;
   unsigned len=0;
   for(unsigned i=0;i<size;i++)
   {
    unsigned char v=data[i];
    switch(status)
    {
     case 0: //expecting 0xff
      if (v!=0xff)sys::msg.error()<<"expecting 0xff "<<io::ascii::hex()<<i<<"\n";
      status=1;
     break;
     case 1: //0xff* code
      if (v==0)  sys::msg.error()<<"expecting 0xff|code "<<io::ascii::hex()<<i<<"\n";
      if (v==0xff) break; //remain in state
      {
       Marker::Code c=(Marker::Code)v;
       sys::msg<<c;
       if (Marker::isSegment(c)) status=4;
          else {sys::msg<<"\n";status=2;}
      }
     break;
     
     case 2:
      if (v==0xff) {status=3;break;}
     break;
     
     case 3:
      if (v==0){status=2;break;}
      if (v==0xff){status=1;break;}
      {
       Marker::Code c=(Marker::Code)v;
       sys::msg<<c;
       if (Marker::isSegment(c)) status=4;
          else   {sys::msg<<"\n";status=2;}
      }
     break;
     
     case 4: //len
      len=((unsigned)v<<8);
      if (v==0xff){status=5;break;}
      status=6;
     break;
     
     case 5: //must be zero
      if (v!=0) sys::msg.error()<<"expecting 0 "<<io::ascii::hex()<<i<<"\n";
      status=6;
     break;
     
     case 6:
      len=len|(unsigned)v;
      if (v==0xff){status=7;break;}
      sys::msg<<io::ascii::setw(5)<<len<<"\n";
      status=2;
     break;
     
     case 7:
      if (v!=0) sys::msg.error()<<"expecting 0 "<<io::ascii::hex()<<i<<"\n";
      sys::msg<<io::ascii::setw(5)<<len<<"\n";
      status=2;
     break;
    }
   }
  }
 }//namespace jpeg
}//pix

