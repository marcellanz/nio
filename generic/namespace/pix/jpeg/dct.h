#ifndef PIX_JPEG_DCT_H
#define PIX_JPEG_DCT_H
//-----------------------------
//DCT 8x8 discrete cosinus transformation
//(c) H.Buchmann 
//$LastChangedRevision: 137 $
//-----------------------------
INTERFACE(pix_jpeg_dct,$LastChangedRevision: 137 $)
namespace pix
{
 namespace jpeg
 {
  class DCT
  {
   public:
    static void inverse(int in[],double out[],unsigned dy,double dqt[]);
    static double range(double v);  
  };
 }//namespace jpeg
}//namespace pix 
#endif
