#ifndef PIX_JPEG_CONSUMER_H
#define PIX_JPEG_CONSUMER_H
//------------------------
//consumer
//(c) H.Buchmann FHSO 2005
//$LastChangedRevision: 137 $
//------------------------
INTERFACE(pix_jpeg_consumer,$LastChangedRevision: 137 $)
namespace pix
{
 namespace jpeg
 {
//------------------------------------------- Consumer
  class Consumer
  {
   public:
//------------------------------------------- Consumer::Listener
    struct Listener
    {
     virtual ~Listener();
     virtual Consumer* onConsume(Consumer*)=0;
    };

   protected:
    Listener* li;
    Consumer():li(0){}
    Consumer(Listener* li):li(li){}
    
   public:    
    virtual ~Consumer();
    virtual Consumer* onByte(unsigned char byte)=0; 
            //returns next Consumer or null if error
  };

//------------------------------------------- some useful Consumers
  class Null:public Consumer
  {
   private:
    Consumer* onByte(unsigned char byte);
   
   public:
    Null();
   ~Null();
  };

//------------------------------------------- Marker
  class Marker:public Consumer
  {
   private:
    unsigned status;

   public:
             Marker(Listener* li);
    virtual ~Marker();
    Consumer* onByte(unsigned char byte);
  };  
 }//namespace jpeg
}//namespace pix
#endif
