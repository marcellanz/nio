#ifndef PIX_JPEG_DECODER_H
#define PIX_JPEG_DECODER_H
//---------------------------------
//decoder
//(c) H.Buchmann FHSO 2005
//$LastChangedRevision: 137 $
//---------------------------------
INTERFACE(pix_jpeg_decoder,$LastChangedRevision: 137 $)
#include "pix/image.h"
#include "pix/jpeg/consumer.h"
namespace pix
{
 namespace jpeg
 {
  class Decoder:public Consumer::Listener
  {
   private:
    Null null;
    Marker soi;

    Consumer* consumer; //the current one
    Consumer* onConsume(Consumer*);
    
   public:
             Decoder(Image<RGB8&>::Listener&);
    virtual ~Decoder();
    bool put(unsigned char c); //one byte of comprimat
  };
 }//namespace jpeg
}//namespace pix
#endif
