//------------------------
//consumer abstract class
//(c) H.Buchmann FHSO 2005
//$LastChangedRevision: 137 $
//------------------------
#include "sys/sys.h"
IMPLEMENTATION(pix_jpeg_consumer,$LastChangedRevision: 137 $)
#include "pix/jpeg/consumer.h"
#include "sys/msg.h"
namespace pix
{
 namespace jpeg
 {
//------------------------------------------- Consumer::Listener
  Consumer::Listener::~Listener()
  {
  }
  
//------------------------------------------- Consumer
  Consumer::~Consumer()
  {
  }
  
//------------------------------------------- Null
  Null::Null()
  {
  }
  
  Null::~Null()
  {
  }
  
  Consumer* Null::onByte(unsigned char byte)
  {
   sys::msg<<"Null consumer\n";
   return 0;
  }

//------------------------------------------- Marker
  Marker::Marker(Listener* li)
  :Consumer(li),
   status(0)
  {
  }
  
  Marker::~Marker()
  {
  }
  
  Consumer* Marker::onByte(unsigned char b)
  {
   switch(status)
   {
    case 0: //expecting first 0xff
     if (b!=0xff) return 0;
     status=1;
    return this;
    
    case 1: //0xff | number
     if (b==0xff) return this; //remains in state
     sys::msg<<"Marker "<<io::ascii::hex()<<b<<"\n";
     if (li) li->onConsume(this);
    return 0;    
   }//switch 
  }
 }//namespace jpeg
}//namespace pix
