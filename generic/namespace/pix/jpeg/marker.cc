//--------------------
//marker
//(c) H.Buchmann FHSO 2005
//$Id: marker.cc 193 2006-01-11 15:21:16Z buchmann $
//--------------------
#include "sys/sys.h"
IMPLEMENTATION(pix_jpeg_marker,$LastChangedRevision: 137 $)
#include "pix/jpeg/marker.h"
namespace pix
{
 namespace jpeg
 {
  Marker Marker::marker;
  Marker::Marker()
  {
   for(unsigned i=0;i<256;i++) name[i]="???";
   name[SOF_0]  = "SOF_0";
   name[DHT]    = "DHT";  
   name[SOI]    = "SOI";  
   name[SOS]    = "SOS";  
   name[APP_0]  = "APP_0";
   name[APP_1]  = "APP_1";
   name[APP_2]  = "APP_2";
   name[APP_3]  = "APP_3";
   name[APP_4]  = "APP_4";
   name[APP_5]  = "APP_5";
   name[APP_6]  = "APP_6";
   name[APP_7]  = "APP_7";
   name[APP_8]  = "APP_8";
   name[APP_9]  = "APP_9";
   name[APP_10] = "APP_10";
   name[APP_11] = "APP_11";
   name[APP_12] = "APP_12";
   name[APP_13] = "APP_13";
   name[APP_14] = "APP_14";
   name[APP_15] = "APP_15";
   name[DQT]    = "DQT";
   name[EOI]    = "EOI";
   name[RST_0]  = "RST_0";
   name[RST_1]  = "RST_1"; 
   name[RST_2]  = "RST_2"; 
   name[RST_3]  = "RST_3"; 
   name[RST_4]  = "RST_4"; 
   name[RST_5]  = "RST_5"; 
   name[RST_6]  = "RST_6"; 
   name[RST_7]  = "RST_7"; 
  }
  
  io::ascii::Writer& Marker::show(io::ascii::Writer& out,Code c)
  {
   out<<marker.name[c]<<"\t("<<io::ascii::hex()<<(unsigned char)c<<")";
   return out;
  }
  
  bool Marker::isSegment(Code code)
  {
   return !((code==SOI) || code==(EOI) || (RST_0<=code)&&(code<=RST_7));
  }
  
 }//namespace jpeg
}//pix
